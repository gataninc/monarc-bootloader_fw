
// CLIENT PORTS
//#define CONF_HOST_ROC_CMD_PORT             (1)
#define CONF_FPB_HOST_ROC_CMD_PORT         (12)
#define CONF_FPB_HOST_PAC_CMD_PORT         (13)

// SERVER PORT
#define CONF_SERVER_FW_UPDATE_PORT         (3)

void FirmwareUpdaterRxTask(void *pvParameters);
