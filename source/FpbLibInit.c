#include "HostInterface.h"
#include "Uart.h"
#include "lib_init.h"
#include "FlashWrapper.h"
#include "UartSerialCom.h"
#include "SerialComClient.h"

static uart_link_context_t ulCtxt;
void FpbConsole(const char *str)
{
   	os_print(&cout,"%s",str);
}

void FpbLibInit(void)
{
	InitFlashWrapper();
	SetupUartStream(uart_id_4, &cin, &cout);

	NetLib_Init(CONF_NET_FPB_ADDR);
	Link_InitUart(&ulCtxt, UART3, UART3_CLK_SRC, UART3_RX_TX_IRQn, 5);
	Net_SetRoute(CONF_NET_ROC_ADDR, &ulCtxt.link);
	Net_SetRoute(CONF_NET_PAC_ADDR, &ulCtxt.link);

	fpb_enet_init(&istr, &ostr);
}

void FpbLibDeinit(void)
{
	Link_DeinitUart(&ulCtxt);
	DeinitUartStream(uart_id_4, &cin, &cout);
	fpb_enet_deinit(&istr, &ostr);
}


