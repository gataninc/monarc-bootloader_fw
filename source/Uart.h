/*
 * Uart.h
 *
 *  Created on: Nov 27, 2018
 *      Author: stelasang
 */

#ifndef UART_H_
#define UART_H_

#include "mios.h"

typedef enum uart_id_t {
	uart_id_1 = 1,
	uart_id_2,
	uart_id_3,
	uart_id_4
} uart_id_t;

void SetupUartStream(uart_id_t uartId, iostream_t *is, iostream_t *os);

void DeinitUartStream(uart_id_t uartId, iostream_t *is, iostream_t *os);
void cprintf ( const char * format, ... );
void PUT_BUFF(char* s, uint32_t len);

#endif /* UART_H_ */
