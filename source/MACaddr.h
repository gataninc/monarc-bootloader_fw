/*
 * MACaddr.h
 *
 *  Created on: Sep 7, 2017
 *      Author: Nuc User
 */

#ifndef MACADDR_H_
#define MACADDR_H_


#define configMAC_ADDR0 0x00
#define configMAC_ADDR1 0x50
#define configMAC_ADDR2 0xc0
#define configMAC_ADDR3 0x05
#define configMAC_ADDR4 0x90
#define configMAC_ADDR5 0x01


#endif /* MACADDR_H_ */
