/*
 * utils.c
 *
 *  Created on: Jul 9, 2018
 *      Author: LKODHELI
 */


#include "utils.h"
#include "mios.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <math.h>
#include <ctype.h>

#include "FreeRTOSConfig.h"
#include <FreeRTOS.h>
#include <event_groups.h>











void _ios_notify(iostream_t *stream, int32_t event);

int IO_BYTE_CNT(iostream_t *IOSTREAM)
{
	int size = 0;
	if (IOSTREAM->writeIndex == IOSTREAM->readIndex){
		size = 0;
	}
	else if(IOSTREAM->writeIndex > IOSTREAM->readIndex){
		size = (IOSTREAM->writeIndex - IOSTREAM->readIndex);
	}
	else {
		size = (IOSTREAM->bufferSize - IOSTREAM->readIndex + IOSTREAM->writeIndex);
	}
	return size;
}

int IO_FREE_CNT(iostream_t *IOSTREAM) { return IOSTREAM->bufferSize - IO_BYTE_CNT(IOSTREAM) - 1;}

int IO_CONTIGUOS_FREE_CNT(iostream_t *IOSTREAM)
{
	int size = 0;

	if (IOSTREAM->readIndex > IOSTREAM->writeIndex){
		size = IOSTREAM->readIndex - IOSTREAM->writeIndex - 1; // leave one space
	}
	else {
		size = IOSTREAM->bufferSize - IOSTREAM->writeIndex;
		if (size == IOSTREAM->bufferSize){
	        size = IOSTREAM->bufferSize - 1;
		}
	}
	return size;
}

int IO_CONTIGUOS_USED_CNT(iostream_t *IOSTREAM) {
	int size = 0;

	if (IOSTREAM->writeIndex >= IOSTREAM->readIndex){
		size = IOSTREAM->writeIndex - IOSTREAM->readIndex;
	}
	else { // rd > wr
		size = IOSTREAM->bufferSize - IOSTREAM->readIndex;
	}
	return size;
}

void ios_init(iostream_t *stream, uint8_t *buf, uint32_t size, uint32_t byteCnt)
{
	if ((stream == NULL) || (buf == NULL)){
		return;
	}
	stream->Buffer = buf;
	stream->bufferSize = size;
	stream->readIndex = 0;
	stream->writeIndex = byteCnt % size;
	stream->receive = (receive_pfn)NULL;
	stream->wait = (wait_pfn)NULL;
	stream->send = (send_pfn)NULL;
	stream->waitArg = (void*)0;
	stream->receiveArg = (void*)0;
	stream->sendArg = (void*)0;
	stream->ios = IOS_GOOD;
	//stream->xTxMutex = xSemaphoreCreateMutex();
	//configASSERT(stream->xTxMutex);
}



void ios_deinit(iostream_t *stream)
{
	if (stream == NULL){
		return;
	}
	ios_disable_wait(stream);
	memset(stream,0,sizeof(iostream_t));
}

void is_set_rx_cb(iostream_t *istream, receive_pfn receive, void *fnArg)
{
	if (istream == NULL){
		return;
	}
	istream->receive = receive;
	istream->receiveArg = fnArg;
}


#define FREERTOS_IOSTREAM_NOTEMPTY       (1<<0)
#define FREERTOS_IOSTREAM_ERROR          (1<<1)
#define FREERTOS_IOSTREAM_NOTFULL        (1<<2)

IO_StreamState_t IOstream_FreeRtosWait(void *pv, uint32_t timeoutMs)
{
	EventBits_t ev;
	ev = xEventGroupWaitBits((EventGroupHandle_t)pv,
			                 FREERTOS_IOSTREAM_NOTEMPTY|FREERTOS_IOSTREAM_ERROR,
							 pdTRUE, //should be cleared before returning
							 pdFALSE, // don't wait for all the bits to be set, any will do
			                 timeoutMs/portTICK_PERIOD_MS);
	if (ev & FREERTOS_IOSTREAM_NOTEMPTY){
		return IOS_GOOD;
	}
	else if (ev & FREERTOS_IOSTREAM_ERROR) {
		return IOS_ERROR;
	}
	else {
		return IOS_UNDERFLOW;
	}
}

void _ios_notify(iostream_t *stream, int32_t event)
{
	if (stream->waitArg){
		xEventGroupSetBits(stream->waitArg, event);
	}
}

IO_StreamState_t is_put(iostream_t *stream, uint8_t *buf, uint32_t size, uint32_t *cntPosted)
{
	int cnt = 0;
	if ((stream == NULL) || (buf == NULL) || (size == 0)){
		return IOS_ERROR;
	}

	stream->ios = IOS_GOOD;

	int freeCnt = IO_FREE_CNT(stream);

    int itr = 2;
    int sizeToCopy;
    do {
        sizeToCopy = min( IO_CONTIGUOS_FREE_CNT(stream), size); // top half and bottom half
        if (sizeToCopy > 0){
            memcpy(&stream->Buffer[stream->writeIndex], buf, sizeToCopy);
            stream->writeIndex += sizeToCopy;
            if (stream->writeIndex >= stream->bufferSize){
    	        stream->writeIndex = 0;
            }
            size -= sizeToCopy;
            buf += sizeToCopy;
            cnt += sizeToCopy;
        }
        itr--;
    } while(itr);

    _ios_notify(stream, FREERTOS_IOSTREAM_NOTEMPTY);
    if (size > 0){
    	stream->ios = IOS_OVERFLOW;
    	_ios_notify(stream, FREERTOS_IOSTREAM_ERROR);
    	printf("is overflow\n"); // debug
    }
    if (cntPosted != NULL){
        *cntPosted = cnt;
    }
    return stream->ios;
}

void ios_enable_wait(iostream_t *stream)
{
	if (stream == NULL){
		return;
	}
    if (stream->wait == NULL){
	    EventGroupHandle_t hevt = xEventGroupCreate();
        if (hevt == NULL){
    	    stream->ios = IOS_ERROR;
    	    return;
        }
        stream->wait = IOstream_FreeRtosWait;
        stream->waitArg = hevt;
    }
}
void ios_disable_wait(iostream_t *stream)
{
	if (stream == NULL){
		return;
	}

    if (stream->waitArg != NULL){
        vEventGroupDelete(stream->waitArg);
        stream->waitArg = (void *)0;
    }
    if (stream->wait != NULL){
    	stream->wait = (wait_pfn)NULL;
    }

}




IO_StreamState_t is_get_byte(iostream_t *istream, uint8_t *pByte)
{
	if ((istream == NULL) || (pByte == NULL)){
		return IOS_ERROR;
	}
	int byteCnt = IO_BYTE_CNT(istream);
	if (byteCnt == 0){
		istream->ios = IOS_UNDERFLOW;
		return istream->ios;
	}

	*pByte = istream->Buffer[istream->readIndex++]; // return the character increment the index
	istream->readIndex = istream->readIndex % istream->bufferSize;
	istream->ios = IOS_GOOD;
	return istream->ios;
}

IO_StreamState_t is_get_byte_timeout(iostream_t *istream, uint8_t *pByte, uint32_t timeoutMs)
{
	IO_StreamState_t res;
	size_t received = 0;

	if ((istream == NULL) || (pByte == NULL)){
		return IOS_ERROR;
	}
	int byteCnt = IO_BYTE_CNT(istream);
	// if the buffer is empty fill it with received characters
	if(byteCnt == 0) {
		if( istream->receive != NULL ) { // if buffer empty triggers reads then go fill the buffer
		    received = 0;
		    int sizeToReceive = IO_CONTIGUOS_FREE_CNT(istream);
	        res = istream->receive(istream->receiveArg, &istream->Buffer[istream->writeIndex],
	        		               sizeToReceive, &received, timeoutMs);
		    if ((res != IOS_GOOD) || (received == 0) || (received > sizeToReceive)){
		    	istream->ios = res;
		        return istream->ios; // some error occurred
		    }
		    istream->writeIndex += received;
		    if (istream->writeIndex >= istream->bufferSize){
		    	istream->writeIndex = 0;
		    }
		}
		else if( istream->wait != NULL ){
			// if wait is configured and some other thread fills it then wait
			res = istream->wait(istream->waitArg, timeoutMs);
			if (res != IOS_GOOD){
				istream->ios = res;
				return istream->ios; // some error occurred
            }
		}
	}
	byteCnt = IO_BYTE_CNT(istream);
	if (byteCnt == 0){
		istream->ios = IOS_UNDERFLOW;
		return istream->ios; // timeout
	}

	*pByte = istream->Buffer[istream->readIndex++]; // return the character increment the index
	if (istream->readIndex >= istream->bufferSize){
		istream->readIndex = 0;
	}
	istream->ios = IOS_GOOD;

	return istream->ios;
}


int ios_get_free_space(iostream_t *stream)
{
	if (stream == NULL){
		return 0;
	}
	return IO_FREE_CNT(stream);
}
int ios_get_byte_count(iostream_t *stream)
{
	if (stream == NULL){
		return 0;
	}
	return IO_BYTE_CNT(stream);
}


void os_set_tx_cb(iostream_t *ostream, send_pfn send, void *fnArg)
{
	if (ostream == NULL){
		return;
	}
	ostream->send = send;
	ostream->sendArg = fnArg;
}

IO_StreamState_t os_flush(iostream_t *ostream)
{
	int res = 0;
    int byteCnt;

	if ((ostream == NULL) || (ostream->send == (send_pfn)0)){
		return IOS_ERROR;
	}

    byteCnt = IO_CONTIGUOS_USED_CNT(ostream);
    if (byteCnt == 0){
    	ostream->ios = IOS_GOOD;
        return IOS_GOOD;
    }
    if(ostream->send != (send_pfn)0){
    	while(byteCnt != 0){
			res = ostream->send(ostream->sendArg, &ostream->Buffer[ostream->readIndex], byteCnt);
			ostream->readIndex += byteCnt;
            if (ostream->readIndex >= ostream->bufferSize){
				ostream->readIndex = 0;
			}
			if (res != 0){
				ostream->ios = IOS_ERROR;
				return ostream->ios;
			}

			byteCnt = IO_CONTIGUOS_USED_CNT(ostream);
    	}
    }
    ostream->ios = IOS_GOOD;
    _ios_notify(ostream, FREERTOS_IOSTREAM_NOTFULL);

	return ostream->ios;
}

IO_StreamState_t os_send_byte(iostream_t *ostream, uint8_t byte)
{
	IO_StreamState_t ios = IOS_GOOD;

	if ((ostream == NULL) || (ostream->Buffer == NULL)){
		return IOS_ERROR;
	}

	if (IO_FREE_CNT(ostream) < (ostream->bufferSize/4)){
		ios = os_flush(ostream);
	}
	else {
		ostream->ios = IOS_GOOD;
	}
    if (IO_FREE_CNT(ostream) == 0){
    	ostream->ios = IOS_OVERFLOW;
    	return IOS_OVERFLOW;
    }
	ostream->Buffer[ostream->writeIndex++] = byte;
    if (ostream->writeIndex >= ostream->bufferSize){
    	ostream->writeIndex = 0;
    }

	return ios;
}

IO_StreamState_t os_send_buffer(iostream_t *ostream, uint8_t *buf, uint32_t size)
{
	IO_StreamState_t res;
	int byteIndex = 0;

	if ((ostream == NULL) || (buf == NULL)){
		return IOS_ERROR;
	}

	while(byteIndex < size){
	    res = os_send_byte(ostream, buf[byteIndex++]);
	    if (res != IOS_GOOD){
	    	return res;
	    }
	}
    return IOS_GOOD;
}

IO_StreamState_t os_send_buffer_flush(iostream_t *ostream, uint8_t *buf, uint32_t size)
{
	IO_StreamState_t res;

	res = os_send_buffer(ostream, buf, size);
    if (res != IOS_GOOD){
    	return res;
    }
    return os_flush(ostream);
}



bool GetLineBlocking(iostream_t *istream, uint8_t *buf, uint32_t bufSize, uint32_t *rcvSize, uint32_t timeoutMs)
{
	int byteIndex = 0;
	bool endOfLineSeen = false;
	uint8_t rxByte;
	int res = 0;

	if ((istream == NULL) || (buf == NULL) || (rcvSize == NULL) || (bufSize <2)){
		return false;
	}

	TickType_t xTicksAtStart = xTaskGetTickCount();
	TickType_t xTicksAtTimeout = xTicksAtStart + timeoutMs/portTICK_PERIOD_MS;
	uint32_t byteTimeoutMs;
	bool bTimeout = false;
	enum LineCharState {LCS_IDLE, LCS_INCHAR, LCS_INCR, LCS_INEOL} lcs;

	*rcvSize = 0;
	bufSize--; // leave space for null terminator

	byteTimeoutMs = timeoutMs/bufSize;
	if (byteTimeoutMs < 10){ // assuming an 80 character line on average,
		byteTimeoutMs = 10;
	}

	lcs = LCS_IDLE;
	while(!endOfLineSeen && !bTimeout)
	{
		res = is_get_byte_timeout(istream, &rxByte, byteTimeoutMs);
		if (res != IOS_GOOD){ // timeout or error
			if ( TimeDifference(xTicksAtTimeout, xTaskGetTickCount() ) > 0 ){
				bTimeout = true;
			}
			continue;
		}

		if (byteIndex == bufSize-1){
			break;
		}

        switch(lcs){
            case LCS_IDLE:
            	byteIndex = 0;
            	// fall through
            case LCS_INCHAR:
            	if (rxByte == '\r') {
            		lcs = LCS_INCR;
            		endOfLineSeen = true;
            	}
            	else if (rxByte == '\n'){
            		endOfLineSeen = true;
            		lcs = LCS_INEOL;
            	}
            	else {
            		buf[byteIndex++] = rxByte;
            		lcs = LCS_INCHAR;
            	}
            	break;
            case LCS_INCR: // \r\n
            	if (rxByte == '\n'){
            		//endOfLineSeen = true; notified before
            		lcs = LCS_INEOL; // \r\n sequence, new_line notified already
            	}
            	else if (rxByte == '\r') {
            		endOfLineSeen = true; // sequence of \r\r
            	}
            	else {
            		buf[byteIndex++] = rxByte;
            		lcs = LCS_INCHAR;
            	}
            	break;
            case LCS_INEOL:
            	if (rxByte == '\n'){
            		endOfLineSeen = true; // \n\n sequence of new lines
            	}
            	else if (rxByte == '\r') {  // \n\r this is just
            		endOfLineSeen = true;
            	}
            	else {
            		buf[byteIndex++] = rxByte;
            		lcs = LCS_INCHAR;
            	}
            	break;
            default:
            	lcs = LCS_IDLE;
            	break;
        }
	}
	*rcvSize = byteIndex;
	buf[byteIndex] = '\0'; // terminate the line

	return endOfLineSeen;
}

bool GetLine(iostream_t *inStrmBuf, uint8_t *buf, uint32_t bufSize, uint32_t *numCharsCopied)
{
	bool endOfLineSeen = false;
	uint8_t rxByte;
	enum LineCharState {LCS_IDLE, LCS_INCHAR, LCS_INCR, LCS_INEOL} lcs;
	uint32_t numCharsToProcess;
	uint8_t *end;
	uint8_t *pos;

	if (bufSize < 2){
		return false;
	}

	pos = buf;
	end = buf + bufSize - 1; // save space for the terminating null character
	numCharsToProcess = IO_BYTE_CNT(inStrmBuf);
	lcs = LCS_IDLE;

	while(!endOfLineSeen && (numCharsToProcess-- > 0) && (pos < end))
	{
		is_get_byte(inStrmBuf, &rxByte);

        switch(lcs){
            case LCS_IDLE:
            	pos = buf;
            	// fall through
            case LCS_INCHAR:
            	if (rxByte == '\r'){
            		lcs = LCS_INCR;
            	}
            	else if (rxByte == '\n'){
            		endOfLineSeen = true;
                    lcs = LCS_INEOL;
            	}
            	else {
            		*pos++ = rxByte;
            	}
            	break;
            case LCS_INCR:
            	if (rxByte == '\n'){
            	    endOfLineSeen = true;
            	    lcs = LCS_INEOL;
            	}
            	else {
                    lcs = LCS_IDLE;
            	}
            	break;
            default:
            	lcs = LCS_IDLE;
            	break;
        }
	}
	*pos = '\0'; // terminate the line

	if (numCharsCopied != NULL){
		*numCharsCopied = pos - buf;
	}

	return endOfLineSeen;
}

int os_print(iostream_t *os, const char *fmt, ...)
{
	char maxString[OS_MAX_FMT_MSG_SIZE + 1]; // add space for the null character
	int strLen = 0;
	IO_StreamState_t ios;

	if ((os == NULL) || (fmt == NULL)){
		return 0;
	}
	va_list arglist;
	va_start(arglist, fmt);
	strLen = vsprintf(maxString, fmt, arglist);
	va_end(arglist);
	ios = os_send_buffer_flush(os, (uint8_t*)maxString, strLen);
	if(ios != IOS_GOOD){
		return 0;
	}
	else {
		return strLen;
	}
}
