#include "SpiSerialCom.h"
//#include "dbglog_target.h"
#include "SerialComTarget.h"
#include "FlashWrapper.h"
#include "SerialComIOS.h"
#include "lib_init.h"
#include "stdio.h"

static spi_link_context_t   spiRoclink;


static serial_com_ios_server_t pac_com_ios_srv; // pac serial com server

void PacConsole(const char *str)
{
#if !defined (__SEMIHOST_HARDFAULT_DISABLE)
   	printf("%s",str);
#endif
}

void InitPacSpi_Link(void)
{
	// this spi is the bottom tx/rx layer providing the link to the roc board
	int res = Link_InitSlaveSpi(&spiRoclink, SPI1);
	if (res != SSER_SUCCESS) {
		DIE;
	}
	// the link provides paths to the ROC and FPB destinations, update the routing table
	//  add the path to ROC net, addr reachable through this link
	Net_SetRoute(CONF_NET_ROC_ADDR, &spiRoclink.link);
	//  add the path to FPB net, addr reachable through this link
	Net_SetRoute(CONF_NET_FPB_ADDR, &spiRoclink.link);
}


void PacLibInit(void)
{


	InitFlashWrapper();
	NetLib_Init(CONF_NET_PAC_ADDR);
	InitPacSpi_Link();

	// configure io stream on top of serial communication protocol
	pac_com_ios_srv.os = &ostr;
	pac_com_ios_srv.is = &istr;
	pac_com_ios_srv.server_port = CONF_SERVER_FW_UPDATE_PORT;
	SetupSerialComIosServer(&pac_com_ios_srv);
}

void PacLibDeinit(void)
{
	Link_DeinitSpi(&spiRoclink);
}


