
#include "lib_init.h"
#include "fsl_port.h"
#include "fsl_clock.h"

#include "FpbLibInit.h"
#include "RocLibInit.h"
#include "PacLibInit.h"

#include "board_init.h"

#include "utils.h"

// define the buffers for the streams
#define COUT_BUF_SIZE             (100)
#define CIN_BUF_SIZE              (100)
#define OSTREAM_BUF_SIZE          (256)
#define ISTREAM_BUF_SIZE          (512)

uint8_t COUT_BUF[COUT_BUF_SIZE];
uint8_t CIN_BUF[COUT_BUF_SIZE];
uint8_t OSTREAM_BUF[OSTREAM_BUF_SIZE];
uint8_t ISTREAM_BUF[ISTREAM_BUF_SIZE];

iostream_t cout; // console out
iostream_t cin;  // console in
iostream_t ostr;  // output stream for the bootloader protocol
iostream_t istr;  // input stream for the bootloader protocol

static int BOARD_ID;

/*FUNCTION**********************************************************************
 *
 * Function Name : BOARD_InitPins
 * Description   : Configures pin routing and optionally pin electrical features.
 *
 *END**************************************************************************/

void LIB_CPRINT(const char *str, int len)
{
	if (BOARD_ID == BOARD_ID_FPB){
		PUT_BUFF((uint8_t *)str, len);
    }
    else if(BOARD_ID == BOARD_ID_ROC){
	//#elif defined( __TARGET_ROC)
    	PUT_BUFF((uint8_t *)str, len);
    }
	else if (BOARD_ID == BOARD_ID_PAC){
	//#elif defined( __TARGET_PAC)
	    PacConsole(str, len);
    }
}

uint32_t LIB_GET_TIMEOUT(void)
{
	if (BOARD_ID == BOARD_ID_PAC){
		return 10000;
	}
	else {
		return 100000;
	}
}

void LIB_Init(void) {

	BOARD_ID = BOARD_GetBoardId();

	ios_init(&cout, COUT_BUF, COUT_BUF_SIZE, 0);
	ios_init(&cin, CIN_BUF, CIN_BUF_SIZE, 0);
	ios_enable_wait(&cin);
	ios_init(&ostr, OSTREAM_BUF, OSTREAM_BUF_SIZE, 0);
	ios_init(&istr, ISTREAM_BUF, ISTREAM_BUF_SIZE, 0);
	ios_enable_wait(&istr);

//#if defined __TARGET_FPB
    if (BOARD_ID == BOARD_ID_FPB){
	    FpbLibInit();
    }
    else if(BOARD_ID == BOARD_ID_ROC){
//#elif defined( __TARGET_ROC)
	    RocLibInit();
    }
    else if (BOARD_ID == BOARD_ID_PAC){
//#elif defined( __TARGET_PAC)
	    PacLibInit();
    }
    else {
    	DIE;
    }
//#endif


}

void LIB_Deinit(void) {

	int BOARD_ID = BOARD_GetBoardId();

	//ios_init(&cout, COUT_BUF, COUT_BUF_SIZE, 0);
	//ios_init(&cin, CIN_BUF, CIN_BUF_SIZE, 0);
	//ios_enable_wait(&cin);
	//ios_init(&ostr, OSTREAM_BUF, OSTREAM_BUF_SIZE, 0);
	//ios_init(&istr, ISTREAM_BUF, ISTREAM_BUF_SIZE, 0);
	//ios_enable_wait(&istr);

//#if defined __TARGET_FPB
    if (BOARD_ID == BOARD_ID_FPB){
	    FpbLibDeinit();
    }
    else if(BOARD_ID == BOARD_ID_ROC){
//#elif defined( __TARGET_ROC)
	    RocLibDeinit();
    }
    else if (BOARD_ID == BOARD_ID_PAC){
//#elif defined( __TARGET_PAC)
	    PacLibDeinit();
    }
    else {
    	DIE;
    }
//#endif


}


/*******************************************************************************
 * EOF
 ******************************************************************************/
