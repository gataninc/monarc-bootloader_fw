/*
 * HostInterface.h
 *
 *  Created on: Nov 27, 2018
 *      Author: stelasang
 */

#ifndef HOSTINTERFACE_H_
#define HOSTINTERFACE_H_

#include "mios.h"

#define TCP_STACKSIZE ((2*1024) + 100)

void enet(void *pvParameters);
void fpb_enet_init(iostream_t *instream, iostream_t *outstream);
void fpb_enet_deinit(iostream_t *instream, iostream_t *outstream);
void EthWatchdog();
void enet_reset(void *pvParameters);
void enet_data(void *pvParameters);
void enet_exec(void *pvParameters);
void PutBuf(char *buffer);
void IpAddr();
void UploadFile(char *filename);
void DownloadFile(char *filename);
void Transfer();
#endif /* HOSTINTERFACE_H_ */
