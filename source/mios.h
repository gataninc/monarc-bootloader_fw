/*
 * utils.h
 *
 *  Created on: Nov 27, 2018
 *      Author: LKODHELI
 */

#ifndef MIOS_H_
#define MIOS_H_

#include <stdint.h>
#include <ctype.h>
#include <stdbool.h>

/*
 * INPUT OUTPUT STREAMS FORM A SERIAL CHANNEL
 *  TX: TRANSMITTER, SENDS DATA
 *  RX: RECEIVER, RECEIVES DATA
 *
 *         +---------------+
 *  TX <-- | OUTPUT STREAM | <-- PRODUCER
 *         +---------------+
 *
 **        +---------------+
 *  RX --> | INPUT STREAM  | --> CONSUMER
 *         +---------------+
 *
 * */


/*
 * Stream buffer structures
 * */

typedef enum IO_StreamState_t {
	IOS_GOOD = 0,
	IOS_ERROR,
	IOS_OVERFLOW,
	IOS_UNDERFLOW,
} IO_StreamState_t;

typedef IO_StreamState_t     (*receive_pfn)(void *p, uint8_t* buf, int size, size_t *received, uint32_t timeout);
typedef IO_StreamState_t     (*send_pfn)(void *p, uint8_t* buf, int size);
typedef IO_StreamState_t     (*wait_pfn)(void *p, uint32_t timeoutMs);

typedef struct iostream_t {
	uint8_t             *Buffer;
	uint32_t             bufferSize;    // size of the buffer holding the bytes
	int                  readIndex;     // point to the first byte to get
	int                  writeIndex;    // point to the first byte to write
	receive_pfn          receive;       // get function to call to fill the buffer
	wait_pfn             wait;          // wait function to call to wait for incomming bytes
	send_pfn             send;
	void                *waitArg;
	void                *receiveArg;
	void                *sendArg;
	//void                *lock;
	IO_StreamState_t     ios;
//	SemaphoreHandle_t    xTxMutex;
//	SemaphoreHandle_t    xRxMutex;
} iostream_t;


/* name       : ios_init
 * description:
 *              Initializes a stream buffer.
 * parameters :
 *   stream   : stream object
 *   buf      : buffer used to hold the data
 *   size     : size of the buffer
 *   byteCnt  : number of bytes initially contained in the buffer
 * return     : Nothing
 * */
void ios_init(iostream_t *stream, uint8_t *buf, uint32_t size, uint32_t byteCnt);

void ios_deinit(iostream_t *stream);

/* name       : is_set_rx_cb
 * description:
 *              Input stream with callback to fill the buffer.
 *              Usually the buffer fill is handled by a thread an not
 *              by callback, but this is available.
 *
 * parameters :
 *   stream   : stream object
 *   receive  : receive function to call to fill the buffer.
 *   fnArg    : arguments to pass to the receive function.
 *
 * return     : Nothing
 * */
void is_set_rx_cb(iostream_t *istream, receive_pfn receive, void *fnArg);

// enables timeout waits for activity
/* name       : ios_enable_wait
 * description:
 *              Input stream with callback to fill the buffer.
 *              Usually the buffer fill is handled by a thread an not
 *              by callback, but this is available.
 *
 * parameters :
 *   stream   : stream object
 *   receive  : receive function to call to fill the buffer.
 *   fnArg    : arguments to pass to the receive function.
 *
 * return     : Nothing
 * */
void ios_enable_wait(iostream_t *stream);

void ios_disable_wait(iostream_t *stream);


/* name       : is_put
 * description:
 *              Fills the input stream buffer with data.
 *              There is symetry of input output operations.
 *              The producer layer uses this function to fill the data.
 * parameters   :
 *   stream     : stream object
 *   buf        : buffer with data to post
 *   size       : size of the buffer
 *   cntPosted  : number of bytes copied to the the buffer
 * return       : IOS_GOOD if successful, error otherwise
 * */
IO_StreamState_t is_put(iostream_t *stream, uint8_t *buf, uint32_t size, uint32_t *cntPosted);


/* name       : is_get_byte
 * description:
 *              Extracts a byte from the input stream.
 *              The consumer uses this function to get the data.
 *              If there is no data in the buffer this function returns an error.
 *
 * parameters   :
 *   stream     : stream object
 *   pByte      : destination pointer.
 *
 * return       : IOS_GOOD if successful, error otherwise
 * */
IO_StreamState_t is_get_byte(iostream_t *istream, uint8_t *pByte);


/* name       : is_get_byte_timeout
 * description:
 *              Extracts a byte from the input stream.
 *              The consumer uses this function to get the data.
 *              If there is no data in the buffer this function blocks
 *              for the specified timeout in milliseconds.
 *
 * parameters   :
 *   stream     : stream object
 *   pByte      : destination pointer.
 *
 * return       : IOS_GOOD if successful, error otherwise
 * */
IO_StreamState_t is_get_byte_timeout(iostream_t *istream, uint8_t *pByte, uint32_t timeoutMs);


/* name       : ios_get_free_space
 * description:
 *              Returns the free space byte cnt on the stream.
 *
 * parameters   :
 *   stream     : stream object
 *
 * return       : free space byte cnt on the stream
 * */
int ios_get_free_space(iostream_t *stream);


/* name       : ios_get_byte_count
 * description:
 *              Returns the number of bytes in the stream.
 *
 * parameters   :
 *   stream     : stream object
 *
 * return       : the number of bytes in the stream
 * */
int ios_get_byte_count(iostream_t *stream);


/* name       : os_set_tx_cb
 * description:
 *              Callback to send data to the transmitter.
 *
 * parameters   :
 *   stream     : stream object
 *   send       : function pointer, call to transmit a buffer.
 *   fnArg      : first argument to pass to the function pointer
 *
 * return       : Nothing
 * */
void os_set_tx_cb(iostream_t *ostream, send_pfn send, void *fnArg);


/* name       : os_flush
 * description:
 *              Flushes the buffer by sending all data to the transmitter.
 *
 * parameters   :
 *   stream     : stream object
 *
 * return       : IOS_GOOD if successful, error otherwise
 * */
IO_StreamState_t os_flush(iostream_t *ostream);


/* name       : os_send_byte
 * description:
 *              Puts one byte into the output stream buffer.
 *
 * parameters   :
 *   stream     : stream object
 *   byte       : byte to put into the buffer
 *
 * return       : IOS_GOOD if successful, error otherwise
 * */
IO_StreamState_t os_send_byte(iostream_t *ostream, uint8_t byte);


/* name       : os_send_buffer
 * description:
 *              Puts the content of the buffer into the output stream buffer.
 *              It flushes the stream if running out of space.
 *
 * parameters   :
 *   stream     : stream object
 *   buf        : pointer to data buffer
 *   size       : size of the buffer
 *
 * return       : IOS_GOOD if successful, error otherwise
 * */
IO_StreamState_t os_send_buffer(iostream_t *ostream, uint8_t *buf, uint32_t size);


/* name       : os_send_buffer_flush
 * description:
 *              Puts the content of the buffer into the output stream buffer and flushes it.
 *
 * parameters   :
 *   stream     : stream object
 *   buf        : pointer to data buffer
 *   size       : size of the buffer
 *
 * return       : IOS_GOOD if successful, error otherwise
 * */
IO_StreamState_t os_send_buffer_flush(iostream_t *ostream, uint8_t *buf, uint32_t size);


/* name         : GetLineBlocking
 * description  :
 *              blocking call, returns a line terminated by \r\n.
 *              It doesn't return the new line \r\n.
 *
 * parameters   :
 *   stream     : stream object
 *   buf        : pointer to data buffer
 *   size       : size of the buffer
 *
 * return       : IOS_GOOD if successful, error otherwise
 * */
bool GetLineBlocking(iostream_t *istream, uint8_t *buf, uint32_t bufSize, uint32_t *rcvSize, uint32_t timeoutMs);


/* name         : GetLine
 * description  :
 *              non blocking call, returns a line terminated by \r\n.
 *              It doesn't return the new line \r\n.
 *              copies up to new line but not including the new line.
 *
 * parameters   :
 *   stream     : stream object
 *   buf        : pointer to data buffer
 *   size       : size of the buffer
 *
 * return       : IOS_GOOD if successful, error otherwise
 * */
bool GetLine(iostream_t *istream, uint8_t *buf, uint32_t bufSize, uint32_t *numCharsCopied);

#define OS_MAX_FMT_MSG_SIZE          (256)
int os_print(iostream_t *os, const char *fmt, ...);

#endif /* MIOS_H_ */
