#include <fpb_pin_mux.h>
#include "StatusLeds.h"
#include <stdint.h>
#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define NO_ERROR					(0)
#define ROC_COMM_ERROR_MASK			(0x00000001)
#define PAC_COMM_ERROR_MASK			(0x00000002)
#define SPEC_COMM_ERROR_MASK		(0x00000004)
#define PMT_ERROR_MASK				(0x00000008)
#define PMT_ALARM_MASK				(0x00000010)
#define MOTOR_ERROR_MASK			(0x00000020)
/*
#define ENTRY_SLIT_ERROR_MASK		(0x00000020)
#define EXIT_SLIT_ERROR_MASK		(0x00000040)
#define FILTER_WHEEL_ERROR_MASK		(0x00000080)
#define TRANS_MOT_ERROR_MASK		(0x00000100)
#define HUB_ERROR_MASK				(0x00000200)
#define MIRX_ERROR_MASK				(0x00000400)
#define MIRY_ERROR_MASK				(0x00000800)
*/
typedef enum{
	LedOff,
	LedOn,
	LedBlink,
	LedBlinkOnceStayOn,
	LedBlinkOnceStayOff
}LedState_e;
static LedState_e DmConnState = LedOff;
static LedState_e RocConnState = LedOn;
static LedState_e PacConnState = LedOn;
static LedState_e SpectConnState = LedOn;
static LedState_e ErrorState = LedOff;
static LedState_e PmtCoolerState = LedOff;
static LedState_e res0State = LedOff;
static LedState_e res1State = LedOff;

static QueueHandle_t statusq = 0;

static uint32_t SystemErrorState = 0;

void SetLedState(int pin, LedState_e state)
{
	switch(state){
	case LedOff:
		GPIO_WritePinOutput(LEDGPIO, pin, 0);
		break;
	case LedOn:
		GPIO_WritePinOutput(LEDGPIO, pin, 1);
		break;
	case LedBlink:
	case LedBlinkOnceStayOff:
	case LedBlinkOnceStayOn:
		GPIO_TogglePinsOutput(LEDGPIO, 1 << pin);
		break;
	}
}

void TransitionToNextState(LedState_e * statevar)
{
	switch(*statevar){
		case LedBlinkOnceStayOff:
			*statevar = LedOff;
			break;
		case LedBlinkOnceStayOn:
			*statevar = LedOn;
			break;
		default:
			break;
	}
}

void ShowStatusLed()
{
	SetLedState(DM_CONN_LED, DmConnState);
	SetLedState(ROC_CONN_LED, RocConnState);
	SetLedState(PAC_CONN_LED, PacConnState);
	SetLedState(SPECT_CONN_LED, SpectConnState);
	SetLedState(ERROR_LED, ErrorState);
	SetLedState(PMT_COOLER_LED, PmtCoolerState);
	SetLedState(RES_0_LED, res0State);
	SetLedState(RES_1_LED, res1State);

	TransitionToNextState(&DmConnState);
	TransitionToNextState(&RocConnState);
	TransitionToNextState(&PacConnState);
	TransitionToNextState(&SpectConnState);
	TransitionToNextState(&ErrorState);
	TransitionToNextState(&PmtCoolerState);

}

void LED(void *pvParameters) {
	LedStatus_e x;

	statusq = xQueueCreate( 4,sizeof(LedStatus_e));
	for (;;) {
		if (xQueueReceive(statusq,&x,200) == pdTRUE)
		{
			switch(x){
			case PmtCoolerOff:
				PmtCoolerState = LedOff;
				break;
			case PmtCoolerEnabled:
				PmtCoolerState = LedOn;
				break;
			case PmtCoolerCooling:
				PmtCoolerState = LedBlink;
				SystemErrorState &= (~PMT_ALARM_MASK);
				SystemErrorState &= (~PMT_ERROR_MASK);
				break;
			case PmtCoolerAtSetPoint:
				PmtCoolerState = LedOn;
				SystemErrorState &= (~PMT_ALARM_MASK);
				SystemErrorState &= (~PMT_ERROR_MASK);
				break;
			case PmtCoolerHasAlarm:
				PmtCoolerState = LedBlink;
				SystemErrorState |= PMT_ALARM_MASK;
				break;
			case PmtCoolerHasError:
				SystemErrorState |= PMT_ERROR_MASK;
				break;
			case DmCommunicationConnected:
				DmConnState = LedOn;
				break;
			case DmCommunicationAlive:
				DmConnState = LedBlinkOnceStayOn;
				break;
			case DmCommunicationBroke:
				DmConnState = LedOff;
				break;
			case ROCCommunicationActive:
				RocConnState = LedBlinkOnceStayOn;
				SystemErrorState &= (~ROC_COMM_ERROR_MASK);
				break;
			case RocCommunicationGood:
				RocConnState = LedOn;
				SystemErrorState &= (~ROC_COMM_ERROR_MASK);
				break;
			case RocCommunicationError:
				RocConnState = LedBlink;
				SystemErrorState |= ROC_COMM_ERROR_MASK;
				break;
			case PACCommunicationActive:
				PacConnState = LedOff;
				SystemErrorState &= (~PAC_COMM_ERROR_MASK);
				break;
			case PACCommunicationError:
				PacConnState = LedBlink;
				SystemErrorState |= PAC_COMM_ERROR_MASK;
				break;
			case PACCommunicationGood:
				SystemErrorState &= (~PAC_COMM_ERROR_MASK);
				PacConnState = LedOn;
				break;
			case SpectromenterConnectionAlive:
				SpectConnState = LedOn;
				break;
			case SpectrometerConnectionBroke:
				SpectConnState = LedBlink;
				break;
			case InstrumentInError:
				ErrorState = LedOn;
				break;
			case InstrumentOK:
				ErrorState = LedOff;
				break;
			case MotorHasAnError:
				SystemErrorState |= MOTOR_ERROR_MASK;
				break;
			case MotorHasNoError:
				SystemErrorState &= (~MOTOR_ERROR_MASK);
				break;
			}

			if(SystemErrorState == NO_ERROR){
				ErrorState = LedOff;
			} else {
				ErrorState = LedOn;
			}
		} else {
			TOGGLE_HB();
			ShowStatusLed();
		}
	}
}

void SendStatus(LedStatus_e status)
{
	xQueueSend(statusq, &status, 0);
}
