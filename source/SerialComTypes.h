/*
 * SerialComTypes.h
 *
 *  Created on: Aug 30, 2019
 *      Author: lkodheli
 */

#ifndef __SERIALCOMTYPES_H_
#define __SERIALCOMTYPES_H_

#include <stdint.h>
#include <stdbool.h>

#include "FreeRTOSConfig.h"
#include <FreeRTOS.h>
#include "queue.h"
#include "semphr.h"
#include <event_groups.h>
#include "ConfigSerialCom.h"
#include "utils.h"


enum SERIAL_COM_RESULT {
SER_SUCCESS                    = 0 ,// returned by send and receive function
SER_TIMEOUT                    = 5 ,// matches kStatus_Timeout returned by receive function, the count is updated
SER_BAD_ARGUMENT,
SER_NET_INVALID_ROUTE          ,
SER_NET_INVALID_PKT_TYPE       ,
SER_NET_INVALID_BASE_PKT_LEN   ,
SER_NET_PKT_NOT_SUPPORTED      ,

SER_TRAN_INVALID_END_POINT     ,
SER_TRAN_INVALID_BASE_PKT_LEN,
SER_TRAN_PKT_TYPE_NOT_SUPPORTED,
SER_TRAN_FREE_PKT_INFO_EMPTY,
SER_TRAN_ACCESS_SEM_CREATE,
SER_TRAN_ACCESS_SEM_TAKE,
SER_TRAN_ACCESS_SEM_GIVE,
SER_TRAN_RETRANSMIT_TIMER_SCHEDULE,
SER_TRAN_RETRANSMIT_TIMER_STOP,
SER_TRAN_END_POINT_EXISTS,

SER_TRAN_END_MAX_CNT_OVERFLOW,
SER_TRAN_END_INPUT_QUEUE_OVERFLOW,
SER_TRAN_END_POINT_RX_TIMEOUT,
SER_LINK_INVALID_PKT_VERSION   ,
SER_LINK_INVALID_BASE_PKT_LENGTH   ,
SER_LINK_INVALID_PKT_SIZE          ,
SER_LINK_PKT_NOT_SUPPORTED     ,
SER_LINK_INIT_TX_SEM_CREATE     ,
SER_LINK_INIT_RX_QUEUE_CREATE     ,
SER_LINK_INIT_TX_QUEUE_CREATE     ,


SER_LINK_INIT_SERVICE_TASK_CREATE     ,
SER_LINK_TX_SEM_TAKE     ,
SER_LINK_TX_SEM_GIVE     ,
SER_LINK_IO_ERROR     ,
SER_LINK_TX_QUEUE_PUSH,
SER_INVALID_PKT_TYPE           ,
SER_INVALID_PKT_LEN           ,
SER_RESOURCE_OUT_QUEUE_CREATE           ,
SER_RESOURCE_IN_QUEUE_CREATE           ,
SER_RESOURCE_FREE_FRM_EMPTY        ,

SER_RESOURCE_FREE_FRM_FULL        ,
SER_RESOURCE_USE_CNT_ERROR        ,
SER_RESOURCE_QUEUE_RECEIVE_ERROR        ,
SER_END_POINT_TX_SEM_CREATE,
SER_END_POINT_TX_SEM_TAKE,
SER_END_POINT_TX_SEM_GIVE,
SER_END_POINT_RX_SEM_CREATE,
SER_END_POINT_ACCESS_SEM_CREATE,
SER_END_POINT_ACCESS_SEM_TAKE,
SER_END_POINT_ACCESS_SEM_GIVE,

SER_END_POINT_TX_EVT_CREATE,
SER_END_POINT_RX_EVT_CREATE,
SER_END_POINT_TX_QUEUE_CREATE,
SER_END_POINT_RX_QUEUE_CREATE,
SER_END_POINT_RESET,
SER_END_POINT_SESSION_ERROR,
SER_END_POINT_NOT_OPEN,
SER_END_POINT_SESSION_OPEN_TIMEOUT,
SER_END_POINT_RX_ERROR,
SER_END_POINT_TX_TIMEOUT,
SER_END_POINT_RX_TIMEOUT,

SER_END_POINT_RX_RESET,
SER_END_POINT_BASE_PKT_LEN,
SER_END_POINT_DATA_BASE_PKT_LEN,
SER_END_POINT_MAX_REACHED,
SER_LIB_SEM_CREATE,
SER_LIB_EVT_CREATE,
SER_LIB_SEM_TAKE,
SER_EVT_CREATE,
SER_ERROR_MAX,
};



/*
 *
 * Repeating the START PATTERN 2 TIMES ON THE BYTE_CNT FIELD IS invalid configuration
 * That won't happen for frame payloads up to 41 KB, the max frame payload we want to
 * support is 512 bytes.
 *
 * FRAME fixed overhead 5 bytes
 *
 *
 *  Frame format:
 *
 *              |<------------------------ PAYLOAD ------------------------>|
 * +------------+----------+-------------------------------+----------------+
 * | 1 BYTE     | 2 BYTES  |              N BYTES          |     4 BYTES    |
 * +------------+----------+-------------------------------+----------------+
 * | START_BYTE | BYTE_CNT |              DATA             |       CRC      |
 * +------------+----------+-------------------------------+----------------+
 * |<-- FRAME HEADER    -->|                               |<-FRAME FOOTER->|
 *                         |                               |
 *                         |                               |
 *            +------------+                               |
 *           /                                             |
 *          +--------------------+-------------------------+       <------
 *          | 1 BYTE             |       N BYTES           |        Link Layer Packet
 *          +--------------------+-------------------------+
 *          | VERS & LNK_PKT_TYPE|       MSG               |
 *          +--------------------+-------------------------+       <------
 *          |<--  LINK HEADER -->|                         |
 *                               |                         |
 *                       +-------+                         |
 *                      /                                  |
 *                     +------------+------+------+--------+       <-------
 * Net Layer view      |   1 BYTE   |1 BYTE|1 BYTE| N BYTES|       Net Layer Packet
 *                     +------------+------+------+--------+
 *                     |NET_PKT_TYPE|SOURCE| DEST |   MSG  |
 *                     +------------+------+------+--------+
 *                     |<---    NET HEADER    --->|        |
 *                                                |        |
 *        +---------------------------------------+        +---------------------------------------------------------+     <-------
 *        |                                                                                                          |       Transport
 *        +-------------+---------+---------+---------+---------+----------+------_---+------_---+---------+---------+
 *        | 1 BYTE      | 1 BYTE  | 1 BYTE  | 1 BYTE  | 1 BYTE  |   1 BYTE | 4 BYTES  | 4 BYTES  | 4 BYTES | N BYTES |
 *        +-------------+---------+---------+---------+---------+----------+----------+----------+---------+---------+
 *        |TRAN_PKT_TYPE|DEST_SLOT| SRC_SLOT|SRC_PORT |DEST_PORT|SESSION_ID|SENT_TIME |SENT_BYTES|RCV_BYTES|   MSG   |
 *        +-------------+---------+---------+---------+---------+----------+----------+----------+---------+---------+
 *        |<------------------                  TRANSPORT HEADER                        ------------------>|
 *
 *
 *  BYTE_CNT = SIZEOF(DATA) + SIZEOF(CRC)
 *
 * */

#define MAX_DELAY                         (0xFFFFFFFFUL)



#define START_FRAME                       (0xA5)
#define FRAME_START_SIZE                  (1)
#define FRAME_BYTE_CNT_LEN                (2)

#define FRAME_CRC_SIZE                    (4)




#define PKT_VERSION_02                    (2)

//#define PKT_SIZE_LEN             (2)
// Link header fields
#define PKT_LINK_VERSION_LEN              (1)

#define PKT_LINK_HEADER_LEN               (PKT_LINK_VERSION_LEN)
#define FRAME_PAYLOAD_MIN                 (FRAME_BYTE_CNT_LEN + PKT_LINK_HEADER_LEN + FRAME_CRC_SIZE)
#define FRAME_IS_BYTE_CNT_VALID(BYTE_CNT)  ( (((BYTE_CNT)+FRAME_BYTE_CNT_LEN) >= FRAME_PAYLOAD_MIN) && (((BYTE_CNT)+FRAME_BYTE_CNT_LEN) <= FRAME_PAYLOAD_SIZE) )

#define PKT_NET_PKT_TYPE_LEN              (1)
#define PKT_NET_SOURCE_DEV_LEN            (1)
#define PKT_NET_DEST_DEV_LEN              (1)

#define PKT_NET_HEADER_LEN                ( \
		                                   PKT_NET_PKT_TYPE_LEN + \
		                                   PKT_NET_SOURCE_DEV_LEN +\
										   PKT_NET_DEST_DEV_LEN \
										   )

#define PKT_TRAN_PKT_TYPE_LEN             (1)
#define PKT_TRAN_DEST_SLOT_LEN            (1)
#define PKT_TRAN_SRC_SLOT_LEN             (1)
#define PKT_TRAN_SRC_PORT_LEN             (1)
#define PKT_TRAN_DEST_PORT_LEN            (1)
#define PKT_TRAN_SESSION_ID_LEN           (1)
#define PKT_TRAN_SENT_TIME_LEN            (4)
#define PKT_TRAN_SENT_BYTES_CNT_LEN       (4)
#define PKT_TRAN_RCV_BYTES_CNT_LEN        (4)

#define PKT_TRANSPORT_HEADER_LEN        (\
                                         PKT_TRAN_PKT_TYPE_LEN + \
		                                 PKT_TRAN_DEST_SLOT_LEN + \
										 PKT_TRAN_SRC_SLOT_LEN + \
										 PKT_TRAN_SRC_PORT_LEN + \
										 PKT_TRAN_DEST_PORT_LEN + \
										 PKT_TRAN_SESSION_ID_LEN + \
										 PKT_TRAN_SENT_TIME_LEN + \
										 PKT_TRAN_SENT_BYTES_CNT_LEN + \
										 PKT_TRAN_RCV_BYTES_CNT_LEN \
                                         )



#define PKT_OVERHEAD                     ( \
		                                  PKT_LINK_HEADER_LEN + \
										  PKT_NET_HEADER_LEN + \
		                                  PKT_TRANSPORT_HEADER_LEN \
										  )
#define PKT_MSG_SIZE_MAX                  (190)

#define PKT_SIZE_MAX                      (PKT_OVERHEAD + PKT_MSG_SIZE_MAX) // most transactions will fit this packet size

#define PKT_LINK_MSG_SIZE_MAX                              (PKT_SIZE_MAX - PKT_LINK_HEADER_LEN) // what follows link header
#define PKT_NET_MSG_SIZE_MAX                               (PKT_LINK_MSG_SIZE_MAX - PKT_NET_HEADER_LEN)
#define PKT_TRANSPORT_MSG_SIZE_MAX                         (PKT_NET_MSG_SIZE_MAX - PKT_TRANSPORT_HEADER_LEN)

#define FRAME_PAYLOAD_SIZE                (FRAME_BYTE_CNT_LEN + PKT_SIZE_MAX + FRAME_CRC_SIZE)
#define FRAME_SIZE                        (FRAME_START_SIZE FRAME_SIZE)


#define FRM_PKT_OFFSET                   (FRAME_BYTE_CNT_LEN)





#define PKT_TYPE_LINK_LAYER_SRV                            (1)
#define PKT_TYPE_LINK_LAYER_DATA                           (0)

#define PKT_TYPE_NET_LAYER_SRV                             (1)
#define PKT_TYPE_NET_LAYER_DATA                            (0)

#define PKT_TYPE_TRANSPORT_LAYER_SRV                       (1)
#define PKT_TYPE_TRANSPORT_LAYER_DATA                      (0)

//#define PKT_TYPE_END_POINT_LAYER_SRV                       (4)
//#define PKT_TYPE_END_POINT_DATA                            (5)

#define PKT_TYPE_TRANSPORT_LAYER_SRV_OPEN_SESSION          (1)
#define PKT_TYPE_TRANSPORT_LAYER_SRV_CLOSE_SESSION         (2)
#define PKT_TYPE_TRANSPORT_LAYER_SRV_SESSION_INFO          (3)
#define PKT_TYPE_TRANSPORT_LAYER_SRV_SESSION_INFO_CONF     (4)
#define PKT_TYPE_TRANSPORT_LAYER_SRV_BAD_SESSION           (5)


#define NET_PATHS_MAX                    (10)
#define NET_HOST_IS_MAX                  (255)
#define DEVICE_MAX                       (255)
#define PORT_MAX                         (255)
//#define PKT_TYPE_NET_LEN                 ()




// every packet processed by the Link layer is a SLinkPacket
typedef struct __attribute__((packed)) LinkHeader_t {
	uint8_t      lnkPktType;
	// LINK PKT VERSION  uses the upper nibble of the TYPE byte
	// LINK PKT TYPE uses the lower nibble of the TYPE byte
} LinkHeader_t;


// every packet processed by the network layer is a NetPacket
typedef struct __attribute__((packed))  NetHeader_t {
	uint8_t          pktType;     // network layer has many packet types,
	uint8_t          srcDev;         // source device originating the transfer
	uint8_t          destDev;        // destination device, the intended recipient
} NetHeader_t;


// every packet processed by the transport layer is a TransportPacket
typedef struct __attribute__((packed))  TransportHeader_t {
	uint8_t          pktType;
	uint8_t          destSlot;
	uint8_t          srcSlot;
	uint8_t          srcPort;
	uint8_t          destPort;
	uint8_t          sessionId;
	uint32_t         sentTime;
	uint32_t         sentBytes;
	uint32_t         rcvBytes;
} TransportHeader_t;


typedef struct __attribute__((packed)) Packet_t {
	uint16_t              frmByteCnt;        // this is a field, bytes following the byteCnt field including the crc
	LinkHeader_t          lnkHdr;
	NetHeader_t           netHdr;
	TransportHeader_t     tranHdr;
	uint8_t               msg[PKT_TRANSPORT_MSG_SIZE_MAX];
	uint32_t              crc;
	// helper member, not serialized
	uint16_t              pktByteCnt; // will not corrupt the message or the crc since it placed after the crc
} Packet_t;


#define _DEBUG_FRM_
#define _DEBUG_FRM_HIST_SIZE   (10)
typedef struct __attribute__((packed))  Frame_t{
	 // this is the payload
	union      {
		uint8_t                    bytes[FRAME_PAYLOAD_SIZE]; // ADD data + crc as well since it will be copied
		Packet_t                   pkt;
	}; // end payload

	// helper members, not serialized
	uint16_t    payloadLength;       // packet + crc = payload length
	uint8_t    *data;
	int useCnt;

#ifdef _DEBUG_FRM_
	uint16_t _useHist[_DEBUG_FRM_HIST_SIZE];
	uint8_t  _useIdx;
#endif
} Frame_t;

#define DWORD_NET2HOST(DWORD) \
	( \
      (((DWORD) & 0x000000ff) << 24) | (((DWORD) & 0x0000ff00) << 8) | (((DWORD) & 0x00ff0000) >> 8) \
			| (((DWORD) & 0xff000000) >> 24) \
	)

#define WORD_NET2HOST(WORD) \
    ( \
	  (((WORD) & 0x00ff) << 8) | (((WORD) & 0xff00) >> 8) \
    )

#define DWORD_HOST2NET(DWORD)       DWORD_NET2HOST(DWORD)
#define WORD_HOST2NET(WORD)        WORD_NET2HOST(WORD)

#define PKT_SIZE(MSG_SIZE)     (PKT_OVERHEAD + (MSG_SIZE))

#define GET_LINK_VERSION(FRM) ( ((Frame_t*)(FRM))->pkt.lnkHdr.lnkPktType >> 4)
#define GET_LINK_TYPE(FRM) ( ((Frame_t*)(FRM))->pkt.lnkHdr.lnkPktType & 0x0F )
#define SET_LINK_VERSION_TYPE(FRM, VER, TYPE) (\
		((Frame_t*)(FRM))->pkt.lnkHdr.lnkPktType = \
		        (((VER)&0x0F) << 4) | ((TYPE)&0x0F) )

#define GET_FRM_BYTE_CNT(FRM) ( WORD_NET2HOST( ((Frame_t*)(FRM))->pkt.frmByteCnt ) )
#define SET_FRM_BYTE_CNT(FRM, BYTE_CNT)( ((Frame_t*)(FRM))->pkt.frmByteCnt = WORD_HOST2NET(BYTE_CNT))
#define GET_FRM_DATA_LEN(FRM)            GET_FRM_BYTE_CNT(FRM)
#define GET_FRM_PAYLOAD_LEN(FRM)         (GET_FRM_BYTE_CNT(FRM) + FRAME_BYTE_CNT_LEN + FRAME_CRC_SIZE)
#define GET_FRM_CRC_OFFSET(FRM)         (GET_FRM_PAYLOAD_LEN(FRM) - 4)

#define GET_NET_SRC_DEV(FRM) ( ((Frame_t*)(FRM))->pkt.netHdr.srcDev )
#define GET_NET_DEST(FRM) ( ((Frame_t*)(FRM))->pkt.netHdr.destDev )
#define GET_NET_PKT_TYPE(FRM) ( ((Frame_t*)(FRM))->pkt.netHdr.pktType )

#define SET_NET_DEST_DEV(FRM, DEST_DEV) ( ((Frame_t*)(FRM))->pkt.netHdr.destDev = (DEST_DEV))
#define SET_NET_SRC_DEV(FRM, SRC_DEV) ( ((Frame_t*)(FRM))->pkt.netHdr.srcDev = (SRC_DEV))
#define SET_NET_PKT_TYPE(FRM, NPKT_TYPE) ( ((Frame_t*)(FRM))->pkt.netHdr.pktType = (NPKT_TYPE) )


#define GET_TRAN_MSG_SIZE(FRM) (\
	GET_FRM_BYTE_CNT(FRM) - PKT_LINK_HEADER_LEN - PKT_NET_HEADER_LEN - PKT_TRANSPORT_HEADER_LEN)

#define GET_TRAN_MSG_BYTES(FRM) ( ((Frame_t*)(FRM))->pkt.msg )

#define GET_TRAN_MSG_FRAME(MSG_BYTES) ( ((uint8_t*)(MSG_BYTES)) - PKT_TRANSPORT_HEADER_LEN - PKT_NET_HEADER_LEN - PKT_LINK_HEADER_LEN - FRAME_BYTE_CNT_LEN)

#define GET_FRM_CRC(FRM) ( \
                           DWORD_NET2HOST( \
                 	         *((uint32_t*) (& (((Frame_t*)(FRM))->bytes[GET_FRM_CRC_OFFSET(FRM)]) ) )\
				         )\
                        )
#define SET_FRM_CRC(FRM, CRC) (\
		                        *((uint32_t*) (& (((Frame_t*)(FRM))->bytes[GET_FRM_CRC_OFFSET(FRM)]) ) ) = DWORD_HOST2NET( CRC )\
							  )

#define GET_TRAN_PKT_TYPE(FRM) ( ((Frame_t*)(FRM))->pkt.tranHdr.pktType )
#define GET_TRAN_TX_TIME(FRM) ( DWORD_NET2HOST( ((Frame_t*)(FRM))->pkt.tranHdr.sentTime ) )
#define GET_TRAN_TX_CNT(FRM) ( DWORD_NET2HOST( ((Frame_t*)(FRM))->pkt.tranHdr.sentBytes ) )
#define GET_TRAN_RX_CNT(FRM) ( DWORD_NET2HOST( ((Frame_t*)(FRM))->pkt.tranHdr.rcvBytes ) )
#define GET_TRAN_SESSION_ID(FRM) ( ((Frame_t*)(FRM))->pkt.tranHdr.sessionId)
#define GET_TRAN_SRC_PORT(FRM) ( ((Frame_t*)(FRM))->pkt.tranHdr.srcPort )
#define GET_TRAN_DEST_PORT(FRM) ( ((Frame_t*)(FRM))->pkt.tranHdr.destPort )
#define GET_TRAN_DEST_SLOT(FRM) ( ((Frame_t*)(FRM))->pkt.tranHdr.destSlot )
#define GET_TRAN_SRC_SLOT(FRM) ( ((Frame_t*)(FRM))->pkt.tranHdr.srcSlot )

#define GET_TRAN_PKT_TYPE_STR(FRM)\
	(GET_TRAN_PKT_TYPE(FRM)==PKT_TYPE_TRANSPORT_LAYER_DATA)? "DATA" :\
	(GET_TRAN_MSG_BYTES(FRM)[0]==PKT_TYPE_TRANSPORT_LAYER_SRV_CLOSE_SESSION)? "CLOSE" :\
			(GET_TRAN_MSG_BYTES(FRM)[0]==PKT_TYPE_TRANSPORT_LAYER_SRV_OPEN_SESSION)? "OPEN" :\
					(GET_TRAN_MSG_BYTES(FRM)[0]==PKT_TYPE_TRANSPORT_LAYER_SRV_SESSION_INFO)? "INFO" :\
							(GET_TRAN_MSG_BYTES(FRM)[0]==PKT_TYPE_TRANSPORT_LAYER_SRV_SESSION_INFO_CONF)? "INFO CONF" :\
									(GET_TRAN_MSG_BYTES(FRM)[0]==PKT_TYPE_TRANSPORT_LAYER_SRV_BAD_SESSION)? "BAD SESSION" : "UNKNOWN"\

#define SET_TRAN_TYPE(FRM, TPKT_TYPE) ( ((Frame_t*)(FRM))->pkt.tranHdr.pktType = (TPKT_TYPE) )
#define SET_TRAN_DEST_SLOT(FRM, DEST_SLOT) ( ((Frame_t*)(FRM))->pkt.tranHdr.destSlot = (DEST_SLOT) )
#define SET_TRAN_SRC_SLOT(FRM, SRC_SLOT) ( ((Frame_t*)(FRM))->pkt.tranHdr.srcSlot = (SRC_SLOT) )
#define SET_TRAN_SRC_PORT(FRM, SRC_PORT) ( ((Frame_t*)(FRM))->pkt.tranHdr.srcPort = (SRC_PORT) )
#define SET_TRAN_DEST_PORT(FRM, DEST_PORT) ( ((Frame_t*)(FRM))->pkt.tranHdr.destPort = (DEST_PORT) )
#define SET_TRAN_SESSION_ID(FRM, SESSION_ID) ( ((Frame_t*)(FRM))->pkt.tranHdr.sessionId = (SESSION_ID) )
#define SET_TRAN_TX_TIME(FRM, TX_TIME) ( ((Frame_t*)(FRM))->pkt.tranHdr.sentTime = DWORD_HOST2NET(TX_TIME) )
#define SET_TRAN_TX_CNT(FRM, TX_CNT) ( ((Frame_t*)(FRM))->pkt.tranHdr.sentBytes = DWORD_HOST2NET(TX_CNT) )
#define SET_TRAN_RX_CNT(FRM, RX_CNT) ( ((Frame_t*)(FRM))->pkt.tranHdr.rcvBytes = DWORD_HOST2NET( RX_CNT ) )

#define LINK_STATUS_INITIALIZED             (1)

typedef	enum FrameState_t{
	FrmState_Idle,
	FrmState_InStart,
	FrmState_InCnt,
	FrmState_InData,
	FrmState_InCrC,
	FrmState_FrameComplete,
} FrameState_t;

typedef struct FieldParseCtxt_t {
	enum FLD_PARSE {
		FLDPRS_IDDLE,
		FLDPRS_ESCAPE,
//		FLDPRS_ESCAPE_DATA,
		FLDPRS_DATA,
	} fldState;
	uint32_t  index;
	uint32_t  len;
	uint8_t  *dest;
} FieldParseCtxt_t;

typedef struct FrameRxCtxt_t {
	FrameState_t     frameState;
	FieldParseCtxt_t fldState;
	Frame_t         *pFrame;
	uint16_t         dataLen;
} FrameRxCtxt_t;

typedef struct FrameTxCtxt_t {
	Frame_t         *pFrame;
	uint8_t         *pdata;
	uint8_t         *pend;
	uint16_t         nextFldWidth;
} FrameTxCtxt_t;

typedef struct SerialLink_t {
	QueueHandle_t        rxFrmQue;
	QueueHandle_t        txFrmQue;
	TaskHandle_t         xHandle;
	FrameRxCtxt_t        rxCtxt; // receive context
	FrameTxCtxt_t        txCtxt; // transmit context
	uint32_t             stickyStatus;
	//uint32_t           linkStatus;
} SerialLink_t ;





typedef struct NetPath_t {
	uint8_t      dest;
	SerialLink_t   *plink;
} NetPath;

typedef struct NetConfig_t {
	uint8_t                hostId;
    NetPath                routes[NET_PATHS_MAX];
    int                    numRoutes;
} NetConfig_t;

typedef enum EndPointState_t {
	EP_STATE_IDLE,
	EP_STATE_LISTEN,
	EP_STATE_OPEN_REQ,
	EP_STATE_OPEN,
	EP_STATE_RESETING,
	EP_STATE_IN_ERROR
} EndPointState;

#define EP_RESOURCE_TAKE_ERROR                       (1<<1)
#define EP_RESOURCE_GIVE_ERROR                       (1<<2)
#define EP_ACCESS_TAKE_ERROR                         (1<<3)
#define EP_ACCESS_GIVE_ERROR                         (1<<4)


typedef enum EndpointType_t {
	EP_TYPE_SERVER,
	EP_TYPE_CLIENT,
	EP_TYPE_ACCEPTOR,
} EndpointType_t;

// some of the sticky errors can't be fixed
#define EP_COOKIE                                    (0xBEEFCEEF)
typedef struct Connection_t Connection_t;
typedef struct ConnectionEndPoint_t {
	uint8_t                        remoteDev;         // device to talk to
	uint8_t                        remoteSlot;        // set during session open
	uint8_t                        remotePort;        // the port number on the remote device
	uint8_t                        hostSlot;          // the slot on the host
	uint8_t                        hostPort;          // the port on the host
	uint32_t                       hostRxByteCnt;     // received byte count
	uint32_t                       hostTxByteCnt;     // confirmed transmited byte count
	uint32_t                       txBytesInFlight;   // sent a message of this size, but not confirmed yet
	uint32_t                       lastDataArrivalTime;    // last received new data time
	uint32_t                       lastDataArrivalSize;    // last received new data time

	uint32_t                       stickyError;       // sticky error bits
	SemaphoreHandle_t              epAccessSemaphore; // operation serialization mutex

	EndPointState                  epState;
	uint8_t                        sessionId;
	TickType_t                     roundTripTicks;
	TickType_t                     ticksAtFirstTx;
	QueueHandle_t                  txMsgQue;
	QueueHandle_t                  rxMsgQue;
	EventGroupHandle_t             rxEvent;
	EventGroupHandle_t             txEvent;

	bool                           rxActive;
	bool                           txActive;

 	uint32_t                       cookie;
 	EndpointType_t                 epType;
 	struct ConnectionEndPoint_t   *next;

} ConnectionEndPoint_t;

struct Connection_t {
	SemaphoreHandle_t      rxSemaphore;       // mutex for serializing read requests
	SemaphoreHandle_t      txSemaphore;
	ConnectionEndPoint_t   ep;
};

//#define TRANSPORT_PKT_ROUND_TRIP_MS_MAX         (500)
#define TRANSPORT_PKT_ROUND_TRIP_MS_MAX         (1000)

#define EP_INPUT_FRM_CNT_MAX                    (2) // 2 outstanding input frames per end point connection
#define EP_OUTPUT_FRM_CNT_MAX                   (2) // 2 outstanding 1 OUT PKT + 1 ACK PKT
#define LINK_INPUT_FRM_CNT_MAX                  (4) // every link will have 1 FRAME to receive the data


#define RESOURCE_QUEUE_PACKET_CNT               ( (EP_INPUT_FRM_CNT_MAX + EP_OUTPUT_FRM_CNT_MAX) * CFG_END_POINT_CNT_MAX\
		                                          + LINK_INPUT_FRM_CNT_MAX)
#define TRAN_PKT_INFO_ARRAY_SIZE                 (RESOURCE_OUT_QUEUE_PACKET_CNT) // 2 outstanding for each connection, OUT and ACK


#define TRANSPORT_PKT_RETRANSMIT_CNT            (5)





#define TRAN_ERROR_TIMER_STOP                   (1<<0)
#define TRAN_ERROR_TIMER_SCHEDULE               (1<<1)
#define TRAN_ERROR_ACCESS_SEM_TAKE              (1<<2)
#define TRAN_ERROR_ACCESS_SEM_GIVE              (1<<3)
#define TRAN_ERROR_END_POINT_MAXED_OUT          (1<<4)
#define TRAN_ERROR_END_POINT_RESOURCE_CREATE    (1<<5)

#define HOST_PORT_USE_DWORDS               (8)
#define HOST_END_POINT_USE_DWORDS          (2)
typedef struct TransportConfig_t {
	ConnectionEndPoint_t   *EpArr[CFG_END_POINT_CNT_MAX];
	uint32_t                EndPointUse[HOST_END_POINT_USE_DWORDS];
	uint32_t                PortUse[HOST_PORT_USE_DWORDS];
	int                     NumEndPoints;                         // Number of end points configured

	SemaphoreHandle_t       accessSemaphore;
	//TimerHandle_t          retransmitTimer;
	uint32_t                error;                                // errors seen, is dynamically updated
	uint32_t                stickyError;                          // errors seen, is sticky, not cleared
	uint8_t                 nextSessionId;

} TransportConfig_t;




#endif /* __SERIALCOMTYPES_H_ */
