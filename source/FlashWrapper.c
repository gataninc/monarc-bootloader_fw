/*
 * FlashWrapper.c
 *
 *  Created on: Jun 29, 2018
 *      Author: stelasang
 */


#include "FlashWrapper.h"
#include "Crc.h"

#include "memory_map.h"

static flash_config_t s_flashDriver;
static uint32_t s_flashBlockBase = 0;
static uint32_t s_flashTotalSize = 0;
static uint32_t s_flashSectorSize = 0;
static uint32_t s_appPartitionStartOffset=0;
static uint32_t s_appPartitionEndOffset=0;
static uint32_t s_appPartitionSize=0;
static bool     bFlashInitialized = false;
uint32_t GetApplicationPartitionOffset()
{
	return s_appPartitionStartOffset;
}
uint32_t GetApplicationPartitionSize()
{
	return s_appPartitionSize;
}
int InitFlashWrapper()
{
	status_t result;
	memset(&s_flashDriver, 0, sizeof(flash_config_t));
	result = FLASH_Init(&s_flashDriver);
	if (kStatus_FLASH_Success == result)
	{
		FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflashBlockBaseAddr, &s_flashBlockBase);
		FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflashTotalSize, &s_flashTotalSize);
		FLASH_GetProperty(&s_flashDriver, kFLASH_PropertyPflashSectorSize, &s_flashSectorSize);
		//uint32_t *pappbase = &__base_APPLICATION_FLASH;
		//uint32_t *papptop = &__top_APPLICATION_FLASH;
		s_appPartitionStartOffset = (uint32_t)__base_APPLICATION_FLASH;
		s_appPartitionEndOffset = (uint32_t)__top_APPLICATION_FLASH;
		s_appPartitionSize = s_appPartitionEndOffset - s_appPartitionStartOffset - s_flashSectorSize;
		bFlashInitialized = true;
	}

	return result;
}

#if 0
int SetUpperBlockSwapState_Update(flash_swap_state_config_t *flashSwapState, uint32_t *loc)
{
	int res = FLASH_SetSwapState_Update(&s_flashDriver, GetUpperHalfOffset() - s_flashSectorSize, flashSwapState);
	*loc = v_flash_exec_loc;
	return res;
}

int SetUpperBlockSwapState_Complete(flash_swap_state_config_t *flashSwapState, uint32_t *loc)
{
	if (s_flashDriver.PFlashTotalSize == 0){
		InitFlashWrapper();
	}
	int res = FLASH_SetSwapState_Complete(&s_flashDriver, GetUpperHalfOffset() - s_flashSectorSize, flashSwapState);
	*loc = v_flash_exec_loc;
	return res;
}

int GetUpperBlockSwapState(flash_swap_state_config_t *flashSwapState)
{
	int res = FLASH_GetSwapState(&s_flashDriver, GetUpperHalfOffset() - s_flashSectorSize, flashSwapState);
	return res;
}

int MarkUpperHalfAsBootImage()
{
	//status_t result;
	//int retval = FALSE;
	//flash_security_state_t state;

	//result = FLASH_GetSecurityState(&s_flashDriver,&state);
	//if (kStatus_FLASH_Success != result){
	//	return retval;
	//}
	//if (state != kFLASH_SecurityStateNotSecure){
	//	return FALSE;
	//}

	return FLASH_Swap(&s_flashDriver, GetUpperHalfOffset() - s_flashSectorSize, kFLASH_SwapFunctionOptionEnable);
	//if (kStatus_FLASH_Success == result)
	//{
	//	retval = TRUE;
	//}
	//return retval;
}
#endif

BOOL IsUpperBlockNeedsErasing()
{
	BOOL retval = FALSE;
	uint32_t flashcontent;
	uint32_t i;
	/* Verify programming by reading back from flash directly*/
	for (i = s_appPartitionStartOffset;
		 i < s_appPartitionStartOffset + s_appPartitionSize;
		 i+=4)
	{
		flashcontent = *(volatile uint32_t *)(i);
		if (flashcontent != 0xFFFFFFFF)
		{
			retval = TRUE;
			break;
		}
	}
	return retval;
}



int EraseApplicationPartition()
{
	status_t result;


	if (!bFlashInitialized){
		return -1;
	}

	result = FLASH_Erase(&s_flashDriver, s_appPartitionStartOffset, s_appPartitionSize, kFLASH_ApiEraseKey);
	if (kStatus_FLASH_Success == result)
	{
		result = FLASH_VerifyErase(&s_flashDriver, s_appPartitionStartOffset, s_appPartitionSize, kFLASH_MarginValueUser);
	}

	return result;
}


int WriteToApplicationPartition(uint32_t offset, uint32_t* buf, uint32_t sizeInBytes)
{
	status_t result;

	uint32_t failAddr, failDat;
	uint32_t maxoffset = s_appPartitionStartOffset + s_appPartitionSize;

	if (!bFlashInitialized){
		return -1;
	}

	if (offset < s_appPartitionStartOffset ){
		return -2;
	}

	if(sizeInBytes % FSL_FEATURE_FLASH_PFLASH_BLOCK_WRITE_UNIT_SIZE != 0){
		sizeInBytes += FSL_FEATURE_FLASH_PFLASH_BLOCK_WRITE_UNIT_SIZE;
		sizeInBytes /= FSL_FEATURE_FLASH_PFLASH_BLOCK_WRITE_UNIT_SIZE;
		sizeInBytes *= FSL_FEATURE_FLASH_PFLASH_BLOCK_WRITE_UNIT_SIZE;
	}

	if ( (offset + sizeInBytes) > maxoffset) {
		return -3;
	}

	result = FLASH_Program(&s_flashDriver, offset, buf, sizeInBytes);
	if (kStatus_FLASH_Success == result)
	{
		result = FLASH_VerifyProgram(&s_flashDriver, offset, sizeInBytes, (uint32_t*)buf, kFLASH_MarginValueUser,
										 &failAddr, &failDat);
	}

	/* Verify programming by Program Check command with user margin levels */

	return result;

}


BOOL GetCrcForApplicationPartition(uint32_t offset, uint32_t sizeInBytes, uint32_t *crc)
{
	uint32_t *flash_ptr;

	flash_ptr = (uint32_t*)offset;

	uint32_t maxoffset = s_appPartitionStartOffset + s_appPartitionSize;
	if ( (offset + sizeInBytes) > maxoffset) {
		return FALSE;
	}
    *crc = Crc32(0, flash_ptr, sizeInBytes);

	return TRUE;

}


