/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * TEXT BELOW IS USED AS SETTING FOR THE PINS TOOL *****************************
PinsProfile:
- !!product 'Pins v2.0'
- !!processor 'MK64FN1M0xxx12'
- !!package 'MK64FN1M0VLL12'
- !!mcu_data 'ksdk2_0'
- !!processor_version '1.0.9'
 * BE CAREFUL MODIFYING THIS COMMENT - IT IS YAML SETTINGS FOR THE PINS TOOL ***
 */

#include <fpb_pin_mux.h>
#include "fsl_common.h"
#include "fsl_port.h"
#include "board.h"

#define PIN16_IDX                       16u   /*!< Pin number for pin 16 in a port */

#define PIN17_IDX                       17u   /*!< Pin number for pin 17 in a port */

#define SOPT5_UART0TXSRC_UART_TX      0x00u   /*!< UART 0 transmit data source select: UART0_TX pin */

/*
 * TEXT BELOW IS USED AS SETTING FOR THE PINS TOOL *****************************
BOARD_InitPins:
- options: {coreID: singlecore, enableClock: 'true'}
- pin_list:
  - {pin_num: '62', peripheral: UART0, signal: RX, pin_signal: PTB16/SPI1_SOUT/UART0_RX/FTM_CLKIN0/FB_AD17/EWM_IN}
  - {pin_num: '63', peripheral: UART0, signal: TX, pin_signal: PTB17/SPI1_SIN/UART0_TX/FTM_CLKIN1/FB_AD16/EWM_OUT_b}
 * BE CAREFUL MODIFYING THIS COMMENT - IT IS YAML SETTINGS FOR THE PINS TOOL ***
*/

/*FUNCTION**********************************************************************
 * 
 * Function Name : BOARD_InitBootPins
 * Description   : Calls initialization functions.
 * 
 *END**************************************************************************/
void BOARD_FpbInitBootPins(void) {
  BOARD_FpbInitPins();
}

#define PIN0_IDX                         0u   /*!< Pin number for pin 0 in a port */
#define PIN1_IDX                         1u   /*!< Pin number for pin 1 in a port */
#define PIN5_IDX                         5u   /*!< Pin number for pin 5 in a port */
#define PIN11_IDX                       11u   /*!< Pin number for pin 5 in a port */
#define PIN12_IDX                       12u   /*!< Pin number for pin 12 in a port */
#define PIN13_IDX                       13u   /*!< Pin number for pin 13 in a port */
#define PIN14_IDX                       14u   /*!< Pin number for pin 14 in a port */
#define PIN15_IDX                       15u   /*!< Pin number for pin 15 in a port */
#define PIN16_IDX                       16u   /*!< Pin number for pin 16 in a port */
#define PIN17_IDX                       17u   /*!< Pin number for pin 17 in a port */
#define PIN18_IDX                       18u   /*!< Pin number for pin 18 in a port */



/*FUNCTION**********************************************************************
 *
 * Function Name : BOARD_InitPins
 * Description   : Configures pin routing and optionally pin electrical features.
 *
 *END**************************************************************************/
void BOARD_FpbInitPins(void) {


    port_pin_config_t configSDHC = {0};
    configSDHC.pullSelect = kPORT_PullUp;
    configSDHC.driveStrength = kPORT_HighDriveStrength;
    configSDHC.mux = kPORT_MuxAlt4;



  CLOCK_EnableClock(kCLOCK_PortA);                           /* Port B Clock Gate Control: Clock enabled */
  CLOCK_EnableClock(kCLOCK_PortB);                           /* Port B Clock Gate Control: Clock enabled */
  CLOCK_EnableClock(kCLOCK_PortC);
  CLOCK_EnableClock(kCLOCK_PortD);
  CLOCK_EnableClock(kCLOCK_PortE);

  /* UART4 */
#if (BOARD_TYPE == 0)
  PORT_SetPinMux(PORTE, 24U, kPORT_MuxAlt3);
  PORT_SetPinMux(PORTE, 25U, kPORT_MuxAlt3);
#else
  PORT_SetPinMux(PORTC, 14U, kPORT_MuxAlt3);
  PORT_SetPinMux(PORTC, 15U, kPORT_MuxAlt3);
#endif


//  PORT_SetPinMux(PORTB, PIN16_IDX, kPORT_MuxAlt3);           /* PORTB16 (pin 62) is configured as UART0_RX */
//  PORT_SetPinMux(PORTB, PIN17_IDX, kPORT_MuxAlt3);           /* PORTB17 (pin 63) is configured as UART0_TX */
  SIM->SOPT5 = ((SIM->SOPT5 &
    (~(SIM_SOPT5_UART0TXSRC_MASK)))                          /* Mask bits to zero which are setting */
      | SIM_SOPT5_UART0TXSRC(SOPT5_UART0TXSRC_UART_TX)       /* UART 0 transmit data source select: UART0_TX pin */
    );

	/* Initializes SDHC pins below */
	PORT_SetPinConfig(PORTE, 0U, &configSDHC);
	PORT_SetPinConfig(PORTE, 1U, &configSDHC);
	PORT_SetPinConfig(PORTE, 2U, &configSDHC);
	PORT_SetPinConfig(PORTE, 3U, &configSDHC);
	PORT_SetPinConfig(PORTE, 4U, &configSDHC);
	PORT_SetPinConfig(PORTE, 5U, &configSDHC);
	configSDHC.pullSelect = kPORT_PullDown;
	configSDHC.driveStrength = kPORT_LowDriveStrength;
	configSDHC.mux = kPORT_MuxAsGpio;
	PORT_SetPinConfig(PORTC, 19U, &configSDHC);


#if (BOARD_TYPE < 2)
  /* Led pin mux Configuration */
  PORT_SetPinMux(PORTB, 22U, kPORT_MuxAsGpio);
  PORT_SetPinMux(PORTB, 21U, kPORT_MuxAsGpio);
  PORT_SetPinMux(PORTE, 26U, kPORT_MuxAsGpio);

  /* Initialize LED pins below */
  LED_RED_INIT(1U);
  LED_BLUE_INIT(0U);
  LED_GREEN_INIT(1U);
#endif
  gpio_pin_config_t phy_reset_opin = {kGPIO_DigitalOutput, 0};
  PORT_SetPinMux(PORTA, 11U, kPORT_MuxAsGpio);
  GPIO_PinInit(GPIOA, 11U, &phy_reset_opin);  // LOW should reset phy
  GPIO_WritePinOutput(GPIOA, 11U, 1);

  PORT_SetPinMux(PORTA, PIN12_IDX, kPORT_MuxAlt4);           /* PORTA12 (pin 42) is configured as RMII0_RXD1 */
  PORT_SetPinMux(PORTA, PIN13_IDX, kPORT_MuxAlt4);           /* PORTA13 (pin 43) is configured as RMII0_RXD0 */
  PORT_SetPinMux(PORTA, PIN14_IDX, kPORT_MuxAlt4);           /* PORTA14 (pin 44) is configured as RMII0_CRS_DV */
  PORT_SetPinMux(PORTA, PIN15_IDX, kPORT_MuxAlt4);           /* PORTA15 (pin 45) is configured as RMII0_TXEN */
  PORT_SetPinMux(PORTA, PIN16_IDX, kPORT_MuxAlt4);           /* PORTA16 (pin 46) is configured as RMII0_TXD0 */
  PORT_SetPinMux(PORTA, PIN17_IDX, kPORT_MuxAlt4);           /* PORTA17 (pin 47) is configured as RMII0_TXD1 */
  PORT_SetPinMux(PORTA, PIN5_IDX, kPORT_MuxAlt4);            /* PORTA5 (pin 39) is configured as RMII0_RXER */

  const port_pin_config_t portb0_pin53_config = {
    kPORT_PullUp,                                            /* Internal pull-up resistor is enabled */
    kPORT_FastSlewRate,                                      /* Fast slew rate is configured */
    kPORT_PassiveFilterDisable,                              /* Passive filter is disabled */
    kPORT_OpenDrainEnable,                                   /* Open drain is enabled */
    kPORT_LowDriveStrength,                                  /* Low drive strength is configured */
    kPORT_MuxAlt4,                                           /* Pin is configured as RMII0_MDIO */
    kPORT_UnlockRegister                                     /* Pin Control Register fields [15:0] are not locked */
  };
  PORT_SetPinConfig(PORTB, PIN0_IDX, &portb0_pin53_config);  /* PORTB0 (pin 53) is configured as RMII0_MDIO */
  PORT_SetPinMux(PORTB, PIN1_IDX, kPORT_MuxAlt4);            /* PORTB1 (pin 54) is configured as RMII0_MDC */

  PORT_SetPinMux(PORTC, PIN16_IDX, kPORT_MuxAlt4);           /* PORTC16 (pin 90) is configured as ENET0_1588_TMR0 */
  PORT_SetPinMux(PORTC, PIN17_IDX, kPORT_MuxAlt4);           /* PORTC17 (pin 91) is configured as ENET0_1588_TMR1 */
  PORT_SetPinMux(PORTC, PIN18_IDX, kPORT_MuxAlt4);           /* PORTC18 (pin 92) is configured as ENET0_1588_TMR2 */

#if (BOARD_TYPE < 2)
	/*PTD0-3 for SPI0*/
	PORT_SetPinMux(PORTD, 0u, kPORT_MuxAlt2);  // CS0
	PORT_SetPinMux(PORTD, 1u, kPORT_MuxAlt2);
	PORT_SetPinMux(PORTD, 2u, kPORT_MuxAlt2);
	PORT_SetPinMux(PORTD, 3u, kPORT_MuxAlt2);
#endif

#if (BOARD_TYPE == 2)

	  /* Initialize FP LEDS */

	  gpio_pin_config_t opin = {kGPIO_DigitalOutput, 1};
	  gpio_pin_config_t ipin = {kGPIO_DigitalInput, 1};

	  PORT_SetPinMux(LEDPORT,LED0, kPORT_MuxAsGpio);
	  PORT_SetPinMux(LEDPORT,LED1, kPORT_MuxAsGpio);
	  PORT_SetPinMux(LEDPORT,LED2, kPORT_MuxAsGpio);
	  PORT_SetPinMux(LEDPORT,LED3, kPORT_MuxAsGpio);
	  PORT_SetPinMux(LEDPORT,LED4, kPORT_MuxAsGpio);
	  PORT_SetPinMux(LEDPORT,LED5, kPORT_MuxAsGpio);   // GREEN LED on FRDM board
	  PORT_SetPinMux(LEDPORT,LED6, kPORT_MuxAsGpio);
	  PORT_SetPinMux(LEDPORT,LED7, kPORT_MuxAsGpio);

	  GPIO_PinInit(LEDGPIO, LED0, &opin);
	  GPIO_PinInit(LEDGPIO, LED1, &opin);
	  GPIO_PinInit(LEDGPIO, LED2, &opin);
	  GPIO_PinInit(LEDGPIO, LED3, &opin);
	  GPIO_PinInit(LEDGPIO, LED4, &opin);
	  GPIO_PinInit(LEDGPIO, LED5, &opin);
	  GPIO_PinInit(LEDGPIO, LED6, &opin);
	  GPIO_PinInit(LEDGPIO, LED7, &opin);

	  /* Initialize Heartbeat LEDs */
	  PORT_SetPinMux(HBPORT,HB0, kPORT_MuxAsGpio);
	  PORT_SetPinMux(HBPORT,HB1, kPORT_MuxAsGpio);
	  GPIO_PinInit(HBGPIO, HB0, &opin);
	  opin.outputLogic = 0;
	  GPIO_PinInit(HBGPIO, HB1, &opin);
	  opin.outputLogic = 1;


	    /* Initialize AD Mux GPIOs */
	    PORT_SetPinMux(ADPORT,AD0, kPORT_MuxAsGpio);
	    PORT_SetPinMux(ADPORT,AD1, kPORT_MuxAsGpio);
	    PORT_SetPinMux(ADPORT,AD2, kPORT_MuxAsGpio);
	    PORT_SetPinMux(ADPORT,AD3, kPORT_MuxAsGpio);
	    GPIO_PinInit(ADGPIO, AD0, &opin);
	    GPIO_PinInit(ADGPIO, AD1, &opin);
	    GPIO_PinInit(ADGPIO, AD2, &opin);
	    GPIO_PinInit(ADGPIO, AD3, &opin);

	    /* I2C EEPROM - I2C1 */
	    PORT_SetPinMux(PORTC, 10U, kPORT_MuxAlt2);
	    PORT_SetPinMux(PORTC, 11U, kPORT_MuxAlt2);

	    /* I2C MEZZANINE CARDS - I2C0 */
	    PORT_SetPinMux(PORTD, 8U, kPORT_MuxAlt2);
	    PORT_SetPinMux(PORTD, 9U, kPORT_MuxAlt2);

	    /* Mezz Present Pins */
	    PORT_SetPinMux(PORTA, 24U, kPORT_MuxAsGpio);  // MEZZ0_0
	    PORT_SetPinMux(PORTB, 4U, kPORT_MuxAsGpio);   // MEZZ1_0
	    PORT_SetPinMux(PORTB, 18U, kPORT_MuxAsGpio);  // MEZZ2_0
	    PORT_SetPinMux(PORTD, 15U, kPORT_MuxAsGpio);  // MEZZ3_0
	    GPIO_PinInit(GPIOA, 24U, &ipin);
	    GPIO_PinInit(GPIOB, 4U, &ipin);
	    GPIO_PinInit(GPIOB, 18U, &ipin);
	    GPIO_PinInit(GPIOD, 15U, &ipin);
	    opin.outputLogic = 0;
		PORT_SetPinMux(PORTD, 11U, kPORT_MuxAsGpio);   // MEZZ3_4 - PMT temperature controller switch
		GPIO_PinInit(GPIOD, 11U, &opin);
		opin.outputLogic = 1;

	    PORT_SetPinMux(PORTD, 10U, kPORT_MuxAsGpio);  // MEZZ3_6 - RESET ON MROC
	    GPIO_PinInit(GPIOD, 10U, &opin);
		opin.outputLogic = 0;

	    PORT_SetPinMux(PORTD, 7U, kPORT_MuxAsGpio);  // MEZZ3_6 - BUSY ON MROC
	   	GPIO_PinInit(GPIOD, 7U, &opin);
		opin.outputLogic = 1;


	  /* SETUP UART5 for AUX RS232 */
	  PORT_SetPinMux(PORTE, 8U, kPORT_MuxAlt3);
	  PORT_SetPinMux(PORTE, 9U, kPORT_MuxAlt3);

	  /* MEZZ UART 3 */
	  PORT_SetPinMux(PORTC, 16U, kPORT_MuxAlt3);
	  PORT_SetPinMux(PORTC, 17U, kPORT_MuxAlt3);

	  /* MEZZ UART 2 */
	  PORT_SetPinMux(PORTD, 2U, kPORT_MuxAlt3);
	  PORT_SetPinMux(PORTD, 3U, kPORT_MuxAlt3);

	  /* MEZZ UART 1 */
	  PORT_SetPinMux(PORTC, 3U, kPORT_MuxAlt3);
	  PORT_SetPinMux(PORTC, 4U, kPORT_MuxAlt3);

	  /* MEZZ UART 0 */
	  PORT_SetPinMux(PORTB, 16U, kPORT_MuxAlt3);
	  PORT_SetPinMux(PORTB, 17U, kPORT_MuxAlt3);

	  const port_pin_config_t portd1_pin128_config = {
	      kPORT_PullUp,                                            /* Internal pull-up resistor is enabled */
	      kPORT_FastSlewRate,                                      /* Fast slew rate is configured */
	      kPORT_PassiveFilterDisable,                              /* Passive filter is disabled */
	      kPORT_OpenDrainEnable,                                   /* Open drain is enabled */
	      kPORT_LowDriveStrength,                                  /* Low drive strength is configured */
		  kPORT_MuxAsGpio,                                           /* Pin is configured as GPIO */
	      kPORT_UnlockRegister                                     /* Pin Control Register fields [15:0] are not locked */
	  };
	  PORT_SetPinConfig(PORTD, PIN1_IDX, &portd1_pin128_config);  /* PORTD1 (pin 128) is configured MEZZ3GP7 = INTERLOCK_OVER_RIDE */
	  opin.outputLogic = 0;                                       /* INTERLOCK OVERRIDE DISABLED BY DEFAULT */
	  GPIO_PinInit(GPIOD, PIN1_IDX, &opin);                      /* PORTD pin 1, INTERLOCK OVERRIDE */



#endif


  SIM->SOPT5 = ((SIM->SOPT5 &
    (~(SIM_SOPT5_UART0TXSRC_MASK)))                          /* Mask bits to zero which are setting */
      | SIM_SOPT5_UART0TXSRC(SOPT5_UART0TXSRC_UART_TX)       /* UART 0 transmit data source select: UART0_TX pin */
    );



}

/*******************************************************************************
 * EOF
 ******************************************************************************/
