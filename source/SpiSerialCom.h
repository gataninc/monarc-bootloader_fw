
#ifndef __SPISERIALCOM_H_
#define __SPISERIALCOM_H_

#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>

#include "fsl_uart_freertos.h"
#include "fsl_dspi.h"
#include "utils.h"
#include "SerialComClient.h"
#include "fsl_dspi_freertos.h"


#define RTOS_SPI_COMPLETE     (0x01)
#define RTOS_SPI_ERROR        (0x02)



//#define SPI_FRM_DATA_SIZE       ( FRAME_PAYLOAD_SIZE )
#define FRM_BYTE_CNT_SIZE   (1)     // just the payload size not including the cs
#define FRM_CS_SIZE         (1)     // check sum at the end of the frame

//#define SLAVE_TRANSFER_SIZE (SPI_FRM_DATA_SIZE + FRM_BYTE_CNT_SIZE + FRM_CS_SIZE)        /*! Slave Transfer dataSize */
//#define SLAVE_TRANSFER_SIZE (32)        /*! Slave Transfer dataSize */
#define SLAVE_TRANSFER_SIZE (16)        /*! Slave Transfer dataSize */
#define SPI_FRM_DATA_SIZE       ( SLAVE_TRANSFER_SIZE - FRM_BYTE_CNT_SIZE - FRM_CS_SIZE ) // hold 1 serial frame

//#define FRM_BYTE_CNT_OFFSET (0)
#define FRM_CS_OFFSET       (FRM_BYTE_CNT_SIZE+SPI_FRM_DATA_SIZE)


#define RESOURCE_QUEUE_FRM_CNT             (16)
#define TX_QUEUE_FRM_CNT                   (RESOURCE_QUEUE_FRM_CNT)
#define RX_QUEUE_FRM_CNT                   (RESOURCE_QUEUE_FRM_CNT)

typedef union spi_frame_t {
	struct __attribute__((packed))  field_t {
		uint8_t  cnt;                            // cnt of the data following, not crc
		uint8_t  data[SPI_FRM_DATA_SIZE];
		uint8_t  cs;
	} field;
	uint8_t bytes[SLAVE_TRANSFER_SIZE];
} spi_frame_t;

typedef struct dspi_slave_t {
    dspi_slave_handle_t handle;
    dspi_transfer_t     Xfer;
} dspi_slave_t;

typedef struct dspi_master_t {
	dspi_master_handle_t handle;
} dspi_master_t;

typedef struct spi_shandle_t {
    QueueHandle_t       txFreeFrameQueue; // transmit free frame queue
    QueueHandle_t       rxFreeFrameQueue; // receive free frame queue
    QueueHandle_t       rxFrameQueue;     // receive queue
    QueueHandle_t       txFrameQueue;     // transmit queue
    spi_frame_t         TxFrameArray[TX_QUEUE_FRM_CNT];
    spi_frame_t         RxFrameArray[RX_QUEUE_FRM_CNT];
    EventGroupHandle_t  Event;

    // spi members
    union {
        dspi_slave_t       slave;
        dspi_master_t      master;
        //dspi_rtos_handle_t rtosMaster;
    };
    SPI_Type           *dspi_spi;
} spi_shandle_t;

typedef struct spi_link_context_t {
	spi_shandle_t   spi;
	SerialLink_t    link;
	TaskHandle_t    xTxRxHandle;
	TaskHandle_t    xLinkTxHandle;
	TaskHandle_t    xLinkRxHandle;
} spi_link_context_t;

#define SPI_ERROR_OFFSET          (1000)

enum SPI_SERIAL_COM_RESULT {
    SSER_SUCCESS                    = 0 ,// returned by send and receive function
	SSER_ERROR_START                = SER_ERROR_MAX + SPI_ERROR_OFFSET,
	SSER_CANNOT_CREATE_SLAVE_RXTX_TASK,
	SSER_CANNOT_CREATE_LINK_TX_TASK,
	SSER_CANNOT_CREATE_LINK_RX_TASK,
};

int Link_InitSlaveSpi(spi_link_context_t *pSpiCtxt, SPI_Type *dspi);
int Link_InitMasterSpi(spi_link_context_t *pSpiCtxt, SPI_Type *dspi);
void Link_DeinitSpi(spi_link_context_t *pSpiCtxt);

#endif // __PACSPISERIALCOM_H_
