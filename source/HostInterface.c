/*
 * HostInterface.c
 *
 *  Created on: Nov 27, 2018
 *      Author: stelasang
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <integer.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "lwip/opt.h"
#include "lwip/tcpip.h"
#include "lwip/dhcp.h"
#include "lwip/prot/dhcp.h"
#include "lwip/sockets.h"
#include "netif/ethernet.h"
#include "ethernetif.h"

#include "lwip/sys.h"
#include "lwip/api.h"

#include "HostInterface.h"
#include "utils.h"
#include "Uart.h"
#include "fsl_gpio.h"
#include "mios.h"
#include "lib_init.h"
#include "SerialComClient.h"
#include "SerialComTarget.h"
#include "Uart.h"
#include "dbglog.h"

#define BUSY 1

/* IP address configuration. */
#define configIP_ADDR0 192
#define configIP_ADDR1 168
#define configIP_ADDR2 111
#define configIP_ADDR3 86

//#define configIP_ADDR0 169
//#define configIP_ADDR1 254
//#define configIP_ADDR2 143
//#define configIP_ADDR3 86

/* Netmask configuration. */
#define configNET_MASK0 255
#define configNET_MASK1 255
#define configNET_MASK2 255
#define configNET_MASK3 0

/* Gateway address configuration. */
#define configGW_ADDR0 192
#define configGW_ADDR1 168
#define configGW_ADDR2 111
#define configGW_ADDR3 1

#define configPHY_ADDRESS 1
#define ENETPORT 7700
#define ENET_CMD_QUEUE_PORT  (ENETPORT)
#define ENET_DATA_PORT  ((ENETPORT) + 1)
#define ENET_RESET_PORT  ((ENETPORT) + 2)
#define ENET_ROC_FW_PRG_PORT ((ENETPORT) + 3)
#define ENET_PAC_FW_PRG_PORT ((ENETPORT) + 4)
#define ENET_FPB_FW_PRG_PORT ((ENETPORT) + 5)

#define DBG_FIELD_OFFSET(POS)    ((POS) * 4)
#define DBG_FIELD_INC(POS)    (1 << DBG_FIELD_OFFSET(POS))
#define DBG_LOC_MAP(POS) (0x0F << DBG_FIELD_OFFSET(POS))
#define DBG_LOC_MASK(POS) (~DBG_LOC_MAP(POS))

#define DBG_LOC(SAVE, POS) ( ( (SAVE) & DBG_LOC_MASK(POS) )\
		| ( ((SAVE) + DBG_FIELD_INC(POS)) & DBG_LOC_MAP(POS) ) )

struct netif fsl_netif0;
ip_addr_t fsl_netif0_ipaddr, fsl_netif0_netmask, fsl_netif0_gw;
//struct netconn *conn = NULL;
//struct netconn *newconn = NULL;

typedef struct henet_ctxt_t {
	struct netconn         *conn;
	struct netconn         *newconn;
	iostream_t             *iestream;  // input ethernet stream
	iostream_t             *oestream;  // output ethernet stream
	SemaphoreHandle_t      xMutexEnet;
} henet_ctxt_t;

typedef struct enet_bridge_ctxt_t {
	struct netconn         *conn;
	struct netconn         *newconn;
	Connection_t           conSerialClient;
	Connection_t           *pClient;
	TaskHandle_t           enetTask;
	TaskHandle_t           serialTask;
	SemaphoreHandle_t      xMutexEnet;
	SemaphoreHandle_t      xMutexSerial;
	enum serial_connection_state_t {SerCon_Iddle, SerCon_Connecting, SerCon_Connected} serial_con_state;
} enet_bridge_ctxt_t;

/**
 * Helper struct to hold private data used to operate your ethernet interface.
 */
struct ethernetif
{
    ENET_Type *base;
#if (defined(FSL_FEATURE_SOC_ENET_COUNT) && (FSL_FEATURE_SOC_ENET_COUNT > 0)) || \
    (USE_RTOS && defined(FSL_RTOS_FREE_RTOS))
    enet_handle_t handle;
#endif
    uint32_t phyAddr;
#if USE_RTOS && defined(FSL_RTOS_FREE_RTOS)
//    EventGroupHandle_t enetTransmitAccessEvent;
//    EventBits_t txFlag;
#endif
    uint8_t RxBuffDescrip[ENET_RXBD_NUM * sizeof(enet_rx_bd_struct_t) + ENET_BUFF_ALIGNMENT];
    uint8_t TxBuffDescrip[ENET_TXBD_NUM * sizeof(enet_tx_bd_struct_t) + ENET_BUFF_ALIGNMENT];
//    uint8_t RxDataBuff[ENET_RXBD_NUM * ENET_ALIGN(ENET_RXBUFF_SIZE) + ENET_BUFF_ALIGNMENT];
//    uint8_t TxDataBuff[ENET_TXBD_NUM * ENET_ALIGN(ENET_TXBUFF_SIZE) + ENET_BUFF_ALIGNMENT];
#if defined(FSL_FEATURE_SOC_LPC_ENET_COUNT) && (FSL_FEATURE_SOC_LPC_ENET_COUNT > 0)
    uint8_t txIdx;
#if !(USE_RTOS && defined(FSL_RTOS_FREE_RTOS))
    uint8_t rxIdx;
#endif
#endif
};

henet_ctxt_t fpb_ctxt;
enet_bridge_ctxt_t roc_ctxt;
enet_bridge_ctxt_t pac_ctxt;



IO_StreamState_t enet_fw_prg_tx_cb(void *p, uint8_t* buf, int size);
void enet_fw_prg_rx_channel(void *pvParameters);
void roc_bridge_enet_rx_channel(void *pvParameters);
void roc_bridge_serial_rx_channel(void *pvParameters);
void pac_bridge_enet_rx_channel(void *pvParameters);
void pac_bridge_serial_rx_channel(void *pvParameters);

void send_roc_message(const char *msg);
void send_pac_message(const char *msg);

void init_bridge_context(enet_bridge_ctxt_t *ctxt)
{
	ctxt->serial_con_state = SerCon_Iddle;
	ctxt->pClient = NULL;
	ctxt->xMutexEnet = xSemaphoreCreateMutex();
	configASSERT(ctxt->xMutexEnet);
	ctxt->xMutexSerial = xSemaphoreCreateMutex();
	configASSERT(ctxt->xMutexSerial);
	configASSERT( EP_InitConnection(&ctxt->conSerialClient) == SER_SUCCESS );
}

void init_enet_context(henet_ctxt_t *ctxt)
{
	ctxt->xMutexEnet = xSemaphoreCreateMutex();
	configASSERT(ctxt->xMutexEnet);
}

void fpb_enet_init(iostream_t *instream, iostream_t *outstream)
{

    IP4_ADDR(&fsl_netif0_ipaddr, configIP_ADDR0, configIP_ADDR1, configIP_ADDR2, configIP_ADDR3);
    IP4_ADDR(&fsl_netif0_netmask, configNET_MASK0, configNET_MASK1, configNET_MASK2, configNET_MASK3);
    IP4_ADDR(&fsl_netif0_gw, configGW_ADDR0, configGW_ADDR1, configGW_ADDR2, configGW_ADDR3);

    tcpip_init(NULL, NULL);

    netif_add(&fsl_netif0, &fsl_netif0_ipaddr, &fsl_netif0_netmask, &fsl_netif0_gw, NULL, ethernetif_init, tcpip_input);
    netif_set_default(&fsl_netif0);
    netif_set_up(&fsl_netif0);

    fpb_ctxt.iestream = instream;
    fpb_ctxt.oestream = outstream;

    init_enet_context(&fpb_ctxt);
    os_set_tx_cb(fpb_ctxt.oestream, enet_fw_prg_tx_cb, (void *)(&fpb_ctxt));

    xTaskCreate(enet_fw_prg_rx_channel, "enet_fw_prg_rx",TCP_STACKSIZE, (&fpb_ctxt), PRIORITY_MAX, NULL);

    init_bridge_context(&roc_ctxt);
    xTaskCreate(roc_bridge_enet_rx_channel, "roc_bridge_enet_rx_channel",TCP_STACKSIZE, (&roc_ctxt), PRIORITY_MAX, &roc_ctxt.enetTask);
    xTaskCreate(roc_bridge_serial_rx_channel, "roc_bridge_serial_rx",2*configMINIMAL_STACK_SIZE, (&roc_ctxt), PRIORITY_HIGH, &roc_ctxt.serialTask);

    init_bridge_context(&pac_ctxt);
    xTaskCreate(pac_bridge_enet_rx_channel, "pac_bridge_enet_rx_channel",TCP_STACKSIZE, (&pac_ctxt), PRIORITY_MAX, &roc_ctxt.enetTask);
    xTaskCreate(pac_bridge_serial_rx_channel, "pac_bridge_serial_rx",2*configMINIMAL_STACK_SIZE, (&pac_ctxt), PRIORITY_HIGH, &roc_ctxt.serialTask);
}
void fpb_enet_deinit(iostream_t *instream, iostream_t *outstream)
{
	struct ethernetif *ethernetif = fsl_netif0.state;
	ENET_Deinit(ethernetif->base);
}

void send_fpb_message(const char *msg)
{
	uint32_t sent;
	is_put(fpb_ctxt.iestream, (uint8_t*)msg, strlen(msg), &sent);
}



void send_msg_to_all(const char *msg)
{
	send_fpb_message(msg);
	send_roc_message(msg);
	send_pac_message(msg); //TODO: DEBUG add back when pack bridge is enabled
}




// this function does the transmit for oestream = output stream
IO_StreamState_t enet_fw_prg_tx_cb(void *p, uint8_t* buf, int size)
{
	err_t err = ERR_OK;
	int res = IOS_GOOD;
	henet_ctxt_t *henet = (henet_ctxt_t*)p;

	while(xSemaphoreTake(henet->xMutexEnet, portMAX_DELAY) != pdTRUE) {;}
	//struct netconn *curcon = henet->newconn;

	if (henet->newconn == NULL){
		res = IOS_ERROR;
	}
	else {
	    err = netconn_write(henet->newconn, buf, (size_t)size, NETCONN_COPY);
	    if (err != ERR_OK){
		    netconn_close(henet->newconn);
		    res = IOS_ERROR;
	    }
	}
	xSemaphoreGive(henet->xMutexEnet);// Release the mutex
	return res;
}

int enet_new_server_connection(henet_ctxt_t *henet, int port)
{
	int err = ERR_OK;
	while(xSemaphoreTake(henet->xMutexEnet, portMAX_DELAY) != pdTRUE) {;}
	do {
		if (henet->conn){
			netconn_close(henet->conn);
			netconn_delete(henet->conn);
			henet->conn = NULL;
		}
		henet->conn = netconn_new(NETCONN_TCP);
		if (henet->conn == NULL){
			os_print(&cout, "Error New Connection on %d, is NULL\r\n", port);
			vTaskDelay(100);
			while(1);
			//continue;
		}
		//os_print(&cout, "New Connection on %d\r\n", ENET_ROC_FW_PRG_PORT);
		err = netconn_bind(henet->conn, NULL, port);
		if (err != ERR_OK){
			os_print(&cout, "Error Bind on %d, failed\r\n", port);
			break;
		}

		err = netconn_listen(henet->conn);
		if (err != ERR_OK){
			os_print(&cout, "Error Listening on %d, failed\r\n", port);
			break;
		}

	} while(0);
	if ( (err != ERR_OK ) && (henet->conn != NULL)){
		netconn_close(henet->conn);
		netconn_delete(henet->conn);
		henet->conn = NULL;
	}
	xSemaphoreGive(henet->xMutexEnet);// Release the mutex
	return err;
}
// this function does the receive for iestream => input stream
void enet_fw_prg_rx_channel(void *pvParameters)
{
    s16_t len;
	err_t err, accept_err, recv_err;
	void *data;
	struct netbuf *buf;
	uint32_t sent;
	enum connection_state_t {Con_Iddle, Con_Listening, Con_Accpeted} con_state;

	henet_ctxt_t *henet = (henet_ctxt_t*)pvParameters;
	con_state = Con_Iddle;
	os_print(&cout, "Setup IF on %d\r\n", ENET_FPB_FW_PRG_PORT);
	netif_set_up(&fsl_netif0);

	while(1)
	{
		switch(con_state)
		{
		case Con_Iddle:
			err = enet_new_server_connection(henet, ENET_FPB_FW_PRG_PORT);
			if (err != ERR_OK){
				continue;
			}
			os_print(&cout, "Listening on %d\r\n", ENET_FPB_FW_PRG_PORT);
			con_state = Con_Listening;
			// fall through
		case Con_Listening:
			accept_err = netconn_accept(henet->conn,&henet->newconn);
			if (accept_err != ERR_OK)
			{
				os_print(&cout, "Error accepting on %d, failed\r\n", ENET_FPB_FW_PRG_PORT);
				//netconn_close(henet->newconn);
				//netconn_delete(henet->newconn);
				continue;
			}
			os_print(&cout, "Connection Accepted on %d\r\n", ENET_FPB_FW_PRG_PORT);
			con_state = Con_Accpeted;
			// fall through
		case Con_Accpeted:
			send_msg_to_all("connect\n");// prevent fpb, roc, pac bootloader from starting the app
			while( ((recv_err = netconn_recv(henet->newconn, &buf)) == ERR_OK )
							   && (buf != NULL) )
			{
				do
				{
				   netbuf_data(buf, &data, (u16_t*)&len);
				   if (len > 0){
					   is_put(henet->iestream, data, len, &sent);
				   }
				} while(netbuf_next(buf) > 0);
			    netbuf_delete(buf);
			}
			while(xSemaphoreTake(henet->xMutexEnet, portMAX_DELAY) != pdTRUE){;}
			netconn_close(henet->newconn);
			netconn_delete(henet->newconn);
			henet->newconn = NULL;
			xSemaphoreGive(henet->xMutexEnet);// Release the mutex
			con_state = Con_Listening;
			break;
		}
	} // while(1)
	vTaskDelete(NULL); // delete itself
}






//----------------------------------------------------------------------------------------------------------
//      ROC BRIDGE
//----------------------------------------------------------------------------------------------------------

void bridge_close_client(enet_bridge_ctxt_t *hbridge)
{
	while(xSemaphoreTake(hbridge->xMutexSerial, portMAX_DELAY) != pdTRUE){;}
	if (hbridge->pClient != NULL){
		hbridge->pClient = NULL;
	    EP_CloseConnection(&hbridge->conSerialClient);
	    dl_print("[bridge_close_client]: (%d,%d,%d)",
	    		hbridge->conSerialClient.ep.remoteDev,
				hbridge->conSerialClient.ep.remotePort,
				hbridge->conSerialClient.ep.remoteSlot
				);
	}
	xSemaphoreGive(hbridge->xMutexSerial);// Release the mutex
}

int bridge_send_client(enet_bridge_ctxt_t *hbridge, uint8_t * buf, uint32_t size, uint32_t *pcnt)
{
	int res = SER_SUCCESS;
	if (hbridge->pClient != NULL){
	    res = EP_SendBuffer(&hbridge->conSerialClient, buf, size, pcnt);
        if (res != SER_SUCCESS){
		    bridge_close_client(hbridge);
		}
	}
	else {
		return -1;
	}
	return res;
}

const char * bridge_get_serial_state_str(enet_bridge_ctxt_t *hbridge)
{
	switch(hbridge->serial_con_state){
	case SerCon_Iddle:      return "SerCon_Iddle";
	case SerCon_Connecting: return "SerCon_Connecting";
	case SerCon_Connected:  return "SerCon_Connected";
	default:                return "Unknown";
	}
}

void send_roc_message(const char *msg)
{
	uint32_t sent;

	bridge_send_client(&roc_ctxt, (uint8_t*)msg, strlen(msg),&sent);
}

void send_pac_message(const char *msg)
{
	uint32_t sent;

	bridge_send_client(&pac_ctxt, (uint8_t*)msg, strlen(msg),&sent);
}

int bridge_new_server_connection(enet_bridge_ctxt_t *hbridge, int port)
{
	int err = ERR_OK;
	while(xSemaphoreTake(hbridge->xMutexEnet, portMAX_DELAY) != pdTRUE) {;}
	do {
		if (hbridge->conn){
			netconn_close(hbridge->conn);
			netconn_delete(hbridge->conn);
			hbridge->conn = NULL;
		}
		hbridge->conn = netconn_new(NETCONN_TCP);
		if (hbridge->conn == NULL){
			os_print(&cout, "Error New Connection on %d, is NULL\r\n", port);
			vTaskDelay(100);
			while(1);
			//continue;
		}
		//os_print(&cout, "New Connection on %d\r\n", ENET_ROC_FW_PRG_PORT);
		err = netconn_bind(hbridge->conn, NULL, port);
		if (err != ERR_OK){
			os_print(&cout, "Error Bind on %d, failed\r\n", port);
			break;
		}

		err = netconn_listen(hbridge->conn);
		if (err != ERR_OK){
			os_print(&cout, "Error Listening on %d, failed\r\n", port);
			break;
		}

	} while(0);
	if ( (err != ERR_OK ) && (hbridge->conn != NULL)){
		netconn_close(hbridge->conn);
		netconn_delete(hbridge->conn);
		hbridge->conn = NULL;
	}
	xSemaphoreGive(hbridge->xMutexEnet);// Release the mutex
	return err;
}



// this function does the receive for iestream => input stream
void roc_bridge_enet_rx_channel(void *pvParameters)
{
    s16_t len;
	err_t err, accept_err, recv_err;
	void *data;
	struct netbuf *buf;
	uint32_t sent;
	int res;

	enum connection_state_t {Con_Iddle, Con_Listening, Con_Accpeted} con_state;

	enet_bridge_ctxt_t *hbridge = (enet_bridge_ctxt_t*)pvParameters; // serial , ethernet bridge context
	con_state = Con_Iddle;
	os_print(&cout, "Setup IF on %d\r\n", ENET_FPB_FW_PRG_PORT);
	netif_set_up(&fsl_netif0);

	while(1)
	{
		switch(con_state)
		{
		case Con_Iddle:
			err = bridge_new_server_connection(hbridge, ENET_ROC_FW_PRG_PORT);
			if (err){
				continue;
			}
			con_state = Con_Listening;
			// fall through
		case Con_Listening:
			accept_err = netconn_accept(hbridge->conn, &hbridge->newconn);
			if (accept_err != ERR_OK)
			{
				os_print(&cout, "Error accepting on %d, failed\r\n", ENET_ROC_FW_PRG_PORT);
				continue;
			}
			con_state = Con_Accpeted;
			// fall through
		case Con_Accpeted:
			os_print(&cout, "Connection Accepted on %d\r\n", ENET_ROC_FW_PRG_PORT);
			// tell fpb bootloader to connect since
			//todo reset the target
			send_msg_to_all("connect\n");// prevent fpb, roc, pac bootloader from starting the app
			while( ((recv_err = netconn_recv(hbridge->newconn, &buf)) == ERR_OK )
							   && (buf != NULL) )
			{
				do
				{
				    netbuf_data(buf, &data, (u16_t*)&len);
				    if (len > 0){
					    res = bridge_send_client(hbridge, data, len, &sent);
			            if (res != SER_SUCCESS){
						    dl_print("[roc_bridge_enet_rx]: EP_SendBuffer failed %d serial_rx: %s\r\n", res, bridge_get_serial_state_str(hbridge));
						    vTaskDelay(100); //give time for connection to reestablish
						}
				    }
				} while(netbuf_next(buf) > 0);

				netbuf_delete(buf);
			}
			while(xSemaphoreTake(hbridge->xMutexEnet, portMAX_DELAY) != pdTRUE) {;}
			netconn_close(hbridge->newconn);
			netconn_delete(hbridge->newconn);
			hbridge->newconn = NULL;
			xSemaphoreGive(hbridge->xMutexEnet);// Release the mutex
			con_state = Con_Listening;
			break;
		}
	} // while(1)
	vTaskDelete(NULL); // delete itself
}

// this function does the receive for iestream => input stream
void roc_bridge_serial_rx_channel(void *pvParameters)
{
    uint8_t *prcv;
    uint32_t rcvSize;
    bool clientConOpened;

	int res;

	enet_bridge_ctxt_t *hbridge = (enet_bridge_ctxt_t*)pvParameters;
	hbridge->serial_con_state = SerCon_Iddle;
	os_print(&cout, "Setup IF on %d\r\n");


	while(1)
	{
		switch(hbridge->serial_con_state)
		{
		case SerCon_Iddle:
		case SerCon_Connecting:
			hbridge->serial_con_state = SerCon_Connecting;
			clientConOpened = false;
			cprintf("\r\n[roc_bridge_serial_rx]: opening client connection\r\n");
			do
			{
				// take serial mutex
				while(xSemaphoreTake(hbridge->xMutexSerial, portMAX_DELAY) != pdTRUE) {;}
				res = EP_OpenClientConnection(
						                        &hbridge->conSerialClient,
												CONF_FPB_HOST_ROC_CMD_PORT, // the port on the host, this board for talking to roc
												CONF_NET_ROC_ADDR, // the net address of roc board
												CONF_SERVER_FW_UPDATE_PORT);// the port number where cmd procssor listens
				if (res == SER_SUCCESS) {
					hbridge->pClient = &hbridge->conSerialClient;
					clientConOpened = true;
					dl_print("[roc_bridge_serial_rx]: client connected\r\n");
				}
				else {
					vTaskDelay(5000); // give time for the connection to recover
				}
				// release serial mutex
				xSemaphoreGive(hbridge->xMutexSerial);// Release the mutex
			} while(!clientConOpened);
			hbridge->serial_con_state = SerCon_Connected;
			// fall through
		case SerCon_Connected:
			// take data from ROC and send it over ethernet connection
			while( (res = EP_GetBuffer(&(hbridge->conSerialClient), &prcv, &rcvSize)) == SER_SUCCESS )
			{
				// take ethernet port mutex
				while(xSemaphoreTake(hbridge->xMutexEnet, portMAX_DELAY) != pdTRUE) {;}
				if (hbridge->newconn != NULL){
				    res = netconn_write(hbridge->newconn, prcv, (size_t)rcvSize, NETCONN_COPY);
				    if (res != ERR_OK){
					    netconn_close(hbridge->newconn);
					    vTaskDelay(10); // give time for the connection to recover
				    }
				}
				// release ethernet port mutex
				xSemaphoreGive(hbridge->xMutexEnet);// Release the mutex

				// release the serial protocol buffer
			    EP_ReleaseBuffer(prcv);
			}
			hbridge->serial_con_state = SerCon_Connecting;
			dl_print("[roc_bridge_serial_rx]: closing client connection");
			bridge_close_client(hbridge);

			//break;
		}
	} // end while(1)
	vTaskDelete(NULL); // delete itself
}

//----------------------------------------------------------------------------------------------------------
//      PAC BRIDGE
//----------------------------------------------------------------------------------------------------------

// this function does the receive for iestream => input stream
void pac_bridge_enet_rx_channel(void *pvParameters)
{
    s16_t len;
	err_t err, accept_err, recv_err;
	void *data;
	struct netbuf *buf;
	uint32_t sent;
	int res;
	enum connection_state_t {Con_Iddle, Con_Listening, Con_Accpeted} con_state;

	enet_bridge_ctxt_t *hbridge = (enet_bridge_ctxt_t*)pvParameters; // serial , ethernet bridge context
	con_state = Con_Iddle;
	os_print(&cout, "Setup IF on %d\r\n", ENET_PAC_FW_PRG_PORT);
	netif_set_up(&fsl_netif0);

	while(1)
	{
		switch(con_state)
		{
		case Con_Iddle:
			err = bridge_new_server_connection(hbridge, ENET_PAC_FW_PRG_PORT);
			if (err){
				continue;
			}
			con_state = Con_Listening;
			// fall through
		case Con_Listening:
			accept_err = netconn_accept(hbridge->conn, &hbridge->newconn);
			if (accept_err != ERR_OK)
			{
				os_print(&cout, "Error accepting on %d, failed\r\n", ENET_PAC_FW_PRG_PORT);
				continue;
			}
			con_state = Con_Accpeted;
			// fall through
		case Con_Accpeted:
			os_print(&cout, "Connection Accepted on %d\r\n", ENET_PAC_FW_PRG_PORT);
			send_msg_to_all("connect\n");// prevent fpb, roc, pac bootloader from starting the app;

			while( ((recv_err = netconn_recv(hbridge->newconn, &buf)) == ERR_OK )
							   && (buf != NULL) )
			{
				do
				{
				    netbuf_data(buf, &data, (u16_t*)&len);
				    if (len > 0){
				    	res = bridge_send_client(hbridge, data, len, &sent);
			            if (res != SER_SUCCESS){
						    dl_print("[pac_bridge_enet_rx]: EP_SendBuffer failed %d serial_rx: %s\r\n", res, bridge_get_serial_state_str(hbridge));
						    vTaskDelay(100); //give time for connection to reestablish
						}
				    }
				} while(netbuf_next(buf) > 0);

				netbuf_delete(buf);
			}
			while(xSemaphoreTake(hbridge->xMutexEnet, portMAX_DELAY) != pdTRUE) {;}
			netconn_close(hbridge->newconn);
			netconn_delete(hbridge->newconn);
			hbridge->newconn = NULL;
			xSemaphoreGive(hbridge->xMutexEnet);// Release the mutex
			con_state = Con_Listening;
			break;
		}
	} // while(1)
	vTaskDelete(NULL); // delete itself
}

// this function does the receive for iestream => input stream
void pac_bridge_serial_rx_channel(void *pvParameters)
{
    uint8_t *prcv;
    uint32_t rcvSize;
	//enum ser_connection_state_t {SerCon_Iddle, SerCon_Connected} con_state;
	int res;
	bool clientConOpened;

	enet_bridge_ctxt_t *hbridge = (enet_bridge_ctxt_t*)pvParameters;
	hbridge->serial_con_state = SerCon_Iddle;
	os_print(&cout, "Setup IF on %d\r\n", ENET_FPB_FW_PRG_PORT);


	while(1)
	{
		switch(hbridge->serial_con_state)
		{
		case SerCon_Iddle:
		case SerCon_Connecting:
			hbridge->serial_con_state = SerCon_Connecting;
			dl_print("[pac_bridge_serial_rx]: opening client connection");
			do
			{
				clientConOpened = false;
				// take serial mutex
				while(xSemaphoreTake(hbridge->xMutexSerial, portMAX_DELAY) != pdTRUE) {;}
				res = EP_OpenClientConnection(
						                        &(hbridge->conSerialClient),
												CONF_FPB_HOST_PAC_CMD_PORT, // the port on the host, this board for talking to PAC
												CONF_NET_PAC_ADDR, // the net address of roc board
												CONF_SERVER_FW_UPDATE_PORT);// the port number where cmd procssor listens
				if (res == SER_SUCCESS) {
					hbridge->pClient = &hbridge->conSerialClient;
					clientConOpened = true;
					dl_print("[serial_pac_fw_prg_rx]: client connected");
				}
				else {
					vTaskDelay(5000); // give time for the connection to recover
				}
				xSemaphoreGive(hbridge->xMutexSerial);// Release the mutex
			} while(!clientConOpened);
			hbridge->serial_con_state = SerCon_Connected;
			// fall through
		case SerCon_Connected:
			// take data from ROC and send it over ethernet connection
			while( (res = EP_GetBuffer(&(hbridge->conSerialClient), &prcv, &rcvSize)) == SER_SUCCESS ){
				while(xSemaphoreTake(hbridge->xMutexEnet, portMAX_DELAY) != pdTRUE) {;}
				if (hbridge->newconn != NULL){
					res = netconn_write(hbridge->newconn, prcv, (size_t)rcvSize, NETCONN_COPY);
					if (res != ERR_OK){
						netconn_close(hbridge->newconn);
						vTaskDelay(100); // give time for the connection to recover
					}
				}
				xSemaphoreGive(hbridge->xMutexEnet);// Release the mutex
				    // release the serial protocol buffer
			    EP_ReleaseBuffer(prcv);
			}
			hbridge->serial_con_state = SerCon_Connecting;
			bridge_close_client(hbridge);
			//break;
		}
	} // end while(1)
	vTaskDelete(NULL); // delete itself
}

#if 0
void IpAddr()
{
	ip_addr_t addr;
    cprintf("\r\n************************************************\r\n");
    addr = fsl_netif0.ip_addr;
    cprintf(" IPv4 Address     : %u.%u.%u.%u\r\n",addr.addr&0xff,(addr.addr>>8)&0xff,(addr.addr>>16)&0xff,(addr.addr>>24)&0xff );
    addr = fsl_netif0.netmask;
    cprintf(" IPv4 Subnet mask : %u.%u.%u.%u\r\n",addr.addr&0xff,(addr.addr>>8)&0xff,(addr.addr>>16)&0xff,(addr.addr>>24)&0xff );
    addr = fsl_netif0.gw;
    cprintf(" IPv4 Gateway     : %u.%u.%u.%u\r\n",addr.addr&0xff,(addr.addr>>8)&0xff,(addr.addr>>16)&0xff,(addr.addr>>24)&0xff );
    cprintf(" Port             : %u\r\n", ENET_CMD_QUEUE_PORT);
    cprintf("************************************************\r\n");
    return;
}
#endif


