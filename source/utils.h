/*
 * utils.h
 *
 *  Created on: Nov 27, 2018
 *      Author: stelasang
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <stdint.h>
#include <ctype.h>
#include <stdbool.h>

//#define MIN(A,B) (((A) < (B))? (A): (B))
#define min(A,B)\
	({ __typeof__(A) _a=(A); __typeof__(B) _b=(B); _a < _b ? _a : _b;})
//#define MAX(A,B) (((A) > (B))? (A): (B))
#define max(A,B)\
	({ __typeof__(A) _a=(A); __typeof__(B) _b=(B); _a > _b ? _a : _b;})

// high priority means higher priority tasks, the max allowable is configMAX_PRIORITIES - 1 = 5 - 1 = 4
#define PRIORITY_MAX (configMAX_PRIORITIES - 1)
#define PRIORITY_HIGH (PRIORITY_MAX - 1)
#define PRIORITY_MID_HIGH (PRIORITY_MAX - 2)
#define PRIORITY_MID (PRIORITY_MAX - 3)
#define PRIORITY_LOW (0) // IDLE TASK

#define EXECHAR 'X'
#define CheckExec(p) ( *(p) == EXECHAR )

char *vgethex(char *p, int *v);
int gethdig(char c);
int ishexnum(char c);
char *gethex(char *p, int *v);
char *vgethex(char *p, int *v);
bool HexExec(char *p, int *v);
bool Hex1Exec(char *p, int *v, int* d);
char *ustrtok_r(char *s1, const char *s2, char **s3);

bool get_hex_byte(const char *bstr, uint8_t *dest);
int hex_2_num(const char *start, int len, uint32_t *dest);
char *str_tolower(char *str);

//#define NULL        (void*)0

// useful in calculating time differences
// returns positive number if the second time is later than the first,
// returns negative if the first time is later than the second
// returns 0 if times are equal
int32_t TimeDifference(uint32_t first, uint32_t second);
#define DIE   {while(1);}
#define HANG   {while(1);}
#endif /* UTILS_H_ */
