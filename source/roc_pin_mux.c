/*
 * Copyright (c) 2016, NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file    roc_pin_mux.c
 * @brief   Board pins file.
 */
 
/* This is a template for board specific configuration created by MCUXpresso IDE Project Wizard.*/

#include "fsl_common.h"
#include "fsl_port.h"
#include "roc_pin_mux.h"
#include "board.h"
#include "fsl_gpio.h"


/**
 * @brief Set up and initialize all required blocks and functions related to the board hardware.
 */
void BOARD_RocInitBootPins(void) {

	gpio_pin_config_t opin = {kGPIO_DigitalOutput, 0};
	gpio_pin_config_t ipin = {kGPIO_DigitalInput, 0};

	/* The user initialization should be placed here */
	CLOCK_EnableClock(kCLOCK_PortA);
	CLOCK_EnableClock(kCLOCK_PortB);
	CLOCK_EnableClock(kCLOCK_PortC);
	CLOCK_EnableClock(kCLOCK_PortD);
	CLOCK_EnableClock(kCLOCK_PortE);

	//* Front Panel Switches

	PORT_SetPinMux(PORTA, 9U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA, 10U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA, 11U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA, 12U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA, 13U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA, 14U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA, 15U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA, 16U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA, 17U, kPORT_MuxAsGpio);
	opin.outputLogic = 0;
	GPIO_PinInit(GPIOA, 9U, &opin);
	opin.outputLogic = 1;
	GPIO_PinInit(GPIOA, 10U, &opin);
	GPIO_PinInit(GPIOA, 11U, &ipin);
	GPIO_PinInit(GPIOA, 12U, &ipin);
	GPIO_PinInit(GPIOA, 13U, &ipin);
	GPIO_PinInit(GPIOA, 14U, &ipin);
	GPIO_PinInit(GPIOA, 15U, &ipin);
	GPIO_PinInit(GPIOA, 16U, &ipin);
	GPIO_PinInit(GPIOA, 17U, &ipin);

	//* Shutter control
	// BL IGNORE
	//PORT_SetPinMux(PORTA, SHUTTER_POL, kPORT_MuxAsGpio);
	//PORT_SetPinMux(PORTA, SHUTTER_EN, kPORT_MuxAsGpio);
	//opin.outputLogic = 0;
	//GPIO_PinInit(GPIOA, SHUTTER_POL, &opin);
	//opin.outputLogic = 1;
	//GPIO_PinInit(GPIOA, SHUTTER_EN, &opin);

	/* Setup UART4 on ROC board */
	PORT_SetPinMux(PORTC, 14U, kPORT_MuxAlt3);
	PORT_SetPinMux(PORTC, 15U, kPORT_MuxAlt3);

	/* Setup UART0 on ROC board */
	PORT_SetPinMux(PORTB, 16U, kPORT_MuxAlt3);
	PORT_SetPinMux(PORTB, 17U, kPORT_MuxAlt3);

	/* Setup UART1 on ROC board */
	PORT_SetPinMux(PORTC, 3U, kPORT_MuxAlt3); // UART1, RX
	PORT_SetPinMux(PORTC, 4U, kPORT_MuxAlt3); // UART1, TX

	/* Setup UART2 on ROC board */
	PORT_SetPinMux(PORTD, 2U, kPORT_MuxAlt3);
	PORT_SetPinMux(PORTD, 3U, kPORT_MuxAlt3);

	/* Setup UART3 on ROC board - From FPB */
	PORT_SetPinMux(PORTC, 16U, kPORT_MuxAlt3);
	PORT_SetPinMux(PORTC, 17U, kPORT_MuxAlt3);

	/* Initialize AD Mux GPIOs */
	PORT_SetPinMux(ADPORT, AD0, kPORT_MuxAsGpio);
	PORT_SetPinMux(ADPORT, AD1, kPORT_MuxAsGpio);
	PORT_SetPinMux(ADPORT, AD2, kPORT_MuxAsGpio);
	PORT_SetPinMux(ADPORT, AD3, kPORT_MuxAsGpio);
	GPIO_PinInit(ADGPIO, AD0, &opin);
	GPIO_PinInit(ADGPIO, AD1, &opin);
	GPIO_PinInit(ADGPIO, AD2, &opin);
	GPIO_PinInit(ADGPIO, AD3, &opin);

	/* Setup SPI0 */
	PORT_SetPinMux(PORTC, 5u, kPORT_MuxAlt2);  // CLK
	PORT_SetPinMux(PORTC, 6u, kPORT_MuxAlt2);  // MOSI
	PORT_SetPinMux(PORTC, 7u, kPORT_MuxAlt2);  // MISO
	PORT_SetPinMux(PORTD, 0U, kPORT_MuxAlt2);  // CS0
	PORT_SetPinMux(PORTD, 4U, kPORT_MuxAlt2);  // CS1
	PORT_SetPinMux(PORTD, 5U, kPORT_MuxAlt2);  // CS2
	PORT_SetPinMux(PORTD, 6U, kPORT_MuxAlt2);  // CS3

	// reset pin
	//PORT_SetPinMux(PORTD, 7U, kPORT_MuxAsGpio);
	//GPIO_PinInit(GPIOD, 7U, &ipin);
	//PORT_SetPinInterruptConfig(PORTD, 7U, kPORT_InterruptLogicZero);
	//PORT_SetPinInterruptConfig(PORTD, 7U, kPORT_InterruptFallingEdge);
	//NVIC_EnableIRQ(PORTD_IRQn);
	//NVIC_SetPriority(PORTD_IRQn, 5);


	/* Setup SPI1 */
	PORT_SetPinMux(PORTE, 0u, kPORT_MuxAsGpio);  // CS1, PAC DATA READY
	GPIO_PinInit(GPIOE, 0U, &ipin);

	//PORT_SetPinInterruptConfig(PORTE, 0U, kPORT_InterruptFallingEdge);
	//NVIC_EnableIRQ(PORTE_IRQn);
	//NVIC_SetPriority(PORTE_IRQn, 5);

	PORT_SetPinMux(PORTE, 1u, kPORT_MuxAlt2);  // MOSI
	PORT_SetPinMux(PORTE, 2u, kPORT_MuxAlt2);  // CLK
	PORT_SetPinMux(PORTE, 3U, kPORT_MuxAlt2);  // MISO
	PORT_SetPinMux(PORTE, 4U, kPORT_MuxAlt2);  // CS0

	/*PORT_SetPinMux(PORTE, 5U, kPORT_MuxAsGpio);   //CS2 */
	/* Just to set E5 to high */

	//PORT_SetPinMux(PORTE, 5U, kPORT_MuxAsGpio);
	//opin.outputLogic = 1;
	//GPIO_PinInit(GPIOE, 5U, &opin);

	/* Reset E5 to FTM3 channel 0 */
	PORT_SetPinMux(PORTE, 5U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTE, 6U, kPORT_MuxAsGpio);  // CS3
	opin.outputLogic = 1;
	GPIO_PinInit(GPIOE, 5U, &opin);

	//GPIO_PinInit(GPIOE, 0U, &opin);
	/*GPIO_PinInit(GPIOE, 5U, &opin);*/
	//GPIO_PinInit(GPIOE, 6U, &opin);

	GPIO_PinInit(GPIOE, 06U, &opin);
	opin.outputLogic = 1;
	//ROC BUSY LINE
	//PORT_SetPinMux(PORTD, 10U, kPORT_MuxAsGpio);
	//GPIO_PinInit(GPIOD, 10U, &ipin);
	//PORT_SetPinInterruptConfig(GPIOD, 10U, kPORT_InterruptRisingEdge);
	/* Enable interrupt PORTB_IRQn request in the NVIC */
	//NVIC_EnableIRQ(PORTD_IRQn);

    /* I2C EEPROM */
    PORT_SetPinMux(PORTC, 10U, kPORT_MuxAlt2);
    PORT_SetPinMux(PORTC, 11U, kPORT_MuxAlt2);
	GPIO_WritePinOutput(GPIOC, 18U, 0); GPIOC->PDDR |= (1U<<18U);		// Enable EE WP
    PORT_SetPinMux(PORTC, 18U, kPORT_MuxAsGpio);  						// EE_WP pin

    // BL IGNORE
	/* Initialize Soleniod Pins */
	//GPIO_WritePinOutput(GPIOE, SOL0, 0); GPIOE->PDDR |= (1U<<SOL0);
	//PORT_SetPinMux(PORTE, SOL0, kPORT_MuxAsGpio);
	//GPIO_WritePinOutput(GPIOE, SOL1, 0); GPIOE->PDDR |= (1U<<SOL1);
	//PORT_SetPinMux(PORTE, SOL1, kPORT_MuxAsGpio);
	//GPIO_WritePinOutput(GPIOE, SOL2, 0); GPIOE->PDDR |= (1U<<SOL2);
	//PORT_SetPinMux(PORTE, SOL2, kPORT_MuxAsGpio);
	//GPIO_WritePinOutput(GPIOE, SOL3, 0); GPIOE->PDDR |= (1U<<SOL3);
	//PORT_SetPinMux(PORTE, SOL3, kPORT_MuxAsGpio);
	//GPIO_WritePinOutput(GPIOE, SOL4, 0); GPIOE->PDDR |= (1U<<SOL4);
	//PORT_SetPinMux(PORTE, SOL4, kPORT_MuxAsGpio);
	//GPIO_WritePinOutput(GPIOE, SOL5, 0); GPIOE->PDDR |= (1U<<SOL5);
	//PORT_SetPinMux(PORTE, SOL5, kPORT_MuxAsGpio);
	//GPIO_WritePinOutput(GPIOE, SOL6, 0); GPIOE->PDDR |= (1U<<SOL6);
	//PORT_SetPinMux(PORTE, SOL6, kPORT_MuxAsGpio);
	//GPIO_WritePinOutput(GPIOE, SOL7, 0); GPIOE->PDDR |= (1U<<SOL7);
	//PORT_SetPinMux(PORTE, SOL7, kPORT_MuxAsGpio);

	/* Initialize Soleniod Switch Pins */
	////GPIO_WritePinOutput(GPIOB, SWS0, 0); GPIOB->PDDR &= ~(1U<<SWS0);
	//PORT_SetPinMux(PORTB, SWS0, kPORT_MuxAsGpio);
	////GPIO_WritePinOutput(GPIOB, SWS1, 0); GPIOB->PDDR &= ~(1U<<SWS1);
	//PORT_SetPinMux(PORTB, SWS1, kPORT_MuxAsGpio);
	////GPIO_WritePinOutput(GPIOB, SWS2, 0); GPIOB->PDDR &= ~(1U<<SWS2);
	//PORT_SetPinMux(PORTB, SWS2, kPORT_MuxAsGpio);
	////GPIO_WritePinOutput(GPIOB, SWS3, 0); GPIOB->PDDR &= ~(1U<<SWS3);
	//PORT_SetPinMux(PORTB, SWS3, kPORT_MuxAsGpio);
	////GPIO_WritePinOutput(GPIOB, SWS4, 0); GPIOB->PDDR &= ~(1U<<SWS4);
	//PORT_SetPinMux(PORTB, SWS4, kPORT_MuxAsGpio);
	////GPIO_WritePinOutput(GPIOB, SWS5, 0); GPIOB->PDDR &= ~(1U<<SWS5);
	//PORT_SetPinMux(PORTB, SWS5, kPORT_MuxAsGpio);
	////GPIO_WritePinOutput(GPIOC, SWS6, 0); GPIOC->PDDR &= ~(1U<<SWS6);
	//PORT_SetPinMux(PORTC, SWS6, kPORT_MuxAsGpio);
	////GPIO_WritePinOutput(GPIOC, SWS7, 0); GPIOC->PDDR &= ~(1U<<SWS7);
	//PORT_SetPinMux(PORTC, SWS7, kPORT_MuxAsGpio);
	//GPIO_PinInit(GPIOB, SWS0, &ipin);
	//GPIO_PinInit(GPIOB, SWS1, &ipin);
	//GPIO_PinInit(GPIOB, SWS2, &ipin);
	//GPIO_PinInit(GPIOB, SWS3, &ipin);
	//GPIO_PinInit(GPIOB, SWS4, &ipin);
	//GPIO_PinInit(GPIOB, SWS5, &ipin);
	//GPIO_PinInit(GPIOC, SWS6, &ipin);
	//GPIO_PinInit(GPIOC, SWS7, &ipin);
    // BL IGNORE

	/* Initialize Heartbeat LEDs */
	PORT_SetPinMux(HBPORT,HB0, kPORT_MuxAsGpio);
	PORT_SetPinMux(HBPORT,HB1, kPORT_MuxAsGpio);

	GPIO_PinInit(HBGPIO, HB0, &opin);
	GPIO_PinInit(HBGPIO, HB1, &opin);

	/* Initialize Panel Lights (Logo & Status) */
	opin.outputLogic = 0;
	PORT_SetPinMux(PORTA,24U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA,25U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA,26U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTA,27U, kPORT_MuxAsGpio);

	// gpios for stepper motor 1 and 2: reset for latch circuit
	// BL IGNORE
	//PORT_SetPinMux(PORTA,STEP_MOT_0, kPORT_MuxAsGpio);
	//PORT_SetPinMux(PORTA,STEP_MOT_1, kPORT_MuxAsGpio);
	// BL IGNORE

	GPIO_PinInit(GPIOA, 24U, &opin);
	GPIO_PinInit(GPIOA, 25U, &opin);
	GPIO_PinInit(GPIOA, 26U, &opin);
	GPIO_PinInit(GPIOA, 27U, &opin);

	// stepper motor gpios: keep low by default
	// BL IGNORE
	//GPIO_PinInit(GPIOA, STEP_MOT_0, &opin);
	//GPIO_PinInit(GPIOA, STEP_MOT_1, &opin);
	// BL IGNORE

	//Case interlocks
	PORT_SetPinMux(PORTB,00U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTB,01U, kPORT_MuxAsGpio);
	PORT_SetPinMux(PORTB,02U, kPORT_MuxAsGpio);

	GPIO_PinInit(GPIOB, 00U, &ipin);
	GPIO_PinInit(GPIOB, 01U, &ipin);
	GPIO_PinInit(GPIOB, 02U, &ipin);

	GPIO_PinInit(GPIOB, 00U, &ipin);
	//PORT_SetPinInterruptConfig(PORTB, 00U, kPORT_InterruptEitherEdge);
	/* Enable interrupt PORTB_IRQn request in the NVIC */
	//NVIC_EnableIRQ(PORTB_IRQn);
	//NVIC_SetPriority(PORTB_IRQn, 5);
}
