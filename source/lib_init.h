#ifndef _LIB_INIT_H_
#define _LIB_INIT_H_


/*******************************************************************************
 * Definitions
 ******************************************************************************/



/*!
 * @addtogroup lib_init
 * @{
 */

/*******************************************************************************
 * API
 ******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif

#include "utils.h"
#include "mios.h"

// partially initialized by the library, buffers are setup
// the bottom layer needs to be initialized by the provider/consumer
extern iostream_t ostr; // output stream for the bootloader
extern iostream_t istr; // input stream for the bootloader
extern iostream_t cout; // console out stream
extern iostream_t cin;  // console in stream

void LIB_CPRINT(const char *str, int len);
void LIB_Init(void);
void LIB_Deinit(void);

const char * LIB_GetTargetInfo();
uint32_t LIB_GET_TIMEOUT(void);

#if defined(__cplusplus)
}
#endif

/*!
 * @}
 */
#endif /* _LIB_INIT_H_ */

/*******************************************************************************
 * EOF
 ******************************************************************************/
