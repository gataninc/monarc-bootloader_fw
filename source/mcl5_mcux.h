#ifndef _MCURTOS_H_
#define _MCURTOS_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

extern char xfer_filename[20];
extern char xfer_dir;								// 0 = download, 1 = upload

extern const char *firmware;


extern char cmdline[128];
void cprintf ( const char * format, ... );

void SetPMTTemperature(int temp);
int GetPMTTemperature();
int GetPMTError();


#endif
