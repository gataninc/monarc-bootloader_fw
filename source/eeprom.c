/*
 * eeprom.c
 *
 *  Created on: Nov 27, 2018
 *      Author: stelasang
 */

#include "fsl_i2c.h"
#include "fsl_i2c_freertos.h"
#include "Uart.h"

#include "eeprom.h"
#include "utils.h"
#include "stdbool.h"

#define I2C_MASTER I2C1
#define I2C_MASTER_IRQN (I2C1_IRQn)
#define EE_ADDR (0x57U)
#define I2C_BAUDRATE (100000) /* 100K */
#define EE_BUFFER_SIZE (32)  /* MAX is 256 */

#define I2C_MEZZ I2C0
#define I2C_MEZZ_IRQN (I2C0_IRQn)
#define I2C_BAUDRATE (100000) /* 100K */
#define EE_BUFFER_SIZE (32)  /* MAX is 256 */

i2c_rtos_handle_t rtos_handle;		// local EEPROM
i2c_rtos_handle_t i2c_mezz;			// Mezz EEPROM loop

static bool bInitialized = false;

void EEPROM_init()
{
	i2c_master_config_t masterConfig;
	uint32_t sourceClock;

	NVIC_SetPriority(I2C_MASTER_IRQN, 6);
	I2C_MasterGetDefaultConfig(&masterConfig);
    masterConfig.baudRate_Bps = I2C_BAUDRATE;
	sourceClock = CLOCK_GetFreq((I2C1_CLK_SRC));
	I2C_RTOS_Init(&rtos_handle, I2C_MASTER, &masterConfig, sourceClock);

	//NVIC_SetPriority(I2C_MEZZ_IRQN, 6);
	//I2C_MasterGetDefaultConfig(&masterConfig);
    //masterConfig.baudRate_Bps = I2C_BAUDRATE;
	//sourceClock = CLOCK_GetFreq((I2C0_CLK_SRC));
	//I2C_RTOS_Init(&i2c_mezz, I2C_MEZZ, &masterConfig, sourceClock);

	bInitialized = true;
}

#if 0
void ReadMezzEEPROM(int MezzAddr)
{
	status_t status;
	uint8_t txBuff[EE_BUFFER_SIZE];
    i2c_master_transfer_t masterXfer;

    txBuff[0] = 0; txBuff[1] = 0;
	memset(&masterXfer, 0, sizeof(masterXfer));
    masterXfer.slaveAddress = MezzAddr;
    masterXfer.direction = kI2C_Write;
    masterXfer.subaddress = 0;
    masterXfer.subaddressSize = 0;
    masterXfer.data = txBuff;
    masterXfer.dataSize = 2;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    status = I2C_RTOS_Transfer(&i2c_mezz, &masterXfer);
    if (status != kStatus_Success)
    {
        cprintf("I2C master: error during read transaction, %d", status);
        return;
    }

	memset(&masterXfer, 0, sizeof(masterXfer));
    masterXfer.slaveAddress = MezzAddr;
    masterXfer.direction = kI2C_Read;
    masterXfer.subaddress = 0;
    masterXfer.subaddressSize = 0;
    masterXfer.data = txBuff;
    masterXfer.dataSize = EE_BUFFER_SIZE;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    status = I2C_RTOS_Transfer(&i2c_mezz, &masterXfer);
    if (status != kStatus_Success)
    {
        cprintf("I2C master: error during read transaction, %d", status);
        return;
    }

	for (int i = 0; i<EE_BUFFER_SIZE; i++)
		cprintf("%02x ",(uint8_t)(txBuff[i]&0xff));
	cprintf("\r\n");
}
#endif


int SetEEPROMAddr(uint8_t address, int offset)   // from 0 - 0xfff  (0-4095)
{
	status_t status;
	uint8_t txBuff[2];
    i2c_master_transfer_t masterXfer;

    txBuff[1] = (offset&0xff); txBuff[0] = (offset>>8)&0xf;		// high nibble, then low byte
	memset(&masterXfer, 0, sizeof(masterXfer));
    masterXfer.slaveAddress = address;
    masterXfer.direction = kI2C_Write;
    masterXfer.subaddress = 0;
    masterXfer.subaddressSize = 0;
    masterXfer.data = txBuff;
    masterXfer.dataSize = 2;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    status = I2C_RTOS_Transfer(&rtos_handle, &masterXfer);
    if (status != kStatus_Success)
    {
        return -1;
    }
    return 0;
}

int ReadEEPROM(uint8_t address, int offset, uint8_t *rxBuff, int len)
{
    i2c_master_transfer_t masterXfer;
    status_t result;

    if (!bInitialized) {
    	EEPROM_init();
    }

    result = SetEEPROMAddr(address, offset);
    if (result != 0){
    	return result;
    }

	memset(&masterXfer, 0, sizeof(masterXfer));
    masterXfer.slaveAddress = address;
    masterXfer.direction = kI2C_Read;
    masterXfer.subaddress = 0;
    masterXfer.subaddressSize = 0;
    masterXfer.data = rxBuff;
    masterXfer.dataSize = len;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    result = I2C_RTOS_Transfer(&rtos_handle, &masterXfer);
    if (result != kStatus_Success){
    	return -1;
    }
    return 0;
}

int WriteEEPROM(uint8_t address, int offset, uint8_t *txBuff, int len)
{
	status_t status;
    i2c_master_transfer_t masterXfer;
    uint8_t *buf;

    if (!bInitialized) {
    	EEPROM_init();
    }

    buf = pvPortMalloc(len+2);
    buf[1] = (offset&0xff); buf[0] = (offset>>8)&0xf;
    memcpy(buf+2,txBuff,len);

	memset(&masterXfer, 0, sizeof(masterXfer));
    masterXfer.slaveAddress = address;
    masterXfer.direction = kI2C_Write;
    masterXfer.subaddress = 0;
    masterXfer.subaddressSize = 0;
    masterXfer.data = buf;
    masterXfer.dataSize = len+2;
    masterXfer.flags = kI2C_TransferDefaultFlag;

    status = I2C_RTOS_Transfer(&rtos_handle, &masterXfer);

    vPortFree(buf);
    //if (status != kStatus_Success)
    //{
    //    cprintf("I2C master: error during write transaction, %d", status);
    //}
    return status;
}

#if 0
void UpdateFirmwareVersion()
{
//	uint8_t versionbuf[16];
//	int n;
//
//	n = 16-strlen(FIRMWARE_VERSION);
//	ReadEEPROM(FIRMWARE_ADDR,versionbuf,16);
//	if (memcmp(versionbuf+n,FIRMWARE_VERSION,16)) {
//		memset(versionbuf,0x20,16);
//		memcpy(versionbuf+n,FIRMWARE_VERSION,strlen(FIRMWARE_VERSION));
//		WriteEEPROM(FIRMWARE_ADDR,versionbuf,16);
//	}
}

void PrintEEPROM(int offset, bool printhex, char *cmd)
{
	uint8_t rxBuff[EE_BUFFER_SIZE];
	status_t status;
	int len;
	char c;

	vgethex(cmd,&len);
	status = ReadEEPROM(offset, rxBuff, len);

   if (status != kStatus_Success)
   {
       cprintf("I2C master: error during read transaction, %d\r\n", status);
       return;
   }

   if (printhex) {
	   cprintf("%03x: ",offset);
	   for (int i = 0; i<len; i++)
		   cprintf("%02x ",(uint8_t)(rxBuff[i]&0xff));
   }
	for (int i = 0; i<len; i++) {
		c = (rxBuff[i]&0xff);
		c = ((c>=20) && (c<=0x7f))?c:'.';
		cputc(c);
	}
	cprintf("\r\n");
}

void ASCIIWriteEEPROM(int offset, char *c)
{
	uint8_t txBuff[EE_BUFFER_SIZE];
	int n,p;
	int len;

	c = vgethex(c,&len);
	if (*c != '"') return;
	c++;
	memset(txBuff,' ',len);
	n = strlen(c); p = len-n;
	memcpy(txBuff+p,c,n);
	WriteEEPROM(offset,txBuff,len);
//	printOk();
}

void Eeprom(char* c)
{
	int offset;
	c = vgethex(c,&offset);
	switch (*c) {
		case 'h' : PrintEEPROM(offset, true, c+1); break;
		case 'p' : PrintEEPROM(offset, false, c+1); break;
		case 'w' : ASCIIWriteEEPROM(offset,c+1); break;
	}
}



//void ReadEEPROM()
//{
//	status_t status;
//	uint8_t txBuff[EE_BUFFER_SIZE];
//    i2c_master_transfer_t masterXfer;
//
//    txBuff[0] = 0; txBuff[1] = 0;
//	memset(&masterXfer, 0, sizeof(masterXfer));
//    masterXfer.slaveAddress = EE_ADDR;
//    masterXfer.direction = kI2C_Write;
//    masterXfer.subaddress = 0;
//    masterXfer.subaddressSize = 0;
//    masterXfer.data = txBuff;
//    masterXfer.dataSize = 2;
//    masterXfer.flags = kI2C_TransferDefaultFlag;
//
//    status = I2C_RTOS_Transfer(&rtos_handle, &masterXfer);
//    if (status != kStatus_Success)
//    {
//        cprintf("I2C master: error during read transaction, %d", status);
//        return;
//    }
//
//
//
//	memset(&masterXfer, 0, sizeof(masterXfer));
//    masterXfer.slaveAddress = EE_ADDR;
//    masterXfer.direction = kI2C_Read;
//    masterXfer.subaddress = 0;
//    masterXfer.subaddressSize = 0;
//    masterXfer.data = txBuff;
//    masterXfer.dataSize = EE_BUFFER_SIZE;
//    masterXfer.flags = kI2C_TransferDefaultFlag;
//
//    status = I2C_RTOS_Transfer(&rtos_handle, &masterXfer);
//    if (status != kStatus_Success)
//    {
//        cprintf("I2C master: error during read transaction, %d", status);
//        return;
//    }
//
//	for (int i = 0; i<EE_BUFFER_SIZE; i++)
//		cprintf("%02x ",(uint8_t)(txBuff[i]&0xff));
//	cprintf("\r\n");
//}
//
//void WriteEEPROM()
//{
//	status_t status;
//	uint8_t txBuff[EE_BUFFER_SIZE+2];
//    i2c_master_transfer_t masterXfer;
//
//
//	for (int i=2; i<EE_BUFFER_SIZE+2; i++)
//		txBuff[i]=i-2;
//	txBuff[0] = 0; txBuff[1] = 0;
//
//
//	memset(&masterXfer, 0, sizeof(masterXfer));
//    masterXfer.slaveAddress = EE_ADDR;
//    masterXfer.direction = kI2C_Write;
//    masterXfer.subaddress = 0;
//    masterXfer.subaddressSize = 0;
//    masterXfer.data = txBuff;
//    masterXfer.dataSize = EE_BUFFER_SIZE+2;
//    masterXfer.flags = kI2C_TransferDefaultFlag;
//
//    status = I2C_RTOS_Transfer(&rtos_handle, &masterXfer);
//    if (status != kStatus_Success)
//    {
//        cprintf("I2C master: error during read transaction, %d", status);
//        return;
//    }
//
//}
#endif

