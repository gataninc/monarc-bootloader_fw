
/*
 * dbglog.c
 *
 *  Created on: Aug 30, 2019
 *      Author: lkodheli
 */

//#include "dbglog.h"
#include "SerialComImpl.h"

#include <ctype.h>

#include <string.h>
#include "Crc.h"
#include "utils.h"

#include <stdbool.h>

#include <stdio.h>
#include "FreeRTOS.h"
#include "fsl_uart_freertos.h"
#include "dbglog.h"

/*-----------------------------------------------------------------------------------------------------

                                        SERIAL COM DIAGRAM

 TX(2) <------------------------------------ +-------------+                         +-----------+
 RX(2) ------------------------------------> | LINK (2)    | <======================>|           |
                                     +---------------+  ---+                         | NET LAYER |--->
 TX(1) <-- Link_Tx_GetBuffer() <---- | LINK (1)      | <---- Link_SendFrame() <------|           |<---
 RX(1) -->     Link_Rx_Byte() -----> | LINK LAYER    | ----> Net_HandlePacket() ---->|           |
                                     +---------------+                               +-----------+
    TX,RX Hardware                 Serialize  / Frames,                               Handle routing
                                   Deserialize


 ~----------+                                 +-----------------+  Queue Frame ,+-----------+
            | ----> Tran_HandlePacket() ----> |                 | ------------> | END POINT |
  NET LAYER | <---  Net_SendPacket() <------- | TRANSPORT LAYER | <------------ |    (1)    |--------+
            |                                 +-----------------+               +-----------+        |
 ~----------+                                   Reliable transport,                  | END POINT (2) |
                                                source-port, destination-port,       +---------------+
                                                session,                               app end-point

-------------------------------------------------------------------------------------------------------
*/

//#define LIBNET_DEBUG_ON                   (1)

#define OUT_RESOURCE_TIMEOUT_MS           (1000)
#define IN_RESOURCE_TIMEOUT_MS           (1000)

static QueueHandle_t s_FreeFrameQue;
static Frame_t s_FreeFrameArray[RESOURCE_QUEUE_PACKET_CNT];

static SemaphoreHandle_t s_NetLibSemaphore;

static TransportConfig_t s_TrCfg;
static NetConfig_t s_NetCfg;

// LIBRARY EVENTS
#define NETLIB_EVT_TRANSPORT_TIMER_SERVICE                (1 << 0)
//#define NETLIB_EVT_TRANSPORT_DOWN                 (1 << 1)


#define RESOURCE_FRM_RELEASE_ERROR               (1 << 0)
#define RESOURCE_FRM_ACQUIRE_ERROR               (1 << 1)
#define RESOURCE_FRM_USE_CNT_ERROR               (1 << 2)
#define RESOURCE_LIB_SEM_ACQUIRE_ERROR           (1 << 3)
#define RESOURCE_LIB_SEM_RELEASE_ERROR           (1 << 4)


#define DEBUG_SERIAL_COM_NET_SEND

typedef enum EpEventEnum_t {
	EPEVT_PKT_RCV           = 0x1,    // some message was received
	EPEVT_SESSION_CLOSE_RCV = 0x2,    //
	EPEVT_SESSION_OPEN      = 0x4,
	EPEVT_TX_ERROR          = 0x8,
	EPEVT_PKT_SENT          = 0x10,    // some message was received
} EpEventEnum_t;

typedef enum Direction_t {
	DIR_TX, DIR_RX,
} Direction_t;

uint32_t s_resourceError;
uint32_t s_resourceStickyError;

#define DEBUG_SERIAL_COM

int EP_SendAckPacket(ConnectionEndPoint_t *ep);
void Link_Service(void *pvParameters);
void Net_Init(uint8_t my_addr);
void log_frame(Frame_t *pkt, Direction_t dir);

int NetLib_Init(uint8_t my_addr)
{
	uint8_t index;
	int res;

	s_NetLibSemaphore = xSemaphoreCreateMutex();
	if (s_NetLibSemaphore == NULL){
		//Dbg_SerialCommError("Failed to create s_NetLibSemaphore");
	    return SER_LIB_SEM_CREATE;
	}

	s_FreeFrameQue = xQueueCreate(RESOURCE_QUEUE_PACKET_CNT,sizeof(uint8_t));
	if (s_FreeFrameQue == NULL){
		vSemaphoreDelete(s_NetLibSemaphore);
		//Dbg_SerialCommError("Failed to create s_FreeFrameQue");
		return SER_RESOURCE_OUT_QUEUE_CREATE;
	}

    for(index = 0; index < RESOURCE_QUEUE_PACKET_CNT; index++){
    	xQueueSend(s_FreeFrameQue, &index, portMAX_DELAY);
    }
    //Dbg_SerialCommInfo("%s", __FUNCTION__);
    Net_Init(my_addr);
    res =  Tran_Init();

    return res;
}

void NetLib_Deinit(void)
{
	vQueueDelete( s_FreeFrameQue );
	vSemaphoreDelete(s_NetLibSemaphore);
	Tran_Deinit();
}

void _NetLib_FrameInit(Frame_t *pFrame)
{
	pFrame->data = &pFrame->bytes[FRM_PKT_OFFSET]; // data starts after byte count
}

void _NetLib_GetFreeFrame(Frame_t **ppFrame ,uint32_t timeoutMs, uint16_t lnNum)
{
	uint8_t freeIndex;

	*ppFrame = NULL;

	if(xQueueReceive(s_FreeFrameQue, &freeIndex,  timeoutMs/portTICK_PERIOD_MS) == pdTRUE){
#ifdef LIBNET_DEBUG_ON
		s_resourceError &= ~RESOURCE_FRM_ACQUIRE_ERROR;
#endif
		if (freeIndex >= RESOURCE_QUEUE_PACKET_CNT){
			//Dbg_SerialCommError("Invalid frame index:%d ", freeIndex);
#ifdef _DEBUG_FRM_
		    while(1);
#endif
		}
		else {
		    // the caller owns the frame and is responsible for decrementing it's use count
		    s_FreeFrameArray[freeIndex].useCnt = 1;
		    _NetLib_FrameInit(&s_FreeFrameArray[freeIndex]);

#ifdef _DEBUG_FRM_
		    s_FreeFrameArray[freeIndex]._useHist[0] = lnNum;
		    s_FreeFrameArray[freeIndex]._useIdx = 1;
#endif

		    //Dbg_SerialCommInfo(" got Frame: %d", freeIndex);
		    *ppFrame = &s_FreeFrameArray[freeIndex];
		}
    }
	else {
	    //Dbg_SerialCommWarning("Failed to get a free frame");
#ifdef LIBNET_DEBUG_ON
	    s_resourceError |= RESOURCE_FRM_ACQUIRE_ERROR;
#endif
	    s_resourceStickyError |= RESOURCE_FRM_ACQUIRE_ERROR;
	}
}

void _NetLib_GetFreeFrameFromIsr(Frame_t **ppFrame, BaseType_t *pxTaskWokenByReceive, uint16_t lnNum)
{
	uint8_t freeIndex;

	*ppFrame = NULL;

	if(xQueueReceiveFromISR(s_FreeFrameQue, &freeIndex,  pxTaskWokenByReceive) == pdTRUE){
#ifdef LIBNET_DEBUG_ON
		s_resourceError &= ~RESOURCE_FRM_ACQUIRE_ERROR;
#endif

		if (freeIndex < RESOURCE_QUEUE_PACKET_CNT){
		    // the caller owns the frame and is responsible for decrementing it's use count
		    s_FreeFrameArray[freeIndex].useCnt = 1;

#ifdef _DEBUG_FRM_
		    s_FreeFrameArray[freeIndex]._useHist[0] = lnNum;
		    s_FreeFrameArray[freeIndex]._useIdx = 1;
#endif

		    //Dbg_SerialCommInfo(" got Frame: %d", freeIndex);
		    *ppFrame = &s_FreeFrameArray[freeIndex];
		}
		else {
#ifdef _DEBUG_FRM_
		    while(1);
#endif
		}
    }
	else {
#ifdef LIBNET_DEBUG_ON
	    s_resourceError |= RESOURCE_FRM_ACQUIRE_ERROR;
#endif
	    s_resourceStickyError |= RESOURCE_FRM_ACQUIRE_ERROR;
	}
}

void _NetLib_ReleaseFrame(Frame_t *pFrame, uint32_t timeoutMs)
{
	uint8_t Index = pFrame -s_FreeFrameArray;

	if (Index >= RESOURCE_QUEUE_PACKET_CNT){
		//Dbg_SerialCommError("Failed to release frame:%d s_FreeFrameQue", Index);
#ifdef _DEBUG_FRM_
		    while(1);
#endif

        return;
	}

	if (xQueueSend(s_FreeFrameQue, &Index, timeoutMs/portTICK_PERIOD_MS) == pdTRUE){
		//Dbg_SerialCommInfo(" release Frame: %d", Index);
#ifdef LIBNET_DEBUG_ON
		s_resourceError &= ~RESOURCE_FRM_RELEASE_ERROR;
#endif

	}
	else {
#ifdef LIBNET_DEBUG_ON
	    s_resourceError |= RESOURCE_FRM_RELEASE_ERROR;
#endif
#ifdef _DEBUG_FRM_
	    while(1);
#endif
	    //Dbg_SerialCommError("Failed to release frame:%d s_FreeFrameQue", Index);
	    s_resourceStickyError |= RESOURCE_FRM_RELEASE_ERROR;
	}
}
//#define FRAME_GIVE(PFRM, TIMEOUT) _NetLib_ReleaseFrame(PFRM ,TIMEOUT); Dbg_SerialCommTrace("GIVE FRAME")
#define FRAME_GIVE(PFRM, TIMEOUT) _NetLib_ReleaseFrame(PFRM ,TIMEOUT)

// increment use count to prevent frame release from other layers
void _FRAME_USE_CNT_INC(Frame_t *frame, uint16_t lnNum)
{
	//uint8_t Index = frame -s_FreeFrameArray;
	if(pdFALSE == xSemaphoreTake(s_NetLibSemaphore, portMAX_DELAY)){
		s_resourceStickyError |= RESOURCE_LIB_SEM_ACQUIRE_ERROR;
	}

	frame->useCnt++;
#ifdef _DEBUG_FRM_
	if (frame->_useIdx < _DEBUG_FRM_HIST_SIZE)
	frame->_useHist[frame->_useIdx++] = lnNum;
#endif
	//Dbg_SerialCommInfo(" inc Frame: %d, cnt: %d", Index, frame->useCnt);
	if (pdFALSE == xSemaphoreGive(s_NetLibSemaphore)){
		s_resourceStickyError |= RESOURCE_LIB_SEM_RELEASE_ERROR;
	}
}

//#define FRAME_USE_CNT_INC(FRM) _FRAME_USE_CNT_INC(FRM); Dbg_SerialCommTrace("INC frame: %d, use cnt %d", FRM-s_FreeFrameArray,FRM->useCnt)
//#define FRAME_USE_CNT_INC(FRM) _FRAME_USE_CNT_INC(FRM)
#define FRAME_USE_CNT_INC(FRM) _FRAME_USE_CNT_INC(FRM, __LINE__)

// don't release frames directly, decrement its use count
void _FRAME_USE_CNT_DEC(Frame_t *frame, uint16_t lnNum)
{
	uint8_t Index = frame -s_FreeFrameArray;
	if (Index >= RESOURCE_QUEUE_PACKET_CNT) {
#ifdef _DEBUG_FRM_
	    while(1);
#endif
	    return;
	}
	if(pdFALSE == xSemaphoreTake(s_NetLibSemaphore, portMAX_DELAY)){
		s_resourceStickyError |= RESOURCE_LIB_SEM_ACQUIRE_ERROR;
	}

	frame->useCnt--;
#ifdef _DEBUG_FRM_
	if (frame->_useIdx < _DEBUG_FRM_HIST_SIZE)
	    frame->_useHist[frame->_useIdx++] = lnNum;
#endif
	//Dbg_SerialCommInfo(" dec Frame: %d, cnt: %d", Index, frame->useCnt);
	if(frame->useCnt == 0){
		_NetLib_ReleaseFrame(frame, OUT_RESOURCE_TIMEOUT_MS);
	}
	else if (frame->useCnt < 0){
		//Dbg_SerialCommError("dec frame error : %d, cnt: %d", Index, frame->useCnt);
#ifdef _DEBUG_FRM_
	    while(1);
#endif
	}

	if (pdFALSE == xSemaphoreGive(s_NetLibSemaphore)){
		s_resourceStickyError |= RESOURCE_LIB_SEM_RELEASE_ERROR;
	}
}
//#define FRAME_USE_CNT_DEC(FRM) _FRAME_USE_CNT_DEC(FRM); Dbg_SerialCommTrace("DEC frame: %d, use cnt %d", FRM-s_FreeFrameArray,FRM->useCnt)
//#define FRAME_USE_CNT_DEC(FRM) _FRAME_USE_CNT_DEC(FRM)
#define FRAME_USE_CNT_DEC(FRM) _FRAME_USE_CNT_DEC(FRM,__LINE__)

#define IS_FRAME_IN_USE(FRM) (FRM->useCnt > 0)







int Link_Init(SerialLink_t *link)
{
	BaseType_t xReturned;
	link->rxFrmQue = xQueueCreate(5, sizeof(Frame_t *));
	if (link->rxFrmQue == NULL){
		return SER_LINK_INIT_RX_QUEUE_CREATE;
	}

	link->txFrmQue = xQueueCreate(5, sizeof(Frame_t *));
	if (link->txFrmQue == NULL){
		vQueueDelete( link->rxFrmQue );
        return SER_LINK_INIT_TX_QUEUE_CREATE;
	}

    link->xHandle = NULL;
    // Services always run at highest priority
    xReturned = xTaskCreate(Link_Service, "Link_Service",configMINIMAL_STACK_SIZE*2, link, PRIORITY_HIGH, &link->xHandle);
    if (xReturned != pdPASS){
    	vQueueDelete( link->rxFrmQue );
    	vQueueDelete( link->txFrmQue );
    	return SER_LINK_INIT_SERVICE_TASK_CREATE;
    }
    link->rxCtxt.pFrame = NULL;
    link->txCtxt.pFrame = NULL;
    link->stickyStatus = LINK_STATUS_INITIALIZED;
    return SER_SUCCESS;
}

void Link_Deinit(SerialLink_t *link)
{
	//xSemaphoreGive(link->txSemaphore);
	//xSemaphoreGive(link->rxSemaphore);

	//vSemaphoreDelete(link->txSemaphore);
	//vSemaphoreDelete(link->rxSemaphore);
	//todo: need to do proper deinitialization
}

void Link_InitFrameRxCtxt(SerialLink_t *link)
{
	link->rxCtxt.frameState = FrmState_Idle;
}

void Link_InitFrameTxCtxt(SerialLink_t *link)
{
	link->txCtxt.nextFldWidth = 0;
}

void Link_ParseFieldInit(FieldParseCtxt_t *ctxt, uint32_t len, uint8_t *dest)
{
	ctxt->fldState = FLDPRS_IDDLE;
	ctxt->len = len;
	ctxt->index = 0;
	ctxt->dest = dest;
}

int Link_ParseField(FieldParseCtxt_t *ctxt, uint8_t byte)
{

    switch(ctxt->fldState)
    {
        case FLDPRS_IDDLE:
        {
    	   if(byte != START_FRAME){
    		   *(ctxt->dest) = byte;
    		   ctxt->fldState = FLDPRS_DATA;
    	   }
    	   else {
    		   ctxt->fldState = FLDPRS_ESCAPE;
    	   }
        }break;
        case FLDPRS_ESCAPE:
        {
    	   if(byte == START_FRAME){
    		   *(ctxt->dest) = byte;
    		   ctxt->fldState = FLDPRS_DATA; // second escape, write the data
    	   }
    	   else {
    		   ctxt->fldState = FLDPRS_ESCAPE;
    		   return -1; // error escape to data transition after first escape
    	   }
        }break;
        case FLDPRS_DATA:
        {
    	   if(byte != START_FRAME){
    		   *(ctxt->dest) = byte;
    	   }
    	   else {
    		   ctxt->fldState = FLDPRS_ESCAPE; // don't write the byte
    	   }
        }break;

    }
    if (ctxt->fldState != FLDPRS_ESCAPE) {
    	ctxt->dest++;
    	ctxt->index++;
    	if (ctxt->index == ctxt->len) {
    		return 1;
    	}
    }
    return 0; // parsing continues
}

void Link_ParseFrameByte(FrameRxCtxt_t *ctxt, uint8_t rxByte)
{
	int res;
	if(ctxt->frameState != FrmState_FrameComplete)
	{
	    if ((ctxt->frameState == FrmState_Idle) && (rxByte != START_FRAME) ){
	    	return; // while we don't see a start character throw away the data
        }

        // Frame receive FSM
        switch(ctxt->frameState){
            case FrmState_Idle:
        	   if (rxByte == START_FRAME) {
        		   ctxt->frameState = FrmState_InStart; // potentially a start, it could be an escape sequence
        	   }
        	   break;
            case FrmState_InStart:
            	// rx frame start and the next byte is in rxByte
            	if (rxByte == START_FRAME) {
            		ctxt->frameState = FrmState_Idle;
            	}
            	else { // byteState can NOT be EscapeData, the packet would be too long
            		ctxt->frameState = FrmState_InCnt; // an escape sequence maybe escaping cnt
            		Link_ParseFieldInit(&ctxt->fldState, 2, (uint8_t*)(&ctxt->dataLen));
            		Link_ParseField(&ctxt->fldState,rxByte);
            	}
            	break;
            case FrmState_InCnt:
            	// rx first byte cnt and the next byte is in rxByte
            	res = Link_ParseField(&ctxt->fldState, rxByte);
                if (res == -1) {
                	ctxt->frameState = FrmState_InStart;
                }
                else if (res == 1) { // field has been parsed
                	ctxt->pFrame->pkt.frmByteCnt = ctxt->dataLen; // preserve arrival order
                	ctxt->dataLen = WORD_NET2HOST(ctxt->dataLen);

                	if (FRAME_IS_BYTE_CNT_VALID(ctxt->dataLen)) { // setup to receive the next field
                		ctxt->frameState = FrmState_InData;
                		Link_ParseFieldInit(&ctxt->fldState, ctxt->dataLen + FRAME_CRC_SIZE, ctxt->pFrame->data);
                		//Link_ParseField(&ctxt->fldState, rxByte);
                	}
                	else { // invalid frame size
                    	ctxt->frameState = FrmState_Idle;
                    }
                }
                // else 0, still parsing
                break;
            case FrmState_InData:
            	res = Link_ParseField(&ctxt->fldState, rxByte);
                if (res == -1) {
                	ctxt->frameState = FrmState_InStart;
                }
                else if (res == 1) { // field has been parsed
                	ctxt->frameState = FrmState_FrameComplete; // an escape sequence maybe escaping cnt
                }
                // else 0, keep getting data
                break;
            default:
            	ctxt->frameState = FrmState_Idle;
            	break;
        }
	}
}

void Link_Rx_ByteFromIsr(SerialLink_t *link, uint8_t rxByte, BaseType_t *pxTaskWoken)
{
	BaseType_t xTaskWokenByReceive = pdFALSE;
	BaseType_t xTaskWokenBySend = pdFALSE;
	if ((pxTaskWoken == NULL) || (link == NULL)){ return; }

    if (link->rxCtxt.pFrame == NULL){
    	FRAME_GET_FROM_ISR(&link->rxCtxt.pFrame, &xTaskWokenByReceive); // acquire a frame
    	if (xTaskWokenByReceive != pdFALSE) {
    		*pxTaskWoken = xTaskWokenByReceive;
    	}
    	if (link->rxCtxt.pFrame == NULL){
            return;
    	}
    	Link_InitFrameRxCtxt(link);
    }

    Link_ParseFrameByte(&link->rxCtxt, rxByte);

    if (link->rxCtxt.frameState == FrmState_FrameComplete){
        if (xQueueSendFromISR(link->rxFrmQue, &link->rxCtxt.pFrame, &xTaskWokenBySend) == pdTRUE){
        	link->rxCtxt.pFrame = NULL;
        }
        if (xTaskWokenBySend != pdFALSE){
        	*pxTaskWoken = xTaskWokenBySend;
        }
    }
}

void Link_Rx_Byte(SerialLink_t *link, uint8_t rxByte)
{
	if (link == NULL){ return; }
    if (link->rxCtxt.pFrame == NULL){
    	FRAME_GET(&link->rxCtxt.pFrame, portMAX_DELAY); // acquire a frame

    	if (link->rxCtxt.pFrame == NULL){
    		//Dbg_SerialCommInfo("[Link_PushByte] Error: TIMEOUT: %d",LINK_FRAME_GET_TIMEOUT_MS);
            return;
    	}
    	Link_InitFrameRxCtxt(link);
    }

    Link_ParseFrameByte(&link->rxCtxt, rxByte);

    if (link->rxCtxt.frameState == FrmState_FrameComplete){
        if (xQueueSend(link->rxFrmQue, &link->rxCtxt.pFrame, portMAX_DELAY) == pdTRUE){
        	link->rxCtxt.pFrame = NULL;
        }
    }
}

int Link_Rx_MinCntExpected(SerialLink_t *link)
{
	if (link == NULL){ return 0; }
    if ((link->rxCtxt.pFrame == NULL) ||
    	(link->rxCtxt.frameState != FrmState_InData)){
    	return FRAME_PAYLOAD_MIN;
    }
    else {
    	return (link->rxCtxt.fldState.len - link->rxCtxt.fldState.index);
    }
}

// I assume that send packet is serialized
int Link_SendFrame(SerialLink_t *link, Frame_t *outFrame)
{
	//int res = SER_SUCCESS;
    BaseType_t xResult;
	uint16_t dataSize;
	uint32_t crc = 0;
	uint8_t *pktBytes;


	pktBytes = outFrame->data;

#if 0
	uint8_t msgSize = GET_TRAN_MSG_SIZE(outFrame);

	uint8_t *msg = GET_TRAN_MSG_BYTES(outFrame);
	uint8_t m0 =0, m1=0;
    if (msgSize > 0){
		m0 = msg[0];
	}
	if (msgSize > 1){
		m1 = msg[1];
	}
	
	Dbg_SerialCommInfo("[SEND:%d] %s host:%d,%d remote:%d,%d,%d tx/rx:%d,%d msg[%d]={%d %d}",
			GET_TRAN_SESSION_ID(outFrame),
			GET_TRAN_PKT_TYPE_STR(outFrame),
			GET_NET_SRC_DEV(outFrame),GET_TRAN_SRC_PORT(outFrame),
			GET_NET_DEST(outFrame),GET_TRAN_DEST_PORT(outFrame),GET_TRAN_DEST_SLOT(outFrame),
			GET_TRAN_TX_CNT(outFrame),
			GET_TRAN_RX_CNT(outFrame),
			msgSize,m0, m1
			);
#endif

	SET_LINK_VERSION_TYPE(outFrame, PKT_VERSION_02, PKT_TYPE_LINK_LAYER_DATA);
	outFrame->payloadLength = GET_FRM_PAYLOAD_LEN(outFrame);
	dataSize = GET_FRM_DATA_LEN(outFrame);
	crc = Crc32(crc, pktBytes, dataSize);
	SET_FRM_CRC(outFrame, crc);

	FRAME_USE_CNT_INC(outFrame); // when pushing to queue, increment count
	xResult = xQueueSend(link->txFrmQue, &outFrame,  portMAX_DELAY);
	if (xResult != pdPASS){
		return SER_LINK_TX_QUEUE_PUSH;
	}
    return SER_SUCCESS;
}

// returns -1 error, 0 there is more, 1 done
int Link_GetNextEncodedByte(FrameTxCtxt_t *txCtxt, uint8_t *byte)
{
	uint8_t data;

	data = txCtxt->pdata[0];
	if (txCtxt->nextFldWidth == 1){
		*byte = data;
		txCtxt->pdata++;
		if (txCtxt->pdata < txCtxt->pend){
			if (txCtxt->pdata[0] != START_FRAME){// forward look to the next field
			    txCtxt->nextFldWidth = 1;
			}
			else {
				txCtxt->nextFldWidth = 2;
			}
		}
		else {
			return 1; // done
		}
	}
	else if(txCtxt->nextFldWidth == 2){
		*byte = data; // repeat the byte
		txCtxt->nextFldWidth--;
	}
	else { // zero, start
		txCtxt->pend = txCtxt->pFrame->bytes + txCtxt->pFrame->payloadLength;
		txCtxt->pdata = txCtxt->pFrame->bytes;
		*byte = START_FRAME;

		if (txCtxt->pdata[0] != START_FRAME){// forward look to the next field
		    txCtxt->nextFldWidth = 1;
		}
		else {
			txCtxt->nextFldWidth = 2;
		}
	}
    return 0; // more data to come
}

int Link_Tx_GetBuffer(SerialLink_t *link, uint8_t *byte, uint32_t size, uint32_t *cnt)
{
	int enRes;
	UBaseType_t txMsgCnt;
	uint8_t *endp = byte + size;
	uint8_t *startp = byte;

	if ((link == NULL) || (byte == NULL) || (cnt == NULL) || (size == 0) ){ return SER_BAD_ARGUMENT; }

	*cnt = 0;

	do {
		// block only if the byte count is 0
        if (link->txCtxt.pFrame != NULL)
		{
        	do {
        		enRes = Link_GetNextEncodedByte(&link->txCtxt, byte);
        	    byte++;
        	    if (enRes == 1) { // done or so unreleased frame
        		    FRAME_USE_CNT_DEC(link->txCtxt.pFrame);
        		    link->txCtxt.pFrame = NULL;
        		    break;
        	    }
        	} while(byte < endp);
		}
        *cnt = byte - startp;

        if (byte == endp){
        	return SER_SUCCESS;
        }

		if (link->txCtxt.pFrame == NULL)
		{
			// block only if the byte count is 0
			if ((*cnt == 0) || (txMsgCnt = uxQueueMessagesWaiting(link->txFrmQue)) )
			{
			    if (xQueueReceive(link->txFrmQue, &link->txCtxt.pFrame, portMAX_DELAY) != pdTRUE)
			    {
				    link->txCtxt.pFrame = NULL;
				    return SER_RESOURCE_QUEUE_RECEIVE_ERROR;
			    }
			    Link_InitFrameTxCtxt(link);
			}
			else {
				// there is some data in the spi buffer and no more queued frames
				// just send what you got
				break;
            }
		}

	} while((byte < endp) && (link->txCtxt.pFrame != NULL));
	*cnt = byte - startp;
	return SER_SUCCESS;
}

int Link_Tx_GetBufferNonBlocking(SerialLink_t *link, uint8_t *byte, uint32_t size, uint32_t *cnt)
{
	int enRes;
	UBaseType_t txMsgCnt;
	uint8_t *endp = byte + size;
	uint8_t *startp = byte;

	if ((link == NULL) || (byte == NULL) || (cnt == NULL) || (size == 0) ){ return SER_BAD_ARGUMENT; }

	*cnt = 0;

	do {
		// block only if the byte count is 0
        if (link->txCtxt.pFrame != NULL)
		{
        	do {
        		enRes = Link_GetNextEncodedByte(&link->txCtxt, byte);
        	    byte++;
        	    if (enRes == 1) { // done or some unreleased frame
        		    FRAME_USE_CNT_DEC(link->txCtxt.pFrame);
        		    link->txCtxt.pFrame = NULL;
        		    break;
        	    }
        	} while(byte < endp);
		}
        *cnt = byte - startp;
        if (byte == endp){
        	return SER_SUCCESS;
        }
		if (link->txCtxt.pFrame == NULL)
		{
			txMsgCnt = uxQueueMessagesWaiting(link->txFrmQue);
			if ( txMsgCnt > 0 )
			{
			    if (xQueueReceive(link->txFrmQue, &link->txCtxt.pFrame, portMAX_DELAY) != pdTRUE)
			    {
				    link->txCtxt.pFrame = NULL;
				    return SER_RESOURCE_QUEUE_RECEIVE_ERROR;
			    }
			    Link_InitFrameTxCtxt(link);
			}
		}

	} while((byte < endp) && (link->txCtxt.pFrame != NULL));
	*cnt = byte - startp;
	return SER_SUCCESS;
}


#if 0
int Link_Tx_GetBufferFromIsr(SerialLink_t *link, uint8_t *byte, uint32_t size, uint32_t *cnt, BaseType_t *pxTaskWoken)
{
	int res = SER_SUCCESS;
	int enRes;
	UBaseType_t txMsgCnt;
	uint8_t *endp = byte + size;
	uint8_t *startp = byte;

	if ((link == NULL) || (byte == NULL) || (cnt == NULL) || (size == 0) ){ return SER_BAD_ARGUMENT; }

	*cnt = 0;

	do {
		// block only if the byte count is 0
        if (link->txCtxt.pFrame != NULL)
		{
        	do {
        		enRes = Link_GetNextEncodedByte(&link->txCtxt, byte);
        	    byte++;
        	    if (res == 1) { // done or some unreleased frame
        		    FRAME_USE_CNT_DEC(link->txCtxt.pFrame);
        		    link->txCtxt.pFrame = NULL;
        		    break;
        	    }
        	} while(byte < endp);
		}
        *cnt = byte - startp;

		if (link->txCtxt.pFrame == NULL)
		{
			// block only if the byte count is 0
			if ((*cnt == 0) || (txMsgCnt = uxQueueMessagesWaiting(link->txFrmQue)) )
			{
			    if (xQueueReceive(link->txFrmQue, &plink->txCtxt.pFrame, portMAX_DELAY) != pdTRUE)
			    {
				    link->txCtxt.pFrame = NULL;
				    return SER_RESOURCE_QUEUE_RECEIVE_ERROR;
			    }
			    Link_InitFrameTxCtxt(link);
			}
			else {
				// there is some data in the spi buffer and no more queued frames
				// just send what you got
				break;
            }
		}

	} while((byte < endp) && (link->txCtxt.pFrame != NULL));
	*cnt = byte - startp;
	return res;
}
#endif


int Link_HandleFrame(Frame_t *inFrame)
{
    // check if the packet is to be handled by the link layer

	if (GET_LINK_VERSION(inFrame) != PKT_VERSION_02){
		return SER_LINK_INVALID_PKT_VERSION;
	}
	if (GET_LINK_TYPE(inFrame) == PKT_TYPE_LINK_LAYER_SRV){
		//print some warning
		return SER_LINK_PKT_NOT_SUPPORTED;
	}

	return Net_HandlePacket(inFrame);
}

/*
 * Received packet path
 * */

void Link_Service(void *pvParameters)
{
    Frame_t *frame;// = Link_GetFrame(link);
    uint32_t frmcrc, dataLen;
    uint32_t calcCrc;
    SerialLink_t *link = (SerialLink_t *)pvParameters;

    for(;;) {
        //Dbg_SerialCommTrace("Enter");
        if(xQueueReceive(link->rxFrmQue, &frame, portMAX_DELAY) != pdTRUE){
    	    return;
        }
        //Dbg_SerialCommTrace("Got Frame");
        frmcrc = GET_FRM_CRC(frame);
        dataLen = GET_FRM_DATA_LEN(frame);

        calcCrc = Crc32(0, frame->data, dataLen);
        if ( calcCrc == frmcrc ) {
    	    Link_HandleFrame(frame);
        }
   	    FRAME_USE_CNT_DEC(frame);
   	    //Dbg_SerialCommTrace("Exit");
    }
}

//--------------------------------------------------------------------------------------------
// Network Layer protocol implementation
//--------------------------------------------------------------------------------------------

void Net_Init(uint8_t my_addr)
{
	s_NetCfg.hostId = my_addr; // invalid host id, set it later
	s_NetCfg.numRoutes = 0;
}

int Net_SetHostId(uint8_t hostId)
{
	if (pdFALSE == xSemaphoreTake(s_NetLibSemaphore, 0))
	{
		return SER_LIB_SEM_TAKE;
	}
	s_NetCfg.hostId = hostId;
	if (pdFALSE == xSemaphoreGive(s_NetLibSemaphore))
	{
	    return SER_LINK_TX_SEM_GIVE;
	}
	return SER_SUCCESS;
}

NetPath * Net_SetRoute(uint8_t dest, SerialLink_t *plink)
{
	NetPath *np;
	if (s_NetCfg.numRoutes >= NET_PATHS_MAX){
		return NULL;
	}
	np = &s_NetCfg.routes[s_NetCfg.numRoutes];
    np->dest = dest;
    np->plink = plink;
    s_NetCfg.numRoutes++;

    return np;
}

int Net_HandlePacket(Frame_t *inFrame)
{
	int res = SER_SUCCESS;

	if ( GET_NET_DEST(inFrame) == s_NetCfg.hostId){ // packet destination is this host
		if ( GET_NET_PKT_TYPE(inFrame) == PKT_TYPE_NET_LAYER_DATA){
			res = Tran_HandlePacket(inFrame);
		}
		else {
		    // for now, later net ping and reply may be implemented here
		    return SER_NET_PKT_NOT_SUPPORTED;
		}
	}
	else{ // check the if a route exists for that destination
		res = SER_NET_INVALID_ROUTE;
		for(int i = 0; i < s_NetCfg.numRoutes; i++)
		{
			NetPath *path = &s_NetCfg.routes[i];
			// if a route exists to the packet destination
			if (path->dest == GET_NET_DEST(inFrame) ){
				res = Link_SendFrame(path->plink, inFrame);

#ifdef DEBUG_SERIAL_COM_NET_SEND
	log_frame(inFrame, DIR_RX);
#endif
				break;
			}
		}
	}

	return res;
}

int Net_SendPacket(Frame_t *frame)
{
	int res = SER_SUCCESS;

	SET_LINK_VERSION_TYPE(frame, PKT_VERSION_02, PKT_TYPE_LINK_LAYER_DATA);
	// configure the net header
	SET_NET_SRC_DEV(frame, s_NetCfg.hostId); // the source is the host id
	SET_NET_PKT_TYPE(frame,PKT_TYPE_NET_LAYER_DATA);

#ifdef DEBUG_SERIAL_COM
	log_frame(frame, DIR_TX);
#endif
	res = SER_NET_INVALID_ROUTE;
	for(int i = 0; i < s_NetCfg.numRoutes; i++)
	{
		NetPath *path = &s_NetCfg.routes[i];
		if ( path->dest == GET_NET_DEST(frame) )
		{ // if a route exists to the packet destination then use it
			res = Link_SendFrame(path->plink, frame);
			break;
		}
	}

	return res;
}

//--------------------------------------------------------------------------------------------
// Transport Layer protocol implementation
//     Transport layer guarantees reliable transport of packets (retransmits the packet, etc)
//--------------------------------------------------------------------------------------------
#if 0
void Tran_PIList_Init()
{
	for(int i = 0; i < TRAN_PKT_INFO_ARRAY_SIZE-1; i++){
		s_TrCfg.PktList.PktInfoArr[i].next = &(s_TrCfg.PktList.PktInfoArr[i+1]);
	}
	// terminate the list
	s_TrCfg.PktList.PktInfoArr[TRAN_PKT_INFO_ARRAY_SIZE-1].next = NULL;
	s_TrCfg.PktList.pFreeListHead = s_TrCfg.PktList.PktInfoArr;
	s_TrCfg.PktList.pWaitListHead = NULL;
}
#endif

//void Tran_WaitTimerCallback( TimerHandle_t xTimer);

int Tran_Init()
{
	s_TrCfg.NumEndPoints = 0;

	s_TrCfg.error = 0;
	s_TrCfg.stickyError = 0;
	s_TrCfg.nextSessionId = 2;

	s_TrCfg.accessSemaphore = xSemaphoreCreateMutex();
	if (s_TrCfg.accessSemaphore == NULL){
		return SER_TRAN_ACCESS_SEM_CREATE;
	}

	for(int i = 0; i < CFG_END_POINT_CNT_MAX; i++){
		s_TrCfg.EpArr[i] = NULL;
	}

	for(int i = 0; i < HOST_END_POINT_USE_DWORDS; i++){
		s_TrCfg.EndPointUse[i] = 0;
	}
	for(int i = 0; i < HOST_PORT_USE_DWORDS; i++){
		s_TrCfg.PortUse[i] = 0;
	}

    return SER_SUCCESS;
}


void Tran_Deinit(void)
{
	s_TrCfg.NumEndPoints = 0;
	vSemaphoreDelete(s_TrCfg.accessSemaphore);
}

int Tran_AccessTake(){
	int res = SER_SUCCESS;
	//Dbg_SerialCommTrace("Entry");
	if(pdFALSE == xSemaphoreTake(s_TrCfg.accessSemaphore, portMAX_DELAY)){
		s_TrCfg.stickyError |= TRAN_ERROR_ACCESS_SEM_TAKE;
		res = SER_TRAN_ACCESS_SEM_TAKE;
	}
	//Dbg_SerialCommTrace("Exit");
	return res;
}

int Tran_AccessGive()
{
	int res = SER_SUCCESS;
	//Dbg_SerialCommTrace("Entry");
	if (pdFALSE == xSemaphoreGive(s_TrCfg.accessSemaphore)){
		s_TrCfg.stickyError |= TRAN_ERROR_ACCESS_SEM_GIVE;
		res = SER_TRAN_ACCESS_SEM_GIVE;
	}
	//Dbg_SerialCommTrace("Exit");
	return res;
}




int Tran_SendPacket(Frame_t *outFrame)
{
	//TickType_t txTime;
	//uint32_t msgSize;

	int res = SER_SUCCESS;
	SET_TRAN_TX_TIME(outFrame, xTaskGetTickCount());
	res = Net_SendPacket(outFrame);

	return res;
}


bool Tran_IsValidEndPointFrame(ConnectionEndPoint_t *ep, Frame_t *rxFrame)
{
	if ((ep->hostPort == GET_TRAN_DEST_PORT(rxFrame))
			&& (ep->remotePort == GET_TRAN_SRC_PORT(rxFrame))
			&& (ep->sessionId == GET_TRAN_SESSION_ID(rxFrame)))
	{
        return true;
	}
	else {
		return false;
	}
}


void Tran_NotifyEpEvent(EventGroupHandle_t he, EpEventEnum_t epEvt)
{
	xEventGroupSetBits(he, epEvt);
}

void Tran_PostEpFrame(QueueHandle_t qh, Frame_t *frame)
{
	Frame_t *tmpFrm;
	BaseType_t res;

	//Dbg_SerialCommTrace("Entry");
	// when queuing frames we need to increment the use count since
	// another thread will be handling them in addition to the current
	// so it is the responsibility of the queue handling thread to decrement
	// use count as well as the calling thread
	FRAME_USE_CNT_INC(frame);

	res = xQueueSend(qh, &frame,  10);
	if (res != pdTRUE){
		//Dbg_SerialCommInfo("xQueueSend(%p) failed", qh);
		// remove the first queued message
		if (xQueueReceive(qh, &tmpFrm, 1) == pdTRUE) {
			// if we are throwing away a frame, dispose it properly
			FRAME_USE_CNT_DEC(tmpFrm);
		}

		res = xQueueSend(qh, &frame,  10);
	}

	if (res != pdTRUE) {
		//Dbg_SerialCommError("failed to queue frame : %x", frame);
		FRAME_USE_CNT_DEC(frame);
	}
	//Dbg_SerialCommTrace("Exit");
}



void Tran_PacketConfig(ConnectionEndPoint_t *ep,
		               uint8_t pktType,
					   uint8_t *buf,
					   uint32_t size,
					   Frame_t *frame)
{
	uint8_t *pktBytes;
	uint16_t frmByteCnt;

	frmByteCnt = PKT_SIZE(size);

	SET_FRM_BYTE_CNT(frame, frmByteCnt);

	SET_NET_DEST_DEV(frame, ep->remoteDev);

	SET_TRAN_TYPE(frame, pktType);
	SET_TRAN_DEST_SLOT(frame, ep->remoteSlot);
	SET_TRAN_SRC_SLOT(frame, ep->hostSlot);
	SET_TRAN_SRC_PORT(frame, ep->hostPort);
	SET_TRAN_DEST_PORT(frame, ep->remotePort);

	SET_TRAN_SESSION_ID(frame, ep->sessionId);

	SET_TRAN_TX_CNT(frame, ep->hostTxByteCnt);
	SET_TRAN_RX_CNT(frame, ep->hostRxByteCnt);

	// copy the bytes into the packet
    if (size) {
	    pktBytes = GET_TRAN_MSG_BYTES(frame);
	    memcpy(pktBytes, buf, size);
    }
}

int Tran_SendCloseSessionPkt(ConnectionEndPoint_t *ep)
{
	Frame_t  *frame;
	int res = 0;
	uint8_t  srvData[1] = {PKT_TYPE_TRANSPORT_LAYER_SRV_CLOSE_SESSION};

	//Dbg_SerialCommTrace("Entry");
    FRAME_GET(&frame,OUT_RESOURCE_TIMEOUT_MS);
	if (NULL == frame){
		return SER_RESOURCE_FREE_FRM_EMPTY;
	}

	Tran_PacketConfig(ep, PKT_TYPE_TRANSPORT_LAYER_SRV, srvData, 1, frame); // zero message length
	res = Tran_SendPacket(frame);
	FRAME_USE_CNT_DEC(frame);
	//Dbg_SerialCommTrace("Exit");

	return res;
}





bool Tran_IsPortNumberTaken(uint8_t portNumber)
{
	int wordIndex = portNumber/32;
	int bitIndex = portNumber % 32;

	if ((s_TrCfg.PortUse[wordIndex] >> bitIndex) & 1 ){ // number in use
		return true;
	}
	return false;
}

bool Tran_TakePortNumber(uint8_t portNumber)
{
	int wordIndex = portNumber/32;
	int bitIndex = portNumber % 32;
	uint32_t bitPos = 1 << bitIndex;

	if (s_TrCfg.PortUse[wordIndex] & bitPos){ // number in use
		return false;
	}
	s_TrCfg.PortUse[wordIndex] |= bitPos;
	return true;
}
void Tran_ReleasePortNumber(uint8_t portNumber)
{
	int wordIndex = portNumber/32;
	int bitIndex = portNumber % 32;
	uint32_t bitPos = 1 << bitIndex;

	s_TrCfg.PortUse[wordIndex] &= ~bitPos;
}

void Tran_ReleaseEpSlot(uint8_t index)
{
	int wordIndex = index/32;
	int bitIndex = index % 32;
	uint32_t bitPos = 1 << bitIndex;

	s_TrCfg.EndPointUse[wordIndex] &= ~bitPos;
	s_TrCfg.EpArr[index] = NULL;
}

void Tran_TakeEpSlot(uint8_t index, ConnectionEndPoint_t *ep)
{
	int wordIndex = index/32;
	int bitIndex = index % 32;
	uint32_t bitPos = 1 << bitIndex;

	s_TrCfg.EndPointUse[wordIndex] |= bitPos;
	s_TrCfg.EpArr[index] = ep;
}

bool Tran_FindNextFreeSlot(uint32_t *entMap, int numEntries, uint8_t *index)
{
	int i;
	int wordIndex;
	int bitIndex;
	uint32_t map;

	for (i=0; i < numEntries; i++){
	   	if (entMap[i] != 0xFFFFFFFF){
	   		break;
	   	}
	}
	if (i == numEntries){
		return false;
	}
	map = entMap[i];
	wordIndex = i;

	for (i = 0; i < 32; i++){
	   if ( (map & (1<<i)) == 0){
		   break;
	   }
	}
	bitIndex = i;

	*index = (wordIndex * 32) + bitIndex;

	return true;
}

// finds the open server and acceptor
void Tran_FindOpenServerAcceptor(uint8_t hostPort,
		                         uint8_t remoteDev,
								 uint8_t remotePort,
								 ConnectionEndPoint_t **ppSrvEp,
		                         ConnectionEndPoint_t **ppAcceptorEp)
{
    ConnectionEndPoint_t *ep;
    ConnectionEndPoint_t *srv = NULL;
    ConnectionEndPoint_t *acceptor = NULL;

    *ppAcceptorEp = NULL;
    *ppSrvEp      = NULL;

    //Dbg_SerialCommTrace("Entry");
    if (!Tran_IsPortNumberTaken(hostPort)){ // there is no server listening
    	//Dbg_SerialCommWarning("Port %d not in use", hostPort);
    	return;
    }

    for (int i = 0; i < CFG_END_POINT_CNT_MAX; i++){
    	ep = s_TrCfg.EpArr[i];
    	if (ep == NULL){
    		continue;
    	}
    	// there may be many acceptors handling requests for the same host port
    	// but there can be only one acceptor handling connection to remote port
    	if (ep->hostPort == hostPort) {
    		if (ep->epType == EP_TYPE_SERVER) {
    			srv = ep;
    		}
    		else if (   (ep->epType == EP_TYPE_ACCEPTOR)
    				 && (ep->remoteDev == remoteDev)
    				 && (ep->remotePort == remotePort) )
    		{
    			acceptor = ep;
    		}
    	}
    }
    if ( (acceptor == NULL) && (srv != NULL) && (srv->next != NULL) ){
    	// find a free acceptor, from the servers linked list
    	acceptor = srv->next;
    	srv->next = acceptor->next;
    	acceptor->next = NULL;
    	if (srv->next == NULL){
    		srv->epState = EP_STATE_IDLE;
    	}
    }
    //else {
    //	Dbg_SerialCommWarning("srv %p, accpt %p", srv, acceptor);
    //}

    *ppSrvEp = srv;
    *ppAcceptorEp = acceptor;
    //Dbg_SerialCommTrace("Exit");
}

void Tran_FindClientOrAcceptor( uint8_t hostPort,
		                         uint8_t remoteDev,
								 uint8_t remotePort,
								 ConnectionEndPoint_t **ppEp)
{
    ConnectionEndPoint_t *ep;
    *ppEp      = NULL;
    //Dbg_SerialCommTrace("Entry");
    for (int i = 0; i < CFG_END_POINT_CNT_MAX; i++){
    	ep = s_TrCfg.EpArr[i];
    	if ((ep == NULL) || (ep->epType == EP_TYPE_SERVER)){
    		continue;
    	}
    	// there may be many acceptors handling requests for the same host port
    	// but there can be only one acceptor handling connection to remote port
    	if ( (ep->hostPort == hostPort)
    		&& (ep->remoteDev == remoteDev)
			&& (ep->remotePort == remotePort))
    	{
    		*ppEp = ep;
    		break; // found the endpoint
    	}
    }
    //Dbg_SerialCommTrace("Exit");
}

int Tran_AddEndPoint(ConnectionEndPoint_t *newEp)
{
	uint8_t slotIndex;
	int res = SER_SUCCESS;

	if ((newEp->epType != EP_TYPE_SERVER) && (newEp->epType != EP_TYPE_CLIENT)){
		return SER_BAD_ARGUMENT;
	}
	//Dbg_SerialCommTrace("Entry");
	Tran_AccessTake();
	do{
		if ( Tran_IsPortNumberTaken(newEp->hostPort) ){ // number in use
			//Dbg_SerialCommWarning("Port %d taken req %p can't be completed", newEp->hostPort, newEp);
			res = SER_TRAN_END_POINT_EXISTS;
			break;
		}

		if(!Tran_FindNextFreeSlot(s_TrCfg.EndPointUse, HOST_END_POINT_USE_DWORDS, &slotIndex)){
			res = SER_TRAN_END_MAX_CNT_OVERFLOW;
			break;
		}

		Tran_TakeEpSlot(slotIndex, newEp);
		Tran_TakePortNumber(newEp->hostPort);
		newEp->hostSlot = slotIndex;
		newEp->epState = EP_STATE_IDLE;
		newEp->cookie  = EP_COOKIE;
		newEp->next = NULL;

		//newEp->epType = epType;
		//Dbg_SerialCommInfo("Port %d slot %d use req %p type:%d", newEp->hostPort, slotIndex, newEp, newEp->epType);
	} while(0);
	Tran_AccessGive();
	//Dbg_SerialCommTrace("Exit");

	return res;
}

int Tran_AddAcceptorEndPoint(ConnectionEndPoint_t *servEp, ConnectionEndPoint_t *acceptEp)
{
	uint8_t slotIndex;
	int res = SER_SUCCESS;


	if((NULL == servEp) || (NULL == acceptEp)){
		return SER_BAD_ARGUMENT;
	}
	if (( servEp->epType != EP_TYPE_SERVER ) || (acceptEp->epType != EP_TYPE_ACCEPTOR)){
		return SER_BAD_ARGUMENT;
	}
	if (servEp->hostSlot >= CFG_END_POINT_CNT_MAX){
		return SER_BAD_ARGUMENT;
	}
	//Dbg_SerialCommTrace("Entry");
	do {
		// check if server has been added
        if ( s_TrCfg.EpArr[servEp->hostSlot] != servEp )
        {
        	res = SER_BAD_ARGUMENT;
        	break;
        }
		if(!Tran_FindNextFreeSlot(s_TrCfg.EndPointUse, HOST_END_POINT_USE_DWORDS, &slotIndex)){
			res = SER_TRAN_END_MAX_CNT_OVERFLOW;
			break;
		}
		Tran_TakeEpSlot(slotIndex, acceptEp);
		acceptEp->hostPort = servEp->hostPort;
		acceptEp->hostSlot = slotIndex;
		acceptEp->remoteDev = DEVICE_MAX; // don't make it eligible for search to find it
		acceptEp->remotePort = PORT_MAX;  // don't make it eligible for search to find it
		acceptEp->next = servEp->next;
		acceptEp->epState = EP_STATE_IDLE;
		acceptEp->cookie  = EP_COOKIE;
		servEp->next = acceptEp;
		servEp->epState = EP_STATE_LISTEN;
		//Dbg_SerialCommInfo("Acceptor: Port %d slot %d use req %p type:%d", acceptEp->hostPort, slotIndex, acceptEp, acceptEp->epType);
	} while(0);
	//Dbg_SerialCommTrace("Exit");

	return res;
}

void Tran_RemoveEndPointImpl(ConnectionEndPoint_t *oldEp)
{
	//Dbg_SerialCommTrace("Entry, ep %p", oldEp);
	if ( (oldEp->epType == EP_TYPE_SERVER) || (oldEp->epType == EP_TYPE_CLIENT) ){
		Tran_ReleasePortNumber(oldEp->hostPort);
	}

	Tran_ReleaseEpSlot(oldEp->hostSlot);
	oldEp->cookie  = 0;
}

void Tran_RemoveEndPoint(ConnectionEndPoint_t *oldEp)
{
	//Dbg_SerialCommTrace("Entry");
	Tran_AccessTake();
	Tran_RemoveEndPointImpl(oldEp);
	Tran_AccessGive();
}

//----------------------------------------------------------------------------------------------------------------
// SESSION MANAGEMENT
//----------------------------------------------------------------------------------------------------------------

int Tran_CloseSession(ConnectionEndPoint_t *ep)
{
	int res = SER_SUCCESS;

	//Dbg_SerialCommTrace("Entry");
	EP_DisableSession_Internal(ep);
	if (ep->epType != EP_TYPE_SERVER) {
	    Tran_SendCloseSessionPkt(ep);
	}
	if (ep->cookie == EP_COOKIE) {
		Tran_RemoveEndPoint(ep);
	}
	//Dbg_SerialCommTrace("Exit");

	return res;
}

int Tran_SendSessionInfo(ConnectionEndPoint_t *ep)
{
	Frame_t  *frame;
	int res = 0;
	uint8_t  srvData[2] = {PKT_TYPE_TRANSPORT_LAYER_SRV_SESSION_INFO, 0};

	//Dbg_SerialCommTrace("Entry");
	FRAME_GET(&frame,OUT_RESOURCE_TIMEOUT_MS);
	if (NULL == frame){
		return SER_RESOURCE_FREE_FRM_EMPTY;
	}

	srvData[1] = ep->hostSlot;
	Tran_PacketConfig(ep, PKT_TYPE_TRANSPORT_LAYER_SRV, srvData, 2, frame); // zero message length
	res = Tran_SendPacket(frame);
	FRAME_USE_CNT_DEC(frame);
	//Dbg_SerialCommTrace("Exit");

	return res;
}

int Tran_SendSessionInfoConfirmation(ConnectionEndPoint_t *ep)
{
	Frame_t  *frame;
	int res = 0;
	uint8_t  clientData[1] = {PKT_TYPE_TRANSPORT_LAYER_SRV_SESSION_INFO_CONF};

	//Dbg_SerialCommTrace("Entry");
	FRAME_GET(&frame,OUT_RESOURCE_TIMEOUT_MS);
	if (NULL == frame){
		return SER_RESOURCE_FREE_FRM_EMPTY;
	}

	Tran_PacketConfig(ep, PKT_TYPE_TRANSPORT_LAYER_SRV, clientData, 1, frame); // zero message length
	res = Tran_SendPacket(frame);
	FRAME_USE_CNT_DEC(frame);
	//Dbg_SerialCommTrace("Exit");

	return res;
}

int Tran_SendBadSessionNack(uint8_t hostSlot,uint8_t destSlot, uint8_t hostPort, uint8_t remotePort, uint8_t remoteDev, uint8_t sessionId)
{
	Frame_t  *frame;
	uint8_t *pktBytes;
	uint16_t frmByteCnt;
	int res = 0;

	FRAME_GET(&frame, OUT_RESOURCE_TIMEOUT_MS);
	if (NULL == frame){
		return SER_RESOURCE_FREE_FRM_EMPTY;
	}

	frmByteCnt = PKT_SIZE(1); // 1 byte of data

	SET_FRM_BYTE_CNT(frame, frmByteCnt);

	SET_NET_DEST_DEV(frame, remoteDev);

	SET_TRAN_TYPE(frame, PKT_TYPE_TRANSPORT_LAYER_SRV);
	SET_TRAN_DEST_SLOT(frame, destSlot);
	SET_TRAN_SRC_SLOT(frame, hostSlot);
	SET_TRAN_SRC_PORT(frame, hostPort);
	SET_TRAN_DEST_PORT(frame, remotePort);

	SET_TRAN_SESSION_ID(frame, sessionId);

	SET_TRAN_TX_CNT(frame, 0);
	SET_TRAN_RX_CNT(frame, 0);

	// copy the bytes into the packet
    pktBytes = GET_TRAN_MSG_BYTES(frame);
	pktBytes[0] = PKT_TYPE_TRANSPORT_LAYER_SRV_BAD_SESSION;
	res = Tran_SendPacket(frame);
	FRAME_USE_CNT_DEC(frame);

	return res;
}


void Tran_HandleBadSessionNack(Frame_t *inFrame)
{
	ConnectionEndPoint_t *ep;
	uint8_t hostPort    = GET_TRAN_DEST_PORT(inFrame); // who is the intended recipient
	uint8_t remotePort  = GET_TRAN_SRC_PORT(inFrame);
	uint8_t remoteDev   = GET_NET_SRC_DEV(inFrame);
	uint8_t sessionId   = GET_TRAN_SESSION_ID(inFrame);

	//Dbg_SerialCommTrace("Entry");
	Tran_FindClientOrAcceptor(hostPort, remoteDev, remotePort, &ep);
	if (ep == NULL) {
		//Dbg_SerialCommInfo("Bad endpoint");
		//Dbg_SerialCommTrace("Exit");
		return;
	}
	else if (ep->sessionId != sessionId) {
		//Dbg_SerialCommInfo("Bad session host %d, remote %d", ep->sessionId, sessionId);
		//Dbg_SerialCommTrace("Exit");
		return;
	}

	EP_DisableSession_Internal(ep); // review this
	Tran_RemoveEndPointImpl(ep);
	//Dbg_SerialCommTrace("Exit");
}

// DATA MANAGEMENT

// Transport does only retransmission, it won't do packet ordering
void Tran_HandleDataPacket(Frame_t *inFrame)
{
	//TransportPacket_t *tranPkt = &inFrame->transport;
	uint8_t destSlotId = GET_TRAN_DEST_SLOT(inFrame);
	uint8_t srcSlotId = GET_TRAN_SRC_SLOT(inFrame);
	ConnectionEndPoint_t *ep = NULL;
	uint8_t hostPort    = GET_TRAN_DEST_PORT(inFrame); // who is the intended recipient
	uint8_t remotePort  = GET_TRAN_SRC_PORT(inFrame);
	uint8_t remoteDev   = GET_NET_SRC_DEV(inFrame);
	uint8_t sessionId   = GET_TRAN_SESSION_ID(inFrame);
	int rxMsgSize;
	uint32_t remoteTxCnt, remoteRxCnt;

	if (destSlotId < CFG_END_POINT_CNT_MAX){
		ep = s_TrCfg.EpArr[destSlotId];
	}
    if (ep == NULL){
#ifdef DEBUG_SERIAL_COM
    	dl_print("[TRAN-DATA]: Bad slot:[%d]", destSlotId);
#endif
    	Tran_SendBadSessionNack(destSlotId, srcSlotId, hostPort, remotePort, remoteDev, sessionId);
    	return;
    }
	if (
			  ((ep->epType != EP_TYPE_CLIENT) && (ep->epType != EP_TYPE_ACCEPTOR) )// wrong end point type
			|| (ep->sessionId != sessionId)
			|| (ep->hostPort != hostPort)
			|| (ep->remotePort != remotePort)
			|| (ep->remoteDev != remoteDev)

		)
	{

		//Dbg_SerialCommWarning("[Bad session:%d] ep %p sesid:%d type:%d, host:%d,%d remote: %d", sessionId, ep, ep->sessionId, ep->epType, hostPort, slotId, remotePort);
		Tran_SendBadSessionNack(destSlotId, srcSlotId, hostPort, remotePort, remoteDev, sessionId);
		return;
	}

	//Dbg_SerialCommTrace("Entry");
	switch(ep->epState){
        case EP_STATE_IDLE:
#ifdef DEBUG_SERIAL_COM
        	dl_print("[TRAN-DATA]: End Point Idle");
#endif
        	Tran_SendBadSessionNack(destSlotId, srcSlotId, hostPort, remotePort, remoteDev, sessionId);
        	break;
        case EP_STATE_OPEN_REQ:
        	if (ep->epType == EP_TYPE_ACCEPTOR) {
        		// can't process data until a session confirmation has been received
        		Tran_SendSessionInfo(ep);
        	}
        	break;
        case EP_STATE_OPEN:
		    rxMsgSize =  GET_TRAN_MSG_SIZE(inFrame);
		    remoteTxCnt = GET_TRAN_TX_CNT(inFrame);
		    remoteRxCnt = GET_TRAN_RX_CNT(inFrame);
		    // notification that the remote received the sent packet
		    if ( (ep->txBytesInFlight > 0) && (remoteRxCnt == (ep->hostTxByteCnt + ep->txBytesInFlight)) ){
		    	ep->hostTxByteCnt += ep->txBytesInFlight;
		    	ep->txBytesInFlight = 0;
		    	Tran_NotifyEpEvent(ep->txEvent, EPEVT_PKT_SENT);
		    }
		    if ( rxMsgSize > 0 )
		    {
				if ( remoteTxCnt == ep->hostRxByteCnt )
				{
					ep->hostRxByteCnt += rxMsgSize;
					// record the timestamp of last data arrival
					ep->lastDataArrivalTime = GET_TRAN_TX_TIME(inFrame);
					ep->lastDataArrivalSize = rxMsgSize;

					Tran_SendAckPacket(ep);
					Tran_PostEpFrame(ep->rxMsgQue, inFrame);
					Tran_NotifyEpEvent(ep->rxEvent, EPEVT_PKT_RCV);
				}
				else
				{
					if (    (remoteTxCnt == (ep->hostRxByteCnt - ep->lastDataArrivalSize))
						 && (ep->lastDataArrivalSize == rxMsgSize)
					   )
					{
					    // this packet has already been acknowledged, check timestamp to make sure
					    // that the next ack is not sent too quickly
					    int timeDiff = TimeDifference(ep->lastDataArrivalTime, GET_TRAN_TX_TIME(inFrame));
					    if ((timeDiff < 0) || (timeDiff >= ep->roundTripTicks)){
						    ep->lastDataArrivalTime = GET_TRAN_TX_TIME(inFrame);
						    Tran_SendAckPacket(ep);
					    }
					}
					else { // wrong session probably
						Tran_SendBadSessionNack(destSlotId, srcSlotId, hostPort, remotePort, remoteDev, sessionId);
					}
				}
	        }
	        // else size 0 msg is ack packet, they don't affect the size of remote tx
            break;

        default:
        	//Dbg_SerialCommWarning("Bad End Point state %d", ep->epState);
        	break;
	}
	//Dbg_SerialCommTrace("Exit");
}

void Tran_HandleSessionInfoPacket(Frame_t *inFrame)
{
	uint8_t slotId = GET_TRAN_DEST_SLOT(inFrame);
	ConnectionEndPoint_t *client = NULL;
    uint8_t hostPort    = GET_TRAN_DEST_PORT(inFrame); // who is the intended recipient
    uint8_t remotePort  = GET_TRAN_SRC_PORT(inFrame);
    uint8_t remoteDev   = GET_NET_SRC_DEV(inFrame);
    uint8_t sessionId   = GET_TRAN_SESSION_ID(inFrame);
    uint8_t *bytes      = GET_TRAN_MSG_BYTES(inFrame);
    uint8_t remoteSlot  = bytes[1];   // put by the server, listener

    if (slotId < CFG_END_POINT_CNT_MAX){
    	client = s_TrCfg.EpArr[slotId];
    }
    if(client == NULL){
    	dl_print("Bad slot:[%d]", slotId);
    	return;
    }

	if (       (client->epType != EP_TYPE_CLIENT) // wrong end point type
			|| (client->hostPort != hostPort)
			|| (client->remotePort != remotePort)
			|| (client->remoteDev != remoteDev)
			)
	{
		dl_print("[Bad session:%d] ep %p sesid:%d type:%d, host:%d,%d remote: %d",
				 sessionId,
				 client,
				 client->sessionId,
				 client->epType,
				 hostPort,
				 slotId,
				 remotePort);
		return;
	}
	//Dbg_SerialCommTrace("Entry");
	switch(client->epState) {
	    case EP_STATE_IDLE:
	    	// fall through, open req was sent
	    case EP_STATE_OPEN_REQ:
	    	client->remoteSlot = remoteSlot; // the acceptor is in this slot
	    	client->sessionId = sessionId;
	    	client->hostTxByteCnt = 0;
	    	client->hostRxByteCnt = 0;
	    	client->epState = EP_STATE_OPEN;
	    	Tran_SendSessionInfoConfirmation(client);
	    	Tran_NotifyEpEvent(client->rxEvent, EPEVT_SESSION_OPEN);
	        break;
	    case EP_STATE_OPEN:
	    	if(client->sessionId == sessionId){
	    	    Tran_SendSessionInfoConfirmation(client);
	    	}
	    	else { // delayed response
	    		if ((client->hostTxByteCnt == 0) && (client->hostRxByteCnt == 0)){
	    		    client->sessionId = sessionId;
	    		    Tran_SendSessionInfoConfirmation(client);
	    		    Tran_NotifyEpEvent(client->rxEvent, EPEVT_SESSION_OPEN);
	    		}
	    		else {
	    			Tran_NotifyEpEvent(client->rxEvent, EPEVT_SESSION_CLOSE_RCV);
	    		}
	    	}
	        break;

	    default:
	    	//Dbg_SerialCommWarning("Bad End Point state %d", client->epState);
	    	break;
	}
	//Dbg_SerialCommTrace("Exit");
}

void Tran_HandleSessionInfoConfirmationPacket(Frame_t *inFrame)
{
	uint8_t slotId = GET_TRAN_DEST_SLOT(inFrame);
	ConnectionEndPoint_t *acceptor = NULL;
    //uint8_t hostPort    = GET_TRAN_DEST_PORT(inFrame); // who is the intended recipient
    //uint8_t remotePort  = GET_TRAN_SRC_PORT(inFrame);
    //uint8_t remoteDev   = GET_NET_SRC_DEV(inFrame);
    uint8_t sessionId   = GET_TRAN_SESSION_ID(inFrame);

    if (slotId < CFG_END_POINT_CNT_MAX){
        acceptor = s_TrCfg.EpArr[slotId];
    }

	if (acceptor == NULL){ // there is no endpoint on this index here
		//Dbg_SerialCommWarning("Invalid acceptor at idx: %d", slotId);
		return;
	}

	//if( (acceptor->epType != EP_TYPE_ACCEPTOR) // wrong end point type
	//		|| (acceptor->hostPort != hostPort)
	//		|| (acceptor->remotePort != remotePort)
	//		|| (acceptor->remoteDev != remoteDev)
	//  )
	//{
	//	Dbg_SerialCommWarning("host: %d,%d,%d type:%d", acceptor->hostPort, acceptor->remotePort, acceptor->remoteDev, acceptor->epType);
	//	return;
	//}
	//Dbg_SerialCommTrace("Entry");
	switch(acceptor->epState) {
	    case EP_STATE_IDLE:
	    	//TODO: send bad session id, this slot has stale info
	    	//Dbg_SerialCommWarning("Bad End Point state %d", acceptor->epState);
	    	break;
	    case EP_STATE_OPEN_REQ:
	    	acceptor->hostTxByteCnt = 0;
	    	acceptor->hostRxByteCnt = 0;
	    	acceptor->epState = EP_STATE_OPEN;
	    	if (acceptor->sessionId == sessionId){
	    	    Tran_NotifyEpEvent(acceptor->rxEvent, EPEVT_SESSION_OPEN);
	    	}
	    	break;
	    case EP_STATE_OPEN:
	    	break;

	    default:
	    	//Dbg_SerialCommWarning("Bad End Point state %d", acceptor->epState);
	    	break;
	}
	//Dbg_SerialCommTrace("Exit");
}

void Tran_HandleSessionClosePacket(Frame_t *inFrame)
{
	uint8_t slotId = GET_TRAN_DEST_SLOT(inFrame);
	ConnectionEndPoint_t *ep = NULL;
    uint8_t hostPort    = GET_TRAN_DEST_PORT(inFrame); // who is the intended recipient
    uint8_t remotePort  = GET_TRAN_SRC_PORT(inFrame);
    uint8_t remoteDev   = GET_NET_SRC_DEV(inFrame);
    uint8_t sessionId   = GET_TRAN_SESSION_ID(inFrame);

    if (slotId < CFG_END_POINT_CNT_MAX){
        ep = s_TrCfg.EpArr[slotId];
    }
    if (ep == NULL) {
#ifdef DEBUG_SERIAL_COM
    	dl_print("[TRAN-CLOSE]: Bad slot:[%d]", slotId);
#endif
    	return;
    }

	if (     //  (ep->epType != EP_TYPE_SERVER) ||  // wrong end point type
			   (ep->hostPort != hostPort)
			|| (ep->remotePort != remotePort)
			|| (ep->remoteDev != remoteDev)
			|| (ep->sessionId != sessionId)
			)
	{
#ifdef DEBUG_SERIAL_COM
		dl_print("[TRAN-CLOSE]: Bad session:%d ep %p sesid:%d type:%d, host:%d,%d remote: %d", sessionId, ep, ep->sessionId, ep->epType, hostPort, slotId, remotePort);
#endif
		return;
	}
	//Dbg_SerialCommTrace("Entry");
	switch(ep->epState) {
	    case EP_STATE_IDLE:
	    	// there is no session to close
	    	//ep->remotePort = PORT_MAX;
	    	//ep->remoteDev = DEVICE_MAX;
	    	//Dbg_SerialCommWarning("Bad End Point state %d", ep->epState);
	    	break;
	    case EP_STATE_OPEN_REQ:
	    	ep->epState = EP_STATE_IDLE;
	    	Tran_NotifyEpEvent(ep->rxEvent, EPEVT_SESSION_CLOSE_RCV);
	    	Tran_NotifyEpEvent(ep->txEvent, EPEVT_SESSION_CLOSE_RCV);
	    	Tran_RemoveEndPointImpl(ep);
	    	break;
	    case EP_STATE_OPEN:
	    	EP_DisableSession_Internal(ep); // review this
	    	Tran_RemoveEndPointImpl(ep);
	    	break;

	    default:
	    	//Dbg_SerialCommWarning("Bad End Point state %d", ep->epState);
	    	break;
	}
	//Dbg_SerialCommTrace("Exit");
}

uint8_t Tran_GetNextSessionId()
{
	uint8_t nextSessionId = s_TrCfg.nextSessionId++;
	if (nextSessionId == 0){
		s_TrCfg.nextSessionId++;
	}
	return s_TrCfg.nextSessionId;
}

void Tran_HandleSessionOpenPacket(Frame_t *inFrame)
{
	ConnectionEndPoint_t *srv, *acceptor;
    uint8_t hostPort    = GET_TRAN_DEST_PORT(inFrame); // who is the intended recipient
    uint8_t remotePort  = GET_TRAN_SRC_PORT(inFrame);
    uint8_t remoteDev   = GET_NET_SRC_DEV(inFrame);
    //uint8_t sessionId   = GET_TRAN_SESSION_ID(inFrame);
    uint8_t *bytes      = GET_TRAN_MSG_BYTES(inFrame);
    uint8_t remoteSlot  = bytes[1];

	// check if any endpoint is listening on this port

    //Dbg_SerialCommTrace("Entry");
    Tran_FindOpenServerAcceptor(hostPort, remoteDev, remotePort, &srv, &acceptor);
    if (acceptor == NULL) // there is no free acceptor
    {
    	dl_print("Can't find server %p, acceptor %p host port:%d remote:%d,%d",
    			srv, acceptor,
    			hostPort, remoteDev, remotePort);
    	return;
    }

    switch(acceptor->epState) {
      case EP_STATE_IDLE:
    	  acceptor->remotePort = remotePort;
    	  acceptor->remoteDev = remoteDev;
    	  acceptor->sessionId = Tran_GetNextSessionId();
    	  acceptor->remoteSlot = remoteSlot;
    	  acceptor->epState = EP_STATE_OPEN_REQ;
    	  Tran_SendSessionInfo(acceptor);
    	  break;
      case EP_STATE_OPEN_REQ:
    	  // do the sessions match, remotePort and hostPort matches, since that is how find finds it
    	  //if ( acceptor->sessionId == sessionId ) {
    	  //	  acceptor->remoteSlot = remoteSlot; // maybe the remote, rebooted
    	  //
    	  //}
    	  //else {
    	  //	  acceptor->epState = EP_STATE_IDLE;
    	  //	  //Tran_RemoveEndPointImpl(acceptor);
    	  //	  //if (srv != NULL){
    	  //	  //    //RETURN THE ACCEPTOR, if server still there
    	  //	//	  Tran_AddAcceptorEndPoint(srv, acceptor);
    	  //	 // }
    	  //  }
    	  Tran_SendSessionInfo(acceptor);
          break;
      case EP_STATE_OPEN:
    	  //if ( acceptor->sessionId != sessionId ) {
    	      //Dbg_SerialCommWarning("Session:%d already opened, host port:%d remote:%d,%d,%d",
    	      //		  acceptor->sessionId, hostPort, remoteDev, remotePort, sessionId);
    	      //Dbg_SerialCommWarning("[Session already opened] ep %p sesid:%d type:%d, host:%d,%d remote: %d,%d", acceptor, acceptor->sessionId, acceptor->epType, hostPort, acceptor->hostSlot, remotePort, remoteSlot);
    	      acceptor->epState = EP_STATE_IDLE;
		      EP_DisableSession_Internal(acceptor);
		      Tran_RemoveEndPointImpl(acceptor);
		      // don't return the acceptor to the server, have the server do
		      // another accept
    	  //}
          break;

      default:
    	  break;
    }
    //Dbg_SerialCommTrace("Exit");
}

void log_frame(Frame_t *pkt, Direction_t dir)
{
	const int STR_SIZE = 300;
	char tmpStr[STR_SIZE+1];
	uint32_t n;
	char *p;
	uint32_t copySize;
	uint8_t pktType = GET_TRAN_PKT_TYPE(pkt);
	uint8_t msgSize = GET_TRAN_MSG_SIZE(pkt);
	uint8_t sessionId = GET_TRAN_SESSION_ID(pkt);
	uint8_t *msg = GET_TRAN_MSG_BYTES(pkt);

	if (dir == DIR_TX){
	    n = sprintf(tmpStr,"[TX:%d]",sessionId);
	}
	else {
		n = sprintf(tmpStr,"[RX:%d]",sessionId);
	}
	p = tmpStr + n;
	n = sprintf(p," %s: t:%08d dest:%d,%d,%d, src: %d,%d,%d tx/rx: %d/%d msg[%d]={",
				GET_TRAN_PKT_TYPE_STR(pkt),
				GET_TRAN_TX_TIME(pkt),
				GET_NET_DEST(pkt),GET_TRAN_DEST_PORT(pkt),GET_TRAN_DEST_SLOT(pkt),
				GET_NET_SRC_DEV(pkt),GET_TRAN_SRC_PORT(pkt),GET_TRAN_SRC_SLOT(pkt),
				GET_TRAN_TX_CNT(pkt),
				GET_TRAN_RX_CNT(pkt),
				msgSize);
	p = p + n;
	if ( pktType == PKT_TYPE_TRANSPORT_LAYER_DATA )
	{
		if (msgSize > 0){
			copySize = min(STR_SIZE - (p - tmpStr - 1), msgSize);// +1 so the last character is copied
	        memcpy(p, msg, copySize);

	        for (int i = 0; i < copySize; i++){
	           if (p[i] == '\n') { p[i] = '.';}
	        }
	        p = p + copySize;
		}
	}
	else {
		uint8_t m0 =0, m1=0;
		if (msgSize > 0){
			m0 = msg[0];
		}
		if (msgSize > 1){
			m1 = msg[1];
		}
		n = sprintf(p,"%d %d", m0, m1);
		p = p + n;
	}

	*p = '}';
	p++;
	*p = '\0';
	tmpStr[STR_SIZE-1] = '\0';
	dl_printstr(tmpStr, p - tmpStr);
}

// Transport does only retransmission, it won't do packet ordering
int Tran_HandlePacket(Frame_t *inFrame)
{
	int res = SER_SUCCESS;
	Packet_t *pkt = &inFrame->pkt;

	uint8_t pktType;


	// take library lock
	pktType = GET_TRAN_PKT_TYPE(inFrame);

#ifdef DEBUG_SERIAL_COM
	log_frame(inFrame, DIR_RX);
#endif
	Tran_AccessTake();


	if ( pktType == PKT_TYPE_TRANSPORT_LAYER_DATA )
	{// handle data packets
		Tran_HandleDataPacket(inFrame);
	}
	else if( pktType == PKT_TYPE_TRANSPORT_LAYER_SRV ) // handle service packets
	{
		if ( pkt->msg[0] == PKT_TYPE_TRANSPORT_LAYER_SRV_CLOSE_SESSION )
		{
			Tran_HandleSessionClosePacket(inFrame);
		}
		else if ( pkt->msg[0] == PKT_TYPE_TRANSPORT_LAYER_SRV_OPEN_SESSION )
		{
			Tran_HandleSessionOpenPacket(inFrame);
		}
		else if ( pkt->msg[0] == PKT_TYPE_TRANSPORT_LAYER_SRV_SESSION_INFO )
		{
			Tran_HandleSessionInfoPacket(inFrame);
		}
		else if ( pkt->msg[0] == PKT_TYPE_TRANSPORT_LAYER_SRV_SESSION_INFO_CONF )
		{
			Tran_HandleSessionInfoConfirmationPacket(inFrame);
		}
		else if ( pkt->msg[0] == PKT_TYPE_TRANSPORT_LAYER_SRV_BAD_SESSION )
		{
			Tran_HandleBadSessionNack(inFrame);
		}
		else {
			//Dbg_SerialCommWarning("Unknown message id %d", pkt->msg[0]);
		}
	}


	Tran_AccessGive();

	return res;
}





int EP_AccessTake(ConnectionEndPoint_t *ep)
{
	int res = SER_SUCCESS;
	//Dbg_SerialCommTrace("Entry");
	if(pdFALSE == xSemaphoreTake(ep->epAccessSemaphore, portMAX_DELAY)){
		res = SER_END_POINT_ACCESS_SEM_TAKE;
	}
	//Dbg_SerialCommTrace("Exit");
	return res;
}
int EP_AccessGive(ConnectionEndPoint_t *ep)
{
	int res = SER_SUCCESS;
	//Dbg_SerialCommTrace("Entry");
	if (pdFALSE == xSemaphoreGive(ep->epAccessSemaphore))
	{
		res = SER_END_POINT_ACCESS_SEM_GIVE;
	}
	//Dbg_SerialCommTrace("Exit");
	return res;
}




int EP_DisableSession_Internal(ConnectionEndPoint_t *ep)
{
	Frame_t *frame;
	bool txActive, rxActive;

	//Dbg_SerialCommTrace("Entry");
	ep->epState = EP_STATE_IDLE;

	rxActive = ep->rxActive;
	if (rxActive){
	    // this will unblock any pending reading
		//Dbg_SerialCommWarning("Disabling session:%d port:%d remote:%d,%d",
		//		ep->sessionId, ep->hostPort, ep->remoteDev,ep->remotePort);
		Tran_NotifyEpEvent(ep->rxEvent, EPEVT_SESSION_CLOSE_RCV);
	}

    // send close session to remote
	// empty rx queue
	while(xQueueReceive(ep->rxMsgQue, &frame, 1) == pdTRUE) {
        FRAME_USE_CNT_DEC(frame);
	}

	// this will unbloc pending tx
	txActive = ep->txActive;
	if (txActive){
	    Tran_NotifyEpEvent(ep->txEvent, EPEVT_SESSION_CLOSE_RCV);
	}

    // empty tx queue
	while(xQueueReceive(ep->txMsgQue, &frame, 1) == pdTRUE) {
        FRAME_USE_CNT_DEC(frame);
	}


    while(ep->rxActive || ep->txActive) {
       vTaskDelay(10);
       rxActive = ep->rxActive;
       txActive = ep->txActive;
    }

	//Dbg_SerialCommTrace("Exit");

	return SER_SUCCESS;
}

void EP_DeinitEndPoint(ConnectionEndPoint_t *ep)
{
	//Dbg_SerialCommTrace("Entry");
	if (ep->epAccessSemaphore != NULL) {
		xSemaphoreGive(ep->epAccessSemaphore);
		vSemaphoreDelete(ep->epAccessSemaphore);
		ep->epAccessSemaphore = NULL;
	}
	if (ep->txEvent != NULL){vEventGroupDelete( ep->txEvent );}
	if (ep->rxEvent != NULL){vEventGroupDelete( ep->rxEvent );}
	if (ep->txMsgQue != NULL){vQueueDelete( ep->txMsgQue );}
	if (ep->rxMsgQue != NULL){vQueueDelete( ep->rxMsgQue );}
	//Dbg_SerialCommTrace("Exit");
}

int EP_InitEndPoint(ConnectionEndPoint_t *ep)
{
    int res = SER_SUCCESS;

    //Dbg_SerialCommTrace("Entry");

	do {
		memset(ep,0, sizeof(ConnectionEndPoint_t));

		ep->epAccessSemaphore = xSemaphoreCreateMutex();
		if (ep->epAccessSemaphore == NULL){
			s_TrCfg.stickyError |= TRAN_ERROR_END_POINT_RESOURCE_CREATE;
			res = SER_END_POINT_ACCESS_SEM_CREATE;
			break;
		}

		ep->rxEvent = xEventGroupCreate();
		if (ep->rxEvent == NULL){
			s_TrCfg.stickyError |= TRAN_ERROR_END_POINT_RESOURCE_CREATE;
			res = SER_END_POINT_RX_EVT_CREATE;
			break;
		}

		ep->txEvent = xEventGroupCreate();
        if (ep->txEvent == NULL){
			s_TrCfg.stickyError |= TRAN_ERROR_END_POINT_RESOURCE_CREATE;
			res = SER_END_POINT_TX_EVT_CREATE;
			break;
		}

		ep->txMsgQue = xQueueCreate(1,sizeof(Frame_t *));
		if (ep->txMsgQue == NULL){
			s_TrCfg.stickyError |= TRAN_ERROR_END_POINT_RESOURCE_CREATE;
			res = SER_END_POINT_TX_QUEUE_CREATE;
			break;
		}

		ep->rxMsgQue = xQueueCreate(1,sizeof(Frame_t *));
		if (ep->rxMsgQue == NULL){
			s_TrCfg.stickyError |= TRAN_ERROR_END_POINT_RESOURCE_CREATE;
			res = SER_END_POINT_RX_QUEUE_CREATE;
			break;
		}

	} while(0);

	if (res != SER_SUCCESS){
		EP_DeinitEndPoint(ep);
	}
	//Dbg_SerialCommTrace("Exit %d", res);

    return res;
}

int EP_InitConnection(Connection_t *conn)
{
	int res = SER_SUCCESS;
	//Dbg_SerialCommTrace("Entry");

    do {
		conn->txSemaphore = xSemaphoreCreateMutex();
		if (conn->txSemaphore == NULL){
			s_TrCfg.stickyError |= TRAN_ERROR_END_POINT_RESOURCE_CREATE;
			res = SER_END_POINT_TX_SEM_CREATE;
            break;
		}

		conn->rxSemaphore = xSemaphoreCreateMutex();
		if (conn->rxSemaphore == NULL){
			s_TrCfg.stickyError |= TRAN_ERROR_END_POINT_RESOURCE_CREATE;
			vSemaphoreDelete(conn->txSemaphore);
			res = SER_END_POINT_RX_SEM_CREATE;
			break;
		}


		res = EP_InitEndPoint(&conn->ep);
		if (res != SER_SUCCESS){
			vSemaphoreDelete(conn->txSemaphore);
			vSemaphoreDelete(conn->rxSemaphore);
			res = SER_END_POINT_RX_SEM_CREATE;
			break;
		}
    } while(0);
    //Dbg_SerialCommTrace("Exit");

    return res;
}

void EP_DeinitConnection(Connection_t *conn)
{
	//Dbg_SerialCommTrace("Entry");
	if (conn->txSemaphore != NULL){
		xSemaphoreGive(conn->txSemaphore);
		vSemaphoreDelete(conn->txSemaphore);
		conn->txSemaphore = NULL;
	}

	if (conn->rxSemaphore != NULL){
		xSemaphoreGive(conn->rxSemaphore);
		vSemaphoreDelete(conn->rxSemaphore);
		conn->rxSemaphore = NULL;
	}

	EP_DeinitEndPoint(&conn->ep);
	//Dbg_SerialCommTrace("Exit");
}


void EP_ConfigureEndPoint(ConnectionEndPoint_t *ep, uint8_t hostPort, uint8_t destDev, uint8_t destPort, EndpointType_t epType)
{
	ep->epState = EP_STATE_IDLE;
	ep->remoteDev = destDev;
	ep->remotePort = destPort;
	ep->hostPort = hostPort;
	ep->epType = epType;

	ep->hostRxByteCnt = 0;
	ep->hostTxByteCnt = 0;
	ep->txBytesInFlight = 0;

	ep->stickyError = 0;
	ep->sessionId = 0;
	ep->roundTripTicks = TRANSPORT_PKT_ROUND_TRIP_MS_MAX;
	ep->remoteSlot = 0; // populated later
	ep->txActive = false;
	ep->rxActive = false;
	ep->cookie = 0;
	//xQueueReset(ep->txMsgQue);
	//xQueueReset(ep->rxMsgQue);
}

int EP_SendClientConnectReq(ConnectionEndPoint_t *ep)
{
	Frame_t  *frame;
    int res = 0;
    uint8_t  clntData[2] = {
    		PKT_TYPE_TRANSPORT_LAYER_SRV_OPEN_SESSION,0
    };

    //Dbg_SerialCommTrace("Entry");
    clntData[1] = ep->hostSlot; // send the client's slot position to the server

	FRAME_GET(&frame,OUT_RESOURCE_TIMEOUT_MS);
    if (NULL == frame){
		return SER_RESOURCE_FREE_FRM_EMPTY;
	}

	Tran_PacketConfig(ep, PKT_TYPE_TRANSPORT_LAYER_SRV, clntData, 2, frame); // zero message length
	res =  Tran_SendPacket(frame);
	FRAME_USE_CNT_DEC(frame);

	return res;
}

EpEventEnum_t EP_WaitForTxEvent(ConnectionEndPoint_t *ep, TickType_t ticks)
{
	EventBits_t ev;
	//Dbg_SerialCommTrace("Entry");
	ev = xEventGroupWaitBits(ep->txEvent,
			                 EPEVT_PKT_SENT|EPEVT_SESSION_CLOSE_RCV|EPEVT_SESSION_OPEN|EPEVT_TX_ERROR,
					         pdTRUE, //should be cleared before returning
					         pdFALSE, // don't wait for all the bits to be set, any will do
							 ticks);
	return ev;
}

EpEventEnum_t EP_WaitForRxEvent(ConnectionEndPoint_t *ep,TickType_t ticks)
{
	EventBits_t ev;
	//Dbg_SerialCommTrace("Entry");
	ev = xEventGroupWaitBits(ep->rxEvent,
			                 EPEVT_PKT_RCV|EPEVT_SESSION_CLOSE_RCV|EPEVT_SESSION_OPEN|EPEVT_TX_ERROR,
					         pdTRUE, //should be cleared before returning
					         pdFALSE, // don't wait for all the bits to be set, any will do
							 ticks);
	return ev;
}



int EP_OpenClientConnection(Connection_t *conn,
		              uint8_t hostPort,
					  uint8_t destDev,
					  uint8_t destPort)
{
	int res = 0;
	bool connOpen = false;
	EpEventEnum_t event;
	int retrys = 5;

	//Dbg_SerialCommTrace("Entry");
	EP_ConfigureEndPoint(&conn->ep, hostPort, destDev, destPort, EP_TYPE_CLIENT);

    res = Tran_AddEndPoint(&conn->ep);
    if (res != SER_SUCCESS){
    	return res;
    }

    while ( !connOpen && (retrys-- > 0) ) {
		res = EP_SendClientConnectReq(&conn->ep);
		if (SER_SUCCESS != res){
			continue;
		}
		event = EP_WaitForRxEvent(&conn->ep, 6 * conn->ep.roundTripTicks);
		if ( event != 0 ){
			if ( EPEVT_SESSION_OPEN == event ) {
				res = SER_SUCCESS;
				connOpen = true;
				break;
			}
			else {
				res = SER_END_POINT_SESSION_ERROR;
				break;
			}
		}
    }
    if ( !connOpen ) {
    	Tran_RemoveEndPoint(&conn->ep);
    	res = SER_END_POINT_SESSION_OPEN_TIMEOUT;
    }
    //Dbg_SerialCommTrace("Exit");
    return res;
}



int EP_OpenServerConnection(Connection_t *conn, uint8_t hostPort)
{
	int res = 0;

	EP_ConfigureEndPoint(&conn->ep, hostPort, DEVICE_MAX, PORT_MAX, EP_TYPE_SERVER);

	res = Tran_AddEndPoint(&conn->ep);
	return res;
}

int EP_AcceptConnection(Connection_t *conn,Connection_t *newConn)
{
	int res = SER_SUCCESS;
	EpEventEnum_t evt;

	if ((conn == NULL) || (newConn == NULL)){
			return SER_BAD_ARGUMENT;
	}
	// check if the server connection is already regitered

	EP_ConfigureEndPoint(&newConn->ep, conn->ep.hostPort, DEVICE_MAX, PORT_MAX, EP_TYPE_ACCEPTOR);

	// add endpoint to the list of endpoints
	Tran_AccessTake();
	res = Tran_AddAcceptorEndPoint(&conn->ep, &newConn->ep);
	Tran_AccessGive();
	if (res != SER_SUCCESS){
	    return res;
	}

	evt = EP_WaitForRxEvent(&newConn->ep, portMAX_DELAY);
	if (evt != EPEVT_SESSION_OPEN){
		Tran_RemoveEndPoint(&newConn->ep);
		if ( evt == 0 ){
			res = SER_END_POINT_RX_TIMEOUT;
		}
		else {
			res = SER_END_POINT_SESSION_ERROR;
		}
	}

	return res;
}

int EP_CloseConnection(Connection_t *conn)
{
	if (conn == NULL){
		return SER_BAD_ARGUMENT;
	}
	if (conn->ep.epState != EP_STATE_OPEN){
        return SER_END_POINT_NOT_OPEN;
	}
	//if (pdFALSE == xSemaphoreTake(conn->txSemaphore, portMAX_DELAY))
	//{
	//	return SER_END_POINT_TX_SEM_TAKE;
	//}
    //if (pdFALSE == xSemaphoreTake(conn->rxSemaphore, 0))
    //{
    //	xSemaphoreGive(conn->txSemaphore); this cause lock up if read is active
    //	return SER_END_POINT_TX_SEM_TAKE;
    //}
    //Dbg_SerialCommWarning("Closing conn session:%d port:%d slot:%d", conn->ep.sessionId, conn->ep.hostPort, conn->ep.hostSlot);

	Tran_CloseSession(&conn->ep);

	//if (pdFALSE == xSemaphoreGive(conn->txSemaphore))
	//{
	 //   return SER_END_POINT_TX_SEM_GIVE;
	//}
	//if (pdFALSE == xSemaphoreGive(conn->rxSemaphore))
	//{
	//	return SER_END_POINT_TX_SEM_GIVE;
	//}

    return SER_SUCCESS;
}






int Tran_SendAckPacket(ConnectionEndPoint_t *ep)
{
	Frame_t  *frame;
    int res = 0;

	FRAME_GET(&frame,OUT_RESOURCE_TIMEOUT_MS);
    if (NULL == frame){
		return SER_RESOURCE_FREE_FRM_EMPTY;
	}

	Tran_PacketConfig(ep, PKT_TYPE_TRANSPORT_LAYER_DATA, NULL, 0, frame); // zero message length
	res =  Tran_SendPacket(frame);
	FRAME_USE_CNT_DEC(frame);
	return res;
}


int EP_SendPacket(ConnectionEndPoint_t *ep, Frame_t *outFrame)
{
	int res = SER_SUCCESS;

    Tran_AccessTake();
    ep->txBytesInFlight = GET_TRAN_MSG_SIZE(outFrame);
    ep->ticksAtFirstTx = xTaskGetTickCount();
    SET_TRAN_TX_CNT(outFrame, ep->hostTxByteCnt);
    SET_TRAN_RX_CNT(outFrame, ep->hostRxByteCnt);
    Tran_AccessGive();
	res = Tran_SendPacket(outFrame);

	return res;
}

int EP_SendBuffer(Connection_t *desc, uint8_t *buf, uint32_t size, uint32_t *sent)
{
    int res = 0;
    uint32_t  sendSize;
    uint32_t  sizeRemaining;
    ConnectionEndPoint_t *ep;
    EpEventEnum_t evt;
    int timeoutCnt;

    Frame_t *txFrame = NULL;

    if ((size == 0) || (desc == NULL) || (buf == NULL) || (sent == NULL)){
    	return SER_BAD_ARGUMENT;
    }

    //Dbg_SerialCommTrace("Entry");

    if ((NULL != desc->txSemaphore) && (pdFALSE == xSemaphoreTake(desc->txSemaphore, portMAX_DELAY)))
    {
    	//Dbg_SerialCommTrace("Exit %d", SER_END_POINT_TX_SEM_TAKE);
    	return SER_END_POINT_TX_SEM_TAKE;
    }

    ep = &desc->ep;
    if (ep->epState != EP_STATE_OPEN){
   		//Dbg_SerialCommError("Failed to get new frame");
    	xSemaphoreGive(desc->txSemaphore);
     	//Dbg_SerialCommTrace("Exit %d", SER_RESOURCE_FREE_FRM_EMPTY);
   	   return SER_END_POINT_NOT_OPEN;
   }

	ep->txActive = true;

	sizeRemaining = size;
	timeoutCnt = 0;

	//while( (sizeRemaining = endByteCnt - ep->hostTxByteCnt) > 0 )
    while( sizeRemaining > 0 )
	{
		if (ep->epState != EP_STATE_OPEN){
			res = SER_END_POINT_NOT_OPEN;
			break;
		}

	    FRAME_GET(&txFrame,OUT_RESOURCE_TIMEOUT_MS);
		if (NULL == txFrame){
			dl_print("Failed to get new frame");
			res = SER_RESOURCE_FREE_FRM_EMPTY;
			//Dbg_SerialCommTrace("Exit %d", SER_RESOURCE_FREE_FRM_EMPTY);
			break ;
		}

		sendSize = (sizeRemaining > PKT_TRANSPORT_MSG_SIZE_MAX)? PKT_TRANSPORT_MSG_SIZE_MAX : sizeRemaining;
		// packet config
		Tran_PacketConfig(ep, PKT_TYPE_TRANSPORT_LAYER_DATA, buf, sendSize, txFrame);
		EP_SendPacket(ep, txFrame);
		FRAME_USE_CNT_DEC(txFrame);

        evt = EP_WaitForTxEvent(ep, 3 * ep->roundTripTicks);
		if ( evt == 0 ){
			timeoutCnt++;
			dl_print("EP_WaitForTxEvent timeout %d, cnt %d", 3 * ep->roundTripTicks, timeoutCnt);
			if (timeoutCnt > 5){
				Tran_NotifyEpEvent(ep->rxEvent, EPEVT_TX_ERROR);
				res = SER_END_POINT_TX_TIMEOUT;
				break;
			}
			continue;
		}

		// a msg was received
		if (evt & EPEVT_PKT_SENT) {
			evt &= ~EPEVT_PKT_SENT; // clear the event
		}

		if(evt != 0)  {
			//Dbg_SerialCommWarning("%s event %d",__FUNCTION__ ,evt);
			res = SER_END_POINT_SESSION_ERROR;
			break;
		}
		buf += sendSize;
		sizeRemaining -= sendSize;
	}

	ep->txActive = false;
    *sent = size - sizeRemaining;

    if (pdFALSE == xSemaphoreGive(desc->txSemaphore))
    {
        res = SER_END_POINT_TX_SEM_GIVE;
    }
    //Dbg_SerialCommTrace("sent %d, result %d", *sent, res);

    return res;
}




int EP_GetBuffer(Connection_t *desc, uint8_t **buf, uint32_t *rcvSize)
{
    int res = 0;
    uint32_t rxMsgSize;
    uint32_t remoteTxCnt;
    ConnectionEndPoint_t *ep;
    EpEventEnum_t evt;
    Frame_t *frame;


    *rcvSize = 0;
	*buf = NULL;

	//Dbg_SerialCommTrace("Entry");
    if ( (desc == NULL) || (buf == NULL) || (rcvSize == NULL)){
    	return SER_BAD_ARGUMENT;
    }
    if ((NULL != desc->rxSemaphore)
    	&& (pdFALSE == xSemaphoreTake(desc->rxSemaphore, portMAX_DELAY))
		)
    {
    	return SER_END_POINT_TX_SEM_TAKE;
    }

	ep = &desc->ep;
	ep->rxActive = true;

	if(ep->epState != EP_STATE_OPEN){
		xSemaphoreGive(desc->rxSemaphore);
		//Dbg_SerialCommInfo("Session not opened %d", ep->epState);
		return SER_END_POINT_SESSION_ERROR;
	}

	evt = EP_WaitForRxEvent(ep, portMAX_DELAY);

	if (evt & EPEVT_PKT_RCV)
	{
		if(xQueueReceive(ep->rxMsgQue, &frame, 1) == pdTRUE)
		{
			rxMsgSize =  GET_TRAN_MSG_SIZE(frame);

			*buf = GET_TRAN_MSG_BYTES(frame); // pass frame msg pointer to user
			*rcvSize = rxMsgSize;
		}
		else {
		//	Dbg_SerialCommError("Queue rx error num msgs: %d", uxQueueMessagesWaiting(ep->rxMsgQue));
			res = SER_END_POINT_SESSION_ERROR;
		}
		evt &= ~EPEVT_PKT_RCV;
	}
	if(evt & EPEVT_TX_ERROR) {
		//Dbg_SerialCommError("Event tx error %d", evt);
		res = SER_END_POINT_SESSION_ERROR;
	}
	if(evt & EPEVT_SESSION_CLOSE_RCV){
		//Dbg_SerialCommError("Event close %d", evt);
		res = SER_END_POINT_SESSION_ERROR;
	}

    ep->rxActive = false;

    if (pdFALSE == xSemaphoreGive(desc->rxSemaphore))
    {
        res = SER_END_POINT_TX_SEM_GIVE;
    }
    //Dbg_SerialCommTrace("Exit %d", res);
    return res;
}

void EP_ReleaseBuffer(uint8_t *buf)
{
	if (buf == NULL) {
		//Dbg_SerialCommWarning("null argument");
		return;
	}
    Frame_t * frame = (Frame_t *)GET_TRAN_MSG_FRAME(buf);
    FRAME_USE_CNT_DEC(frame);
}











//--------------------------------------------------------------------------------------------------------------------
// SAND BOX
//--------------------------------------------------------------------------------------------------------------------

#if 0

int Link_Init(SerialLink_t *link,
              send_pfn send, void *sendFnArg, uint8_t *outbuf, uint32_t outbufsize)
{
	link->rxFrmQue = xQueueCreate(5, sizeof(Frame_t *));
	if (link->rxFrmQue == NULL){
		return SER_LINK_INIT_RX_QUEUE_CREATE;
	}

	link->txFrmQue = xQueueCreate(5, sizeof(Frame_t *));
	if (link->txFrmQue == NULL){
        return SER_LINK_INIT_RX_QUEUE_CREATE;
	}

	ios_init(&link->ostream, outbuf, outbufsize, 0);
	os_set_tx_cb(&link->ostream, send, sendFnArg);

	link->txSemaphore = xSemaphoreCreateMutex();
    if (link->txSemaphore == NULL){
    	vQueueDelete( link->rxFrmQue );
    	return SER_LINK_INIT_TX_SEM_CREATE;
    }
    xTaskCreate(FpbSerialLinkRxTask, "FpbSerialLinkRxTask",configMINIMAL_STACK_SIZE*2, NULL, PRIORITY-1, NULL);

    return SER_SUCCESS;
}

IO_StreamState_t os_ep_send(void *p, uint8_t* buf, int size)
{
	uint32_t sent;
	int res = EP_SendBuffer((Connection_t*)p, buf, (uint32_t)size, &sent);

	if (res != SER_SUCCESS){
		return IOS_ERROR;
	}
	return IOS_GOOD;
}

#endif



