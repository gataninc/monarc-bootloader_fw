//#if defined(GTN_FEATURE_DBG_EN) && GTN_FEATURE_DBG_EN

/*
 * dbglog.c
 *
 *  Created on: Aug 30, 2019
 *      Author: lkodheli
 */

#include "dbglog.h"
#include <ctype.h>
#include "FreeRTOS.h"
#include "Uart.h"
#include <string.h>
#include "task.h"
#include "semphr.h"
#include "stdio.h"
#include <stdlib.h>
#include <stdarg.h>
#include "utils.h"

#define DBG_OK                  (0)
#define DBG_RES_OUT_OF_SPACE    (-1)
#define DBG_RES_UNDERFLOW       (-2)
#define DBG_RES_LEN_CORRUPT     (-3)
#define DBG_RES_REC_INVALID     (-4)
#define DBG_RES_CANT_GET_MUTEX  (-5)
#define DBG_RES_MUTEX_IS_NULL   (-6)

#define SIZE_1KB                (1024)

// Make the log buffer size a power of 2
#define LOG_BUF_SIZE            ( 8 * SIZE_1KB ) // define the log size to be 8KB

#define LOG_LEN_SIZE                 (2)    // 2 Bytes to store the log string length
#define LOG_TIMESTAMP_SIZE           (4)    // 4 Bytes to store the time stamp

#define LOG_MSG_SIZE_MIN             (1)    // 1 Byte minimum message string length
#define LOG_MSG_SIZE_MAX             (1024)  // 1024 Byte MAX message string length

#define LOG_LEN_MARKER_MASK           (0xF000)
#define LOG_LEN_MARKER               (0x5000 & LOG_LEN_MARKER_MASK)

#define NEW_MSG_BIT                  (0x01)

/*
 * Log record:
 * TIME_STAMP  : The time in milliseconds. The field is 4 bytes long.
 * LOG_STR_LEN : Follows TIME_STAMP field, stores the size of the MSG_STR.
 *  MSG_STR     : This is the logged string.
 *
 * +--------------+------------+---------------------   --------------------+
 * |    2 Bytes   |   4 Bytes  |             1 .. 1024 Bytes                |
 * +--------------+------------+----------------~~~-------------------------+
 * |    LOG_LEN   | TIME_STAMP |               MSG_STR                      |
 * +--------------+------------+----------------~~~-------------------------+
 * */

#define LOG_RECORD_HEADER_SIZE     ( LOG_LEN_SIZE + LOG_TIMESTAMP_SIZE )

// The minimum Log Record length
#define LOG_RECORD_LEN_MIN ( LOG_RECORD_HEADER_SIZE + LOG_MSG_SIZE_MIN )
// The maximum Log Record length
#define LOG_RECORD_LEN_MAX ( LOG_RECORD_HEADER_SIZE + LOG_MSG_SIZE_MAX )

// The maximum number of messages
#define LOG_MSGS_CNT_MAX ( LOG_BUF_SIZE/LOG_RECORD_LEN_MIN )

#define MsgLen_2_RecordLen(MSG_LEN)     ( LOG_RECORD_HEADER_SIZE + (MSG_LEN) )


/*
 * Read Out Log record:
 * TIME_STAMP  : The time in milliseconds. The field is 4 bytes long.
 * LOG_STR_LEN : Follows TIME_STAMP field, stores the size of the MSG_STR.
 *  MSG_STR     : This is the logged string.
 *
 * +-------------------+---------+---------------~~~----------------+---------+-------------+
 * |     11 Bytes      | 1 Byte  |         1 .. 1024 Bytes          | 2 Bytes |   1 Byte    |
 * +-------------------+---------+---------------~~~----------------+---------+-------------+
 * | TIME_STAMP_STRING | PADDING |             MSG_STR              |  \r\n   | NULL_CHAR   |
 * +-------------------+---------+---------------~~~----------------+---------+-------------+
 * */

// When the log is read back from the ring buffer it is formatted a bit differently
#define TIME_STAMP_STRING_SIZE          (11) // 8 characters
#define PADDING_SIZE                    (1) // 1 Byte padding after the timestamp
#define NEWLINE_SIZE                    (2) // 2 Bytes for carriage return and new line
#define NULL_CHAR_SIZE                  (1)

#define EXTRACTED_LOG_STR_SIZE     (TIME_STAMP_STRING_SIZE + PADDING_SIZE + LOG_MSG_SIZE_MAX + NEWLINE_SIZE + NULL_CHAR_SIZE)

// LITTLE ENDIAN TO BIG ENDIAN for the purposes of serializing
#define U32_BYTE(A,N) ( ( (A) >> (8*(N) ) ) & 0x00FF )
#define U32_LE2BE(A) ( ( U32_BYTE(A,0) << 24 ) | ( U32_BYTE(A,1) << 16) | ( U32_BYTE(A,2) << 8 ) | ( U32_BYTE(A,3) ))
/*
 *  LogBuf holds the debug strings and is used for a circular buffer area.
 *  The first byte of every string will be the string length.
 *  The start of the string follows this byte.
 *  Stored strings don't need to be null terminated since the length is known.
 *
 * */
static uint8_t LogBuf[LOG_BUF_SIZE];
static char s_ReadLogString[EXTRACTED_LOG_STR_SIZE];

typedef struct _DRingBuf {
	uint16_t HeadIndex; // index to the first byte of the first record
	uint16_t TailIndex; // index to the next free byte
	uint8_t  *Buf;      // pointer to the buffer
	uint16_t Size;      // Size of the buffer in bytes
	uint16_t Used;      // Used count in bytes
} DRingBuf;


typedef struct _DbgLogInfo {
	DRingBuf               rBuf;
	//SemaphoreHandle_t      xSemMsgCnt;      // count the messages in the buffer
	TaskHandle_t           xHandlingTask;
	SemaphoreHandle_t      xMutexMsgProd;   // serializes the message insertion
	bool                   Overflow;        // flag: 1 if overflow,  no more space to place logs
	bool                   Corrupted;
	                                        //       0 no overflow
}DbgLogInfo;


DbgLogInfo dbgInfo;
unsigned int LOC_CNT_1 = 0;
unsigned int LOC_CNT_2 = 0;

static char maxString[LOG_MSG_SIZE_MAX + 1]; // add space for the null character
/*
 * Initialize the ring buffer
 * */
void DRB_Init(DRingBuf *pRb, uint8_t *pBuf, uint16_t size)
{
	pRb->HeadIndex = 0;
	pRb->TailIndex = 0;
	pRb->Buf = pBuf;
	pRb->Size = size;
	//pRb->Used = 0;
}

/*
 * Resets the ring buffer
 * */
void DRB_Reset(DRingBuf *pRb)
{
	pRb->HeadIndex = 0;
	pRb->TailIndex = 0;
}

/*
 * Calculate the free space in the ring buffer
 * */
uint32_t DRB_GetFreeSpace(DRingBuf *pRb)
{
	if (pRb->TailIndex >= pRb->HeadIndex){
		return (pRb->Size - (pRb->TailIndex - pRb->HeadIndex) - 1);
	}
	else {
		return (pRb->HeadIndex - pRb->TailIndex - 1);
	}
}

/*
 * Calculate the Used Count in the ring buffer
 * */
uint32_t DRB_GetUsedCnt(DRingBuf *pRb)
{
	if (pRb->TailIndex >= pRb->HeadIndex){
		return  (pRb->TailIndex - pRb->HeadIndex);
	}
	else {
		return(pRb->Size - pRb->HeadIndex + pRb->TailIndex);
	}
}

/*
 * This function copies a buffer to the tail of the ring buffer.
 * Note: The caller should check if there is space to the end of the buffer.
 * */
//void DRB_GetHeadBufferWork(DRingBuf *pRb, uint8_t *buf, int size)
//{
//	memcpy(buf, &pRb->Buf[pRb->HeadIndex], size); // copy the data
//	pRb->Used -= size;
//
//	pRb->HeadIndex += size; // update the tail index
//	if (pRb->HeadIndex >= pRb->Size){
//   	pRb->HeadIndex = 0;
//    }
//}



/*
 * This function copies a buffer to the tail of the ring buffer.
 * Note: The caller should check if there is space in the buffer.
 * */
void DRB_InsertTailBuffer(DRingBuf *pRb, uint8_t *buf, int size)
{
	int contiguousFreeBuf;// = pRb->Size - pRb->TailIndex; // size to index wrap
	int sizeToCopy;

	if (pRb->TailIndex >= pRb->HeadIndex){
		contiguousFreeBuf = pRb->Size - pRb->TailIndex;
	}
	else {
		contiguousFreeBuf = pRb->HeadIndex - pRb->TailIndex - 1;
	}
	if (contiguousFreeBuf == 0){
		return;
	}
	// find the min of size and space to the end of buffer
	sizeToCopy = min(size, contiguousFreeBuf);
	if (sizeToCopy > 0)
	{
		memcpy(&pRb->Buf[pRb->TailIndex], buf, sizeToCopy); // copy the data;
		pRb->TailIndex += sizeToCopy;
		if (pRb->TailIndex == pRb->Size){
			pRb->TailIndex = 0;
		}
		buf += sizeToCopy;
		size -= sizeToCopy; // update the bytesToCopy
	}
    if (size > 0){ // copy to bottom part
    	DRB_InsertTailBuffer(pRb, buf, size);
    }
}

/*
 * This function copies data from the ring buffer into client buffer.
 * Note: The caller should check if this data is in the buffer.
 * */
void DRB_GetHeadBuffer(DRingBuf *pRb, uint8_t *buf, int size)
{
	int dataToEndOfBuf; // size to index wrap

	// this is the space to the end of the buffer
	dataToEndOfBuf = pRb->Size - pRb->HeadIndex;

	// find the min of size and space to the end of buffer
	if (dataToEndOfBuf < size){
		DRB_GetHeadBuffer(pRb, buf, dataToEndOfBuf);
		buf += dataToEndOfBuf;
		size -= dataToEndOfBuf; // update the bytesToCopy
	}

	memcpy(buf, &pRb->Buf[pRb->HeadIndex], size); // copy the data
	pRb->HeadIndex += size; // update the tail index
	if (pRb->HeadIndex >= pRb->Size){
	    pRb->HeadIndex = 0;
	}
}

void DRB_PeekHeadBuffer(DRingBuf *pRb, uint8_t *buf, int size)
{
	int dataToEndOfBuf; // size to index wrap
	uint16_t tmpHeadIndex;

	// this is the space to the end of the buffer
	dataToEndOfBuf = pRb->Size - pRb->HeadIndex;

	tmpHeadIndex = pRb->HeadIndex;
	// find the min of size and space to the end of buffer
	if (dataToEndOfBuf < size){
		memcpy(buf, &pRb->Buf[tmpHeadIndex], dataToEndOfBuf); // copy the data
		tmpHeadIndex += dataToEndOfBuf; // update the tail index;
		if (tmpHeadIndex >= pRb->Size){
			tmpHeadIndex = 0;
		}
		buf += dataToEndOfBuf;
		size -= dataToEndOfBuf; // update the bytesToCopy
	}

	memcpy(buf, &pRb->Buf[tmpHeadIndex], size); // copy the data
}


/*
 * This function inserts a debug message into the
 * */
int DRB_InsertLogRecord(DRingBuf *pRb, uint32_t timeStampMs, char *str, uint16_t size)
{
	uint16_t recordLen;
	uint32_t freeBufferByteCnt;

	do {
		if (size > LOG_MSG_SIZE_MAX) { // truncate to 1024
			size = LOG_MSG_SIZE_MAX;
		}

		recordLen = MsgLen_2_RecordLen (size); // calc the space needed to store this record;
		freeBufferByteCnt = DRB_GetFreeSpace(pRb);    // find how much free space is in the buffer
		if(freeBufferByteCnt < recordLen) { // throw away records if there is not enough free space
			return DBG_RES_OUT_OF_SPACE;
		}

		// use upper byte nibble for marking the byte length
		recordLen |= LOG_LEN_MARKER; // use the higher nibble 0X5000
		//tsBigEndian = U32_LE2BE(timeStampMs);// flip the bytes around so when saved the MSB is at the lowest addr

		DRB_InsertTailBuffer(pRb, (uint8_t*)(&recordLen), LOG_LEN_SIZE);         // insert string size
		DRB_InsertTailBuffer(pRb, (uint8_t*)(&timeStampMs), LOG_TIMESTAMP_SIZE); // insert time stamp
		DRB_InsertTailBuffer(pRb, (uint8_t*)str, size);                          // insert string
	} while(0);

	return DBG_OK;
}

/*
 * This function removes a debug message from the log
 * */
int DRB_RemoveLogRecord(DRingBuf *pRb, char *str, unsigned *size)
{
	uint16_t logLen, tmpLen;
	uint16_t logCnt;
	uint32_t timeStampValMs;
	uint16_t msgLen;
	char *pData = str;
	int result = DBG_OK;
	int n;

	*size = 0;

	//if(xSemaphoreTake(dbgInfo.xMutexMsgProd, 200/portTICK_PERIOD_MS) != pdTRUE)
	//{
	//	return DBG_RES_CANT_GET_MUTEX; // failed to get the semaphore
	//}
    do {
    	logCnt = DRB_GetUsedCnt(pRb);
		if (logCnt < (LOG_LEN_SIZE + LOG_TIMESTAMP_SIZE + LOG_MSG_SIZE_MIN)){
			//DRB_Reset(pRb);
			result = DBG_RES_UNDERFLOW;
			break;
		}

        // peek the length of the record
		DRB_PeekHeadBuffer(pRb, &logLen, LOG_LEN_SIZE);
		if ((logLen & LOG_LEN_MARKER_MASK) != LOG_LEN_MARKER){ // corruption has occurred reset the data
			DRB_Reset(pRb);
			result = DBG_RES_LEN_CORRUPT;
			break;
		}

		// clear the marker
		logLen &= ~LOG_LEN_MARKER_MASK;
		if (logCnt < logLen){
			result = DBG_RES_UNDERFLOW;
			break;
		}
		// The HeadIndex is pointing to the first byte of the log record
		// throw away the log length, we already have it
		DRB_GetHeadBuffer(pRb, (uint8_t*)&tmpLen, LOG_LEN_SIZE);                 // remove the record length

		DRB_GetHeadBuffer(pRb, (uint8_t*)(&timeStampValMs), LOG_TIMESTAMP_SIZE); // remove the time stamp value
		msgLen = logLen - LOG_LEN_SIZE - LOG_TIMESTAMP_SIZE;                     // the length of the message on the log record
        uint32_t hundreds = timeStampValMs % 1000;
        uint32_t thousands = (timeStampValMs/1000) % 1000;
        uint32_t high      = (thousands/1000);
		n = snprintf(pData, 12, "%02d %03d %03d ", high, thousands, hundreds);        // write the timestamp to the output string
		pData += 11;                              // point past the time stamp + padding

		DRB_GetHeadBuffer(pRb, (uint8_t*)pData, msgLen); // read the message into the output string
		pData += msgLen;                               // point past the string

		pData[0] = '\r';                               // add \r\n
		pData[1] = '\n';
		pData += 2;                                    // point past \r\n
		pData[0] = '\0';                               // terminate the message

		*size = (unsigned)(pData - str);
    } while(0);

	//xSemaphoreGive(dbgInfo.xMutexMsgProd);// Release the mutex
	return result;
}

void InitDbgLog()
{
	dbgInfo.xHandlingTask = xTaskGetCurrentTaskHandle();
	dbgInfo.xMutexMsgProd = xSemaphoreCreateMutex();
	configASSERT(dbgInfo.xMutexMsgProd);

	DRB_Init(&dbgInfo.rBuf, LogBuf, LOG_BUF_SIZE);
	dbgInfo.Overflow = false;
	dbgInfo.Corrupted = false;
}


// DO NOT RUN THIS FROM AN ISR Context
void dl_printstr(char *str, unsigned size)
{
	uint32_t timeStampMs;

	if ((str == NULL) || (size == 0))
	{
		return;
	}
	if(xSemaphoreTake(dbgInfo.xMutexMsgProd, 200/portTICK_PERIOD_MS) != pdTRUE)
	{
		return;
	}

	timeStampMs = (uint32_t)(xTaskGetTickCount()/portTICK_PERIOD_MS);

	DRB_InsertLogRecord(&dbgInfo.rBuf, timeStampMs, str, size);
	xTaskNotify(dbgInfo.xHandlingTask, NEW_MSG_BIT, eSetBits); // the message count semaphore

	xSemaphoreGive(dbgInfo.xMutexMsgProd);// Release the mutex
}


void dl_print(const char *fmt, ...)
{

	uint32_t timeStampMs;
	int strLen = 0;
	if (dbgInfo.xMutexMsgProd == NULL){
		return;
	}
	if(xSemaphoreTake(dbgInfo.xMutexMsgProd, 200/portTICK_PERIOD_MS) != pdTRUE)
	{
		return;
	}
	va_list arglist;
	va_start(arglist, fmt);
	strLen = vsprintf(maxString, fmt, arglist);
	va_end(arglist);

	timeStampMs = (uint32_t)(xTaskGetTickCount()/portTICK_PERIOD_MS);

	DRB_InsertLogRecord(&dbgInfo.rBuf, timeStampMs, maxString, strLen);
	xTaskNotify(dbgInfo.xHandlingTask, NEW_MSG_BIT, eSetBits); // the message count semaphore

	xSemaphoreGive(dbgInfo.xMutexMsgProd);// Release the mutex
}

void DbgServiceTask(void *pvParameters)
{
	unsigned logMsgLen = 0;
	//const char *pOvfloMsg = "OVERFLOW\r\n";
	int res = -1;
	BaseType_t xResult;
	uint32_t ulNotifiedValue;

	InitDbgLog();

	for (;;) {
		xResult = xTaskNotifyWait(0x0,                /* Don't clear bits on entry. */
				                  0xFFFFFFFF,         /* Clear all bits on exit. */
						          &ulNotifiedValue,   /* Stores the notified value. */
				                  portMAX_DELAY);
		if (xResult == pdPASS) {
            do {
			    res = DRB_RemoveLogRecord(&dbgInfo.rBuf, s_ReadLogString, &logMsgLen);
			    if ((res == DBG_OK) && (logMsgLen > 0)){
			    	//PUT_BUFF((uint8_t *)s_ReadLogString, logMsgLen);
			    	LIB_CPRINT(s_ReadLogString, logMsgLen);
			    }
            } while((res == DBG_OK) && (logMsgLen > 0));
		} else {
			vTaskDelay(5000/portTICK_PERIOD_MS); // error occured
		}
	}
}

//#endif // defined(GTN_FEATURE_DBG_EN) && GTN_FEATURE_DBG_EN
