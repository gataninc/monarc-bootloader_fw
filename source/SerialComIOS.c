#include "SerialComIOS.h"
#include "SerialComClient.h"
#include "FreeRTOS.h"
#include "mios.h"
//#include <stdlib.h>
#include <stdio.h>
//#include <stdarg.h>
#include "Uart.h"
#include "lib_init.h"
#include "dbglog.h"

//#include "dbglog_target.h"

#define   COM_UART_COMPLETE                          (1)
#define   COM_UART_RING_BUFFER_OVERRUN               (1<<1)
#define   COM_UART_HARDWARE_BUFFER_OVERRUN           (1<<2)

bool scs_badSysState;

IO_StreamState_t ostream_ep_send(void *p, uint8_t* buf, int size)
{
	uint32_t sent;
	serial_com_ios_server_t    *scsCtxt = (serial_com_ios_server_t*)p;

	int res = EP_SendBuffer(&scsCtxt->accept_conn, buf, (uint32_t)size, &sent);
	if (res != SER_SUCCESS){
		EP_CloseConnection(&scsCtxt->accept_conn); // recycle the connection
		return IOS_ERROR;
	}
	return IOS_GOOD;
}

void SerialComStream_ServerRxTask(void *pvParameters)
{
	uint8_t *rxBuf;
	uint32_t  rcvSize;
	serial_com_ios_server_t    *scsCtxt = (serial_com_ios_server_t*)pvParameters;
	int res = 0;

	res = EP_OpenServerConnection(&scsCtxt->srv_conn, scsCtxt->server_port);
	if (res != 0){
		scs_badSysState = true;
	}
	if (scs_badSysState){
	    vTaskDelay(portMAX_DELAY); // this task is brocken
	}

	for (;;) {
		dl_print("[SerialComStream_ServerRxTask]: listening", res);
    	res = EP_AcceptConnection(&scsCtxt->srv_conn, &scsCtxt->accept_conn);
    	if (res != 0){
    		vTaskDelay((TickType_t)300);
    		//Dbg_SerialCommTrace("%s accept connection failed",__FUNCTION__);
    		dl_print("[SerialComStream_ServerRxTask]: accept error: %d", res);
    		continue;
    	}
    	dl_print("[SerialComStream_ServerRxTask]: connection accepted", res);
    	//Dbg_SerialCommTrace("%s CLIENT connection accepted",__FUNCTION__);

    	do {
    	    res = EP_GetBuffer(&scsCtxt->accept_conn, &rxBuf, &rcvSize);
    	    if (res != 0){
    	    	//Dbg_SerialCommTrace("%s EP_GetBuffer failed",__FUNCTION__);
    	    	dl_print("[SerialComStream_ServerRxTask]: error receiving %d", res);
    	    	break;
    		}
    	    else {
    	        // send to input stream
    	        is_put(scsCtxt->is, rxBuf, rcvSize, NULL);
    	        EP_ReleaseBuffer(rxBuf);
    	    }
    	} while(res == 0);
    	dl_print("[SerialComStream_ServerRxTask]: closing connection", res);
    	EP_CloseConnection(&scsCtxt->accept_conn); // recycle the connection

	}
}



int SetupSerialComIosServer(
		serial_com_ios_server_t    *scsCtxt
)
{
	BaseType_t xReturned;
	if ((scsCtxt == NULL) || (scsCtxt->os == NULL) || (scsCtxt->is == NULL) ) {
		return SCS_ERROR_BAD_ARG;
	}

	if( EP_InitConnection(&scsCtxt->srv_conn) != SER_SUCCESS )
	{
		return SCS_CANNOT_INITIALIZE_CONNECTION; //failed state
	}
	if( EP_InitConnection(&scsCtxt->accept_conn) != SER_SUCCESS )
	{
		return SCS_CANNOT_INITIALIZE_CONNECTION; //failed state
	}

	os_set_tx_cb(scsCtxt->os, ostream_ep_send, (void *)(scsCtxt));

    xReturned =xTaskCreate(SerialComStream_ServerRxTask, "SerialComStream_ServerRxTask",configMINIMAL_STACK_SIZE*2, scsCtxt, PRIORITY_HIGH, &scsCtxt->xHandle);
    if (xReturned != pdPASS){
    	return SCS_CANNOT_CREATE_TX_TASK;
    }

    return SCS_SUCCESS;
}

#if 0
int SetupSerialComIosClient(
		uart_link_handle_t  *scsCtxt,
		iostream_t          *os,       // Output stream
		iostream_t          *is,       // Input stream
		// connection configuration
		int                  hostPort, // host port number
		int                  destAddr, // destination address number
		int                  destPort  // destination port number
)
{
	if ((scsCtxt == NULL) || (os == NULL) || (is == NULL) ) {
		return SCS_ERROR_BAD_ARG;
	}
	scsCtxt->os = os;
	scsCtxt->is = is;

	if( EP_InitConnection(&scsCtxt->conn) != SER_SUCCESS )
	{
		return SCS_CANNOT_INITIALIZE_CONNECTION; //failed state
	}

    xReturned =xTaskCreate(UartSerialLinkTxTask, "UartSerialLinkTxTask",configMINIMAL_STACK_SIZE*2, (uartSerCtxt), PRIORITY_HIGH, &uartSerCtxt->xLinkTxHandle);
    if (xReturned != pdPASS){
    	return USC_CANNOT_CREATE_TX_TASK;
    }

    xReturned =xTaskCreate(UartSerialLinkRxTask, "UartSerialLinkRxTask",configMINIMAL_STACK_SIZE*2, (uartSerCtxt), PRIORITY_HIGH, &uartSerCtxt->xLinkTxHandle);
    if (xReturned != pdPASS){
        return USC_CANNOT_CREATE_RX_TASK;
    }

    uart_init(&lnkuart->handle, base, clk_src, prioId, prio);
    //UART_TransferCreateHandle(lnkuart->base, &lnkuart->handle, UART_SERIAL_Callback, uartSerCtxt);
    //UART_TransferStartRingBuffer(lnkuart->base, &lnkuart->handle, lnkuart->RxRingBuffer, UART_RX_RING_BUFFER_SIZE);

    //UART_EnableTx(handle->base, true);
    //UART_EnableRx(handle->base, true);

    return USC_SUCCESS;
}

#endif


