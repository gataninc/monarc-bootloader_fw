/*
 * dbglog.h
 *
 *  Created on: Aug 30, 2019
 *      Author: lkodheli
 */

#ifndef __CONFIG_SERIAL_COM_H__
#define __CONFIG_SERIAL_COM_H__



#define CONF_NET_FPB_ADDR             (1)
#define CONF_NET_ROC_ADDR             (2)
#define CONF_NET_PAC_ADDR             (3)
#define CONF_NET_PC_ADDR              (0)


#define CFG_END_POINT_CNT_MAX                   (3)

#define NET_PATHS_MAX                    (10)
#define NET_HOST_IS_MAX                  (255)
#define DEVICE_MAX                       (255)
#define PORT_MAX                         (255)

#endif /* __CONFIG_SERIAL_COM_H__ */
