/*
 * StausLeds.h
 *
 *  Created on: May 10, 2019
 *      Author: stelasang
 */

#ifndef STATUSLEDS_H_
#define STATUSLEDS_H_

typedef enum{
	PmtCoolerOff,
	PmtCoolerEnabled,
	PmtCoolerCooling,
	PmtCoolerAtSetPoint,
	PmtCoolerHasAlarm,
	PmtCoolerHasError,
	DmCommunicationAlive,
	DmCommunicationBroke,
	DmCommunicationConnected,
	ROCCommunicationActive,
	RocCommunicationError,
	RocCommunicationGood,
	PACCommunicationActive,
	PACCommunicationError,
	PACCommunicationGood,
	SpectromenterConnectionAlive,
	SpectrometerConnectionBroke,
	MotorHasAnError,
	MotorHasNoError,
	InstrumentInError,
	InstrumentOK
}LedStatus_e;

void LED(void *pvParameters) ;
void SendStatus(LedStatus_e status);

#endif /* STATUSLEDS_H_ */
