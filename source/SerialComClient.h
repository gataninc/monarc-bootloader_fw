/*
 * SerialComClient.h
 *
 *  Created on: Aug 30, 2019
 *      Author: lkodheli
 */

#ifndef __SERIALCOMCLIENT_H_
#define __SERIALCOMCLIENT_H_

#include "SerialComTypes.h"


// call this before any other library calls
int NetLib_Init(uint8_t my_addr);

int Link_Init(SerialLink_t *link);

void Link_Deinit(SerialLink_t *link);

// lower level function, used by tx,rx tasks/isr's
void Link_Rx_Byte(SerialLink_t *link, uint8_t rxByte);
void Link_Rx_ByteFromIsr(SerialLink_t *link, uint8_t rxByte, BaseType_t *pxTaskWoken);

//either return with a byte or block waiting.
int Link_Tx_GetBuffer(SerialLink_t *link, uint8_t *byte, uint32_t size, uint32_t *cnt);
int Link_Tx_GetBufferNonBlocking(SerialLink_t *link, uint8_t *byte, uint32_t size, uint32_t *cnt);

int Link_Tx_GetBufferFromIsr(SerialLink_t *link, uint8_t *byte, uint32_t size, uint32_t *cnt, BaseType_t *pxTaskWoken);
int Link_Rx_MinCntExpected(SerialLink_t *link);

int Net_SetHostId(uint8_t hostId);
NetPath * Net_SetRoute(uint8_t dest, SerialLink_t *plink);

int EP_InitConnection(Connection_t *conn);
void EP_DeinitConnection(Connection_t *conn);

int EP_OpenClientConnection(Connection_t *conn,
		              uint8_t hostPort,
					  uint8_t destDev,
					  uint8_t destPort);

int EP_OpenServerConnection(Connection_t *conn, uint8_t hostPort);

int EP_AcceptConnection(Connection_t *conn,Connection_t *newConn);

int EP_CloseConnection(Connection_t *conn);

int EP_GetBuffer(Connection_t *conn, uint8_t **buf, uint32_t *rcvSize);

int EP_SendBuffer(Connection_t *conn, uint8_t *buf, uint32_t size, uint32_t *sent);

void EP_ReleaseBuffer(uint8_t *buf);

void NetLib_Deinit(void);



#endif /* __SERIALCOMCLIENT_H_ */
