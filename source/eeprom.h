/*
 * eeprom.h
 *
 *  Created on: Nov 27, 2018
 *      Author: stelasang
 */

#ifndef EEPROM_H_
#define EEPROM_H_

int ReadEEPROM(uint8_t address, int offset, uint8_t *rxBuff, int len);
int WriteEEPROM(uint8_t address, int offset, uint8_t *txBuff, int len);


#endif /* EEPROM_H_ */
