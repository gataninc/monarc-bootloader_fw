/*
 * dbglog.h
 *
 *  Created on: Aug 30, 2019
 *      Author: lkodheli
 */

#ifndef DBGLOG_H_
#define DBGLOG_H_


#if defined(GTN_FEATURE_DBG_EN) && GTN_FEATURE_DBG_EN
#include <stdbool.h>


// DO NOT RUN THIS FROM AN ISR Context
void DbgLogString(char *str, unsigned size);
void DbgLogPrint(const char *fmt, ...);

void InitDbgLog();
void DbgServiceTask(void *pvParameters);
void DbgSetEnabled(bool on);
extern unsigned int LOC_CNT_1;
extern unsigned int LOC_CNT_2;

#else

//#define LogDbgMessage(A, B)

void dl_printstr(char *str, unsigned size);
void dl_print(const char *fmt, ...);
void DbgServiceTask(void *pvParameters);
//define InitDbgLog()
//void DbgTxTask(void *pvParameters);

#endif /* GTN_FEATURE_DBG_EN */

#endif /* DBGLOG_H_ */
