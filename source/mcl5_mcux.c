/*
 * Copyright (c) 2016, NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    mcurtos.c
 * @brief   Application entry point.
 */


#include <board_init.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include "board.h"
#include "clock_config.h"
#include "MK64F12.h"
//#include "fsl_debug_console.h"



#include "fsl_card.h"
#include "fsl_uart_freertos.h"
#include "fsl_uart.h"
#include "fsl_dspi.h"
#include "fsl_dspi_freertos.h"

#include "fsl_adc16.h"
#include "fsl_rtc.h"
#include "fsl_pit.h"

#include "fsl_vref.h"
#include "fsl_dac.h"
#include "fsl_i2c.h"
#include "fsl_i2c_freertos.h"


/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "lwip/opt.h"
#include "lwip/tcpip.h"
#include "lwip/dhcp.h"
#include "lwip/prot/dhcp.h"
#include "netif/ethernet.h"
#include "ethernetif.h"

#include "lwip/sys.h"
#include "lwip/api.h"

#include "ff.h"
#include "Uart.h"
#include "eeprom.h"
#include "utils.h"
#include "StatusLeds.h"
#include "mcl5_mcux.h"
#include "version.h"
#include "lib_init.h"
#include "FlashWrapper.h"
#include "board_init.h"
#include "memory_map.h"
#include "eeprom.h"
#include "dbglog.h"


const char *firmware = FIRMWARE_VERSION;

//extern unsigned int __base_APPLICATION_FLASH;

// #define DEBUG_SERIAL_COM 1



//QueueHandle_t msgq = 0;

//QueueHandle_t tsmq = 0;


const char * CMD_LIST[] = {
		"connect",
		"erase-app", // erases app area
		"read-flash",  // read --start=0xabc --size=0xabc
		"read-eeprom",  // read --addr=57 --start=0xabc --size=0xabc
		"write-eeprom",  // write --addr=57 --start=0xabc "text"
		"program-app-srec",
		"start-app", // start app or start 0x1bac
		"crc32", // crc32 [app| --start=0xabc --size=0x123]
		"reset", // reset the board
		"help", // help
};

typedef enum CmdEnum {
	CE_CONNECT = 0,
	CE_ERASE_APP,
	CE_READ_FLASH,
	CE_READ_EEPROM,
	CE_WRITE_EEPROM,
	CE_PROGRAM_APP_SREC,
	CE_START_APP,
	CE_CRC32_APP,
	CE_RESET,

	CE_UNKNOWN = 1000
} CmdEnum;
#define CMD_LIST_CNT (sizeof(CMD_LIST)/sizeof(CMD_LIST[0]))

#define IN_CMD_BUF_SIZE      (1024)
uint8_t IN_CMD_BUF[IN_CMD_BUF_SIZE];   	// incoming command

#define CONSOLE_IN_BUF_SIZE   (256)
uint8_t CONSOLE_IN_BUF[IN_CMD_BUF_SIZE];

//#define FLASH_BUF_SIZE      (2048)
#define FLASH_BUF_SIZE      (4096)
#define FLASH_BUF_DW_CNT    (FLASH_BUF_SIZE/4)
typedef struct flash_buffer_t {
	bool     dirty;
	bool     in_use;
	uint32_t next_addr;
	uint32_t start_addr;
	uint32_t end_addr;
    uint32_t dw[FLASH_BUF_DW_CNT];   // flash driver needs this buffer to be 4 byte aligned
} flash_buffer_t;


typedef enum SrecFieldEnum {
	SF_S0 = 0, SF_S1 = 1, SF_S2 = 2, SF_S3 = 3, SF_S4 = 4, SF_S5 = 5,
	SF_S6 = 6, SF_S7 = 7, SF_S8 = 8, SF_S9 = 9, SF_UNKNOWN = 15
}SrecFieldEnum;

#define SREC_MAX_BYTES_FIELD               (600)

typedef struct srec_t {
	SrecFieldEnum  type;
	uint8_t        byte_cnt;
	uint32_t       addr;
	uint8_t        cs;
	uint8_t        bytes[SREC_MAX_BYTES_FIELD]; // raw bytes of the record address + data + check_sum
	// derived
	uint8_t  data_cnt;
	uint8_t *pdata;
} srec_t;

srec_t tmp_srec;
flash_buffer_t tmp_flash_buf;
bool bBootloaderConnected = false;
uint32_t record_cnt;
bool verify_hex_digits(char *start, int len)
{
    for (int i = 0; i < len; i++){
    	if (!ishexnum(start[i])){
    		return false;
    	}
    }
    return true;
}

void handle_crc32_app_help(iostream_t *os)
{
	os_print(os,"crc32-app:\r\n\tusage: crc32-app [--start=abc1 --size=2ac]\r\n"\
			      "\t\t: with no arguments the whole app partition is crc'd\r\n"\
				  "\t\t: --start start offset in hex , no 0x in front\r\n");
	os_print(os,"\t\t: --size of area from start offset in hex, no 0x in front\r\n");
}

void handle_flash_read_help(iostream_t *os)
{
	os_print(os,"read-flash:\r\n\tusage: read-flash [--start=abc1 --size=2ac]\r\n"\
				  "\t\t: --start start offset in hex\r\n");
	os_print(os,"\t\t: --size of area from start offset in hex\r\n");
}

void handle_eeprom_read_help(iostream_t *os)
{
	os_print(os,"read-eeprom:\r\n\tusage: read-eeprom --addr=57 --start=abc1 --size=2ac\r\n"\
			      "\t\t: --addr eeprom i2c address in hex\r\n"\
				  "\t\t: --start start offset in hex\r\n"\
	              "\t\t: --size of area from start offset in hex\r\n");
}

void handle_eeprom_write_help(iostream_t *os)
{
	os_print(os,"write-eeprom:\r\n\tusage: write-eeprom --addr=57 --start=abc1 \"text\"\r\n"\
			      "\t\t: --addr eeprom i2c address in hex\r\n"\
				  "\t\t: --start start offset in hex\r\n"\
	              "\t\t: \"text to write at that offset\"\r\n");
}

/*
 * SREC FORMAT sxbbaaaad*cc invalid length
 *             ||| |   | |
 *             ||| |   | +->Check Sum, 2 hex bytes the checksum field.
 *             ||| |   |    Sum of the values represented by the 2 hex digits for byte cnt, address and data fields
 *             ||| |   |
 *             ||| |   +-> Data, up to 64 hex digits, data field
 *             ||| |
 *             ||| +-> Address, 4/6/8 hex digits, address bytes in big endian
 *             |||
 *             ||+-> Byte count, 2 hex bytes, number of bytes in hex digit pairs in the rest of the record (address+data+checksum)
 *             ||
 *             |+->Record Type, Numeric digit "0" - "9", record type
 *             |
 *             +-> Record start, "S" letter
 *
 *             Note: https://en.wikipedia.org/wiki/SREC_(file_format)
 * */
int parse_srecord(char *pSrecStr, int len, srec_t *psrec)
{
	char *d;
	//uint8_t byte_cnt = 0;

	psrec->type = SF_UNKNOWN;
	if (len < 10){
		return -1; // sxccaaaad*ss invalid length
	}
	if ((pSrecStr[0] != 's')&& (pSrecStr[0] != 'S')){
		return -2;
	}

	// record type
	int32_t sn = pSrecStr[1] - '0';
	if ((sn<0) || (sn>9)){
		return -3;
	}
	psrec->type = sn;

	if (len % 2){
		return -4; // should be even length
	}
	psrec->byte_cnt = 0;
	if ( ! get_hex_byte( &pSrecStr[2], &psrec->byte_cnt ) ){
		return -5;
	}
	if ((psrec->byte_cnt == 0) || (psrec->byte_cnt != ((len/2) - 2))){
		return -6;
	}
	//psrec->data_cnt = byte_cnt - 1; // minus the checksum
	// decode the payload data + cs
	d = &pSrecStr[4];
	uint32_t sum = psrec->byte_cnt; // start with byte count
	for (int i = 0; i < psrec->byte_cnt-1; i++, d +=2){ // convert hex to data bytes
		if ( ! get_hex_byte( d, &psrec->bytes[i] ) ){
			return -7;
		}
		sum += psrec->bytes[i];
	}
	sum = ((~sum) & 0xFF); // keep only least significant byte
    // read the checksum
	if ( ! get_hex_byte( d, &psrec->cs ) ){
		return -8;
	}
    // validate check sum
    if (sum != psrec->cs){
    	return -9; // check sum error
    }

    if (psrec->type == SF_S4){
    	return 0; // nothing to do, unsupported
    }
    // 16 bit address, this is the only guaranteed format
    if ((psrec->type == SF_S0) || (psrec->type == SF_S1) || (psrec->type == SF_S9)){
    	psrec->addr = (((uint16_t)psrec->bytes[0]) << 8) | (uint16_t)psrec->bytes[1];
    	psrec->pdata = &psrec->bytes[2];
    	psrec->data_cnt = psrec->byte_cnt - 3; // byte_count - (2 byte address + 1 byte checksum)
    }
    else if ((psrec->type == SF_S5)||(psrec->type == SF_S6)){
        psrec->pdata = &psrec->bytes[0];
        psrec->data_cnt = psrec->byte_cnt - 1; // byte_count - 1 byte checksum)
    }
    // 24 bit address
    else if ((psrec->type == SF_S2) || (psrec->type == SF_S8)){
        psrec->addr = (((uint32_t)psrec->bytes[0]) << 16) | ((uint32_t)psrec->bytes[1]<<8) | ((uint32_t)psrec->bytes[2]);
        psrec->pdata = &psrec->bytes[3];
        psrec->data_cnt = psrec->byte_cnt - 4; // byte_count - (3 byte address + 1 byte checksum)
    }
    // 32 bit address
    else if ((psrec->type == SF_S3) || (psrec->type == SF_S7)){
        psrec->addr = (((uint32_t)psrec->bytes[0]) << 24) | ((uint32_t)psrec->bytes[1]<<16) | ((uint32_t)psrec->bytes[2]<<8) | ((uint32_t)psrec->bytes[3]);
        psrec->pdata = &psrec->bytes[4];
        psrec->data_cnt = psrec->byte_cnt - 5; // byte_count - (4 byte address + 1 byte checksum)
    }
    return 0;
}

void buffer_init(flash_buffer_t *fb)
{
	fb->dirty       = false;
	fb->in_use      = false;
	fb->next_addr   = 0;
	fb->start_addr  = 0;
	fb->end_addr    = (FLASH_BUF_DW_CNT * 4);

	for (int i = 0; i < FLASH_BUF_DW_CNT; i++){
        fb->dw[i] = 0xFFFFFFFF;
	}
}

void buffer_config(flash_buffer_t *fb, uint32_t start_addr)
{
	fb->in_use = true;
	fb->dirty  = false;
	fb->next_addr   = start_addr;
	fb->start_addr  = start_addr;
	fb->end_addr    = start_addr + (FLASH_BUF_DW_CNT * 4);
}

bool is_srec_data_contained(flash_buffer_t *fb, srec_t *psrec)
{
    return ((psrec->addr >= fb->start_addr) && (psrec->addr < fb->end_addr));
}

void copy_srec_to_buffer(flash_buffer_t *fb, srec_t *psrec, uint32_t *cnt)
{
	uint32_t copy_size;
	uint32_t copy_offset;
	*cnt = 0;
	if (!is_srec_data_contained(fb, psrec)){
		return;
	}
	copy_size = min(fb->end_addr, psrec->addr + psrec->data_cnt) - psrec->addr;
	copy_offset = psrec->addr - fb->start_addr;
	uint8_t *pdest = ((uint8_t*)(fb->dw)) + copy_offset;
	memcpy(pdest, psrec->pdata, copy_size);
	if (!fb->dirty){
	    fb->dirty = true;
	}
	psrec->pdata += copy_size;
	psrec->data_cnt -= copy_size;
	psrec->addr += copy_size;

	if (psrec->addr > fb->next_addr){ // point past this buffer
		fb->next_addr = psrec->addr;
	}

	*cnt = copy_size;
}

bool is_buffer_full(flash_buffer_t *fb)
{
    return (fb->next_addr == fb->end_addr);
}

bool is_buffer_dirty(flash_buffer_t *fb)
{
    return fb->dirty;
}
bool is_buffer_in_use(flash_buffer_t *fb)
{
    return fb->in_use;
}
bool is_data_record(srec_t *psrec)
{
	return ((psrec->type == SF_S1) || (psrec->type == SF_S2) || (psrec->type == SF_S3));
}
bool is_termination_record(srec_t *psrec)
{
	return ((psrec->type == SF_S7) || (psrec->type == SF_S8) || (psrec->type == SF_S9));
}
bool is_count_record(srec_t *psrec)
{
	return ((psrec->type == SF_S5) || (psrec->type == SF_S6));
}
bool is_header_record(srec_t *psrec)
{
	return (psrec->type == SF_S0);
}

int flush_write_buffer(flash_buffer_t *fb)
{
	uint32_t byte_cnt = fb->next_addr - fb->start_addr;
	int res = WriteToApplicationPartition(fb->start_addr, fb->dw, byte_cnt);
	if (res != 0){
		os_print(&ostr,"error %d writing app buffer offset:%x, size:%x.\n", res, fb->start_addr, byte_cnt);
	}
	fb->in_use = false;
	return res;
}


int handle_srecord(flash_buffer_t *fb, srec_t *srec)
{
	typedef enum SrecDownloadStateEn {
		SDS_IDLE, SDS_HEADER, SDS_DATA, SDS_CNT, SDS_TERM
	} SrecDownloadStateEn;
	static SrecDownloadStateEn ds = SDS_IDLE;
	int res;
	uint32_t copy_cnt;
	TickType_t start_time;

	if (srec->type == SF_S4){ // Ignore recored
		return 0;
	}

	res = 0;
	if ( !is_data_record(srec) ){ // SF_S0, SF_S5, SF_S6, SF_S7, SF_S8, SF_S9
		if (ds == SDS_DATA){
			// this means there was a previous download
			if ( is_buffer_dirty(fb) && is_buffer_in_use(fb) ){
				res = flush_write_buffer(fb);
			}
		}
		if( is_header_record(srec) ){ // SF_S0
			record_cnt = 0;
			ds = SDS_HEADER;
		}
		else if( is_termination_record(srec) ) { // SF_S7, SF_S8, SF_S9
			ds = SDS_TERM; // we don't use the start address
		}
		else { // SF_S5, SF_S6 count record
			ds = SDS_CNT; // we don't use the start address
            uint32_t tmpRecCnt = (srec->pdata[0]<<8)|srec->pdata[1];
            if (srec->type == SF_S6){
        	    tmpRecCnt = (tmpRecCnt << 8) |srec->pdata[2];
            }
            if (tmpRecCnt != record_cnt){
        	    os_print(&ostr,"warning, received %d expected %d.\n", record_cnt, tmpRecCnt);
            }
		}
	}
	else
	{ // data record received
		record_cnt++;
		if ( !is_buffer_in_use(fb) )
		{
			// not in use after being written to flash or after init
			// not dirty after init
			buffer_init(fb);
			buffer_config(fb, srec->addr); // start the buffer
		}
		if ( is_buffer_dirty(fb) && (!is_srec_data_contained(fb, srec)))
		{
			// this is the case where there are gaps in data and previous
			// buffer is not completely filled.
			dl_print("\twriting buffer addr: 0x%x size: %d time: %d.\r\n",fb->start_addr, fb->next_addr - fb->start_addr,xTaskGetTickCount());

			res = flush_write_buffer(fb);
			dl_print("\tdone time: %d.\r\n",xTaskGetTickCount());

			buffer_init(fb);
			buffer_config(fb, srec->addr); // start the buffer
		}
		// buffer
		copy_srec_to_buffer(fb, srec, &copy_cnt);
		if ( (copy_cnt > 0) && is_buffer_full(fb) ){
			start_time = xTaskGetTickCount();
			res |= flush_write_buffer(fb);
			dl_print("\twriting buffer addr: 0x%x size: %d time %d.\r\n",fb->start_addr, fb->next_addr - fb->start_addr, TimeDifference(start_time,xTaskGetTickCount()));
		}
		if ( srec->data_cnt > 0 ){ // there is more data remaining
			buffer_init(fb);
			buffer_config(fb, srec->addr); // start the buffer
			copy_srec_to_buffer(fb, srec, &copy_cnt);
		}
		ds = SDS_DATA;
	}


	return res;
}

void handle_connect(iostream_t *os, char *argstr)
{
	bBootloaderConnected = true;
	//dl_print("[%s] bootloader ver %s date %s\n", BOARD_GetBoardStr(), FIRMWARE_VERSION, FIRMWARE_BUILD_DATE_TIME);
	os_print(os,"connected\n");
}
void print_erase_help(iostream_t *os)
{
	os_print(os,"erase:\n\tusage: erase [app|--start=abc1 --size=2ac]");
}
void handle_erase_app(iostream_t *os, char *argstr)
{
	if ((argstr != NULL) && (*argstr != '\0')){
		os_print(os,"error, erase-app doesn't take arguments, given: %s\r\n", argstr);
		//print_erase_help();
		return;
	}

	os_print(os,"erase-app in progress\r\n");
	dl_print("erase-app in progress");
	int res = EraseApplicationPartition();
	if (res == kStatus_FLASH_Success){
		dl_print("erase-app done");
		os_print(os,"erase-app done\r\n");
	}
	else {
		dl_print("nerror, erase-app %d",res);
		os_print(os,"error, erase-app %d\r\n",res);
	}
}

typedef int (*read_fn)(uint32_t start_addr, uint32_t size, uint8_t *buf);
typedef int (*read_i2c_eeprom_fn)(uint8_t i2c_addr, int start_offset, uint8_t *buf, int size);

void print_addr_entry(iostream_t *os, uint32_t record_address)
{
	os_print(os,"| %08x | ", record_address);
}
void print_missing_bytes(iostream_t *os, int cnt, int position)
{
	uint32_t byte_offset;
	for (int i =position; i < (cnt+position); i++){
		byte_offset = i & 0x3;

		os_print(os,"  ");
		if ( byte_offset == 3){
			os_print(os," ");
		}
	}
}
// position should be from 0 16
void print_hex_bytes(iostream_t *os, uint8_t *data, int cnt, int position)
{
	uint32_t byte_offset;

	for (int i = position; i < (position+cnt); i++){
		// is it a word boundary
		byte_offset = i & 0x3;
		os_print(os,"%02x", data[i - position]);
		if (byte_offset == 3){ // insert space after last byte
			os_print(os," ");
		}
	}
}
void print_asci_space(iostream_t *os, int cnt)
{
	for (int i = 0; i < cnt; i++){
		os_print(os," ");
	}
}
void print_ascii_bytes(iostream_t *os, uint8_t *data,int cnt)
{
	for (int i = 0; i < cnt; i++){
		os_print(os,"%c", data[i]);
	}
}
void display_formated_content_as_words(iostream_t *os, uint32_t start_addr, uint32_t size, read_fn read_function)
{
	os_print(os,"\r\n");
	uint8_t d[16];
	uint32_t start_rec = start_addr & (~0xF);
	uint32_t end_addr = start_addr + size;
	int res;
	uint8_t t;

	if (end_addr & 0xF){
		end_addr =  0x10 + (end_addr & (~0xF));
	}
    // header
	os_print(os,"+----------+-------------------------------------+\r\n");
	os_print(os,"|   ADDR   | BYTES                               |\r\n");
	os_print(os,"+----------+-------------------------------------+\r\n");

	// print first partial record
	/*
	if (start_rec < start_addr){
		offset = start_addr - start_rec;
		res = read_function(start_addr, 16 - offset, d);
		if (res != 0){
			os_print(os, "error, read failed\r\n");
			return;
		}
		print_addr_entry(os, start_rec);
		print_missing_bytes(os, offset, 0); // cnt, position
		print_hex_bytes(os,d,16 - offset,offset); // data, count, position
		os_print(os,"| ");
		print_asci_space(os, offset);
		print_ascii_bytes(os,d,16 - offset); // data, count
		os_print(os," |\r\n");
		start_rec += 16;
	}
    */
	// print all full records
	while(start_rec < end_addr){
		res = read_function(start_rec, 16, d);
		// invert order of bytes
		for (int i = 0; i < 16; i += 4){
			t = d[i];
			d[i] = d[i+3];
			d[i+3] = t;
			t = d[i+1];
			d[i+1] = d[i+2];
			d[i+2] = t;
		}
		if (res != 0){
			os_print(os, "error, read failed\r\n");
			return;
		}
		print_addr_entry(os, start_rec);
		print_hex_bytes(os,d,16,0); // data, count, position
		os_print(os,"| ");
		print_ascii_bytes(os,d,16);
		os_print(os," |\r\n");
		start_rec += 16;
	}
	/*
	// print last partial record
	if (start_rec < end_addr){
		offset = end_addr - start_rec;
		res = read_function(start_addr, offset, d);
		if (res != 0){
			os_print(os, "error, read failed\r\n");
			return;
		}
		print_addr_entry(os, start_rec);
		print_hex_bytes(os,d,offset,0); // data, count, position
		print_missing_bytes(os, 16 - offset, offset); // cnt, position
		os_print(os,"| ");

		print_ascii_bytes(os,d,offset); // data, count
		print_asci_space(os, 16-offset);
		os_print(os," |\r\n");
	}
	*/
	// print bottom of the table
	os_print(os,"+----------+-------------------------------------+\r\n");
}

int flash_read(uint32_t start_addr, uint32_t size, uint8_t *buf)
{
	uint8_t *addr = (uint8_t*)start_addr;
	uint8_t *end = addr + size;

	while(addr < end){
		*buf = *addr;
		buf++;
		addr++;
	}
	return 0;
}

void handle_read_flash(iostream_t *os,char *argstr)
{
	uint32_t offset, size;

	if ((argstr == NULL) || (*argstr == '\0')){
		os_print(os,"error, read need arguments\r\n");
		handle_flash_read_help(os);
		return;
	}

	char *offset_arg;
	char *size_arg;
	char *next_arg;
	// process offset arg
	offset_arg = ustrtok_r(argstr, " ", &next_arg); // can't be null
	if ((offset_arg == NULL) || (strncmp(str_tolower(offset_arg),"--start=",8) != 0)){
		os_print(os,"error expecting start argument\r\n");
		handle_flash_read_help(os);
		return;
	}
	offset_arg += 8;
	int len = strlen(offset_arg);
	if ((len==0) || (hex_2_num(offset_arg, len, &offset) != len)){
		os_print(os,"error expecting start argument\r\n");
		handle_flash_read_help(os);
		return;
	}

	size_arg = ustrtok_r(NULL, " ", &next_arg); // can't be null
	if ((size_arg == NULL) || (strncmp(str_tolower(size_arg),"--size=",7) != 0)){
		os_print(os,"error expecting size argument\r\n");
		handle_flash_read_help(os);
		return;
	}
	size_arg += 7;
	len = strlen(size_arg);
	if ((len==0) || (hex_2_num(size_arg, len, &size) != len)){
		os_print(os,"error expecting start argument\r\n");
		handle_flash_read_help(os);
		return;
	}

	display_formated_content_as_words(os, offset, size,flash_read);
}



//typedef int (*read_i2c_eeprom_fn)(uint32_t i2c_addr, uint32_t start_offset, uint8_t *buf, uint32_t size);
void display_formated_eeprom_content_as_bytes(iostream_t *os, uint32_t eeprom_addr , uint32_t start_offset, uint32_t size, read_i2c_eeprom_fn read_function)
{
	os_print(os,"\r\n");
	uint8_t d[16];
	uint32_t start_rec = start_offset;
	uint32_t end_addr = start_offset + size;
	uint32_t cnt;
	int res;

	// header
	os_print(os,"+----------+-------------------------------------+\r\n");
	os_print(os,"|   ADDR   | BYTES                               |\r\n");
	os_print(os,"+----------+-------------------------------------+\r\n");

	// print first partial record

	if (start_rec < start_offset){
		cnt = start_offset - start_rec;
		res = read_function(eeprom_addr, start_offset, d, 16 - cnt);
		if (res != 0){
			os_print(os, "error, read failed\r\n");
			return;
		}
		print_addr_entry(os, start_rec);
		print_missing_bytes(os, cnt, 0); // cnt, position
		print_hex_bytes(os,d,16 - cnt, cnt); // data, count, position
		os_print(os,"| ");
		print_asci_space(os, cnt);
		print_ascii_bytes(os,d,16 - cnt); // data, count
		os_print(os," |\r\n");
		start_rec += 16;
	}

	// print all full records
	while((start_rec+16) <= end_addr){
		res = read_function(eeprom_addr, start_rec, d, 16);
		if (res != 0){
			os_print(os, "error, read failed\r\n");
			return;
		}
		print_addr_entry(os, start_rec);
		print_hex_bytes(os,d,16,0); // data, count, position
		os_print(os,"| ");
		print_ascii_bytes(os,d,16);
		os_print(os," |\r\n");
		start_rec += 16;
	}

	// print last partial record
	if (start_rec < end_addr){
		cnt = end_addr - start_rec;
		res = read_function(eeprom_addr, start_rec, d, cnt);
		if (res != 0){
			os_print(os, "error, read failed\r\n");
			return;
		}
		print_addr_entry(os, start_rec);
		print_hex_bytes(os,d,cnt,0); // data, count, position
		print_missing_bytes(os, 16 - cnt, cnt); // cnt, position
		os_print(os,"| ");

		print_ascii_bytes(os,d,cnt); // data, count
		print_asci_space(os, 16 - cnt);
		os_print(os," |\r\n");
	}

	// print bottom of the table
	os_print(os,"+----------+-------------------------------------+\r\n");
}

bool get_hex_option(char *argstr, char **next_arg, const char *prefix, uint32_t *val)
{
	char *arg;
	int len;

	len = strlen(prefix);

	arg = ustrtok_r(argstr, " ", next_arg); // can't be null
	if ((arg == NULL) || (strncmp(str_tolower(arg),prefix,len) != 0)){
		return false;
	}
	arg += len;
	len = strlen(arg);
	if ((len==0) || (hex_2_num(arg, len, val) != len)){
		return false;
	}
	return true;
}

char *get_quoted_string(char *argstr, char **next_arg)
{
	char *arg;

	arg = argstr;
    // skip leading space
	while(*arg && (*arg == ' ') ){ arg++; }
	if (*arg == '\0') {
		return NULL;
	}
	if (*arg == '\"'){
        arg++;
	}
    // skip to next unescaped string
	char *p = arg;
	while(  *p &&                                 // NOT NULL and
			( (*p != '\"') ||                     // NOT "\"" or
			  ((*p == '\"') && (*(p-1) == '\\') )  // "\"" AND prev character is "\\"
			)
		 ){ // not a null and not a " unless preceded by escape
		p++;
	}

	if (*p != '\0'){
		*next_arg = p+1;
		*p = '\0';
	}
	else {
	    *next_arg = p;
	}
	return arg;
}

void handle_read_eeprom(iostream_t *os,char *argstr)
{
	uint32_t addr, offset, size;
    bool bres;
	if ((argstr == NULL) || (*argstr == '\0')){
		os_print(os,"error, read need arguments\r\n");
		handle_eeprom_read_help(os);
		return;
	}

	char *next_arg;

	bres = get_hex_option(argstr, &next_arg, "--addr=", &addr);
	if (!bres){
		os_print(os,"\r\nerror expecting address argument\r\n");
		handle_eeprom_read_help(os);
		return;
	}
	argstr = next_arg;
	bres = get_hex_option(argstr, &next_arg, "--start=", &offset);
	if (!bres){
		os_print(os,"\r\nerror expecting offset argument\r\n");
		handle_eeprom_read_help(os);
		return;
	}

	argstr = next_arg;
	bres = get_hex_option(argstr, &next_arg, "--size=", &size);
	if (!bres){
		os_print(os,"\r\nerror expecting offset argument\r\n");
		handle_eeprom_read_help(os);
		return;
	}

	display_formated_eeprom_content_as_bytes(os, addr, offset, size, ReadEEPROM);
}

void handle_write_eeprom(iostream_t *os,char *argstr)
{
	uint32_t addr, offset;
    bool bres;
    char *arg;
    int len;


	if ((argstr == NULL) || (*argstr == '\0')){
		os_print(os,"error, read need arguments\r\n");
		handle_eeprom_write_help(os);
		return;
	}

	char *next_arg;

	bres = get_hex_option(argstr, &next_arg, "--addr=", &addr);
	if (!bres){
		os_print(os,"\r\nerror expecting address argument\r\n");
		handle_eeprom_write_help(os);
		return;
	}
	argstr = next_arg;
	bres = get_hex_option(argstr, &next_arg, "--start=", &offset);
	if (!bres){
		os_print(os,"\r\nerror expecting offset argument\r\n");
		handle_eeprom_write_help(os);
		return;
	}
    // scip spaces to the beggining of the string
	argstr = next_arg;
	arg = get_quoted_string(argstr, &next_arg);
	if (arg == NULL) {
		os_print(os,"\r\nerror expecting string in quotation marks\r\n");
		return;
	}

	len = strlen(arg);
	if (len==0){
		os_print(os,"\r\nerror expecting string in quotation marks\r\n");
		return;
	}
    if (WriteEEPROM(addr, offset, (uint8_t*)arg, len) != 0){
    	os_print(os,"\r\nerror, write failed\r\n");
    }
    os_print(os,"\r\ndone\r\n");
}

void handle_reset(iostream_t *os)
{
	os_print(os,"Reseting, bye\r\n");
	vTaskDelay(500);
	NVIC_SystemReset();
}

__attribute__((naked)) static void start_app(uint32_t pc, uint32_t sp)  {
    __asm volatile ("           \n\
          msr msp, r1 /* load r1 into MSP */\n\
          bx r0       /* branch to the address at r0 */\n\
    ");
}

void clear_state(void)
{

}

void handle_start_app(iostream_t *os)
{
	//volatile int cntDown = 1000000;
	os_print(os,"Starting the application\r\n bye\r\n");
	vTaskDelay(500);
	// disable sys tick interrupt

	LIB_Deinit();
	SysTick->LOAD = 0;
	SysTick->CTRL = 0;
	//while(cntDown-- > 0);

	clear_state();

	volatile uint32_t *app_code = (uint32_t *)__base_APPLICATION_FLASH;
	uint32_t app_sp = app_code[0];
	uint32_t app_reset_vector = app_code[1];
	__set_CONTROL(0);
	start_app(app_reset_vector, app_sp);
	while(1){}
#if 0
	// Disable interrupts
	__disable_irq(); //__asm volatile ("cpsid i");
	// RESET SEQUENCE CONFIGUATION
	__set_CONTROL(0);

	// CurrentMode = Mode_Thread HOW do I set this?
	//PRIMASK<0> = 0
	//__set_PRIMASK(0);

	//FAULTMASK<0> = 0  , __asm volatile ("cpsie i");


	// for i = 0 to 511 ExceptionActive[i] = 0
	// ResetSCSRegs()
	// ClearExclusiveLocal(ProcessorID());
	// ClearEventRegister();
	// SET VTOR
	// SET SP_main from VTOR
	// SET SP_process= '00'
	// SET LR 0xFFFFFFFF
	// IPSR<8:0> = Zeros(9)
	// EPSR.T = tbit , thumb bit
	// BranchTo reset vector address





	SCB->VTOR = (uint32_t *)__base_APPLICATION_FLASH;
	__set_MSP(*stack_ptr);
	__set_PSP(*stack_ptr);
	// Call the reset handler (the construct below is 'pointer to a pointer to a function that takes no arguments and returns void')
	void (**reset_handler)(void) = (void(**)(void))(__base_APPLICATION_FLASH + 1);
	(*reset_handler)();  // Call it as if it were a C function
	// Reenable interrupts
	//__asm volatile ("cpsie i");
	while(1);
#endif
}

void handle_crc32_app(iostream_t *os, char *argstr)
{
	uint32_t crc;
	bool s;
	uint32_t offset, size;

	os_print(os,"\r\n");
	if ((argstr == NULL)||(*argstr == '\0')){
		offset = GetApplicationPartitionOffset();
		size = GetApplicationPartitionSize();
	}
	else { // get the arguments
		char *offset_arg;
		char *size_arg;
		char *next_arg;
		// process offset arg
		offset_arg = ustrtok_r(argstr, " ", &next_arg); // can't be null
		if ((offset_arg == NULL) || (strncmp(str_tolower(offset_arg),"--start=",8) != 0)){
			os_print(os,"error expecting start argument\r\n");
			handle_crc32_app_help(os);
			return;
		}
		offset_arg += 8;
		int len = strlen(offset_arg);
		if ((len==0) || (hex_2_num(offset_arg, len, &offset) != len)){
			os_print(os,"error expecting start argument\r\n");
			handle_crc32_app_help(os);
			return;
		}

		size_arg = ustrtok_r(NULL, " ", &next_arg); // can't be null
		if ((size_arg == NULL) || (strncmp(str_tolower(size_arg),"--size=",7) != 0)){
			os_print(os,"error expecting size argument\r\n");
			handle_crc32_app_help(os);
			return;
		}
		size_arg += 7;
		len = strlen(size_arg);
		if ((len==0) || (hex_2_num(size_arg, len, &size) != len)){
			os_print(os,"error expecting start argument\r\n");
			handle_crc32_app_help(os);
			return;
		}
	}

	s = GetCrcForApplicationPartition(offset, size, &crc);
	if (!s){
		os_print(os,"crc32-app error, invalid size\r\n");
	}
	else {
		os_print(os,"crc32(a=%x,s=%x) = %08x\r\n",offset, size,crc);
	}
}

void fw_pgm_task(void *pvParameters)
{
	uint32_t rcvSize;
	bool bLineRecv;
	bool bDownloadingApp = false;
	int res;
	char *cmdstr;
	char *argstr;
	const char *sepl = " "; // space separated


	BOARD_Init();

	LIB_Init();


	// get commands
	while(1)
	{
		rcvSize = 0;
		//500/portTICK_PERIOD_MS
		bLineRecv = GetLineBlocking(&istr, IN_CMD_BUF, IN_CMD_BUF_SIZE, &rcvSize, portMAX_DELAY);
		if (!bLineRecv){
			continue;
		}
		if (rcvSize == 0){
			dl_print("\r\n");
		}
		//dl_print("[rx]:%s\r\n",IN_CMD_BUF);
		cmdstr = ustrtok_r((char*)IN_CMD_BUF, sepl,&argstr);
		if (cmdstr == NULL){
			dl_print("?\r\n");
			continue;
		}

        //memset(response,0, sizeof(response));
		if (bDownloadingApp && (toupper(cmdstr[0])=='S')) {
			res = parse_srecord(cmdstr, strlen(cmdstr), &tmp_srec);
			if (res != 0){
				os_print(&ostr,"error %d: parsing record.\n", res);
				dl_print("\terror %d: parsing record.\r\n",res);
			}
			else {
			    res = handle_srecord(&tmp_flash_buf, &tmp_srec);
			    if (res != 0){
			    	os_print(&ostr,"error %d: handling record.\n", res);
			    	dl_print("\terror %d: handling record.\r\n",res);
				    bDownloadingApp = false;
			    }
			    else {
			    	cmdstr[10] = '\n';cmdstr[11] = '\0';
			    	os_print(&ostr,cmdstr); // confirmation that the record was handled
			    }
			}
		}
		else {
			bDownloadingApp = false;
		}
		int cmd = CE_UNKNOWN;
		if (!bDownloadingApp) {
			str_tolower(cmdstr);

            for (int i= 0; i < CMD_LIST_CNT; i++){
            	//cmdLen = strlen(CMD_LIST);
            	if (strcmp(cmdstr, CMD_LIST[i]) == 0){
            		cmd = i;
            		break;
            	}
            }
            dl_print("[bl]:%s\r\n",IN_CMD_BUF);
            switch(cmd){
                case CE_CONNECT:
                {
                     handle_connect(&ostr, argstr);
                }break;
                case CE_ERASE_APP:
                {
                	handle_erase_app(&ostr, argstr);

                }break;
                case CE_READ_FLASH:
                {
                	handle_read_flash(&ostr, argstr);
                }break;
                case CE_READ_EEPROM:
                {
                	handle_read_eeprom(&ostr, argstr);
                }break;
                case CE_WRITE_EEPROM:
                {
                	handle_write_eeprom(&ostr, argstr);
                }break;
                case CE_PROGRAM_APP_SREC:
                {
                	os_print(&ostr, "transferring srecords:\n",cmdstr);
                    bDownloadingApp = true;
                }break;
                case CE_START_APP:
                {
                	handle_start_app(&ostr);
                }break;
                case CE_CRC32_APP:
                {
                	handle_crc32_app(&ostr, argstr);
                }break;
                case CE_RESET:
                {
                	handle_reset(&ostr);
                }break;
                default:
                	os_print(&ostr,"error, unknown command: %s\r\n",cmdstr);
                	break;
            }
        }

	    os_flush(&ostr);
	}
}


void fw_load_app_task(void *pvParameters)
{
	TickType_t startTime, endTime;

	vTaskDelay(500/portTICK_PERIOD_MS); // just to get the console started
	startTime = xTaskGetTickCount();
	dl_print( "\r\n\t[%d]TIMER TASK STARTING\r\n", startTime);
	vTaskDelay(LIB_GET_TIMEOUT()); // sleep for 3 seconds
	endTime = xTaskGetTickCount();
	dl_print( "\r\n\t[%d]TIMER TASK elapsed %d\r\n",endTime,TimeDifference(startTime, endTime));
	if (!bBootloaderConnected){ // load the application
		handle_start_app(&cout);
	}
	else {
		vTaskDelete(NULL); // delete itself
	}
}


void bl_console_task(void *pvParameters)
{
	uint32_t rcvSize;
	bool bLineRecv;
	char *cmdstr;
	char *argstr;
	const char *sepl = " "; // space separated


	vTaskDelay(500/portTICK_PERIOD_MS);
	dl_print("%s BOOTLOADER %s\r\n", BOARD_GetBoardStr(), FIRMWARE_VERSION);
	// get commands
	while(1)
	{
		dl_print("\r\n[bl]:");
		rcvSize = 0;
		bLineRecv = GetLineBlocking(&cin, CONSOLE_IN_BUF, CONSOLE_IN_BUF_SIZE, &rcvSize, portMAX_DELAY);
		if (!bLineRecv ){
			continue;
		}
		bBootloaderConnected = true;
		if (rcvSize == 0){
			continue;
		}
		//dl_print("[rx]:%s\r\n",IN_CMD_BUF);
		cmdstr = ustrtok_r((char*)CONSOLE_IN_BUF, sepl,&argstr);
		if (cmdstr == NULL){
			dl_print("\r\n?\r\n");
			continue;
		}

		int cmd = CE_UNKNOWN;

		str_tolower(cmdstr);

		for (int i= 0; i < CMD_LIST_CNT; i++){
			//cmdLen = strlen(CMD_LIST);
			if (strcmp(cmdstr, CMD_LIST[i]) == 0){
				cmd = i;
				break;
			}
		}
		//dl_print("[bl]:%s\r\n",IN_CMD_BUF);
		switch(cmd){
			case CE_ERASE_APP:
			{
				handle_erase_app(&cout, argstr);
			}break;
			case CE_READ_FLASH:
			{
				handle_read_flash(&cout, argstr);
			}break;
            case CE_READ_EEPROM:
            {
            	handle_read_eeprom(&cout, argstr);
            }break;
            case CE_WRITE_EEPROM:
            {
            	handle_write_eeprom(&cout, argstr);
            }break;
			case CE_START_APP:
			{
				handle_start_app(&cout);
			}break;
			case CE_CRC32_APP:
			{
				handle_crc32_app(&cout, argstr);
			}break;
			case CE_RESET:
			{
				handle_reset(&cout);
			}break;
			default:
				dl_print("\r\n\t%s error, unknown command\r\n",cmdstr);
				break;
		}


	    os_flush(&cout);
	}
}


int main(void)
{
	BaseType_t result;


    result = xTaskCreate(fw_pgm_task, "fw_pgm_task",configMINIMAL_STACK_SIZE*2, NULL, PRIORITY_MID, NULL);
    result &= xTaskCreate(bl_console_task, "bl_console_task",configMINIMAL_STACK_SIZE*2, NULL, PRIORITY_LOW, NULL);
    xTaskCreate(DbgServiceTask, "DbgServiceTask",configMINIMAL_STACK_SIZE*2, NULL, PRIORITY_LOW, NULL);

    result &= xTaskCreate(fw_load_app_task, "fw_load_app_task",configMINIMAL_STACK_SIZE, NULL, PRIORITY_MID_HIGH, NULL);

    //if (result != pdPASS)
    //{
    //	return -1;
    //}

    vTaskStartScheduler();

    while(1)
    {

    }

    return 0 ;
}
