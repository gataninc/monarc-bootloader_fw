
#ifndef __SERIALCOMIOS_H_
#define __SERIALCOMIOS_H_
#include "SerialComClient.h"
#include "mios.h"

/*
 * This library makes an io stream on top of serial comm library.
 *
 * */



typedef struct serial_com_ios_server_t {
	iostream_t               *os; // Output stream
	iostream_t               *is; // Input stream
	Connection_t             srv_conn; // server connection
	Connection_t             accept_conn; // acceptor connection
	int                      server_port;
	TaskHandle_t             xHandle;
} serial_com_ios_server_t;

#define SERIAL_COM_IOS_ERROR_OFFSET          (40000)

enum SERIAL_COM_IOS_RESULT {
    SCS_SUCCESS                    = 0 ,// returned by send and receive function

	SCS_ERROR_START                = SER_ERROR_MAX + SERIAL_COM_IOS_ERROR_OFFSET,
	SCS_ERROR_NOT_SUPPORTED,
	SCS_ERROR_BAD_ARG,
	SCS_CANNOT_INITIALIZE_CONNECTION,
	SCS_CANNOT_CREATE_TX_TASK,
};

int SetupSerialComIosServer(
		serial_com_ios_server_t    *scsCtxt  // context, holding all info
);

#endif // __SERIALCOMIOS_H_
