/*
 * SerialComImpl.h
 *
 *  Created on: Aug 30, 2019
 *      Author: lkodheli
 */

#ifndef __SERIALCOMIMPL_H_
#define __SERIALCOMIMPL_H_

#include "SerialComTypes.h"
#include "SerialComClient.h"

#define LINK_FRAME_GET_TIMEOUT_MS       (200)

void _NetLib_GetFreeFrame(Frame_t **ppFrame ,uint32_t timeoutMs, uint16_t lnNum);
void _NetLib_GetFreeFrameFromIsr(Frame_t **ppFrame, BaseType_t *pxTaskWokenByReceive,uint16_t lnNum);

//#define FRAME_GET(PPFRM, TIMEOUT) _NetLib_GetFreeFrame(PPFRM ,TIMEOUT); Dbg_SerialCommTrace("GET FRAME")
#define FRAME_GET(PPFRM, TIMEOUT) _NetLib_GetFreeFrame(PPFRM ,TIMEOUT,__LINE__)
#define FRAME_GET_FROM_ISR(PPFRM, WAKE) _NetLib_GetFreeFrameFromIsr(PPFRM , WAKE, __LINE__)

void Link_ParseFrameByte(FrameRxCtxt_t *ctxt, uint8_t rxByte);

Frame_t * Link_GetFrame(SerialLink_t *link);

int Link_SendFrame(SerialLink_t *link, Frame_t *outFrame);
int Link_HandlePacket(Frame_t *inFrame);


int Net_HandlePacket(Frame_t *inFrame);
int Net_SendPacket(Frame_t *inFrame);

int Tran_Init();
void Tran_Deinit(void);
int Tran_ConfigureEndPoint(ConnectionEndPoint_t *ep, uint8_t hostPort, uint8_t destDev, uint8_t destPort);

int Tran_HandlePacket(Frame_t *inFrame);
int Tran_SendPacket(Frame_t *outFrame);

int EP_HandlePacket(ConnectionEndPoint_t *ep, Frame_t *packet);

//int EP_InitConnection(Connection_t *conn);
//void EP_DeinitConnection(Connection_t *conn);


int EP_DisableSession_Internal(ConnectionEndPoint_t *ep);


#endif /* __SERIALCOMIMPL_H_ */
