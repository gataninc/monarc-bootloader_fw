/*
 * utils.c
 *
 *  Created on: Jul 9, 2018
 *      Author: stelasang
 */


#include "utils.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdbool.h>
#include <math.h>
#include <ctype.h>

#include "FreeRTOSConfig.h"
#include <FreeRTOS.h>
#include <event_groups.h>

#if defined (__SEMIHOST_HARDFAULT_DISABLE)
// It clashes with semihosting HardFault handler
/* The prototype shows it is a naked function - in effect this is just an
assembly function. */
void HardFault_Handler( void ) __attribute__( ( naked ) );

/* The fault handler implementation calls a function called
prvGetRegistersFromStack(). */
void HardFault_Handler(void)
{
    __asm volatile
    (
        " tst lr, #4                                                \n"
        " ite eq                                                    \n"
        " mrseq r0, msp                                             \n"
        " mrsne r0, psp                                             \n"
        " ldr r1, [r0, #24]                                         \n"
        " ldr r2, handler2_address_const                            \n"
        " bx r2                                                     \n"
        " handler2_address_const: .word prvGetRegistersFromStack    \n"
    );
}
#endif

void prvGetRegistersFromStack( uint32_t *pulFaultStackAddress )
{
/* These are volatile to try and prevent the compiler/linker optimising them
away as the variables never actually get used.  If the debugger won't show the
values of the variables, make them global my moving their declaration outside
of this function. */
volatile uint32_t r0;
volatile uint32_t r1;
volatile uint32_t r2;
volatile uint32_t r3;
volatile uint32_t r12;
volatile uint32_t lr; /* Link register. */
volatile uint32_t pc; /* Program counter. */
volatile uint32_t psr;/* Program status register. */

    r0 = pulFaultStackAddress[ 0 ];
    r1 = pulFaultStackAddress[ 1 ];
    r2 = pulFaultStackAddress[ 2 ];
    r3 = pulFaultStackAddress[ 3 ];

    r12 = pulFaultStackAddress[ 4 ];
    lr = pulFaultStackAddress[ 5 ];
    pc = pulFaultStackAddress[ 6 ];
    psr = pulFaultStackAddress[ 7 ];

    /* When the following line is hit, the variables contain the register values. */
    for( ;; );
}

int gethdig(char c)
{
	int digit;

	if (c<'0') return 0;

	digit = c - '0';
	if (digit < 10) {
		return digit;
	}
	digit = c - 'A';
	if (digit < 6){
		return digit + 10;
	}
	digit = c - 'a';
	if (digit < 6){
		return digit + 10;
	}

	return 0;
}

bool get_hex_nibble(const char c, uint8_t *dest)
{
	uint8_t n1;

	if ((c >= '0') && (c <= '9')) {
		n1 = c - '0';
	}
	else if (c>='A'&&c<='F'){
		n1 = c - 'A' + 10;
	}
	else if (c>='a'&&c<='f'){
		n1 = c - 'a'+ 10;
	}
	else {
		return false;
	}
	*dest = n1;
	return true;
}

bool get_hex_byte(const char *bstr, uint8_t *dest)
{
	uint8_t n1, n2;

	if (!get_hex_nibble(bstr[0], &n1)){
		return false;
	}
	if (!get_hex_nibble(bstr[1], &n2)){
		return false;
	}

	*dest = (n1 << 4) | n2;
	return true;
}

int ishexnum(char c)
{
	return ((c>='0'&&c<='9') || (c>='A'&&c<='F') || (c>='a'&&c<='f') );
}

int hex_2_num(const char *start, int len, uint32_t *dest)
{
	uint32_t res = 0;
	uint8_t nibble = 0;

	for (int i = 0; i < len; i++){
		if (!get_hex_nibble(start[i], &nibble)) {
			*dest = res;
			return i;
		}
		res = (res << 4) + nibble;
	}
	*dest = res;
	return len;
}


char *gethex(char *p, int *v)
{
	int val = 0;
	while (*p && isspace((int)*p)) p++;
	if(!*p) return NULL;

	while (*p && !isspace((int)*p) && (*p!=EXECHAR))
	{
		if (ishexnum(*p)) {
			val <<= 4; val |= gethdig(*p++);
		} else
			return NULL;
	}
	*v = val;
	while (*p && isspace((int)*p)) p++;
	return p;
}


char *vgethex(char *p, int *v)
{
	int val = 0;
	while (*p && isspace((int)*p))
		p++;
	if(!*p)
		return NULL;

	while (*p && ishexnum(*p))
	{
		 val <<= 4; val |= gethdig(*p++);
	}
	*v = val;
	return p;
}


bool HexExec(char *p, int *v)
{
	if( (p = gethex(p, v)) && *p == EXECHAR )
		return true;
	else
		return false;

}

bool Hex1Exec(char *p, int *v, int* d)
{
	*d = gethdig(*p);
	p++;
	if( (p = gethex(p, v)) && *p == EXECHAR )
		return true;
	else
		return false;

}

bool is_separator(const char c, const char *sep, const int sep_len)
{
	for(int i = 0; i < sep_len; i++){
		if (c == sep[i]){
			return true;
		}
	}

	return false;
}

char *ustrtok_r(char *s1, const char *s2, char **s3)
{
	char *start_tok = NULL;
	int separator_len = 0;
	char *p = NULL;


	if ((s2 == NULL) || (s3 == NULL)){
		return NULL;
	}
	if ( (s1 == NULL) &&
		 ((*s3 == NULL) || (**s3 == '\0'))
		){ // **s3 points to null character or *s points to a null string no string referenced.
		return NULL;
	}
	if ( (s1 != NULL) && (*s1 == '\0')){
		return NULL;
	}

	separator_len = strlen(s2);
	if (separator_len == 0){
		return NULL;
	}

	if(s1 == NULL){
		p = *s3;
	}
	else{
		p = s1;
	}

	// skip all separators occurring at the beginning of the string
	bool in_separator = false;
	do{
		in_separator = is_separator(*p, s2, separator_len);
		if (in_separator){
			p++; // skips the separator
		}
	} while(((*p) != '\0') && in_separator);

	// check if we reached the end of the string
	if (*p == '\0'){
		*s3 = p;
		return NULL;
	}
	// we have a token starting at this address
	// it is not null char and it is not a separator
	start_tok = p;
	// skip all characters that are not separators
	in_separator = false;
	do {
		p++; // take next character
		in_separator = is_separator(*p, s2, separator_len);
	}while( ((*p) != '\0') && (!in_separator));

	// either we are in a separator or hit the end of the string
    if (in_separator){ // in separator
    	*p = '\0';   // terminate the token
    	p++;         // point to the next character, possible start of next token
    }

    *s3 = p; // store the start of next token

    return start_tok;
}

int32_t TimeDifference(uint32_t first, uint32_t second)
{
    uint32_t udiff = second - first;
    return (int32_t)udiff;
}

char *str_tolower(char *str)
{
    for(int i =0; str[i]; i++){
    	str[i] = tolower(str[i]);
    }
    return str;
}

