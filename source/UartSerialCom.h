
#ifndef __UARTSERIALCOM_H_
#define __UARTSERIALCOM_H_
#include "SerialComClient.h"


typedef void (*send_fp)(uint8_t* message, int length);
typedef int (*recieve_fp)(uint8_t* buf, int size, size_t *received, TickType_t timeout);

typedef struct uart_link_context_t {
	send_fp                 Send;
	recieve_fp              Recieve;
	SerialLink_t            link;

	TaskHandle_t            xLinkTxHandle;
	//EventGroupHandle_t    txEvent;
	//EventGroupHandle_t    rxEvent;
} uart_link_context_t;


#define UART_ERROR_OFFSET          (20000)

enum UART_SERIAL_COM_RESULT {
    USC_SUCCESS                    = 0 ,// returned by send and receive function

	USC_ERROR_START                = SER_ERROR_MAX + UART_ERROR_OFFSET,
	USC_ERROR_NOT_SUPPORTED,
	USC_CANNOT_CREATE_TX_TASK,
	USC_CANNOT_CREATE_RX_TASK,
};

int Link_InitUart(uart_link_context_t *uartSerCtxt);
void Link_DeinitUart(uart_link_context_t *uartSerCtxt);

#endif // __ROCUARTSERIALCOM_H_
