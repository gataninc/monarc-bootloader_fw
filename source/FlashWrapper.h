/*
 * FlashWrapper.h
 *
 *  Created on: Jun 29, 2018
 *      Author: stelasang
 */

#ifndef FLASHWRAPPER_H_
#define FLASHWRAPPER_H_

#include "fsl_flash.h"
#include <stdint.h>

typedef bool BOOL;
#define TRUE	(1)
#define FALSE	(0)


int InitFlashWrapper();
int EraseApplicationPartition();
int WriteToApplicationPartition(uint32_t offset, uint32_t* buf, uint32_t sizeInBytes);
BOOL GetCrcForApplicationPartition(uint32_t offset, uint32_t sizeInBytes, uint32_t *crc);
//int MarkUpperHalfAsBootImage();
uint32_t GetApplicationPartitionOffset();
uint32_t GetApplicationPartitionSize(); // minus swap block at the end of the flash
//int SetUpperBlockSwapState_Update(flash_swap_state_config_t *flashSwapState, uint32_t *loc);
//int SetUpperBlockSwapState_Complete(flash_swap_state_config_t *flashSwapState, uint32_t *loc);
//int GetUpperBlockSwapState(flash_swap_state_config_t *flashSwapState);

//BOOL IsUpperBlockNeedsErasing();

#endif /* FLASHWRAPPER_H_ */
