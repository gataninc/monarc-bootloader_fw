#include "SpiSerialCom.h"
#include "SerialComClient.h"
#include "FreeRTOS.h"
#include "fsl_clock.h"
#include "dbglog.h"
#include "string.h"
#include "stdio.h"
#include "queue.h"

//#include "dbglog_target.h"

void DSPI_SlaveUserCallback(SPI_Type *base, dspi_slave_handle_t *handle,
		status_t status, void *userData);
static void DSPI_MasterUser_Callback(SPI_Type *base, dspi_master_handle_t *drv_handle, status_t status, void *userData);
void print_spi_frame(bool in, uint8_t* bytes);

#define TRANSFER_BAUDRATE (300000U)

#define SPI_CHANNEL_SERVICE_TIME             (100)

#define DEBUG_SPI_RX
#define DEBUG_SPI_TX
//#define DEBUG_SPI_TX_MASTER

static QueueHandle_t       Spi1TxQueue;
static QueueHandle_t       Spi1RxQueue;
bool                       Spi1_IsMaster;

#define SPI_TX_FRAME_SIZE_MAX   (256)
#define SPI_RX_FRAME_SIZE_MAX   (1024) // power of 2 size only
typedef struct TxEntry_t {
	uint8_t data[SPI_TX_FRAME_SIZE_MAX];
	uint32_t tx_index;
	uint32_t tx_size;
} TxEntry_t;

#define TX_INFO_NUM_ENTRIES         (4)
#define TX_INFO_NUM_MASK            (TX_INFO_NUM_ENTRIES - 1)
#define TX_INFO_STATUS_OVERFLOW     (1)
typedef struct TxInfo_t {
	TxEntry_t entry[TX_INFO_NUM_ENTRIES];
	uint32_t reader;
	uint32_t writer;
	uint32_t status;
} TxInfo_t;

typedef struct RxEntry_t {
	uint8_t data[SPI_RX_FRAME_SIZE_MAX];
	uint32_t reader;
	uint32_t writer;
	uint32_t status;
} RxEntry_t;

TxInfo_t   TxInfo;
RxEntry_t  RxRb;

//----------------------------------------------------------------------------------------------------------
// SPI SLAVE TX/RX
//----------------------------------------------------------------------------------------------------------

void spi_init_shandle(spi_shandle_t *pspi)
{
	pspi->txFreeFrameQueue = xQueueCreate(TX_QUEUE_FRM_CNT, sizeof(spi_frame_t*));
    assert(pspi->txFreeFrameQueue != NULL);

    pspi->rxFreeFrameQueue = xQueueCreate(RX_QUEUE_FRM_CNT, sizeof(spi_frame_t*));
    assert(pspi->rxFreeFrameQueue != NULL);

    pspi->rxFrameQueue = xQueueCreate(RX_QUEUE_FRM_CNT, sizeof(spi_frame_t*));
    assert(pspi->rxFrameQueue != NULL);

    pspi->txFrameQueue = xQueueCreate(TX_QUEUE_FRM_CNT, sizeof(spi_frame_t*));
    assert(pspi->txFrameQueue != NULL);


    spi_frame_t *pSpiFrame;
    // init free frame queues
    for (uint8_t i = 0; i < TX_QUEUE_FRM_CNT; i++){
    	pSpiFrame = &(pspi->TxFrameArray[i]);
    	xQueueSend(pspi->txFreeFrameQueue, &pSpiFrame, portMAX_DELAY);
    }

    // init free frame queues
    for (uint8_t i = 0; i < RX_QUEUE_FRM_CNT; i++){
    	pSpiFrame = &(pspi->RxFrameArray[i]);
        xQueueSend(pspi->rxFreeFrameQueue, &pSpiFrame,portMAX_DELAY);
    }
    pspi->Event = xEventGroupCreate();
    assert(pspi->Event != NULL);
}

void spi_slave_init(spi_shandle_t *pspi, SPI_Type *dspi_spi)
{
	dspi_slave_config_t dspi_SlaveConfig;

	spi_init_shandle(pspi);


	pspi->dspi_spi = dspi_spi;
	Spi1_IsMaster = false;

	dspi_SlaveConfig.whichCtar = kDSPI_Ctar0;
	dspi_SlaveConfig.ctarConfig.bitsPerFrame = 8;
	dspi_SlaveConfig.ctarConfig.cpol = kDSPI_ClockPolarityActiveHigh;
	dspi_SlaveConfig.ctarConfig.cpha = kDSPI_ClockPhaseFirstEdge;
	dspi_SlaveConfig.enableContinuousSCK = false;
	dspi_SlaveConfig.enableModifiedTimingFormat = false;
	dspi_SlaveConfig.enableRxFifoOverWrite = false;
	dspi_SlaveConfig.samplePoint = kDSPI_SckToSin0Clock;
	DSPI_SlaveInit(pspi->dspi_spi, &dspi_SlaveConfig);

	//DSPI_SlaveTransferCreateHandle(pspi->dspi_spi, &pspi->slave.handle, DSPI_SlaveUserCallback, (void *)(pspi));

	if (dspi_spi == SPI0){
	    NVIC_SetPriority(SPI0_IRQn, 6);
	    EnableIRQ(SPI0_IRQn);
	}
	else {
		FillUpTxFifo(SPI1);
		DSPI_EnableInterrupts(SPI1, kDSPI_TxFifoUnderflowInterruptEnable | kDSPI_RxFifoOverflowInterruptEnable | kDSPI_RxFifoDrainRequestInterruptEnable);
		EnableIRQ(SPI1_IRQn);
		NVIC_SetPriority(SPI1_IRQn, 6);
	}
}

#if 0
void spi_master_init_spi1(spi_shandle_t *pspi)
{
	dspi_master_config_t spiConf;
	uint32_t sourceClock;

	spiConf.whichCtar = kDSPI_Ctar1;
	spiConf.ctarConfig.baudRate = TRANSFER_BAUDRATE;
	spiConf.ctarConfig.bitsPerFrame = 8;
	spiConf.ctarConfig.cpol = kDSPI_ClockPolarityActiveHigh;

	spiConf.ctarConfig.cpha = kDSPI_ClockPhaseFirstEdge;
	spiConf.ctarConfig.direction = kDSPI_MsbFirst;
	spiConf.ctarConfig.pcsToSckDelayInNanoSec = 1000000000U / TRANSFER_BAUDRATE;
	spiConf.ctarConfig.lastSckToPcsDelayInNanoSec = 1000000000U / TRANSFER_BAUDRATE;
	spiConf.ctarConfig.betweenTransferDelayInNanoSec = 1000000000U / TRANSFER_BAUDRATE;

	spiConf.whichPcs = (kDSPI_Pcs0);
	spiConf.pcsActiveHighOrLow = kDSPI_PcsActiveLow;
	spiConf.enableContinuousSCK = false;
	spiConf.enableRxFifoOverWrite = false;
	spiConf.enableModifiedTimingFormat = false;
	spiConf.samplePoint = kDSPI_SckToSin0Clock;

	NVIC_SetPriority(SPI1_IRQn, 6);
	sourceClock = CLOCK_GetFreq(DSPI1_CLK_SRC);
	DSPI_RTOS_Init(&pspi->rtosMaster, SPI1, &spiConf, sourceClock);
}
#endif

void spi_master_init_spi1(spi_shandle_t *pspi)
{
	dspi_master_config_t spiConf;
	uint32_t sourceClock;



	spiConf.whichCtar = kDSPI_Ctar1;
	spiConf.ctarConfig.baudRate = TRANSFER_BAUDRATE;
	spiConf.ctarConfig.bitsPerFrame = 8;
	spiConf.ctarConfig.cpol = kDSPI_ClockPolarityActiveHigh;

	spiConf.ctarConfig.cpha = kDSPI_ClockPhaseFirstEdge;
	spiConf.ctarConfig.direction = kDSPI_MsbFirst;
	spiConf.ctarConfig.pcsToSckDelayInNanoSec = 1000000000U / TRANSFER_BAUDRATE;
	spiConf.ctarConfig.lastSckToPcsDelayInNanoSec = 1000000000U / TRANSFER_BAUDRATE;
	spiConf.ctarConfig.betweenTransferDelayInNanoSec = 1000000000U / TRANSFER_BAUDRATE;

	spiConf.whichPcs = (kDSPI_Pcs0);
	spiConf.pcsActiveHighOrLow = kDSPI_PcsActiveLow;
	spiConf.enableContinuousSCK = false;
	spiConf.enableRxFifoOverWrite = false;
	spiConf.enableModifiedTimingFormat = false;
	spiConf.samplePoint = kDSPI_SckToSin0Clock;

	NVIC_SetPriority(SPI1_IRQn, 5);
	sourceClock = CLOCK_GetFreq(DSPI1_CLK_SRC);
	DSPI_MasterInit(SPI1, &spiConf, sourceClock);

	DSPI_MasterTransferCreateHandle(SPI1, &pspi->master.handle, DSPI_MasterUser_Callback, (void *)pspi);
}


void spi_master_init(spi_shandle_t *pspi, SPI_Type *dspi_spi)
{
	spi_init_shandle(pspi);

	pspi->dspi_spi = dspi_spi;

	if (dspi_spi == SPI0){
	}
	else {
		Spi1_IsMaster = true;
		spi_master_init_spi1(pspi);
	}
}



static void DSPI_MasterUser_Callback(SPI_Type *base, dspi_master_handle_t *drv_handle, status_t status, void *userData)
{
	spi_shandle_t *pspi = (spi_shandle_t *)userData;
	BaseType_t xHigherPriorityTaskWoken, xResult;

    if (status == kStatus_Success){
		xResult = xEventGroupSetBitsFromISR(pspi->Event, RTOS_SPI_COMPLETE, &xHigherPriorityTaskWoken);
    }
    else if (status == kStatus_DSPI_Error){
    	xResult = xEventGroupSetBitsFromISR(pspi->Event, RTOS_SPI_ERROR, &xHigherPriorityTaskWoken);
    }

    if (xResult == pdPASS) {
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}


void FillUpTxFifo(SPI_Type *dspi_spi)
{
	while(DSPI_GetStatusFlags(dspi_spi) & kDSPI_TxFifoFillRequestFlag){
		dspi_spi->PUSHR_SLAVE = 0;
		DSPI_ClearStatusFlags(dspi_spi, kDSPI_TxFifoFillRequestFlag);
	}
}

bool SpiBadState = false;
void SPI1_IRQHandler(void)
{
	uint32_t dataSend = 0;
	uint8_t dataReceived = 0;
	TxEntry_t *pEntry;
	uint32_t status;
	uint32_t writer_next;
	//BaseType_t xHigherPriorityTaskWoken;

	if (!Spi1_IsMaster){

		status = DSPI_GetStatusFlags(SPI1);
		while(status & kDSPI_RxFifoDrainRequestFlag)
		{
			dataSend = 0; // default fill

			// RX data
			dataReceived = SPI1->POPR;
			DSPI_ClearStatusFlags(SPI1, kDSPI_RxFifoDrainRequestFlag);

			if (TxInfo.reader != TxInfo.writer){ // tx not empty
				pEntry = &TxInfo.entry[TxInfo.reader];
				dataSend = pEntry->data[pEntry->tx_index++];
				if (pEntry->tx_index == pEntry->tx_size){
					TxInfo.reader = (TxInfo.reader+1) & TX_INFO_NUM_MASK;
				}
			}
			SPI1->PUSHR_SLAVE = dataSend;
			DSPI_ClearStatusFlags(SPI1, kDSPI_TxFifoFillRequestFlag);

			writer_next = (RxRb.writer + 1) & (SPI_RX_FRAME_SIZE_MAX - 1);
			if (writer_next != (RxRb.reader)){
				RxRb.data[RxRb.writer] = dataReceived;
				RxRb.writer = writer_next;
			}
			else{
                RxRb.status++;
			}
			status = DSPI_GetStatusFlags(SPI1);

			//if ((Spi1TxQueue == NULL) || (xQueueReceiveFromISR( Spi1TxQueue, &dataSend, &xHigherPriorityTaskWoken ) != pdTRUE)){
			//	dataSend = 0;
			//}
			//if ((Spi1RxQueue != NULL)) {
			//	xQueueSendFromISR( Spi1RxQueue, &dataReceived, &xHigherPriorityTaskWoken );
			//}
		}

		status = DSPI_GetStatusFlags(SPI1);
		if (status & (kDSPI_TxFifoUnderflowFlag | kDSPI_RxFifoOverflowFlag)){
			//DSPI_FlushFifo(DSPI_2_PERIPHERAL, true, true);
			DSPI_ClearStatusFlags(SPI1, (kDSPI_TxFifoUnderflowFlag | kDSPI_RxFifoOverflowFlag));
			SpiBadState = true;
		}

		/* Actual macro used here is port specific. */
		//portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}

	else {
		SPI1_DriverIRQHandler();
	}

}


void spi_slave_rx_tx_task(void *pvParameters)
{
	spi_link_context_t *pctxt = (spi_link_context_t *) pvParameters;
    spi_shandle_t *pspi = &pctxt->spi;
    SerialLink_t  *plink = &pctxt->link;
    uint32_t      writer_next;
    TxEntry_t     *pEntry;
    uint32_t      numBytes;
    int           res;

    while(1)
    {
    	// process received bytes
    	while(RxRb.writer != RxRb.reader){
    		Link_Rx_Byte(plink, RxRb.data[RxRb.reader]);
    		RxRb.reader = (RxRb.reader + 1) & (SPI_RX_FRAME_SIZE_MAX - 1);
    	}

    	// check if there is enough space in the ring
    	writer_next = (TxInfo.writer + 1) & TX_INFO_NUM_MASK;

    	while(TxInfo.reader != writer_next)
    	{
    		pEntry = &TxInfo.entry[TxInfo.writer];
    		pEntry->tx_index = 0;

    		res = Link_Tx_GetBufferNonBlocking(plink, pEntry->data, SPI_TX_FRAME_SIZE_MAX, &numBytes);
    		if ((res == SER_SUCCESS) && (numBytes > 0)){
    			pEntry->tx_size = numBytes;
    			TxInfo.writer = writer_next;
    			writer_next = (TxInfo.writer + 1) & TX_INFO_NUM_MASK;
    		}
    		else { // nothing to write so exit
    			break;
    		}
    	}

    	vTaskDelay(15);
    }

}

void spi_master_rx_tx_task(void *pvParameters)
{
	int res;

    EventBits_t ev;
    dspi_transfer_t masterXfer;
    int windowCnt = 0;
    uint32_t serviceTime;
    const int TX_RX_BUFF_SIZE = 256;

    spi_link_context_t *pctxt = (spi_link_context_t *) pvParameters;
    spi_shandle_t *pspi = &pctxt->spi;
    SerialLink_t  *plink = &pctxt->link;

    uint8_t txBuff[TX_RX_BUFF_SIZE];
    uint8_t rxBuff[TX_RX_BUFF_SIZE];
    UBaseType_t numBytes;
    uint32_t txSize;

    while(1) {
    	txSize = TX_RX_BUFF_SIZE;
    	res = Link_Tx_GetBufferNonBlocking(plink, txBuff, TX_RX_BUFF_SIZE, &numBytes);

    	if ((res == SER_SUCCESS) && (numBytes > 0))
    	{
    		txSize = numBytes;
    		windowCnt = 7;
    	}
    	else {
    		memset(txBuff,0, TX_RX_BUFF_SIZE);
    	}

    	masterXfer.dataSize = txSize;
    	masterXfer.txData = txBuff;
    	masterXfer.rxData = rxBuff;
    	masterXfer.configFlags = kDSPI_MasterCtar1 | kDSPI_MasterPcs0 | kDSPI_MasterPcsContinuous;
        //slaveXfer.kDSPI_SlaveCtar1;

    	res = DSPI_MasterTransferNonBlocking(pspi->dspi_spi, &pspi->master.handle, &masterXfer);
    	if (res != kStatus_Success){
    		vTaskDelay(2); // debug it
    	}

    	ev = xEventGroupWaitBits(pspi->Event,
    			                 RTOS_SPI_COMPLETE | RTOS_SPI_ERROR,
    	                         pdTRUE, pdFALSE, portMAX_DELAY);

    	//DSPI_RTOS_Transfer(&pspi->rtosMaster, &masterXfer);

    	if (ev & RTOS_SPI_ERROR){
    		xEventGroupClearBits(pspi->Event, RTOS_SPI_COMPLETE);
    	}
    	else{
    		for(int i = 0; i < txSize; i++){
    			Link_Rx_Byte(plink, rxBuff[i]);
    		}
    	}

		// decide on the wait time
		if(windowCnt <= 0){
			serviceTime = 60;
			windowCnt = 0;
		}
		else if (windowCnt < 2){
			serviceTime = 40;
		}
		else if (windowCnt < 3){
			serviceTime = 25;
		}
		else if (windowCnt < 4){
			serviceTime = 20;
		}
		else if (windowCnt <= 5){
			serviceTime = 15;
		}
		else if (windowCnt <= 6){
			serviceTime = 10;
		}
		else if (windowCnt <= 7){
			serviceTime = 5;
		}
		windowCnt--;

    	if (serviceTime){
            // delay so that the slave can get all the data
            vTaskDelay(serviceTime); // checking if this helps with missing spi frames
    	}
    }

	vTaskDelete(0); // delete itself
}


#if 0
void spi_master_rx_tx_task(void *pvParameters)
{
	int res;

    EventBits_t ev;
    dspi_transfer_t masterXfer;
    int windowCnt = 0;
    uint32_t serviceTime;
    const int TX_RX_BUFF_SIZE = 300;

    spi_shandle_t *pspi = (spi_shandle_t *)pvParameters;

    uint8_t txBuff[TX_RX_BUFF_SIZE];
    uint8_t rxBuff[TX_RX_BUFF_SIZE];
    UBaseType_t numMessages;
    uint32_t txSize;

    // Wait for queue's to be created
    while( (Spi1TxQueue == NULL) || (Spi1RxQueue == NULL)){
    	vTaskDelay(100);
    }

    while(1) {
    	numMessages = uxQueueMessagesWaiting(Spi1TxQueue);

    	if (numMessages > 0) { // get a free frame if not already allocated
    		txSize = min(numMessages, TX_RX_BUFF_SIZE);
    		for (int i = 0; i < txSize; i++){
    	        xQueueReceive(Spi1TxQueue, &txBuff[i],  portMAX_DELAY);
    		}
    	}
    	else {
    		memset(txBuff,0, TX_RX_BUFF_SIZE);
    		txSize = TX_RX_BUFF_SIZE;
    	}

    	//memset(pRxFrame->bytes, 0, SLAVE_TRANSFER_SIZE);
    	masterXfer.dataSize = txSize;
    	masterXfer.txData = txBuff;
    	masterXfer.rxData = rxBuff;
    	masterXfer.configFlags = kDSPI_MasterCtar1 | kDSPI_MasterPcs0 | kDSPI_MasterPcsContinuous;
        //slaveXfer.kDSPI_SlaveCtar1;


    	res = DSPI_MasterTransferNonBlocking(pspi->dspi_spi, &pspi->master.handle, &masterXfer);
    	if (res != kStatus_Success){
    		vTaskDelay(2); // debug it
    	}

    	ev = xEventGroupWaitBits(pspi->Event,
    			                 RTOS_SPI_COMPLETE | RTOS_SPI_ERROR,
    	                         pdTRUE, pdFALSE, portMAX_DELAY);


    	//DSPI_RTOS_Transfer(&pspi->rtosMaster, &masterXfer);

    	if (ev & RTOS_SPI_ERROR){
    		xEventGroupClearBits(pspi->Event, RTOS_SPI_COMPLETE);
    	}
    	else {
    		for (int i = 0; i < txSize;  i++){
   				xQueueSend(Spi1RxQueue, &rxBuff[i], portMAX_DELAY);
     		}
    	}

    	if (numMessages > 0){
    		serviceTime = 5; // next service very fast
    		windowCnt = 5;
    	}
    	else {
			// decide on the wait time
			if(windowCnt <= 0){
				serviceTime = 60;
				windowCnt = 0;
			}
			else if (windowCnt < 2){
				serviceTime = 40;
			}
			else if (windowCnt < 3){
				serviceTime = 20;
			}
			else if (windowCnt < 4){
				serviceTime = 10;
			}
			else if (windowCnt <= 5){
				serviceTime = 5;
			}
			windowCnt--;
    	}


    	if (serviceTime){
            // delay so that the slave can get all the data
            vTaskDelay(serviceTime); // checking if this helps with missing spi frames
    	}
    }

	vTaskDelete(0); // delete itself
}
#endif
//----------------------------------------------------------------------------------------------------------
// SPI SERIAL LINK
//----------------------------------------------------------------------------------------------------------


#if 0
void spi_link_rx_task(void *pvParameters)
{
	spi_link_context_t *pctxt = (spi_link_context_t *) pvParameters;
	spi_shandle_t *pspi = &pctxt->spi;
	SerialLink_t  *plink = &pctxt->link;

	//spi_frame_t *pfrm;
	uint8_t rxData;

	Spi1RxQueue  = xQueueCreate(1024, sizeof(uint8_t));
	assert(Spi1RxQueue);

    for(;;) {
		if(xQueueReceive(Spi1RxQueue, &rxData,  portMAX_DELAY) == pdTRUE){

			//for (int i = 0; i < pfrm->field.cnt; i++){
			Link_Rx_Byte(plink, rxData);
			//}

#ifdef DEBUG_SPI_RX_
    		print_spi_frame(true ,rxData, 1); // Note: pTxFrame is modified by the print function
#endif
		}
    }
}

void spi_link_tx_task(void *pvParameters)
{
	spi_link_context_t *pctxt = (spi_link_context_t *) pvParameters;
	spi_shandle_t *pspi = &pctxt->spi;
	SerialLink_t  *plink = &pctxt->link;

	uint32_t numBytes;
	uint8_t txBuff[100];
	int res;

	Spi1TxQueue  = xQueueCreate(1024, sizeof(uint8_t));
	assert(Spi1TxQueue);

    for(;;) {
    	// get a free spi transmit frame
		res = Link_Tx_GetBuffer(plink, txBuff, 100, &numBytes);
		if (res != SER_SUCCESS){
			continue;
		}

		for (unsigned i = 0; i < numBytes; i++){
			xQueueSend(Spi1TxQueue, &txBuff[i], portMAX_DELAY);// != pdTRUE){
		}

#ifdef DEBUG_SPI_RX_
		print_spi_frame(false ,txBuff, numBytes); // Note: pTxFrame is modified by the print function
#endif

    }
}
#endif


int Link_InitSlaveSpi(spi_link_context_t *pSpiCtxt, SPI_Type *dspi)
{
	BaseType_t xReturned;
	int res = Link_Init(&pSpiCtxt->link);
	if (res != SER_SUCCESS) {
		return res;
	}

	spi_slave_init(&pSpiCtxt->spi, dspi);


	xReturned = xTaskCreate(spi_slave_rx_tx_task, "spi_slave_rx_tx_task",configMINIMAL_STACK_SIZE*2, (pSpiCtxt), PRIORITY_MAX, &pSpiCtxt->xTxRxHandle); // very fast
	if (xReturned != pdPASS){
	    return SSER_CANNOT_CREATE_SLAVE_RXTX_TASK;
	}
	//xReturned = xTaskCreate(spi_link_rx_task, "spi_link_rx_task",configMINIMAL_STACK_SIZE, (pSpiCtxt), PRIORITY_HIGH, &pSpiCtxt->xLinkRxHandle);
	//if (xReturned != pdPASS){
	//    return SSER_CANNOT_CREATE_LINK_RX_TASK;
	//}
	//xReturned = xTaskCreate(spi_link_tx_task, "spi_link_tx_task",configMINIMAL_STACK_SIZE, (pSpiCtxt), PRIORITY_HIGH, &pSpiCtxt->xLinkTxHandle);
	//if (xReturned != pdPASS){
	//    return SSER_CANNOT_CREATE_LINK_TX_TASK;
	//}
	return SSER_SUCCESS;
}

int Link_InitMasterSpi(spi_link_context_t *pSpiCtxt, SPI_Type *dspi)
{
	BaseType_t xReturned;
	int res = Link_Init(&pSpiCtxt->link);
	if (res != SER_SUCCESS) {
		return res;
	}

	spi_master_init(&pSpiCtxt->spi, dspi);

	xReturned = xTaskCreate(spi_master_rx_tx_task, "spi_master_rx_tx_task",configMINIMAL_STACK_SIZE*2, (pSpiCtxt), PRIORITY_MAX, &pSpiCtxt->xTxRxHandle); // very fast
	if (xReturned != pdPASS){
	    return SSER_CANNOT_CREATE_SLAVE_RXTX_TASK;
	}

	//xReturned = xTaskCreate(spi_link_rx_task, "spi_link_rx_task",configMINIMAL_STACK_SIZE, (pSpiCtxt), PRIORITY_MID_HIGH, &pSpiCtxt->xLinkRxHandle);
	//if (xReturned != pdPASS){
	//    return SSER_CANNOT_CREATE_LINK_RX_TASK;
	//}
	//xReturned = xTaskCreate(spi_link_tx_task, "spi_link_tx_task",configMINIMAL_STACK_SIZE, (pSpiCtxt), PRIORITY_MID_HIGH, &pSpiCtxt->xLinkTxHandle);
	//if (xReturned != pdPASS){
	//    return SSER_CANNOT_CREATE_LINK_TX_TASK;
	//}

	return SSER_SUCCESS;
}

void Link_DeinitSpi(spi_link_context_t *pSpiCtxt)
{
	//DSPI_StopTransfer(pSpiCtxt->spi.dspi_spi);
	//DSPI_DisableInterrupts(pSpiCtxt->spi.dspi_spi, kDSPI_AllInterruptEnable);
	//DSPI_FlushFifo(pSpiCtxt->spi.dspi_spi, true, true);
	//DSPI_ClearStatusFlags(pSpiCtxt->spi.dspi_spi, kDSPI_AllStatusFlag);
	DSPI_Deinit(pSpiCtxt->spi.dspi_spi);//(SPI_Type *base)
	//DisableIRQ(s_dspiIRQ[DSPI_GetInstance(pSpiCtxt->spi.dspi_spi)]);
}

//
// ----- SANDBOX
//

#if 0
void spi_slave_rx_tx_task(void *pvParameters)
{
	int res;
    uint16_t cs = 0;
    bool bGotTxFrame, bGotRxFrame;
    EventBits_t ev;
    dspi_transfer_t slaveXfer;

    spi_shandle_t *pspi = (spi_shandle_t *)pvParameters;

    spi_frame_t *pTxFrame;
    spi_frame_t *pRxFrame;
    UBaseType_t txMsgCnt;
    uint8_t rxBuff[16];
    uint8_t rxValue;

    //FillUpTxFifo(pspi->dspi_spi);
    FillUpTxFifo(SPI1);
    DSPI_EnableInterrupts(SPI1, kDSPI_TxFifoUnderflowInterruptEnable | kDSPI_RxFifoOverflowInterruptEnable | kDSPI_RxFifoDrainRequestInterruptEnable);

    bGotRxFrame = false;
    while(1)
    {
    	if (xQueueReceive(Spi1RxQueue, &rxValue,  portMAX_DELAY) != pdTRUE) {
    	    dl_print("Failed to get frame from pspi->rxFreeFrameQueue");
    	    continue; // debug it
    	}
    	dl_print("%1x", rxValue);
    }

}
#endif

#if 0
// this function does the receive for SPI slave.
// It copies the frames into frame buffers.
// the idea is to do as little processing as possible in this task
void spi_slave_rx_tx_task(void *pvParameters)
{
	int res;
    uint16_t cs = 0;
    bool bGotTxFrame, bGotRxFrame;
    EventBits_t ev;
    dspi_transfer_t slaveXfer;

    spi_shandle_t *pspi = (spi_shandle_t *)pvParameters;

    spi_frame_t *pTxFrame;
    spi_frame_t *pRxFrame;
    UBaseType_t txMsgCnt;
    uint8_t rxBuff[16];

    //FillUpTxFifo(pspi->dspi_spi);

    bGotRxFrame = false;
    while(1) {
//#if 0
    	if (!bGotRxFrame) { // get a free frame if not already allocated
    	    if (xQueueReceive(pspi->rxFreeFrameQueue, &pRxFrame,  portMAX_DELAY) != pdTRUE) {
    	    	dl_print("Failed to get frame from pspi->rxFreeFrameQueue");
    	    	continue; // debug it
    	    }
    	    bGotRxFrame = true;
    	}
//#endif
    	//memset(pRxFrame->bytes, 0, SLAVE_TRANSFER_SIZE);
    	//slaveXfer.rxData = pRxFrame->bytes;
    	slaveXfer.rxData = rxBuff;
    	slaveXfer.dataSize = SLAVE_TRANSFER_SIZE;
        slaveXfer.configFlags = kDSPI_SlaveCtar0;

    	// find if there is data to transmit
    	//txMsgCnt = uxQueueMessagesWaiting(pspi->txFrameQueue);
    	//if((txMsgCnt > 0) && (xQueueReceive(pspi->txFrameQueue, &pTxFrame,  portMAX_DELAY) == pdTRUE)){
    	//	bGotTxFrame = true;
    	//	slaveXfer.txData = pTxFrame->bytes;
    	//}
    	//else {
    		bGotTxFrame = false;
    		slaveXfer.txData = (void*)0;
    	//}

    	//do{
    	    res = DSPI_SlaveTransferNonBlocking(pspi->dspi_spi, &pspi->slave.handle, &slaveXfer);
    	//    if(res != kStatus_Success){
    	//    	vTaskDelay(5);
    	//    }
    	//}while(res != kStatus_Success);
    	//if (res != kStatus_Success){
    	//	while(1); // debug it
    	//}

    	ev = xEventGroupWaitBits(pspi->Event,
    			                 RTOS_SPI_COMPLETE | RTOS_SPI_ERROR,
    	                         pdTRUE, pdFALSE, portMAX_DELAY);
    	if (ev & RTOS_SPI_ERROR){
    		xEventGroupClearBits(pspi->Event, RTOS_SPI_COMPLETE);
    		dl_print("SPI error");
    	}
    	else {
    		//if ((pRxFrame->field.cnt > 0) && (pRxFrame->field.cnt <= SPI_FRM_DATA_SIZE)){
    		if ((rxBuff[0] > 0) && (rxBuff[0] <= SPI_FRM_DATA_SIZE)){
    			// validate receive frame
    			//cs = 0;
    			//for (int i = 0; i < FRM_CS_OFFSET; i++){
    			//	cs += pRxFrame->bytes[i];
    			//}
    			//if (cs == pRxFrame->field.cs){ // good frame
    			//	xQueueSend(pspi->rxFrameQueue, &pRxFrame, portMAX_DELAY);
    			//	bGotRxFrame = false; // allocate the next frame
    			//}
    			print_spi_frame(true, rxBuff);
    		}
    	}

    	// return the tx frame
        //if (bGotTxFrame) {
        //	xQueueSend(pspi->txFreeFrameQueue, &pTxFrame, portMAX_DELAY);
        //}
    }

	vTaskDelete(NULL); // delete itself
}
#endif

#if 0
// this task implements the serial link  does the receive for iestream => input stream
void spi_link_tx_task(void *pvParameters)
{
	spi_link_context_t *pctxt = (spi_link_context_t *) pvParameters;
	spi_shandle_t *pspi = &pctxt->spi;
	SerialLink_t  *plink = &pctxt->link;

	spi_frame_t *pfrm;
	uint32_t numBytes;
	uint16_t cs;

	int res;

    for(;;) {
    	// get a free spi transmit frame
		if(xQueueReceive(pspi->txFreeFrameQueue, &pfrm,  portMAX_DELAY) == pdTRUE){

			res = Link_Tx_GetBuffer(plink, pfrm->field.data, SPI_FRM_DATA_SIZE, &numBytes);
			if (res != SER_SUCCESS){
				continue;
			}
			cs = 0;
			pfrm->field.cnt = numBytes;
			for (unsigned i = 0; i < numBytes + 1; i++){
				cs += pfrm->bytes[i];
			}
			// zeroe the remainder of the buffer, skip +1 bytes account for byte cnt
			for (int i = numBytes+1; i < FRM_CS_OFFSET; i++){
				pfrm->bytes[i] = 0;
			}
			pfrm->field.cs = cs;
#ifdef DEBUG_SPI_RX
    		print_spi_frame(false ,pfrm->bytes); // Note: pTxFrame is modified by the print function
#endif
			// queue the frame for transmission
			if (xQueueSend(pspi->txFrameQueue, &pfrm, portMAX_DELAY) != pdTRUE){
				// return the frame something bad happened
				xQueueSend(pspi->txFreeFrameQueue, &pfrm, portMAX_DELAY);
			}
		}
    }
}
#endif

#if 0
// this task implements the serial link  does the receive for iestream => input stream
void spi_link_rx_task(void *pvParameters)
{
	spi_link_context_t *pctxt = (spi_link_context_t *) pvParameters;
	spi_shandle_t *pspi = &pctxt->spi;
	SerialLink_t  *plink = &pctxt->link;

	spi_frame_t *pfrm;

    for(;;) {
		if(xQueueReceive(pspi->rxFrameQueue, &pfrm,  portMAX_DELAY) == pdTRUE){
#ifdef DEBUG_SPI_RX
    		print_spi_frame(true ,pfrm->bytes); // Note: pTxFrame is modified by the print function
#endif
			for (int i = 0; i < pfrm->field.cnt; i++){
				Link_Rx_Byte(plink, pfrm->field.data[i]);
			}
			// release receive frame for reuse
			//if (
			xQueueSend(pspi->rxFreeFrameQueue, &pfrm, portMAX_DELAY);// != pdTRUE){
			//}
		}
    }
}
#endif

#if 0
// this function does the receive for SPI slave.
// It copies the frames into frame buffers.
void spi_master_rx_tx_task(void *pvParameters)
{
	int res;
    uint16_t cs = 0;
    //uint8_t txFrmIdx;
    //uint8_t rxFrmIdx;
    bool bGotTxFrame, bGotRxFrame;
    EventBits_t ev;
    dspi_transfer_t masterXfer;
    int windowCnt = 0;
    uint32_t serviceTime;

    spi_shandle_t *pspi = (spi_shandle_t *)pvParameters;

    spi_frame_t *pTxFrame;
    spi_frame_t *pRxFrame;

    // todo: add implementation

    bGotRxFrame = false;
    while(1) {

    	if (!bGotRxFrame) { // get a free frame if not already allocated
    	    while (xQueueReceive(pspi->rxFreeFrameQueue, &pRxFrame,  portMAX_DELAY) != pdTRUE) {;}
    	    bGotRxFrame = true;
    	}

    	//memset(pRxFrame->bytes, 0, SLAVE_TRANSFER_SIZE);
    	masterXfer.dataSize = SLAVE_TRANSFER_SIZE;
    	masterXfer.rxData = pRxFrame->bytes;
    	masterXfer.configFlags = kDSPI_MasterCtar1 | kDSPI_MasterPcs0 | kDSPI_MasterPcsContinuous;
        //slaveXfer.kDSPI_SlaveCtar1;

    	// decide on the wait time
    	if(windowCnt <= 0){
    	    serviceTime = SPI_CHANNEL_SERVICE_TIME;
    	}
    	else if (windowCnt < 2){
    	    serviceTime = 75;
    	}
    	else if (windowCnt < 3){
    		serviceTime = 50;
    	}
    	else if (windowCnt < 4){
       		serviceTime = 40;
    	}
    	else if (windowCnt <= 5){
      		serviceTime = 20;
    	}

    	// find if there is data to transmit
    	if(xQueueReceive(pspi->txFrameQueue, &pTxFrame,  serviceTime) == pdTRUE){
    		bGotTxFrame = true;
    		masterXfer.txData = pTxFrame->bytes;
    		windowCnt = 5; // there is data to be trasmitted, and most likely there will be a reply so check offten
    	}
    	else {
    		bGotTxFrame = false;
    		masterXfer.txData = (void*)0;
    	}

    	memset(pRxFrame->bytes,0, SLAVE_TRANSFER_SIZE);

    	res = DSPI_MasterTransferNonBlocking(pspi->dspi_spi, &pspi->master.handle, &masterXfer);
    	if (res != kStatus_Success){
    		vTaskDelay(60); // debug it
    	}

    	ev = xEventGroupWaitBits(pspi->Event,
    			                 RTOS_SPI_COMPLETE | RTOS_SPI_ERROR,
    	                         pdTRUE, pdFALSE, portMAX_DELAY);


    	//DSPI_RTOS_Transfer(&pspi->rtosMaster, &masterXfer);




#ifdef DEBUG_SPI_TX_MASTER
    	if(bGotTxFrame){
    		print_spi_frame(false, pTxFrame);
    	}
#endif

    	if (ev & RTOS_SPI_ERROR){
    		xEventGroupClearBits(pspi->Event, RTOS_SPI_COMPLETE);
    	}
    	else {
    		if ((pRxFrame->field.cnt > 0) && (pRxFrame->field.cnt <= SPI_FRM_DATA_SIZE)){
    			// validate receive frame
    			cs = 0;
    			for (int i = 0; i < FRM_CS_OFFSET; i++){
    				cs += pRxFrame->bytes[i];
    			}
    			if (cs == pRxFrame->field.cs){ // good frame
    				xQueueSend(pspi->rxFrameQueue, &pRxFrame, portMAX_DELAY);
    				bGotRxFrame = false; // allocate the next frame
    			}
#ifdef DEBUG_SPI_RX_MASTER
    	        dl_print("[SPI RX:], valid: %d", bGotRxFrame);
    		    print_spi_frame(pRxFrame); // Note: pTxFrame is modified by the print function
#endif
    		}
    	}

    	// return the tx frame
        if (bGotTxFrame) {
        	xQueueSend(pspi->txFreeFrameQueue, &pTxFrame, portMAX_DELAY);
        }
        if (bGotTxFrame || !bGotRxFrame){
        	windowCnt = 5;
        }
        else{
        	windowCnt--;
        	if (windowCnt < 0){
        		windowCnt = 0;
        	}
        }

        // delay so that the slave can get all the data
        vTaskDelay(100); // checking if this helps with missing spi frames
    }

	vTaskDelete(0); // delete itself
}
#endif

#if 0
void DSPI_SlaveUserCallback(SPI_Type *base, dspi_slave_handle_t *handle,
		status_t status, void *userData)
{
	spi_shandle_t *pspi = (spi_shandle_t *)userData;
	BaseType_t xHigherPriorityTaskWoken, xResult;

    if (status == kStatus_Success){
		xResult = xEventGroupSetBitsFromISR(pspi->Event, RTOS_SPI_COMPLETE, &xHigherPriorityTaskWoken);
    }
    else if (status == kStatus_DSPI_Error){
    	xResult = xEventGroupSetBitsFromISR(pspi->Event, RTOS_SPI_ERROR, &xHigherPriorityTaskWoken);
    }
    if (xResult == pdPASS) {
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}
#endif
#if 0
void print_spi_frame(bool in, uint8_t* bytes)
{
	char hex_bytes[2*SLAVE_TRANSFER_SIZE+1];
	uint8_t bc = bytes[0];
	char *p = hex_bytes;

	if (in){
		*p = 'i';
	}
	else{
		*p = 'o';
	}
	p++;
	sprintf(p,"%02x ",bc);
	p += 3;
    for(int i=1; i < bc+1; i++){
    	sprintf(p,"%02x",bytes[i]);
    	p += 2;
    }
    *p = '\0';
    dl_printstr(hex_bytes, p - hex_bytes);
}
#endif
