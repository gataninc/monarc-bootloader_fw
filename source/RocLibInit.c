#include "RocLibInit.h"
#include "lib_init.h"
#include "SerialComClient.h"
#include "SerialComIOS.h"
#include "SerialComTarget.h"
#include "UartSerialCom.h"
#include "fsl_uart.h"
#include "fsl_clock.h"
#include "Uart.h"
#include "FlashWrapper.h"
#include "SpiSerialCom.h"


static serial_com_ios_server_t roc_com_ios_srv;
static uart_link_context_t ulCtxt;
static spi_link_context_t   spiPaclink;

void RocConsole(const char *str)
{
   	os_print(&cout,"%s",str);
}

void RocLibInit(void)
{


	InitFlashWrapper();
	// console output
	SetupUartStream(uart_id_4, &cin, &cout); // console

    // set host net address
	NetLib_Init(CONF_NET_ROC_ADDR);
	// configure serial link through uart3 to FPB
	Link_InitUart(&ulCtxt, UART3, UART3_CLK_SRC, UART3_RX_TX_IRQn, 5);

	Net_SetRoute(CONF_NET_FPB_ADDR, &ulCtxt.link);

	Link_InitMasterSpi(&spiPaclink, SPI1);
	Net_SetRoute(CONF_NET_PAC_ADDR, &spiPaclink.link);

	// configure io stream on top of serial communication protocol
	roc_com_ios_srv.os = &ostr;
	roc_com_ios_srv.is = &istr;
	roc_com_ios_srv.server_port = CONF_SERVER_FW_UPDATE_PORT;

	SetupSerialComIosServer(&roc_com_ios_srv);
}

void RocLibDeinit(void)
{
	Link_DeinitSpi(&spiPaclink);
	Link_DeinitUart(&ulCtxt);
	DeinitUartStream(uart_id_4, &cin, &cout);
}


