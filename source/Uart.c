/*
 * Uart.c
 *
 *  Created on: Nov 27, 2018
 *      Author: stelasang
 */


#include "Uart.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdint.h>

#include "fsl_uart_freertos.h"
#include "fsl_uart.h"
#include "utils.h"


uart_rtos_handle_t handle;
struct _uart_handle t_handle;
uint8_t bufUart4[32];
bool bUart4StreamEn = false;
iostream_t *uart4_ostream;
iostream_t *uart4_istream;

uart_rtos_handle_t hUart3;
struct _uart_handle t_Uart3;
uint8_t bufUart3[256];
bool bUart3StreamEn = false;
iostream_t *uart3_ostream;
iostream_t *uart3_istream;

SemaphoreHandle_t xCprntSemaphore = NULL;
//StaticSemaphore_t xCprntMutexBuffer;

void PUTC(char c)
{
	UART_RTOS_Send(&handle, (uint8_t *)&c, 1);
}
int GETC(char* c)
{
	 return UART_RTOS_Receive(&handle, (uint8_t *)c, 1, NULL);
}
void PUTS(char* s)
{
	 UART_RTOS_Send(&handle, (uint8_t *)s, strlen(s));
}

void PUT_BUFF(char* s, uint32_t len)
{
	 UART_RTOS_Send(&handle, (uint8_t *)s, len);
}
void uart4_init()
{
	uart_rtos_config_t uart_config = {
	    .parity = kUART_ParityDisabled,
	    .stopbits = kUART_OneStopBit,
	};


	// ** Console UART **
	uart_config.srcclk = CLOCK_GetFreq(UART4_CLK_SRC);
	uart_config.base = UART4;
	uart_config.buffer = bufUart4;
	uart_config.buffer_size = sizeof(bufUart4);
	uart_config.baudrate = 115200;
	if (0 > UART_RTOS_Init(&handle, &t_handle, &uart_config))
		vTaskSuspend(NULL);
	NVIC_SetPriority(UART4_RX_TX_IRQn, 5);
}

void uart4_deinit()
{
	//UART_RTOS_Deinit(&handle);
	UART_Deinit(handle.base);
}

void uart3_init()
{
	uart_rtos_config_t uart_config = {
		.parity = kUART_ParityDisabled,
	    .stopbits = kUART_OneStopBit,
	};
#if 1
	// ** ROC UART **
	uart_config.srcclk = CLOCK_GetFreq(UART3_CLK_SRC);
	uart_config.base = UART3;
	uart_config.buffer = bufUart3;
	uart_config.buffer_size = sizeof(bufUart3);
	uart_config.baudrate = 115200;

	if (0 > UART_RTOS_Init(&hUart3, &t_Uart3, &uart_config))
				vTaskSuspend(NULL);
	NVIC_SetPriority(UART3_RX_TX_IRQn, 5);

#else
	const uart_config_t UART_3_config = {
			  //.baudRate_Bps = 115200,//460800,//921600,//115200,
			  .baudRate_Bps = 115200,//460800,//921600,//115200,
			  .parityMode = kUART_ParityDisabled,
			  .stopBitCount = kUART_OneStopBit,
			  .txFifoWatermark = 0,
			  .rxFifoWatermark = 1,
			  .enableTx = true,
			  .enableRx = true
			};
		UART_Init(UART3, &UART_3_config, CLOCK_GetFreq(UART3_CLK_SRC));
		NVIC_SetPriority(UART3_RX_TX_IRQn, 5);
		UART_EnableInterrupts(UART3, kUART_RxDataRegFullInterruptEnable | kUART_RxOverrunInterruptEnable);
		EnableIRQ(UART3_RX_TX_IRQn);
#endif

}
void uart3_deinit()
{
	//UART_RTOS_Deinit(&hUart3);
	UART_Deinit(hUart3.base);
}

void cputc(char c)
{
	UART_RTOS_Send(&handle, (uint8_t *)&c, 1);
}

void cprintf ( const char * format, ... )
{
  static char buffer[256];
  va_list args;
  if( xSemaphoreTake( xCprntSemaphore, ( TickType_t ) 100 ) == pdTRUE )
  {
	  va_start (args, format);
	  vsprintf (buffer,format, args);
	  UART_RTOS_Send(&handle, (uint8_t *)buffer, strlen(buffer));
	  va_end (args);
	  xSemaphoreGive( xCprntSemaphore );
  }
}

void Error(char *rtn, char *msg, bool hang)
{
	char buffer[256];
	char *msgtype = (hang)?"ERROR":"WARNING";
	sprintf(buffer,"[%s] %s: %s\r\n",msgtype,rtn,msg);
	UART_RTOS_Send(&handle, (uint8_t *)buffer, strlen(buffer));
	if (hang) for(;;);																	// hang here!
}

void Dump(uint8_t *buf, int len)
{
	char c;
	for (int i=0; i<len; i+=16) {
		cprintf("\r\n%04x: ",i);
		for (int j=0; j<16; j++) {
			c = buf[i+j];
			cprintf("%02x ",(uint8_t)c&0xff);
		}
		cprintf("  ");
		for (int j=0; j<16; j++) {
			c = buf[i+j];
			cprintf("%c",(c<='~' && c>=' ')?c:'.');
		}
	}
}

// this function does the transmit for oestream = output stream
IO_StreamState_t uart_tx_cb(void *p, uint8_t* buf, int size)
{
	int res;
	res = UART_RTOS_Send(p, buf, size);
	if (res != 0){
		return IOS_ERROR;
	}
	return IOS_GOOD;
}

// this function does the receive for iestream => input stream
void uart3_rx_task(void *pvParameters)
{
	int res;
	uint8_t tmpbuf[128];
	size_t received;
    uint32_t sent;

    while(1) {
    	res = UART_RTOS_ReceiveTMO(&hUart3, tmpbuf, 128, &received, 200);
    	if ((res == kStatus_Success)|| ((res == kStatus_Timeout) && (received !=0 ))){
            is_put(uart3_istream, tmpbuf, received, &sent);
    	}

    } // while(1)

	vTaskDelete(NULL); // delete itself
}

// this function does the receive for iestream => input stream
void uart4_rx_task(void *pvParameters)
{
	int res;
	uint8_t tmpbuf[128];
	size_t received;
    uint32_t sent;

    while(1) {
    	res = UART_RTOS_ReceiveTMO(&handle, tmpbuf, 128, &received, 200);
    	if ((res == kStatus_Success)|| ((res == kStatus_Timeout) && (received !=0 ))){
            is_put(uart4_istream, tmpbuf, received, &sent);
    	}

    } // while(1)

	vTaskDelete(NULL); // delete itself
}

void SetupUartStream(uart_id_t uartId, iostream_t *is, iostream_t *os)
{
    if ((uartId == uart_id_3)&&(!bUart3StreamEn)){
    	uart3_istream = is;
    	uart3_ostream = os;
    	uart3_init();

        os_set_tx_cb(os, uart_tx_cb, (void *)(&hUart3));

        xTaskCreate(uart3_rx_task, "uart3_rx",configMINIMAL_STACK_SIZE, NULL, PRIORITY_MID_HIGH, NULL);
        bUart3StreamEn = true;
    }
    else if ((uartId == uart_id_4) && (!bUart4StreamEn)){
    	uart4_istream = is;
    	uart4_ostream = os;
    	uart4_init();

        os_set_tx_cb(os, uart_tx_cb, (void *)(&handle));

        xTaskCreate(uart4_rx_task, "uart4_rx",configMINIMAL_STACK_SIZE, NULL, PRIORITY_MID_HIGH, NULL);
        bUart4StreamEn = true;

        xCprntSemaphore = xSemaphoreCreateMutex();
        configASSERT( xCprntSemaphore );
    }

}

void DeinitUartStream(uart_id_t uartId, iostream_t *is, iostream_t *os)
{
    if (uartId == uart_id_3){
    	uart3_deinit();
    }
    else if (uartId == uart_id_4){
    	uart4_deinit();
    }
    // incomplete
}


