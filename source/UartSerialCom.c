#include <SerialComConstants.old>
#include <UartSerialCom.h>
#include "SerialComClient.h"
#include "FreeRTOS.h"
#include "fsl_uart.h"
//#include "dbglog_target.h"


void UartSerialLinkTxTask(void *pvParameters)
{
	int res;
	uint8_t buf[100];
	uint32_t numBytes;
	uart_link_context_t *Ctxt = (uart_link_context_t*)pvParameters;
	SerialLink_t        *plink = &uartSerCtxt->link;
	uart_link_handle_t  *puart = &uartSerCtxt->uart;

    while(1) {
    	numBytes = 0;
		res = Link_Tx_GetBuffer(plink, buf, 100, &numBytes);
		if (res != SER_SUCCESS){
			continue;
		}

		Ctxt->Send(buf, numBytes);
    }
}

void UartSerialLinkRxTask(void *pvParameters)
{
	int res;
	uint8_t buf[100];
	size_t received;
	uart_link_context_t *Ctxt = (uart_link_context_t*)pvParameters;
	SerialLink_t        *plink = &Ctxt->link;
	int minExpected;

    while(1) {
		minExpected = Link_Rx_MinCntExpected(plink);
		if (minExpected > 100){
			minExpected = 100;
		}
		received = 0;
		memset(buf, 0, sizeof(buf));
		res = Ctxt->Recieve(buf, minExpected, &received, 200);
		if ((res == kStatus_Success)|| ((res == kStatus_Timeout) && (received != 0 ))){
		    for (int i = 0; i < received; i++){
		    	Link_Rx_Byte(plink, buf[i]);
		    }
		}
    }
}


int Link_InitUart(uart_link_context_t *uartSerCtxt)
{
	BaseType_t xReturned;

    int res = Link_Init(&uartSerCtxt->link);
    if (res != SER_SUCCESS) {
    	return res;
    }

    xReturned =xTaskCreate(UartSerialLinkTxTask, "UartSerialLinkTxTask",configMINIMAL_STACK_SIZE*2, (uartSerCtxt), PRIORITY_HIGH, &uartSerCtxt->xLinkTxHandle);
    if (xReturned != pdPASS){
    	return USC_CANNOT_CREATE_TX_TASK;
    }

    xReturned =xTaskCreate(UartSerialLinkRxTask, "UartSerialLinkRxTask",configMINIMAL_STACK_SIZE*2, (uartSerCtxt), PRIORITY_HIGH, &uartSerCtxt->xLinkTxHandle);
    if (xReturned != pdPASS){
        return USC_CANNOT_CREATE_RX_TASK;
    }

    return USC_SUCCESS;
}

void Link_DeinitUart(uart_link_context_t *uartSerCtxt)
{
	//UART_RTOS_Deinit(&uartSerCtxt->uart.handle); don't call this, it causes crash on exit
	//UART_Deinit(uartSerCtxt->uart.base);
}



