/*
 * Copyright (c) 2016, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * TEXT BELOW IS USED AS SETTING FOR THE PINS TOOL *****************************
PinsProfile:
- !!product 'Pins v2.0'
- !!processor 'MK64FN1M0xxx12'
- !!package 'MK64FN1M0VLL12'
- !!mcu_data 'ksdk2_0'
- !!processor_version '1.0.9'
 * BE CAREFUL MODIFYING THIS COMMENT - IT IS YAML SETTINGS FOR THE PINS TOOL ***
 */

#include "board_init.h"
#include "clock_config.h"
#include "eeprom.h"
#include "fsl_port.h"

#define PART_NO_OFFSET            (96)
#define FPB_EEPROM_ADDR           (0x57U)
#define PAC_EEPROM_ADDR           (0x57U)
#define ROC_EEPROM_ADDR           (0x50U)

//#define GTN_OVERWRITE_FPB

static int BOARD_ID =  BOARD_ID_UNKNOWN;
const char * BOARD_NAMES[] = {"UNKNOWN", "FPB", "ROC", "PAC"};

#include "fsl_sysmpu.h"




void BOARD_FpbInitBootPins(void);
void BOARD_PacInitBootPins(void);
void BOARD_RocInitBootPins(void);

int BOARD_GetBoardId(void)
{
	return BOARD_ID;
}

const char * BOARD_GetBoardStr()
{
	return BOARD_NAMES[BOARD_ID];
}

int BOARD_ReadBoardId(void)
{


	// the content of the ID eeprom seems to be not written

	uint8_t board_pn[16];
	const char *base = "450.50";



	memset(board_pn, 0, 16);

	if (ReadEEPROM(ROC_EEPROM_ADDR, PART_NO_OFFSET, board_pn, 16) == 0) {
		return BOARD_ID_ROC; // ROC's eeprom address is different from FPB and PAC
	}
	if ( ReadEEPROM(FPB_EEPROM_ADDR, PART_NO_OFFSET, board_pn, 16) != 0) { // default to PAC
		return BOARD_ID_PAC;
	}

	size_t base_len = strlen(base);
	char *p = (char*)board_pn;
	while(*p == ' ') {p++;} // skip white space
	if (strncmp(base, p, base_len) != 0){
		return BOARD_ID_UNKNOWN;
	}
	p += base_len;

	if ((p[0] == '0') && (p[1] == '0') && (p[2] == '0')){
		return BOARD_ID_FPB;
	}
	else {
		return BOARD_ID_PAC;
	}

    return BOARD_ID_UNKNOWN;

}

int BOARD_WriteBoardId(uint8_t addr, uint8_t *part_number, uint32_t size)
{
	return WriteEEPROM(addr, PART_NO_OFFSET, part_number, (int)size);
}

/*FUNCTION**********************************************************************
 *
 * Function Name : BOARD_InitPins
 * Description   : Configures pin routing and optionally pin electrical features.
 *
 *END**************************************************************************/
void BOARD_Init(void) {
	CLOCK_EnableClock(kCLOCK_PortC);
	/* I2C EEPROM */
	PORT_SetPinMux(PORTC, 10U, kPORT_MuxAlt2);
	PORT_SetPinMux(PORTC, 11U, kPORT_MuxAlt2);

#ifdef GTN_OVERWRITE_FPB
	char * fpb_board_pn = "450.50000       ";
	BOARD_WriteBoardId(FPB_EEPROM_ADDR,(uint8_t*)fpb_board_pn, strlen(fpb_board_pn));
#endif

	BOARD_ID = BOARD_ReadBoardId();

	//BOARD_ID = 3;

	if (BOARD_ID == BOARD_ID_FPB){

		BOARD_FpbInitBootPins();
	    BOARD_InitBootClocks();
	    SYSMPU_Enable(SYSMPU,false);
	}
	else if((BOARD_ID == BOARD_ID_ROC) || (BOARD_ID == BOARD_ID_UNKNOWN)){
		BOARD_RocInitBootPins();
	    BOARD_InitBootClocks();
	    SYSMPU_Enable(SYSMPU,false);
	}
	else if (BOARD_ID == BOARD_ID_PAC){
		BOARD_PacInitBootPins();
	}



}

/*******************************************************************************
 * EOF
 ******************************************************************************/
