/*
 * Crc.h
 *
 *  Created on: Jul 3, 2018
 *      Author: stelasang
 */

#ifndef CRC_H_
#define CRC_H_

#include <stdint.h>



uint32_t Crc32(uint32_t crc, const void *buf, uint32_t size);


#endif /* CRC_H_ */
