namespace MonarcProgrammer
{
    partial class MonarcProgrammerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private TargetType targetType = TargetType.UnknownTargetType;
        private MonarcManager MM;
        //private ApplicationManager monarcMgr;
        //private System.ComponentModel.BackgroundWorker bwProgramImage;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.debugInstructionsLabel = new System.Windows.Forms.Label();
            this.BTN_Action = new System.Windows.Forms.Button();
            this.helloWorldLabel = new System.Windows.Forms.Label();
            this.CB_ImageType = new System.Windows.Forms.ComboBox();
            this.TB_SelectedImageFile = new System.Windows.Forms.TextBox();
            this.RTB_Log = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BTN_ProgramImage = new System.Windows.Forms.Button();
            this.SS_Status = new System.Windows.Forms.StatusStrip();
            this.TT_ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.TT_Label = new System.Windows.Forms.ToolStripStatusLabel();
            this.BTN_CrcImage = new System.Windows.Forms.Button();
            this.BTN_StartApp = new System.Windows.Forms.Button();
            this.BTN_ResetTarget = new System.Windows.Forms.Button();
            this.SS_Status.SuspendLayout();
            this.SuspendLayout();
            // 
            // debugInstructionsLabel
            // 
            this.debugInstructionsLabel.AutoSize = true;
            this.debugInstructionsLabel.Location = new System.Drawing.Point(22, 59);
            this.debugInstructionsLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.debugInstructionsLabel.Name = "debugInstructionsLabel";
            this.debugInstructionsLabel.Size = new System.Drawing.Size(96, 13);
            this.debugInstructionsLabel.TabIndex = 1;
            this.debugInstructionsLabel.Text = "Select Image Type";
            // 
            // BTN_Action
            // 
            this.BTN_Action.Location = new System.Drawing.Point(407, 75);
            this.BTN_Action.Margin = new System.Windows.Forms.Padding(2);
            this.BTN_Action.Name = "BTN_Action";
            this.BTN_Action.Size = new System.Drawing.Size(97, 21);
            this.BTN_Action.TabIndex = 2;
            this.BTN_Action.Text = "Browse";
            this.BTN_Action.UseVisualStyleBackColor = true;
            this.BTN_Action.Click += new System.EventHandler(this.button1_Click);
            // 
            // helloWorldLabel
            // 
            this.helloWorldLabel.AutoSize = true;
            this.helloWorldLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helloWorldLabel.Location = new System.Drawing.Point(20, 9);
            this.helloWorldLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.helloWorldLabel.Name = "helloWorldLabel";
            this.helloWorldLabel.Size = new System.Drawing.Size(271, 26);
            this.helloWorldLabel.TabIndex = 3;
            this.helloWorldLabel.Text = "Program Monarc Firmware";
            // 
            // CB_ImageType
            // 
            this.CB_ImageType.FormattingEnabled = true;
            this.CB_ImageType.Items.AddRange(new object[] {
            "FPB",
            "ROC",
            "PAC"});
            this.CB_ImageType.Location = new System.Drawing.Point(25, 75);
            this.CB_ImageType.Name = "CB_ImageType";
            this.CB_ImageType.Size = new System.Drawing.Size(121, 21);
            this.CB_ImageType.TabIndex = 4;
            this.CB_ImageType.SelectedIndexChanged += new System.EventHandler(this.CB_ImageType_SelectedIndexChanged);
            // 
            // TB_SelectedImageFile
            // 
            this.TB_SelectedImageFile.Location = new System.Drawing.Point(166, 75);
            this.TB_SelectedImageFile.Name = "TB_SelectedImageFile";
            this.TB_SelectedImageFile.Size = new System.Drawing.Size(236, 20);
            this.TB_SelectedImageFile.TabIndex = 5;
            this.TB_SelectedImageFile.TextChanged += new System.EventHandler(this.TB_SelectedImageFile_TextChanged);
            // 
            // RTB_Log
            // 
            this.RTB_Log.Location = new System.Drawing.Point(25, 136);
            this.RTB_Log.Name = "RTB_Log";
            this.RTB_Log.Size = new System.Drawing.Size(377, 128);
            this.RTB_Log.TabIndex = 6;
            this.RTB_Log.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(163, 59);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Select Image File";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 120);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Log";
            // 
            // BTN_ProgramImage
            // 
            this.BTN_ProgramImage.Enabled = false;
            this.BTN_ProgramImage.Location = new System.Drawing.Point(407, 136);
            this.BTN_ProgramImage.Margin = new System.Windows.Forms.Padding(2);
            this.BTN_ProgramImage.Name = "BTN_ProgramImage";
            this.BTN_ProgramImage.Size = new System.Drawing.Size(97, 21);
            this.BTN_ProgramImage.TabIndex = 10;
            this.BTN_ProgramImage.Text = "Program Image";
            this.BTN_ProgramImage.UseVisualStyleBackColor = true;
            this.BTN_ProgramImage.Click += new System.EventHandler(this.BTN_ProgramImage_Click);
            // 
            // SS_Status
            // 
            this.SS_Status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TT_ProgressBar,
            this.TT_Label});
            this.SS_Status.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.SS_Status.Location = new System.Drawing.Point(0, 270);
            this.SS_Status.Name = "SS_Status";
            this.SS_Status.Size = new System.Drawing.Size(533, 22);
            this.SS_Status.TabIndex = 11;
            this.SS_Status.Text = "statusStrip1";
            // 
            // TT_ProgressBar
            // 
            this.TT_ProgressBar.Name = "TT_ProgressBar";
            this.TT_ProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // TT_Label
            // 
            this.TT_Label.Name = "TT_Label";
            this.TT_Label.Size = new System.Drawing.Size(63, 17);
            this.TT_Label.Text = "cmd: Iddle";
            // 
            // BTN_CrcImage
            // 
            this.BTN_CrcImage.Location = new System.Drawing.Point(407, 161);
            this.BTN_CrcImage.Margin = new System.Windows.Forms.Padding(2);
            this.BTN_CrcImage.Name = "BTN_CrcImage";
            this.BTN_CrcImage.Size = new System.Drawing.Size(97, 21);
            this.BTN_CrcImage.TabIndex = 12;
            this.BTN_CrcImage.Text = "CRC Image";
            this.BTN_CrcImage.UseVisualStyleBackColor = true;
            this.BTN_CrcImage.Click += new System.EventHandler(this.BTN_CrcImage_Click);
            // 
            // BTN_StartApp
            // 
            this.BTN_StartApp.Location = new System.Drawing.Point(407, 186);
            this.BTN_StartApp.Margin = new System.Windows.Forms.Padding(2);
            this.BTN_StartApp.Name = "BTN_StartApp";
            this.BTN_StartApp.Size = new System.Drawing.Size(97, 21);
            this.BTN_StartApp.TabIndex = 13;
            this.BTN_StartApp.Text = "Start App";
            this.BTN_StartApp.UseVisualStyleBackColor = true;
            this.BTN_StartApp.Click += new System.EventHandler(this.BTN_StartApp_Click);
            // 
            // BTN_ResetTarget
            // 
            this.BTN_ResetTarget.Location = new System.Drawing.Point(407, 211);
            this.BTN_ResetTarget.Margin = new System.Windows.Forms.Padding(2);
            this.BTN_ResetTarget.Name = "BTN_ResetTarget";
            this.BTN_ResetTarget.Size = new System.Drawing.Size(97, 21);
            this.BTN_ResetTarget.TabIndex = 14;
            this.BTN_ResetTarget.Text = "Reset Target";
            this.BTN_ResetTarget.UseVisualStyleBackColor = true;
            this.BTN_ResetTarget.Click += new System.EventHandler(this.BTN_ResetTarget_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 292);
            this.Controls.Add(this.BTN_ResetTarget);
            this.Controls.Add(this.BTN_StartApp);
            this.Controls.Add(this.BTN_CrcImage);
            this.Controls.Add(this.SS_Status);
            this.Controls.Add(this.BTN_ProgramImage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RTB_Log);
            this.Controls.Add(this.TB_SelectedImageFile);
            this.Controls.Add(this.CB_ImageType);
            this.Controls.Add(this.helloWorldLabel);
            this.Controls.Add(this.BTN_Action);
            this.Controls.Add(this.debugInstructionsLabel);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Program Monarc";
            this.SS_Status.ResumeLayout(false);
            this.SS_Status.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label debugInstructionsLabel;
        private System.Windows.Forms.Button BTN_Action;
        private System.Windows.Forms.Label helloWorldLabel;
        private System.Windows.Forms.ComboBox CB_ImageType;
        private System.Windows.Forms.TextBox TB_SelectedImageFile;
        private System.Windows.Forms.RichTextBox RTB_Log;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BTN_ProgramImage;
        private System.Windows.Forms.StatusStrip SS_Status;
        private System.Windows.Forms.ToolStripStatusLabel TT_Label;
        private System.Windows.Forms.ToolStripProgressBar TT_ProgressBar;
        private System.Windows.Forms.Button BTN_CrcImage;
        private System.Windows.Forms.Button BTN_StartApp;
        private System.Windows.Forms.Button BTN_ResetTarget;
    }
}

