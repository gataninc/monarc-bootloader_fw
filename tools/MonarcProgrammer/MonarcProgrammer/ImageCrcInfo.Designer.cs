﻿namespace MonarcProgrammer
{
    partial class ImageCrcInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TB_Address = new System.Windows.Forms.TextBox();
            this.TB_Size = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BTN_ImageCrcInfo_OK = new System.Windows.Forms.Button();
            this.BTN_ImageCrcInfo_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TB_Address
            // 
            this.TB_Address.Location = new System.Drawing.Point(105, 36);
            this.TB_Address.Name = "TB_Address";
            this.TB_Address.Size = new System.Drawing.Size(100, 20);
            this.TB_Address.TabIndex = 0;
            this.TB_Address.Text = "80000";
            // 
            // TB_Size
            // 
            this.TB_Size.Location = new System.Drawing.Point(105, 62);
            this.TB_Size.Name = "TB_Size";
            this.TB_Size.Size = new System.Drawing.Size(100, 20);
            this.TB_Size.TabIndex = 1;
            this.TB_Size.Text = "32548";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Address(Hex):";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "CRC32 AREA INFO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Size(Decimal):";
            // 
            // BTN_ImageCrcInfo_OK
            // 
            this.BTN_ImageCrcInfo_OK.Location = new System.Drawing.Point(150, 92);
            this.BTN_ImageCrcInfo_OK.Name = "BTN_ImageCrcInfo_OK";
            this.BTN_ImageCrcInfo_OK.Size = new System.Drawing.Size(75, 23);
            this.BTN_ImageCrcInfo_OK.TabIndex = 5;
            this.BTN_ImageCrcInfo_OK.Text = "OK";
            this.BTN_ImageCrcInfo_OK.UseVisualStyleBackColor = true;
            this.BTN_ImageCrcInfo_OK.Click += new System.EventHandler(this.BTN_ImageCrcInfo_OK_Click);
            // 
            // BTN_ImageCrcInfo_Cancel
            // 
            this.BTN_ImageCrcInfo_Cancel.Location = new System.Drawing.Point(57, 92);
            this.BTN_ImageCrcInfo_Cancel.Name = "BTN_ImageCrcInfo_Cancel";
            this.BTN_ImageCrcInfo_Cancel.Size = new System.Drawing.Size(75, 23);
            this.BTN_ImageCrcInfo_Cancel.TabIndex = 6;
            this.BTN_ImageCrcInfo_Cancel.Text = "Cancel";
            this.BTN_ImageCrcInfo_Cancel.UseVisualStyleBackColor = true;
            this.BTN_ImageCrcInfo_Cancel.Click += new System.EventHandler(this.BTN_ImageCrcInfo_Cancel_Click);
            // 
            // ImageCrcInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 127);
            this.Controls.Add(this.BTN_ImageCrcInfo_Cancel);
            this.Controls.Add(this.BTN_ImageCrcInfo_OK);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TB_Size);
            this.Controls.Add(this.TB_Address);
            this.Name = "ImageCrcInfo";
            this.Text = "ImageCrcInfo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox TB_Address;
        public System.Windows.Forms.TextBox TB_Size;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button BTN_ImageCrcInfo_OK;
        private System.Windows.Forms.Button BTN_ImageCrcInfo_Cancel;
    }
}