using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace MonarcProgrammer
{
    

    public partial class MonarcProgrammerForm : Form
    {
        
        public MonarcProgrammerForm()
        {
            InitializeComponent();
            CB_ImageType.SelectedIndex = 0;
            targetType = TargetType.FpbTargetType;
            MM = new MonarcManager();
            MM.ProgrammerNotification += new ProgrammerNotificationEventHandler(programmer_Notification);
            MM.ProgrammerProgressChange += new ProgrammerProgressChangeEventHandler(programmer_ProgressChange);
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // Click on the link below to continue learning how to build a desktop app using WinForms!
            System.Diagnostics.Process.Start("http://aka.ms/dotnet-get-started-desktop");

        }
        // browse file dialog
        private void button1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Thanks!");
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                //openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "srec files (*.srec)|*.srec|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    TB_SelectedImageFile.Text = openFileDialog.FileName;   
                }
            }
        }
        private void TB_SelectedImageFile_TextChanged(object sender, EventArgs e)
        {
            string sPattern;
            if (CB_ImageType.SelectedItem.ToString() == "FPB")
            {
                sPattern = "monarc-fpb_fw";
                targetType = TargetType.FpbTargetType;
            }
            else if (CB_ImageType.SelectedItem.ToString() == "ROC")
            {
                sPattern = "monarc-roc_fw";
                targetType = TargetType.RocTargetType;
            }
            else
            {                sPattern = "monarc-pac_fw";
                targetType = TargetType.PacTargetType;
            }
            if (!System.Text.RegularExpressions.Regex.IsMatch(TB_SelectedImageFile.Text, sPattern))
            {
                MessageBox.Show("File name has to match: " + sPattern);
                targetType = TargetType.UnknownTargetType;
                return;
            }
            BTN_ProgramImage.Enabled = true;
        }
        private void CB_ImageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CB_ImageType.SelectedItem.ToString() == "FPB")
            {
                 targetType = TargetType.FpbTargetType;
            }
            else if (CB_ImageType.SelectedItem.ToString() == "ROC")
            {
                targetType = TargetType.RocTargetType;
            }
            else
            {
                targetType = TargetType.PacTargetType;
            }
        }
        private void EnableControls(bool enable)
        {
            BTN_ProgramImage.Enabled = enable;
            BTN_Action.Enabled = enable;
            CB_ImageType.Enabled = enable;
            TB_SelectedImageFile.Enabled = enable;
            //BTN_CrcImage.Enabled = enable;
        }
                
        private async void BTN_ProgramImage_Click(object sender, EventArgs e)
        {
            if (targetType == TargetType.UnknownTargetType) {
                MessageBox.Show("Unknown image type: Select a fw image file");
                return;
            }
            EnableControls(false);
            RTB_Log.ResetText();

            TT_Label.Text = "cmd: connecting";
            //programmer = new BootloaderManager(targetType);

            //programmer.ProgrammerNotification += new ProgrammerNotificationEventHandler(programmer_Notification);
            //programmer.ProgrammerProgressChange += new ProgrammerProgressChangeEventHandler(programmer_ProgressChange);
            MonarcManager.ResultType result;
            if (targetType == TargetType.FpbTargetType)
            {
                result = await MM.Program_FpbSrecordFileAsync(TB_SelectedImageFile.Text, 60000);
            }
            else
            {
                result = await MM.Program_NextHop_SrecordFileAsync(targetType, TB_SelectedImageFile.Text, 60000);
            }
            RTB_Log.AppendText("[" + DateTime.Now.ToShortTimeString() + "]");
            if (result != MonarcManager.ResultType.OK)
            {
                RTB_Log.AppendText("IMAGE PROGRAMMING FAILED", Color.Red, true);
                MessageBox.Show("!!!Failed", "Result", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            else {
                RTB_Log.AppendText("IMAGE PROGRAMMING DONE", Color.Green, true);
                MessageBox.Show("SUCCESS", "Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            EnableControls(true);
        }
        private async void BTN_CrcImage_Click(object sender, EventArgs e)
        {
            ImageCrcInfo crcFrm = new ImageCrcInfo();
            if (crcFrm.ShowDialog(this) == DialogResult.OK) {
                RTB_Log.AppendText( "CRC");
                RTB_Log.AppendText("addr: " + crcFrm.TB_Address.Text);
                RTB_Log.AppendText("size: " + crcFrm.TB_Size.Text);
                string crc = await MM.Program_GetCrc(targetType, crcFrm.TB_Address.Text, crcFrm.TB_Size.Text);
                if (crc != null)
                {
                    RTB_Log.AppendText("crc: " + crc);
                }
                else {
                    RTB_Log.AppendText("crc: could not be read");
                }
            }

            crcFrm.Dispose();
            //if (!result)
            //{
            //    RTB_Log.AppendText("CRC32 FAILED", Color.Red, true);
            //}
            //else
            //{
            //    RTB_Log.AppendText("CRC32 DONE", Color.Green, true);
            //}
        }
        private async void BTN_StartApp_Click(object sender, EventArgs e)
        {
            ////RTB_Log.AppendText("[" + DateTime.Now.ToShortTimeString() + "]");
            //if (!result)
            //{
            //    RTB_Log.AppendText("START APP FAILED", Color.Red, true);
            //}
            //else
            //{
            //    RTB_Log.AppendText("START APP DONE", Color.Green, true);
            //}
        }

        private async void BTN_ResetTarget_Click(object sender, EventArgs e)
        {
            MonarcManager.ResultType result;
            RTB_Log.AppendText("Sending Soft Reset");
            
            result = await MM.Program_ResetTargetAsync();
            if ( result != MonarcManager.ResultType.OK)
            {
                RTB_Log.AppendText("Reset Failed!");
            }
            else
            {
                RTB_Log.AppendText("Reset Done!");
            }
        }

        private void programmer_ConnectionUp(object sender, EventArgs e)
        {
            RTB_Log.AppendText("[" + DateTime.Now.ToShortTimeString() + "]");
            RTB_Log.AppendText("Connection Up", Color.Green, true);
        }
        private void programmer_ConnectionDown(object sender, EventArgs e)
        {
            RTB_Log.AppendText("[" + DateTime.Now.ToShortTimeString() + "]");
            RTB_Log.AppendText("Connection Down", Color.Red, true);
        }

        private void programmer_Notification(object sender, ProgrammerNotificationEventArgs e)
        {
            //Control target = ProgrammerProgressChange.Target as Control;
            //if (target != null && target.InvokeRequired)
            //    target.Invoke(ProgrammerProgressChange, new object[] { this, e });
            //else
            RTB_Log.BeginInvoke((MethodInvoker)delegate () { programmer_NotificationAction(sender, e); });
        }
        private void programmer_NotificationAction(object sender, ProgrammerNotificationEventArgs e)
        {
            RTB_Log.AppendText("[" + DateTime.Now.ToShortTimeString() + "]");
            Color c = Color.Black;
            if (e.Severity == ProgrammerNotificationEventArgs.MsgSeverity.Warning)
            {
                c = Color.Brown;
            }
            else if (e.Severity == ProgrammerNotificationEventArgs.MsgSeverity.Error)
            {
                c = Color.Red;
            }
            else if (e.Severity == ProgrammerNotificationEventArgs.MsgSeverity.Status)
            {
                TT_Label.Text = e.Msg;
            }
            RTB_Log.AppendText(e.Msg, c, true);
        }
        private void programmer_ProgressChange(object sender, ProgrammerProgressChangeEventArgs e)
        {
            RTB_Log.BeginInvoke((MethodInvoker)delegate () { TT_ProgressBar.Value = e.Percent; });
        }
        //private void programmer_ProgressChangeAction(object sender, ProgrammerProgressChangeEventArgs e)
        //{
        //    TT_ProgressBar.Value = e.Percent;
        //}

        
    }
    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color, bool addNewLine = false)
        {
            box.SuspendLayout();
            box.SelectionColor = color;
            box.AppendText(addNewLine ? $"{text}{Environment.NewLine}" : text);
            box.ScrollToCaret();
            box.ResumeLayout();
        }
    }
}
