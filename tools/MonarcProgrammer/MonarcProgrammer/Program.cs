using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.ComponentModel;
using System.Text;
using System.Collections.Concurrent;
using System.IO;
using System.Text.RegularExpressions;

namespace MonarcProgrammer
{
    public enum TargetType : ushort {
        FpbTargetType = 0, RocTargetType = 1, PacTargetType = 2, UnknownTargetType = 100
    }
    public enum ServerPort : int {
        FpbProgServerPortNumber = 7705,
        PacProgServerPortNumber = 7704,
        RocProgServerPortNumber = 7703,
        // reset and command ports
        FpbResetServerPortNumber = 7702,
        FpbCommandServerPortNumber = 7700,
    }
    public class Constants {
        public static string ServerIpStr = "192.168.111.86";
    }

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MonarcProgrammerForm());
        }
    }


    public class MonarcClient
    {
        //private Socket clientSocket;
        TcpClient client;
        private NetworkStream networkStream;
        private BackgroundWorker bwReceiver;
        private IPEndPoint serverEP;
        private string networkName;
        private BlockingCollection<string> bc;
        private int InputQueueSize = 100;
        private System.Threading.Semaphore sem;
        private string serverIp;
        private Int32 serverPort;

        public bool Connected
        {
            get
            {
                if (this.client != null) return this.client.Connected;
                else return false;
            }
        }
        public MonarcClient(IPAddress serverIp, int port, string netName)
        {
            //this.serverEP = new IPEndPoint(serverIp, port); try client
            serverPort = port;
            this.serverIp = serverIp.ToString();
            networkName = netName;
            System.Net.NetworkInformation.NetworkChange.NetworkAvailabilityChanged += new
                System.Net.NetworkInformation.NetworkAvailabilityChangedEventHandler(NetworkChange_NetworkAvailabilityChanged);
            bc = new BlockingCollection<string>(InputQueueSize);
            sem = new System.Threading.Semaphore(1, 1);
        }
        private void AddMessage(string msg)
        {
            bc.Add(msg);
        }
        public string ReceiveMessage(int milliseconds)
        {
            string msg;
            bool result;
            result = bc.TryTake(out msg, TimeSpan.FromMilliseconds(milliseconds));
            if (result)
            {
                return msg;
            }
            return null;
        }

        private void NetworkChange_NetworkAvailabilityChanged(object sender, System.Net.NetworkInformation.NetworkAvailabilityEventArgs e)
        {
            if (!e.IsAvailable)
            {
                this.OnNetworkDown(new EventArgs());
            }
            else
                this.OnNetworkUp(new EventArgs());
        }

        private void StartReceive(object sender, DoWorkEventArgs e)
        {
            byte[] buffer = new byte[2048];
            int index = 0;
            while (Connected)
            {
                try
                {
                    int readBytes = this.networkStream.Read(buffer, index, 1);
                    if (readBytes == 0)
                    {
                        break;
                    }

                    if (buffer[index] == 10)
                    { // new line received
                        string msg = Encoding.UTF8.GetString(buffer, 0, index + 1);
                        AddMessage(msg);
                        index = 0;
                    }
                    else
                    {
                        index++;
                    }
                }
                catch {
                    break;
                }
            }
            this.Disconnect();
        }

        public bool ConnectToServer()
        {
            try
            {
                Console.WriteLine($"Socket connect: ({serverIp},{serverPort})");
                //this.clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp); try client
                client = new TcpClient(serverIp, serverPort);
                //this.clientSocket.Connect(this.serverEP); try client
                //this.networkStream = new NetworkStream(this.clientSocket);
                this.networkStream = client.GetStream();
                this.bwReceiver = new BackgroundWorker();
                this.bwReceiver.WorkerSupportsCancellation = true;
                this.bwReceiver.DoWork += new DoWorkEventHandler(StartReceive);
                this.bwReceiver.RunWorkerAsync();
                Console.WriteLine("Socket connect: " + networkName + " done");
                return true;
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}, {1}", e, networkName);
                return false;
            }
            catch (Exception e) {
                Console.WriteLine("Other Exception: {0}, {1}", e, networkName);
                return false;
            }
        }
        
        public bool Disconnect()
        {
            if (this.client != null && this.client.Connected)
            {
                try
                {
                    Console.WriteLine("Socket disconnect: " + networkName);
                    this.bwReceiver.CancelAsync();
                    Thread.Sleep(100);
                    networkStream.Close();
                    client.Close();
                    
                    //this.clientSocket.Shutdown(SocketShutdown.Both);
                    //this.clientSocket.Close();
                    Console.WriteLine("Socket disconnect: " + networkName + " done");
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            else
                return true;
        }

        public bool SendMessage(string msg)
        {
            try
            {
                sem.WaitOne();
                byte[] buffer = Encoding.ASCII.GetBytes(msg);
                this.networkStream.Write(buffer, 0, buffer.Length);
                this.networkStream.Flush();
                sem.Release();
                return true;
            }
            catch
            {
                sem.Release();
                return false;
            }
        }
        public void ClearReceivedMessages()
        {
            string msg;
            while (bc.TryTake(out msg)) ;
        }
        
        
        // Events 
        public event NetworkDownEventHandler NetworkDown;
        protected virtual void OnNetworkDown(EventArgs e)
        {
            if (NetworkDown != null)
            {
                Control target = NetworkDown.Target as Control;
                if (target != null && target.InvokeRequired)
                    target.Invoke(NetworkDown, new object[] { this, e });
                else
                    NetworkDown(this, e);
            }
        }
        public event NetworkUpEventHandler NetworkUp;
        protected virtual void OnNetworkUp(EventArgs e)
        {
            if (NetworkUp != null)
            {
                Control target = NetworkUp.Target as Control;
                if (target != null && target.InvokeRequired)
                    target.Invoke(NetworkUp, new object[] { this, e });
                else
                    NetworkUp(this, e);
            }
        }
        
        public event MessageReceivedEventHandler MessageReceived;
        protected virtual void OnMessageReceived(MessageEventArgs e)
        {
            if (MessageReceived != null)
            {
                Control target = MessageReceived.Target as Control;
                if (target != null && target.InvokeRequired)
                    target.Invoke(MessageReceived, new object[] { this, e });
                else
                    MessageReceived(this, e);
            }
        }
    }
    public class BootloaderManager
    {
        //public string filename;
        public TargetType targetType;
        public MonarcClient FW_Client;
        //private AutoResetEvent evtBlConnected;
        bool bProgrammingInProgress;

        public BootloaderManager(TargetType targetType)
        {
            this.targetType = targetType;
            this.bProgrammingInProgress = false;

            // I need to add reset board so that I can bring board to boot the bootloader

            if (targetType == TargetType.FpbTargetType)
            {
                FW_Client = new MonarcClient(IPAddress.Parse(Constants.ServerIpStr), (int)ServerPort.FpbProgServerPortNumber, "Fpb FW Programmer");
            }
            else if (targetType == TargetType.RocTargetType)
            {
                FW_Client = new MonarcClient(IPAddress.Parse(Constants.ServerIpStr), (int)ServerPort.RocProgServerPortNumber, "Roc FW Programmer");
            }
            else if (targetType == TargetType.PacTargetType)
            {
                FW_Client = new MonarcClient(IPAddress.Parse(Constants.ServerIpStr), (int)ServerPort.PacProgServerPortNumber, "Roc FW Programmer");
            }
            //evtBlConnected = new AutoResetEvent(false);
            
            FW_Client.NetworkUp += new NetworkUpEventHandler(FWP_NetworkUp);
            FW_Client.NetworkDown += new NetworkDownEventHandler(FWP_NetworkDown);
        }
        bool ResetBoard()
        {
            return false;
        }
        
        private void FWP_NetworkUp(object sender, EventArgs e)
        {
            OnProgrammerNotification(new ProgrammerNotificationEventArgs("Network UP", ProgrammerNotificationEventArgs.MsgSeverity.Info));
        }
        private void FWP_NetworkDown(object sender, EventArgs e)
        {
            if (bProgrammingInProgress)
            {
                OnProgrammerNotification(new ProgrammerNotificationEventArgs("Network DOWN", ProgrammerNotificationEventArgs.MsgSeverity.Error));
            }
        }
        public async Task<bool> ConnectToServerAsync(int timeoutMs)
        {
            return await Task.Run(() => {
                bool res = true;
                if (!FW_Client.Connected)
                {
                    res = false;
                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                    int retryCnt = 5;
                    do
                    {
                        sw.Start();
                        res = FW_Client.ConnectToServer();
                        if (res)
                        {
                            break;
                        }
                        // retry
                        //Thread.Sleep(1000);
                        sw.Stop();
                        //} while (sw.ElapsedMilliseconds < timeoutMs);
                        //} while (sw.ElapsedMilliseconds < timeoutMs);
                    } while ((retryCnt--) > 0);
                }
                return res;
            });
        }
        public async Task<bool> DisconnectFromServerAsync()
        {
            return await Task.Run(() => {
                bool res = true;
                if (FW_Client.Connected)
                {
                    res = FW_Client.Disconnect();
                }
                return res;
            });
        }
        public async Task<bool> BootloaderStartAsync()
        {
            //OnProgrammerNotification(new ProgrammerNotificationEventArgs("STARTED", ProgrammerNotificationEventArgs.MsgSeverity.Status));
            FW_Client.ClearReceivedMessages();
            return await Task.Run(() => {
                bool res;
                res = false;
                int retryCnt = 10;
                string rxMsg;
                do
                {
                    if (!FW_Client.Connected) {
                        return false;
                    }
                    OnProgrammerNotification(new ProgrammerNotificationEventArgs("Connecting to bootloader", ProgrammerNotificationEventArgs.MsgSeverity.Info));
                    if (!FW_Client.SendMessage("connect\n")) { return false; }
                    rxMsg = FW_Client.ReceiveMessage(300);
                    if (rxMsg != null) {
                        OnProgrammerNotification(new ProgrammerNotificationEventArgs(rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Info));
                        if (rxMsg == "connected\n") {
                            res = true;
                            break;
                        }
                    }
                    retryCnt--;
                } while (retryCnt > 0);
                return res;
            });
            
        }
        public async Task<bool> EraseAppAsync(int timeoutMs)
        {
            OnProgrammerNotification(new ProgrammerNotificationEventArgs("Erasing App", ProgrammerNotificationEventArgs.MsgSeverity.Status));
            FW_Client.ClearReceivedMessages();
            return await Task.Run(() => {
                bool res;
                res = false;
                int retryCnt = 10;
                string rxMsg;

                if (!FW_Client.Connected)
                {
                    return false;
                }
                OnProgrammerNotification(new ProgrammerNotificationEventArgs("Erasing application area", ProgrammerNotificationEventArgs.MsgSeverity.Info));
                if (!FW_Client.SendMessage("erase-app\n")) { return false; }
                bool bEraseDone = false;
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                string sPattern = "^error";
                do {
                    rxMsg = FW_Client.ReceiveMessage(300);
                    if (rxMsg != null)
                    {
                        if (System.Text.RegularExpressions.Regex.IsMatch(rxMsg, sPattern))
                        { // error occured
                            OnProgrammerNotification(new ProgrammerNotificationEventArgs(rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Error));
                            break;
                        }
                        else {
                            OnProgrammerNotification(new ProgrammerNotificationEventArgs(rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Info));
                        }
                        if (System.Text.RegularExpressions.Regex.IsMatch(rxMsg, "^erase-app done"))
                        {
                            res = true;
                            break;
                        }
                    }
                } while(sw.ElapsedMilliseconds < timeoutMs);
                OnProgrammerNotification(new ProgrammerNotificationEventArgs("Erase exit", ProgrammerNotificationEventArgs.MsgSeverity.Status));
                return res;
            });

        }
        public async Task<string> CrcAreaAsync(string hexStartAddr, string hexSize)
        {
           
            OnProgrammerNotification(new ProgrammerNotificationEventArgs("Crc App", ProgrammerNotificationEventArgs.MsgSeverity.Status));
            FW_Client.ClearReceivedMessages();
            string crcStr = null;

            bool result = await Task.Run(() => {
                
                string rxMsg;
                bool res = false;

                if (FW_Client.Connected)
                {

                    OnProgrammerNotification(new ProgrammerNotificationEventArgs(
                        $"Calculate CRC32 area address 0x{hexStartAddr} size 0x{hexSize}",
                        ProgrammerNotificationEventArgs.MsgSeverity.Info));
                    if (!FW_Client.SendMessage($"crc32 --start={hexStartAddr} --size={hexSize}\n")) { return false; }

                    int retryCnt = 10;
                    do
                    {
                        rxMsg = FW_Client.ReceiveMessage(300);
                        if (rxMsg != null)
                        {
                            if (System.Text.RegularExpressions.Regex.IsMatch(rxMsg, "^error"))
                            { // error occured
                                OnProgrammerNotification(new ProgrammerNotificationEventArgs(rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Error));
                                break;
                            }
                            OnProgrammerNotification(new ProgrammerNotificationEventArgs(rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Info));


                            if (Regex.IsMatch(rxMsg, "^crc32.a=[a-fA-F0-9]+,s=[a-fA-F0-9]+"))
                            {
                                Match match = Regex.Match(rxMsg, "^crc32.a=[a-fA-F0-9]+,s=[a-fA-F0-9]+. = ([a-fA-F0-9]+)");
                                crcStr = match.Groups[1].Value;
                                res = true;
                                break;
                            }
                        }
                        retryCnt--;
                    } while (retryCnt > 0);
                    OnProgrammerNotification(new ProgrammerNotificationEventArgs("Crc exit", ProgrammerNotificationEventArgs.MsgSeverity.Status));
                }
                return res;
            });
            return crcStr;
        }

        public async Task<bool> StartAppAsync()
        {
            OnProgrammerNotification(new ProgrammerNotificationEventArgs("Starting App", ProgrammerNotificationEventArgs.MsgSeverity.Status));
            FW_Client.ClearReceivedMessages();
            return await Task.Run(() => {
                bool res = false;

                string rxMsg;

                if (!FW_Client.Connected)
                {
                    return false;
                }
                OnProgrammerNotification(new ProgrammerNotificationEventArgs(
                    $"Starting Application",
                    ProgrammerNotificationEventArgs.MsgSeverity.Info));
                if (!FW_Client.SendMessage("start-app\n")) { return false; }

                rxMsg = FW_Client.ReceiveMessage(300);
                if (rxMsg != null)
                {
                    if (System.Text.RegularExpressions.Regex.IsMatch(rxMsg, "^error"))
                    { // error occured
                        OnProgrammerNotification(new ProgrammerNotificationEventArgs(rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Error));
                        return false;
                    }
                    else
                    {
                        OnProgrammerNotification(new ProgrammerNotificationEventArgs(rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Info));
                    }
                }

                return true;
            });

        }
        public async Task<bool> DownloadSrecordFileAsync(string filename)
        {
            OnProgrammerNotification(new ProgrammerNotificationEventArgs("Downloading", ProgrammerNotificationEventArgs.MsgSeverity.Status));
            FW_Client.ClearReceivedMessages();
            return await Task.Run(() => {
                bool res;
                res = false;
                int retryCnt = 10;
                string rxMsg;

                if (!FW_Client.Connected)
                {
                    return false;
                }
                OnProgrammerNotification(new ProgrammerNotificationEventArgs("Downloading srec file: " + filename, ProgrammerNotificationEventArgs.MsgSeverity.Info));
                OnProgrammerNotification(new ProgrammerNotificationEventArgs("Downloading" + filename, ProgrammerNotificationEventArgs.MsgSeverity.Status));
                if (!FW_Client.SendMessage("program-app-srec\n")) { return false; }
                rxMsg = FW_Client.ReceiveMessage(300);
                if (rxMsg == null) {
                    OnProgrammerNotification(new ProgrammerNotificationEventArgs("cmd: program-app-srec, no response: " + filename, ProgrammerNotificationEventArgs.MsgSeverity.Error));
                    return false;
                }
                if (!System.Text.RegularExpressions.Regex.IsMatch(rxMsg, "^transferring srecords:")) {
                    OnProgrammerNotification(new ProgrammerNotificationEventArgs("expected: transferring srecords, got: " + rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Error));
                    return false;
                }
            
                System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                var lineCountTotal = File.ReadLines(filename).Count();
                System.IO.StreamReader file = new System.IO.StreamReader(filename);
                FW_Client.ClearReceivedMessages();
                string line;
                int lineCount = 0;
                int percent = 0;
                double dp;
                long lastProgressUpdate = sw.ElapsedMilliseconds;
                OnProgrammerProgressChange(new ProgrammerProgressChangeEventArgs(percent));
                while ((line = file.ReadLine()) != null)
                {
                    //Thread.Sleep(1);
                    if (!FW_Client.SendMessage(line + "\n")) { return false; }
                    retryCnt = 10;
                    //OnProgrammerNotification(new ProgrammerNotificationEventArgs(line, ProgrammerNotificationEventArgs.MsgSeverity.Info));
                    do
                    {
                        rxMsg = FW_Client.ReceiveMessage(2500);
                        if (rxMsg == null)
                        {
                            OnProgrammerNotification(new ProgrammerNotificationEventArgs("No response to program record.\n", ProgrammerNotificationEventArgs.MsgSeverity.Warning));
                        }
                        else
                        {
                            if (System.Text.RegularExpressions.Regex.IsMatch(rxMsg, "^error"))
                            { // error occured
                                OnProgrammerNotification(new ProgrammerNotificationEventArgs(rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Error));
                                return false;
                            }
                            // drop \n
                            else if ( !line.StartsWith(rxMsg.TrimEnd('\n') ) ){
                                OnProgrammerNotification(new ProgrammerNotificationEventArgs(rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Warning));
                            }

                            //else if (!System.Text.RegularExpressions.Regex.IsMatch(rxMsg, "^.$"))
                            //{
                            //    OnProgrammerNotification(new ProgrammerNotificationEventArgs(rxMsg, ProgrammerNotificationEventArgs.MsgSeverity.Warning));
                            //}
                            else
                            {
                                // advance
                                break;
                            }
                        }
                    } while ((retryCnt--) > 0);
                    if (retryCnt == 0) {
                        OnProgrammerNotification(new ProgrammerNotificationEventArgs("No response to srecord", ProgrammerNotificationEventArgs.MsgSeverity.Error));
                        return false;
                    }
                    lineCount++;
                    dp = ((double)lineCount / (double)lineCountTotal) * 100;
                    if ((sw.ElapsedMilliseconds % 1000) == 0) { // update every second
                        percent = (int)dp;
                        OnProgrammerProgressChange(new ProgrammerProgressChangeEventArgs(percent));
                    }
                }// while (sw.ElapsedMilliseconds < timeoutMs);
                OnProgrammerProgressChange(new ProgrammerProgressChangeEventArgs(percent));
                OnProgrammerNotification(new ProgrammerNotificationEventArgs("Dowload exit", ProgrammerNotificationEventArgs.MsgSeverity.Status));
                return true;
            });

        }
        public async Task<bool> ProgramImageAsync(string filename)
        {
            bool result;

            
            result = await EraseAppAsync(180*1000); // wait 3 minutes
            if (!result)
            {
                return result;
            }
            result = await DownloadSrecordFileAsync(filename);
            if (!result)
            {
                return result;
            }

            return result;
        }
        
        // Events
        public event ProgrammerNotificationEventHandler ProgrammerNotification;
        private void OnProgrammerNotification(ProgrammerNotificationEventArgs e)
        {
            if (ProgrammerNotification != null) {
                ProgrammerNotification(this, e);
            }
        }
        public event ProgrammerProgressChangeEventHandler ProgrammerProgressChange;
        private void OnProgrammerProgressChange(ProgrammerProgressChangeEventArgs e)
        {
            if (ProgrammerProgressChange != null)
            {
                ProgrammerProgressChange(this, e);
            }
        }
    }

    public class ApplicationManager
    {
        //public string filename;
        
        public MonarcClient CMD_Client;
        public MonarcClient RESET_Client;
        //private BackgroundWorker bwFwDownload;
        private AutoResetEvent evtBlConnected;


        public ApplicationManager()
        {

            // I need to add reset board so that I can bring board to boot the bootloader

            CMD_Client = new MonarcClient(IPAddress.Parse(Constants.ServerIpStr), (int)ServerPort.FpbCommandServerPortNumber, "Fpb Command Server");
            RESET_Client = new MonarcClient(IPAddress.Parse(Constants.ServerIpStr), (int)ServerPort.FpbResetServerPortNumber, "Fpb Reset Server");
            
            //FW_Client.NetworkUp += new NetworkUpEventHandler(FWP_NetworkUp);
            //FW_Client.NetworkDown += new NetworkDownEventHandler(FWP_NetworkDown);
        }
        

        //private void FWP_NetworkUp(object sender, EventArgs e)
        //{
        //    OnProgrammerNotification(new ProgrammerNotificationEventArgs("Network UP", ProgrammerNotificationEventArgs.MsgSeverity.Info));
        //}
        //private void FWP_NetworkDown(object sender, EventArgs e)
        //{
        //    if (bProgrammingInProgress)
        //    {
        //        OnProgrammerNotification(new ProgrammerNotificationEventArgs("Network DOWN", ProgrammerNotificationEventArgs.MsgSeverity.Error));
        //    }
        //}
        public async Task<bool> ConnectToServerAsync(int timeoutMs)
        {
            return await Task.Run(() => {
                bool res = true;
                if (!CMD_Client.Connected)
                {
                    res = false;
                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                    do
                    {
                        sw.Start();
                        Thread.Sleep(300);
                        res = CMD_Client.ConnectToServer();

                        if (res)
                        {
                            break;
                        }
                        // retry

                        sw.Stop();
                    } while (sw.ElapsedMilliseconds < timeoutMs);
                }
                if (res == false) {
                    return res;
                }
                if (!RESET_Client.Connected)
                {
                    res = false;
                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                    do
                    {
                        sw.Start();
                        res = RESET_Client.ConnectToServer();
                        if (res)
                        {
                            break;
                        }
                        // retry
                        Thread.Sleep(300);
                        sw.Stop();
                    } while (sw.ElapsedMilliseconds < timeoutMs);
                }
                if (res == false)
                {
                    return res;
                }
                return true;
            });
        }
        public async Task<bool> DisconnectFromServerAsync()
        {
            return await Task.Run(() => {
                bool cmdres= true, resetres = true;
                if (CMD_Client.Connected)
                {
                     cmdres = CMD_Client.Disconnect();
                }
                if (RESET_Client.Connected)
                {
                    resetres = CMD_Client.Disconnect();
                }
                if (!cmdres || !resetres)
                {
                    return false;
                }
                return true;
            });
        }
        
        public async Task<bool> ResetTargetAsync(TargetType targetType)
        {
            bool connectres;
            OnProgrammerNotification(new ProgrammerNotificationEventArgs("Reseting", ProgrammerNotificationEventArgs.MsgSeverity.Status));

            if ((targetType == TargetType.FpbTargetType) && (!RESET_Client.Connected)) { 
                connectres = await ConnectToServerAsync(300);
                if (!connectres) {
                    OnProgrammerNotification(new ProgrammerNotificationEventArgs("Connect Error", ProgrammerNotificationEventArgs.MsgSeverity.Error));
                    return false;
                }
            }
            if (targetType == TargetType.FpbTargetType)
            {
                RESET_Client.ClearReceivedMessages();
                bool result = await Task.Run(() =>
                {
                    //OnProgrammerNotification(new ProgrammerNotificationEventArgs("Resetting Fpb", ProgrammerNotificationEventArgs.MsgSeverity.Info));
                    if (!RESET_Client.SendMessage("{SET,SYS,RESET,1}\n")) {
                        return false;
                    }
                    //OnProgrammerNotification(new ProgrammerNotificationEventArgs("Erase exit", ProgrammerNotificationEventArgs.MsgSeverity.Status));
                    return true;
                });
                bool disResult = await DisconnectFromServerAsync();
                return result;
            }
            if (targetType == TargetType.RocTargetType)
            {
                CMD_Client.ClearReceivedMessages();
                bool result = await Task.Run(() =>
                {
                    if (!CMD_Client.SendMessage("{SET,ROC,RESET,?}\n"))
                    {
                        return false;
                    }
                    return true;
                });
                bool disResult = await DisconnectFromServerAsync();
                return result;
            }
            // Need to add PAC RESET
            return false;
        }
        public async Task<bool> EnableBridgeToTargetAsync(TargetType targetType, bool enable)
        {
            bool connectres;
            
            if (targetType == TargetType.FpbTargetType) {
                return true; // there is no bridge to this target since it is connected directly
            }
            string bridgeInfo;
            if (enable) { bridgeInfo = "Bridge On"; }
            else { bridgeInfo = "Bridge Off"; }
            OnProgrammerNotification(new ProgrammerNotificationEventArgs(bridgeInfo, ProgrammerNotificationEventArgs.MsgSeverity.Status));
 
            if (!RESET_Client.Connected)
            {
                 OnProgrammerNotification(new ProgrammerNotificationEventArgs("Not Connected", ProgrammerNotificationEventArgs.MsgSeverity.Error));
                 return false;
            }
            
            string enStr = (enable) ? "1" : "0";
            string cmdStr;
            if (targetType == TargetType.RocTargetType)
            {
                cmdStr = "{SET,ROCFW,EN," + enStr + "}\n";
            }
            else {
                cmdStr = "{SET,PACFW,EN," + enStr + "}\n";
            }
            
            RESET_Client.ClearReceivedMessages();
            bool result = await Task.Run(() =>
            {
                if (!RESET_Client.SendMessage(cmdStr))
                {
                    return false;
                }
                return true;
            });
            
            return result;
        }

        

        // Events
        public event ProgrammerNotificationEventHandler ProgrammerNotification;
        private void OnProgrammerNotification(ProgrammerNotificationEventArgs e)
        {
            if (ProgrammerNotification != null)
            {
                ProgrammerNotification(this, e);
            }
        }
        public event ProgrammerProgressChangeEventHandler ProgrammerProgressChange;
        private void OnProgrammerProgressChange(ProgrammerProgressChangeEventArgs e)
        {
            if (ProgrammerProgressChange != null)
            {
                ProgrammerProgressChange(this, e);
            }
        }
    }

    public class MonarcManager
    {
        public ApplicationManager appMgr;
        public BootloaderManager blMgr;

        public enum ResultType {
            OK,
            WrongTargetFile,
            CanNotConnectToFpbApp,
            CanNotConnectToFpbBootloader,
            BootloaderStartFailed,
            BootloaderFwDownloadFailed,
            EnableBridgeFailed,
            ResetFpbAppFailed,
            ResetTargetFailed,
        }

        public enum TargetRunState {
            BootloaderRunning, ApplicationRunning, Unknown
        }

        //async Task<bool> GetFpbRunStateAsync(TargetRunState runState)
        //{
        //}
        public MonarcManager()
        {
            appMgr = new ApplicationManager();
            appMgr.ProgrammerNotification += new ProgrammerNotificationEventHandler(OnProgrammerNotification);
            appMgr.ProgrammerProgressChange += new ProgrammerProgressChangeEventHandler(OnProgrammerProgressChange);
            //blMgr = new BootloaderManager();
        }
        TargetType FindFileTargetType(string filename)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(filename, "monarc-fpb_fw"))
            {
                return TargetType.FpbTargetType;
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(filename, "monarc-roc_fw"))
            {
                return TargetType.RocTargetType;
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(filename, "monarc-pac_fw"))
            {
                return TargetType.PacTargetType;
            }
            else
            {
                return TargetType.UnknownTargetType;
            }
        }

        public async Task<ResultType> Program_FpbSrecordFileAsync(string filename, int timeoutMs)
        {
            bool bSuccess;
            ResultType result = ResultType.OK;
            TargetType tt;
            BootloaderManager tgtBl;
            bool bAppConnected = false;


            // find the run state of the FPB
            // create a bootloader object to handle the programming
            do
            {
                bSuccess = await appMgr.ConnectToServerAsync(300);
                if (bSuccess)
                {
                    Console.WriteLine("Connected to the app");
                    bSuccess = await appMgr.ResetTargetAsync(TargetType.FpbTargetType);
                    await appMgr.DisconnectFromServerAsync();  // since just reset the target
                    if (!bSuccess)
                    {
                        result = ResultType.ResetFpbAppFailed;
                        break;
                    }
                    Thread.Sleep(3000); // delay to allow the target socket to come up
                }
                else
                {
                    Console.WriteLine("Could NOT Connect to the app");
                    //result = ResultType.CanNotConnectToFpbApp;
                }

                // try connecting to FPB bootloader
                tgtBl = new BootloaderManager(TargetType.FpbTargetType);
                tgtBl.ProgrammerNotification += new ProgrammerNotificationEventHandler(OnProgrammerNotification);
                tgtBl.ProgrammerProgressChange += new ProgrammerProgressChangeEventHandler(OnProgrammerProgressChange);
                //int retryCnt = 3;
                //do
                //{
                bSuccess = await tgtBl.ConnectToServerAsync(300);
                //} while (!bSuccess && (retryCnt--) > 0);
                if (!bSuccess)
                {
                    Console.WriteLine("Could NOT Connect to the Bootloader");
                    result = ResultType.CanNotConnectToFpbBootloader;
                    break;
                }
                result = ResultType.OK;
                Console.WriteLine("Connected to the Bootloader");
                result = ResultType.OK;
                bSuccess = await tgtBl.BootloaderStartAsync();
                if (!bSuccess)
                {
                    Console.WriteLine("Could NOT Start the Bootloader Process");
                    await tgtBl.DisconnectFromServerAsync();
                    result = ResultType.BootloaderStartFailed;
                    break;
                }
                Console.WriteLine("Started the Bootloader Process");
                bSuccess = await tgtBl.ProgramImageAsync(filename);
                await tgtBl.DisconnectFromServerAsync();
                if (!bSuccess)
                {
                    result = ResultType.BootloaderFwDownloadFailed;
                }
                else {
                    result = ResultType.OK;
                }
                
            } while (false);

            bSuccess = await appMgr.DisconnectFromServerAsync();

            return result;
        }

        public async Task<ResultType> Program_NextHop_SrecordFileAsync(TargetType target, string filename, int timeoutMs)
        {
            bool bSuccess;
            ResultType result = ResultType.OK;
            TargetType tt;
            BootloaderManager tgtBl = null;
            bool bBootloaderConnected = false;


            // find the run state of the FPB
            // create a bootloader object to handle the programming
            do
            {
                bSuccess = await appMgr.ConnectToServerAsync(300);
                if (bSuccess)
                {
                    Console.WriteLine("Connected to the app");
                    bSuccess = await appMgr.ResetTargetAsync(TargetType.FpbTargetType);
                    await appMgr.DisconnectFromServerAsync();  // since just reset the target
                    if (!bSuccess)
                    {
                        result = ResultType.ResetFpbAppFailed;
                        break;
                    }
                    Thread.Sleep(3000); // delay to allow the target socket to come up
                }
                else
                {
                    Console.WriteLine("Could NOT Connect to the app");
                    //result = ResultType.CanNotConnectToFpbApp;
                }

                // bring up the bridge that manages protocol forwarding to ROC
                //bSuccess = await appMgr.EnableBridgeToTargetAsync(target, true);
                //if (!bSuccess) {
                //    result = ResultType.EnableBridgeFailed;
                //    break;
                //}
                //bBridgeEnabled = true;
                // reset the target so we can connect to the bootloader
                

                // try connecting to FPB bootloader
                tgtBl = new BootloaderManager(target);
                tgtBl.ProgrammerNotification += new ProgrammerNotificationEventHandler(OnProgrammerNotification);
                tgtBl.ProgrammerProgressChange += new ProgrammerProgressChangeEventHandler(OnProgrammerProgressChange);
                bSuccess = await tgtBl.ConnectToServerAsync(300);
                if (!bSuccess)
                {
                    Console.WriteLine("Could NOT Connect to the Bootloader");
                    result = ResultType.CanNotConnectToFpbBootloader;
                    break;
                }
                result = ResultType.OK;
                bBootloaderConnected = true;
                Console.WriteLine("Connected to the Bootloader");
                bSuccess = await tgtBl.BootloaderStartAsync();
                if (!bSuccess)
                {
                    Console.WriteLine("Could NOT Start the Bootloader Process");
                    result = ResultType.BootloaderStartFailed;
                    break;
                }
                Console.WriteLine("Started the Bootloader Process");
                bSuccess = await tgtBl.ProgramImageAsync(filename);
                if (!bSuccess)
                {
                    result = ResultType.BootloaderFwDownloadFailed;
                }
                else
                {
                    result = ResultType.OK;
                }

            } while (false);

            // tear down resources
            if (bBootloaderConnected) {
                await tgtBl.DisconnectFromServerAsync();
            }
            //if (bBridgeEnabled) {
            //    await appMgr.EnableBridgeToTargetAsync(target, false);
            //}
            

            return result;
        }

        public async Task<string> Program_GetCrc(TargetType target, string hexStartAddr, string hexSize)
        {
            bool bSuccess;
            ResultType result = ResultType.OK;
            TargetType tt;
            BootloaderManager tgtBl = null;
            bool bBootloaderConnected = false;
            string crcStr = null;


            // find the run state of the FPB
            // create a bootloader object to handle the programming
            do
            {
                bSuccess = await appMgr.ConnectToServerAsync(300);
                if (bSuccess)
                {
                    Console.WriteLine("Connected to the app");
                    bSuccess = await appMgr.ResetTargetAsync(TargetType.FpbTargetType);
                    await appMgr.DisconnectFromServerAsync();  // since just reset the target
                    if (!bSuccess)
                    {
                        result = ResultType.ResetFpbAppFailed;
                        break;
                    }
                    Thread.Sleep(3000); // delay to allow the target socket to come up
                }
                else
                {
                    Console.WriteLine("Could NOT Connect to the app");
                    //result = ResultType.CanNotConnectToFpbApp;
                }

                // try connecting to FPB bootloader
                tgtBl = new BootloaderManager(target);
                tgtBl.ProgrammerNotification += new ProgrammerNotificationEventHandler(OnProgrammerNotification);
                tgtBl.ProgrammerProgressChange += new ProgrammerProgressChangeEventHandler(OnProgrammerProgressChange);
                bSuccess = await tgtBl.ConnectToServerAsync(300);
                if (!bSuccess)
                {
                    Console.WriteLine("Could NOT Connect to the Bootloader");
                    result = ResultType.CanNotConnectToFpbBootloader;
                    break;
                }
                result = ResultType.OK;
                bBootloaderConnected = true;
                Console.WriteLine("Connected to the Bootloader");
                bSuccess = await tgtBl.BootloaderStartAsync();
                if (!bSuccess)
                {
                    Console.WriteLine("Could NOT Start the Bootloader Process");
                    result = ResultType.BootloaderStartFailed;
                    break;
                }
                Console.WriteLine("Started the Bootloader Process");
                crcStr = await tgtBl.CrcAreaAsync(hexStartAddr, hexSize);
                //if (!bSuccess)
                //{
                //    result = ResultType.BootloaderFwDownloadFailed;
                //}
                //else
                //{
                //    result = ResultType.OK;
                //}

            } while (false);

            // tear down resources
            if (bBootloaderConnected)
            {
                await tgtBl.DisconnectFromServerAsync();
            }
            //if (bBridgeEnabled) {
            //    await appMgr.EnableBridgeToTargetAsync(target, false);
            //}


            return crcStr;
        }

        public async Task<ResultType> Program_ResetTargetAsync()
        {
            bool bSuccess;
            ResultType result = ResultType.OK;
            
            // find the run state of the FPB
            // create a bootloader object to handle the programming
            do
            {
                bSuccess = await appMgr.ConnectToServerAsync(300);
                if (bSuccess)
                {
                    Console.WriteLine("Connected to the app");
                    bSuccess = await appMgr.ResetTargetAsync(TargetType.RocTargetType);
                    await appMgr.DisconnectFromServerAsync();  // since just reset the target
                    if (!bSuccess)
                    {
                        result = ResultType.ResetTargetFailed;
                        break;
                    }
                    Thread.Sleep(3000); // delay to allow the target socket to come up
                }
                else
                {
                    Console.WriteLine("Could NOT Connect to the app");
                    //result = ResultType.CanNotConnectToFpbApp;
                }

            } while (false);

            return result;
        }

        // Events
        public event ProgrammerNotificationEventHandler ProgrammerNotification;
        private void OnProgrammerNotification(object sender, ProgrammerNotificationEventArgs e)
        {
            if (ProgrammerNotification != null)
            {
                //Control target = ProgrammerNotification.Target as Control;
                //if (target != null && target.InvokeRequired)
                //    target.Invoke(ProgrammerNotification, new object[] { this, e });
                //else
                    ProgrammerNotification(this, e);
            }
        }
        public event ProgrammerProgressChangeEventHandler ProgrammerProgressChange;
        private void OnProgrammerProgressChange(object sender, ProgrammerProgressChangeEventArgs e)
        {
            if (ProgrammerProgressChange != null)
            {
                //Control target = ProgrammerProgressChange.Target as Control;
                //if (target != null && target.InvokeRequired)
                //    target.Invoke(ProgrammerProgressChange, new object[] { this, e });
                //else
                    ProgrammerProgressChange(this, e);
            }
        }
    }

    

    // delegates
    public delegate void MessageReceivedEventHandler(object sender, MessageEventArgs e);
    public delegate void MessageSendFailureEventHandler(object sender, MessageEventArgs e);
    public delegate void MessageSentEventHandler(object sender, MessageEventArgs e);
    public class MessageEventArgs : EventArgs
    {
        private string msg;
        public string Msg {
            get { return msg; }
        }
        public MessageEventArgs(string newMsg)
        {
            msg = newMsg;
        }
    }
    public delegate void NetworkUpEventHandler(object sender, EventArgs e);
    public delegate void NetworkDownEventHandler(object sender, EventArgs e);
    
    public delegate void ProgrammerNotificationEventHandler(object sender, ProgrammerNotificationEventArgs e);
    public class ProgrammerNotificationEventArgs : EventArgs
    {
        public enum MsgSeverity { Info, Warning, Error, Status};
        private string msg;
        private MsgSeverity severity;
        public string Msg
        {
            get { return msg; }
        }
        public MsgSeverity Severity
        {
            get { return severity; }
        }
        public ProgrammerNotificationEventArgs(string newMsg, MsgSeverity severity = MsgSeverity.Info)
        {
            msg = newMsg;
            this.severity = severity;
        }
    }
    public delegate void ProgrammerProgressChangeEventHandler(object sender, ProgrammerProgressChangeEventArgs e);
    public class ProgrammerProgressChangeEventArgs : EventArgs
    {
        private int percent;
        public int Percent
        {
            get { return percent; }
        }
        public ProgrammerProgressChangeEventArgs(int percent)
        {
            this.percent = percent;
        }
    }


}
