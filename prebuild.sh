#!/bin/sh

VERSION_FILE=$(pwd)/../source/version.h
DATETIME=`date +"%Y%m%d%H%M%S"`

sed -i "s/FIRMWARE_BUILD_DATE_TIME.*$/FIRMWARE_BUILD_DATE_TIME \"${DATETIME}\"/g " ${VERSION_FILE}

